<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Settings;
use Tygh\Registry;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
function fn_ab__mcd_get_descs ($params = array(), $lang_code = CART_LANGUAGE){
$default_params = array(
'category_id' => 0,
'desc_id' => 0,
'status' => '',
'items_per_page' => 0,
);
$params = array_merge($default_params, $params);
$fields = array (
'd.desc_id',
'd.category_id',
'd.status',
'd.position',
'dd.lang_code',
'dd.title',
'dd.description',
);
$sortings = array(
'name' => array(
'd.status',
'd.position',
'dd.title',
)
);
$condition = $limit = $join = '';
if (!empty($params['desc_id'])){
$condition .= db_quote(' AND d.desc_id in (?n) ', $params['desc_id']);
}
if (!empty($params['category_id'])){
$condition .= db_quote(' AND d.category_id in (?n) ', $params['category_id']);
}
if (isset($params['status']) and in_array($params['status'], array('A', 'D'))){
$condition .= db_quote(' AND d.status in (?a) ', $params['status']);
}
if (!empty($params['limit'])) {
$limit = db_quote(' LIMIT 0, ?i', $params['limit']);
}
$sorting = db_sort($params, $sortings, 'name', 'asc');
$join .= db_quote(" INNER JOIN ?:ab__mcd_desc_descriptions AS dd ON (dd.desc_id = d.desc_id AND dd.lang_code = ?s ) ", $lang_code);
$data = db_get_hash_array('SELECT ' . implode(',', $fields) . " FROM ?:ab__mcd_descs as d $join WHERE 1 ?p $sorting ?p", 'desc_id', $condition, $limit);
if (empty($data)) return false;
return array($data, $params);
}
function fn_ab__mcd_upd_desc ($data, $id = 0, $lang_code = CART_LANGUAGE){
if (!empty($data)){
if (!empty($data['category_id']) and !empty($data['title'])){
$d = array(
'category_id' => $data['category_id'],
'title' => trim($data['title']),
'description' => trim($data['description']),
'status' => $data['status'],
'position' => $data['position'],
'lang_code' => $lang_code,
);
if (!$id){
$d['desc_id'] = db_query("INSERT INTO ?:ab__mcd_descs ?e", $d);
foreach (fn_get_translation_languages() as $d['lang_code'] => $_d) {
db_query("INSERT INTO ?:ab__mcd_desc_descriptions ?e", $d);
}
}else{
db_query("UPDATE ?:ab__mcd_descs SET ?u WHERE desc_id = ?i", $d, $id);
db_query("UPDATE ?:ab__mcd_desc_descriptions SET ?u WHERE desc_id = ?i AND lang_code = ?s", $d, $id, $lang_code);
}
}
}
}
function fn_ab__mcd_del_desc ($id = 0){
if (!empty($id)){
db_query("DELETE FROM ?:ab__mcd_descs WHERE desc_id in (?n)", (array) $id);
db_query("DELETE FROM ?:ab__mcd_desc_descriptions WHERE desc_id in (?n)", (array) $id);
}
}
function fn_ab__multiple_cat_descriptions_get_category_data_post ($category_id, $field_list, $get_main_pair, $skip_company_condition, $lang_code, &$category_data){
if (AREA == "C"){
list($descs) = fn_ab__mcd_get_descs(array('category_id'=>$category_id, 'status' => 'A'), $lang_code);
if (!empty($descs) and is_array($descs)){
$main_description = array();
if (strlen(trim($category_data['description']))){
$main_description = array(
array(
'title' => __('description'),
'description' => trim($category_data['description']),
),
);
}
$category_data['ab__mcd_descs'] = array_merge($main_description, $descs);
}
}
}
