#!/bin/bash

echo $1
echo $2
echo $3

wget -O $1 -q $2

if [ $? -eq 0 ]
then
  convert $1 -quiet -resize "1000x1000>" $3
  rm $1
  if [ -f $3 ]
  then
      echo "done"
      exit 0
  else
      exit 1
  fi
else
  exit 1
fi
