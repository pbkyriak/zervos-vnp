<?php

use Tygh\Registry;
use Tygh\UpgradeCenter\Console\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Application as ConsoleApplication;
use Slx\ProductMerger\Command\LoadSupplierCommand;

if (defined('CONSOLE') && CONSOLE) {
    if ($mode == 'console') {
        printf("\n\nProduct Updater\n");
        printf("---------------\n\n");
        $application = new ConsoleApplication();

        $cmdReg = new \Slx\Util\RegisterCommands();
        $cmdReg->setApplication($application)
               ->setDir(Registry::get('config.dir.addons').'slx_pu/Slx/PU/Command/')
               ->setNamespace('Slx\\PU\\Command')
               ->register();

        $input = new ArgvInput();
        $output = new ConsoleOutput();

        Registry::set('runtime.console.output', $output);

        $application->run($input, $output);
    }
}