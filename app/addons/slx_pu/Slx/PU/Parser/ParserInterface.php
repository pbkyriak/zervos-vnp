<?php
namespace Slx\PU\Parser;

/**
 * Product data file parser interface.
 * Product data parsers must implement this interface
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 */
interface ParserInterface {

    public function __construct($inFile);
    public function open();
    public function next();
    public function close();
    
}
