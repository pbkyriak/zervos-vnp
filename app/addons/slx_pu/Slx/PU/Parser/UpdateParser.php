<?php

namespace Slx\PU\Parser;

use Slx\PU\Parser\ParserInterface;

/**
 * Description of UpdateParser
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class UpdateParser implements ParserInterface {

    private $inFile = '';
    private $handle = null;
    
    public function __construct($inFile) {
        $this->inFile = $inFile;
        $this->handle = null;
    }

    public function open() {
        $out = false;
        if(file_exists($this->inFile)) {
            $this->handle = fopen($this->inFile, 'r');
            if($this->handle) {
                $out = true;
            }
        }
        return $out;
    }

    public function close() {
        if( $this->handle ) {
            fclose($this->handle);
        }
    }

    public function next() {
        
    }

}
