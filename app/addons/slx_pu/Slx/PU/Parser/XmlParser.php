<?php

namespace Slx\PU\Parser;

use Slx\PU\Core\Product;
use Slx\Logger\Logger;

/**
 * Parses vnp standard xml product file.
 * DO NOT ADD any bussines logic here!! 
 * A parser is just a parser! 
 * Any product or supplier specific business logic goes to 
 * ProductValidator or ProductPreparator classes. Not here!
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class XmlParser implements ParserInterface {

    private $inFile;
    private $inXml = null;
    private $prodCnt = 0;
    const FEATURES_TAG = 'features';
    public function __construct($inFile) {
        $this->inFile = $inFile;
        $this->inXml = null;
        printf("%s\n\n", __CLASS__);
    }

    public function open() {
        $out = false;
        $this->inXml = new \XMLReader();
        if ($this->inXml->open($this->inFile, 'utf-8', LIBXML_NOERROR | LIBXML_NOWARNING | 1)) {
            $out = true;
            $this->prodCnt = 0;
        }
        return $out;
    }

    public function close() {
        $this->inXml->close();
    }

    public function nextSupplier() {
        try {
            while ($this->readXml()) { //start reading.
                if ($this->inXml->nodeType == \XMLReader::ELEMENT) { //only opening tags.
                    $tag = $this->inXml->name; //make $tag contain the name of the tag
                    if ($tag == 'SupplierProducts') {
                        return $this->inXml->getAttribute('name');
                    }
                }
            }
        } catch (\Exception $ex) {
            Logger::getInstance()->log("error in parsing xml file :" . $ex->getMessage());
        }
        return null;        
    }
    
    public function next() {
        try {
            while ($this->readXml()) { //start reading.
                if ($this->inXml->nodeType == \XMLReader::ELEMENT) { //only opening tags.
                    $tag = $this->inXml->name; //make $tag contain the name of the tag
                    if ($tag == 'Product') {
                        $this->prodCnt++;
                        return $this->parseProduct();
                    }
                }
                if( $this->inXml->nodeType==\XMLReader::END_ELEMENT && $this->inXml->name=='SupplierProducts' ) {
                    break;
                }
            }
        } catch (\Exception $ex) {
            Logger::getInstance()->log("error in parsing xml file :" . $ex->getMessage());
        }
        return null;
    }

    private function parseProduct() {
        $product = new Product();
        try {
            while ($this->readXml()) {
                $type = $this->inXml->nodeType;
                $tag = $this->inXml->name;
                if ($type == \XMLReader::END_ELEMENT && $tag == 'Product') {
                    return $product;
                }
                if ($type == \XMLReader::ELEMENT) {
                    $field = $this->tagToProductMapping($tag);
                    if ($field) {
                        if ($field == 'image') {
                            $product->add($field, $this->readStringXml());
                        }
                        elseif( $field==self::FEATURES_TAG ) {
                            $fff = (string)$this->inXml->readString();
                            $ff = @unserialize($fff);
                            if(!is_array($ff)) {
                                $ff = [];
                            }
                            $product->set($field, $ff);
                        } else {
							if( in_array($field , array('short_description', 'full_description')) ) {
								$n1 = base64_decode($this->inXml->readString());
								$product->set($field, $n1);
							}
							else {
								$product->set($field, $this->readStringXml());
							}
						}
                    }
                }
            }
        } catch (\Exception $ex) {
            Logger::getInstance()->log("error in parsing xml file :" . $ex->getMessage());
        }
        return null;
    }

    private function readXml() {
        $previousEntityState = libxml_disable_entity_loader(true);
        $previousSetting = libxml_use_internal_errors(true);

        $out = @$this->inXml->read();

        $errors = libxml_get_errors();
        libxml_clear_errors();
        libxml_use_internal_errors($previousSetting);
        libxml_disable_entity_loader($previousEntityState);
        if ($errors) {
			$msg ='';
            foreach ($errors as $error) {
                $msg .= $this->display_xml_error($error, $this->inXml);
            }
            throw new \Exception($msg);
        }

        return $out;
    }

    private function readStringXml() {
        $previousEntityState = libxml_disable_entity_loader(true);
        $previousSetting = libxml_use_internal_errors(true);

        $out = @$this->inXml->readString();

        $errors = libxml_get_errors();
        libxml_clear_errors();
        libxml_use_internal_errors($previousSetting);
        libxml_disable_entity_loader($previousEntityState);
        if ($errors) {
			$msg ='';
            foreach ($errors as $error) {
                $msg .= $this->display_xml_error($error, $this->inXml);
            }
            throw new \Exception($msg);
        }

        return $out;
    }

    private function display_xml_error($error, $xml) {
        //$return = $xml[$error->line - 1] . "\n";
        //$return .= str_repeat('-', $error->column) . "^\n";
		$return = '';
        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "Warning $error->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error $error->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error $error->code: ";
                break;
        }

        $return .= trim($error->message) .
                "\n  Line: $error->line" .
                "\n  Column: $error->column";

        if ($error->file) {
            $return .= "\n  File: $error->file";
        }

        return "$return\n\n--------------------------------------------\n\n";
    }

    /**
     * xml tag to product field mapping.
     * An allaksei onoma tag sto xml allazoyme tin aristeri meria (to key)
     * @param string $tag
     * @return string
     */
    private function tagToProductMapping($tag) {
        // tag -> product field
        $map = array(
            'category_id' => 'category_id',
            'product_code' => 'product_code',
            'purchase_price' => 'purchase_price',
            'amount' => 'amount',
            'part_number' => 'part_number',
            'ean' => 'ean',
            'title' => 'title',
            'short_description' => 'short_description',
            'full_description' => 'full_description',
            'search_words' => 'search_words',
            'seo_name' => 'seo_name',
            'brand' => 'brand',
            'image' => 'image',
            'shop_availability' => 'shop_availability',
            'skroutz_availability' => 'skroutz_availability',
            'status' => 'status',
            'weight' => 'weight',
            'free_shipping' => 'free_shipping',
            'cart_product_id' => 'cart_product_id',
            'x_product_id' => 'x_product_id',
            'Supplier'              =>'Supplier',
            'features'              =>'features',
        );
        $out = '';
        if (isset($map[$tag])) {
            $out = $map[$tag];
        }
        return $out;
    }

}
