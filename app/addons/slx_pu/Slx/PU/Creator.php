<?php

namespace Slx\PU;

use Slx\PU\Core\AbstractCU;
use Tygh\Registry;
use Slx\Logger\Logger;

/**
 * Description of Creator
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Creator extends AbstractCU {

    /**
     * Called to create a new product in db
     * 
     * @param type $p
     */
    public function update($p) {
        $productId = $this->createProductRecord($p);
        $this->updateSupplierProduct($this->supplierId, $productId,$p->get('product_code'),$p->get('x_product_id'));
		//db_query("UPDATE cscart_products SET code_makis=?i WHERE product_id=?i", $productId, $productId);
        $this->setCategory($p);
        $this->setDescriptions($p, 'el');
        $this->setDescriptions($p, 'en');
        $this->setPrice($p);
        $this->addProductImages($p->get('product_id'), $p->get('image'));
		if($this->getProductImageCount($p->get('product_id'))==0) {
			$data = array();
			$data['status']= 'D';
			db_query("UPDATE ?:products SET ?u WHERE product_id=?i", $data, $p->get('product_id'));
		}

        if (function_exists('fn_create_seo_name')) {
            fn_create_seo_name($productId, 'p', $p->get('seo_name'), 0, '', Registry::get('runtime.forced_company_id'), fn_get_corrected_seo_lang_code('en'));
        }
        if($p->get('x_product_id')!=-1) {
            db_query("update slx_product set cproduct_id=?i where id=?i", $p->get('product_id'), $p->get('x_product_id'));
        }
        Logger::getInstance()->log("create product");
        XProductStatus::update($p->get('x_product_id'), 1);
    }

    private function createProductRecord($p) {
        $data = $this->getDefaultInsertData();
        $data['status'] = $p->get('status');
        $data['amount'] = $p->get('amount');
        $data['weight'] = $p->get('weight');
        $data['free_shipping_banner'] = $p->get('free_shipping');
        $data['product_code'] = $p->get('product_code');
        $data['ean'] = $p->get('ean');
        $data['product_codeB'] = $p->get('part_number');	//
        $data['product_codeC'] = $p->get('product_code');	//
        $data['my_supplier_id'] = $this->supplierId;		//
        $data['price_buy'] = round($p->get('purchase_price')*$this->vat,2);
        $data['shop_availability'] = $p->get('shop_availability');
        $data['skroutz_availability'] = $p->get('skroutz_availability');
        $data['shop_feed_avail_opt'] = $p->get('shop_feed_avail_opt');
        $data['supplier_name'] = $p->get('Supplier');
		$data['last_disable_timestamp'] = time();
		$data['shipping_freight'] = $p->get('shipping_freight');
        $productId = db_query("INSERT INTO ?:products ?e", $data);
        $p->set('product_id', $productId);
        return $productId;
    }
/*
    protected function setCategory($p) {
		$this->updateProductCategory($p->get('product_id'), $p->get('new_products_category_id'), 'M');
        $cats = array($p->get('new_products_category_id'));        
        fn_update_product_count($cats);
    }
	*/
}
