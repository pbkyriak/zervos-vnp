<?php

namespace Slx\PU;

use Slx\PU\Core\AbstractWorker;
/**
 * workflow
 *  1. call prepare
 *  foreach product row
 *      2. call InputValidator->isValid (if false then product is skipped)
 *      3. call ProductPreparator->prepare
 *      4. call ProductValidator->isValid (if false then product is skipped)
 *      5. persist product
 *  end foreach product row
 *  6. call finish
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Worker extends AbstractWorker {

    /**
     * prepare worker
     */
    protected function prepare() {
        printf("Setup disabler...\n");
        $this->disabler->setup();
        // add any before update jobs you need here
    }
    
    /**
     * called after worker finishes updates
     */
    protected function finish() {
        $this->disabler->disableNotUpdatedProducts();
        // add any after update jobs you need here
    }
        
}
