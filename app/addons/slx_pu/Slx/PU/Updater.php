<?php

namespace Slx\PU;
use Slx\PU\Core\AbstractCU;
use Slx\Logger\Logger;
use Tygh\Registry;
/**
 * Description of Updater
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Updater extends AbstractCU {

	const UPDATE_TITLE = false;	// if true updates title else does not update product title
	
    /**
     * Called to update a product in db
     * 
     * @param type $p
     * @return type
     */
    public function update($p) {
        Logger::getInstance()->log(sprintf("update product pid=%s skip_updater=%s %s",$p->get('product_id'), $p->get('skip_updater'),($p->get('skip_updater') == 'Y')));
        if ($p->get('skip_updater') == 'Y') {   // do not update 
			Logger::getInstance()->log('skipped updater');
			$this->updateProductLastUpdateTimestamp($p);
            XProductStatus::update($p->get('x_product_id'), 3);
            return;
        }
		Logger::getInstance()->log('not skipped updater');
		if ($p->get('skip_updater') != 'P') {  // update only price
			$this->updateProductRecord($p);
			if ($p->get('skip_updater') == 'S') { // stock updated in updateProductRecord, now return to avoid price update
				$this->updateProductLastUpdateTimestamp($p);
				return;
			}
			if( $p->get('skip_updater') != 'R' ) {			
				$this->setCategory($p);
				$update_title = false; //(13==$this->supplierId);
				$this->setDescriptions($p, 'el', $update_title); 
				$this->setDescriptions($p, 'en', $update_title);
/*
				foreach($p->get('image') as $image) {
	Logger::getInstance()->log(sprintf("pid=%s img=%s\n", $p->get('product_id'), $image));
}
*/
				// an to proion exei ligoteres images apo aytes poy fernei o updater tote, diegrapse tis yparxoyses kai prosthese tis nees
				if( $this->getProductImageCount($p->get('product_id'))==0) {  
					//$this->removeProductImages($p->get('product_id'));
					$this->addProductImages($p->get('product_id'), $p->get('image'));
				}
				if($this->getProductImageCount($p->get('product_id'))==0) {
				    $data = array();
				    $data['status']= 'D';
				    db_query("UPDATE ?:products SET ?u WHERE product_id=?i", $data, $p->get('product_id'));
				}
			}
		}
        $this->updateSupplierProduct($this->supplierId, $p->get('product_id'),$p->get('product_code'),$p->get('x_product_id'));
		$this->setPrice($p);
		$this->updateProductLastUpdateTimestamp($p);
		XProductStatus::update($p->get('x_product_id'), 2);
    }
    
    private function updateProductRecord($p) {
		$data = array();
        $data['amount']=$p->get('amount');
		$data['supplier_name'] = $p->get('Supplier');
        if ($p->get('skip_updater') == 'N') {
            $data['status']=$p->get('status');
			$data['weight']=$p->get('weight'); 
            $data['free_shipping_banner']=$p->get('free_shipping');
            $data['ean']=$p->get('ean');
            $data['product_codeB']=$p->get('part_number');	//
            $data['my_supplier_id']=$this->supplierId;		//
            $data['price_buy']=round($p->get('purchase_price')*$this->vat,2);
			$data['shipping_freight'] = $p->get('shipping_freight');
            if($p->get('pricelist_updater_skip_skroutz')=='N') {
				$data['shop_availability']=$p->get('shop_availability');
                $data['skroutz_availability']=$p->get('skroutz_availability');
				$data['shop_feed_avail_opt'] = $p->get('shop_feed_avail_opt');
            }
        }
        db_query("UPDATE ?:products SET ?u WHERE product_id=?i", $data, $p->get('product_id'));
    }
	
	private function updateProductLastUpdateTimestamp($p) {
		$data = array();
        $data['last_disable_timestamp']=time();
		db_query("UPDATE ?:products SET ?u WHERE product_id=?i", $data, $p->get('product_id'));
	}
}
