<?php

namespace Slx\PU\PricingRules;

use Tygh\Registry;
use Slx\Logger\Logger;
/**
 * Description of PricingRules
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PricingRules {

    private $rules;
    private $vat;
    static $iSupplierId=0;
    static $iCategoryId=0;
    static $instance=null;
    
    private function __construct($rules) {
        $this->rules = $rules;
        $this->vat = Registry::get('product_merger.vat_rate')/100+1;
    }

    /**
     * 
     * @param type $cId
     * @param type $supplierId
     * @return \Tygh\PriceUpdater\PricingRules\PricingRules
     */
    public static function create($cId, $supplierId) {
        if( $supplierId==static::$iSupplierId && $cId==static::$iCategoryId && static::$instance ) {
            return static::$instance;
        }
        else {
            static::$iSupplierId = $supplierId;
            static::$iCategoryId = $cId;
            $supRules = PricingRulesFactory::getSupplierRules($supplierId);
            if ($supRules) {
                static::$instance = new PricingRules($supRules->getCategoryRules($cId));
            }
            return static::$instance;
        }
    }

    //Am iparxei pricing rule se katigories tote
    //kanei override to calcPriceForUserGroup mesa apo tous markaper twn suppliers
    public function applyToPrice($basePrice) {
        $out = 0;
        if (count($this->rules)) {
            foreach ($this->rules as $rule) {
                Logger::getInstance()->log(sprintf("rule=%s %s\n", print_R($rule, true),$basePrice));
                if ((float) $rule['from_price'] < $basePrice) {
                    Logger::getInstance()->log(sprintf("Base Price: %s\n",$basePrice));
                    if ($rule['markup_type'] == 'A')
                        $out = $basePrice + $rule['markup'];
                    else
                        $out = $basePrice * (1 + $rule['markup'] / 100);
                    //    $out = $basePrice / (1 - $rule['markup'] / 100); ������ ������ ����������� ������� ������� https://www.qwerty.gr/howto/margin-vs-markup
                    Logger::getInstance()->log(sprintf("Base Price after rules: %s \n", $out));
                    $out = $out * $this->vat;
                    Logger::getInstance()->log(sprintf("Price without HAT (with taxes): %s \n", $out));
                    $out += (float) Registry::get('settings.General.christmass_hat');
                    //echo "Price with HAT: $out<br />";
                    if ($rule['round_to'] == 0)
                        $out = round($out, 0);
                    else
                        $out = $this->round_to($out, $rule['round_to'] == 0 ? l : $rule['round_to'] / 100);
                    //echo "Price after rounding: $out<br />";
                    Logger::getInstance()->log(sprintf("matched rule=%s \n baseprice=%s price=%s\n", print_R($rule, true), $basePrice, $out));
                    break;
                }
            }
        }
        //printf("baseprice=%s out=%s <br />", $basePrice, $out);
        return $out;
    }

    protected function round_to($number, $increments) {
        $increments = 1 / $increments;
        return (round($number * $increments) / $increments);
    }

}
