<?php

namespace Slx\PU\PricingRules;

use Tygh\Registry;
use Slx\Logger\Logger;
/**
 * A factory for supplier pricing rules classes.
 * Added Jul 2015
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PricingRulesFactory {
    
    public static function getRulesLocation() {
        return Registry::get('product_merger.pricing_rules_dir');
    }
    /**
     * Get princing rules per category for a supplier. Returns
     * instance of SupplierPricingRulesInterface.
     * @param type $supplierId
     * @return \PriceUpdater\PricingRules\SupplierPricingRulesInterface
     */
    public static function getSupplierRules($supplierId) {
        $classname = 'Slx\\PU\\PricingRules\\Supplier\\SupplierPricingRules'.$supplierId;
        $classfile = sprintf(PricingRulesFactory::getRulesLocation() . '/SupplierPricingRules%s.php', $supplierId);
        Logger::getInstance()->log(sprintf("Supplier Pricing Rules: %s", $classname));
        require_once $classfile;
        return new $classname();
    }
    
}
