<?php

namespace Slx\PU\PricingRules;

/**
 * Abstract class for PricingRules per supplier, contains only functionality
 * to serve data in subclasses.
 * Added Jul 2015
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractSupplierPricingRules implements SupplierPricingRulesInterface {

    protected $rules;

    public function getRules() {
        return $this->rules;
    }

    public function getCategoryRules($categoryId) {
        if (isset($this->rules[$categoryId])) {
            return $this->rules[$categoryId];
        }
        return null;
    }

}
