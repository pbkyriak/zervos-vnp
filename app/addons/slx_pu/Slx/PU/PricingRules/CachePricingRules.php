<?php

namespace Slx\PU\PricingRules;

/**
 * Dimourgei arxei me caching twn pricing rules ana katigoria kai supplier
 * Classes are stored to priceupdater_core/Tygh/PriceUpdater/PricingRules/Supplier
 * Added Jul 2015
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class CachePricingRules {

    private $suppliers;
    private $classTemplate;

    public function __construct() {
        $this->suppliers = db_get_fields("SELECT supplier_id FROM ?:suppliers WHERE status='A'"); // and default_product_status='A'");
        $this->categories = db_get_fields("SELECT category_id FROM ?:categories WHERE status!='D' ORDER BY id_path");
        $this->classTemplate = <<<EOT
<?php
namespace Slx\PU\PricingRules\Supplier; 
use Slx\PU\PricingRules\AbstractSupplierPricingRules;
                
class SupplierPricingRules%%supplier_id%% extends AbstractSupplierPricingRules {
		
	public function __construct() {
		@this->rules = array();
%%rules%%
	}
	
}
EOT;
    }

    public function run() {
        foreach ($this->suppliers as $supplierId) {
            $this->makeSupplierClass($supplierId);
        }
    }

    private function makeSupplierClass($supplierId) {
        $rulesCode = '';
        foreach ($this->categories as $categoryId) {
            $rules = $this->loadRules($supplierId, $categoryId);

            if ($rules) {
                $rulesCode .= sprintf("\t\t@this->rules[%s] = array();\n", $categoryId);
                foreach ($rules as $rule) {
                    $a = sprintf("\t\t@this->rules[%s][] = array('from_price'=>%s, 'markup'=>%s, 'markup_type'=>'%s', 'round_to'=>%s);\n", $categoryId, $rule['from_price'], $rule['markup'], $rule['markup_type'], $rule['round_to']
                    );

                    $rulesCode .= $a;
                }
            }
        }

        $classCode = str_replace('%%rules%%', $rulesCode, $this->classTemplate);
        $classCode = str_replace('%%supplier_id%%', $supplierId, $classCode);
        $classCode = str_replace('@', '$', $classCode);
        $dir = PricingRulesFactory::getRulesLocation(); //sprintf(DIR_ROOT . '/var/cache/misc/Supplier');
        $fn = sprintf($dir . '/SupplierPricingRules%s.php', $supplierId);
        if( !file_exists($dir) ) {
            fn_mkdir($dir,0777);
        }
        $p = file_put_contents($fn, $classCode);
        @chmod($fn, 0775);
    }

    private function loadRules($sId, $cId) {
        $parents = $this->getCategoryParents($cId);
        $rules = array();
        foreach ($parents as $cId) {
            $rules = $this->loadCategoryForSupplierRules($cId, $sId);
            if (count($rules))
                break;
        }
        return $rules;
    }

    private function loadCategoryForSupplierRules($cId, $supplier) {
        $rules = db_get_array("SELECT * FROM ?:category_pricing_rules 
    		WHERE category_id=?i AND supplier_id=?i ORDER BY from_price DESC", $cId, $supplier);
        return $rules;
    }

    private function getCategoryParents($cId) {
        $path_ids = db_get_field("SELECT id_path FROM ?:categories WHERE category_id=?i", $cId);
        return array_reverse(explode('/', $path_ids));
    }

}
