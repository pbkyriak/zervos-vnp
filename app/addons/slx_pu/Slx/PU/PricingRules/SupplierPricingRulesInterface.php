<?php

/*
 * Supplier Pricing Rules Interface.
 * 
 */

namespace Slx\PU\PricingRules;

/**
 * Added Jul 2015
 * @author Panos Kyriakakis <panos@salix.gr>
 */
interface SupplierPricingRulesInterface {
    
    public function getRules();
    public function getCategoryRules($categoryId);
}
