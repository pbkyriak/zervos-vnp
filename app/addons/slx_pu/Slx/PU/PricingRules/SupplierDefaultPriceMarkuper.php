<?php

namespace Slx\PU\PricingRules;
use Tygh\Registry;
/**
 * Default supplier Markuper. Uses the settings from store's backend.
 * See supplier page
 *
 * Added Jul 2015
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SupplierDefaultPriceMarkuper {
    private $data;
    
    public function __construct($supplierId) {
        $this->supplierId = $supplierId;
        $this->data = array(
            'markup_profit_type'=>'A',
            'markup_profit_amount'=>0,
            'markup_add_hat'=>'N',
            'markup_tax_amount'=>0,
			'wmarkup_enable'=>'N',
			'wmarkup_weight'=>0,
			'wmarkup_cweight_type'=>'A',
			'wmarkup_calc_type' => 'A',
			'wmarkup_amount' => 0,
			'default_product_status' => 'A',
            );
		$fields = implode(',', array_keys($this->data));
        $row = db_get_row("SELECT ".$fields." FROM ?:suppliers WHERE supplier_id=?i", $supplierId);
        if( $row ) {
            $this->data = $row;
        }
    }
    
    public function calcPriceForUserGroup($basePrice) {
        $out = $basePrice;
        if( $this->data['markup_profit_type']=='A' ) {
            $out += $this->data['markup_profit_amount'];
        }
        else {
            $out = round($out * (1+$this->data['markup_profit_amount']/100),2);
        }
        if( $this->data['markup_add_hat'] ) {
            $out += $this->getHat();
        }
        $out = round($out * (1+$this->data['markup_tax_amount']/100),2);
        return $out;
    }

	public function calcWeightSurcharge($basePrice, $weight) {
		$out = $basePrice;
		if( $this->data['wmarkup_enable']=='Y' && $weight>$this->data['wmarkup_weight'] ) {
			$cweight = $this->getCalcWeight($weight);
			if( $this->data['wmarkup_calc_type']=='A' ) {
				$out = $basePrice + $weight*$this->data['wmarkup_amount'];
			}
			else {
				$out = $basePrice + $this->data['wmarkup_amount'];
			}
		}
		return round($out,2);
	}
	
	private function getCalcWeight($weight) {
		$out = 0;
		switch( $this->data['wmarkup_cweight_type'] ) {
			case 'A':
				$out = $weight;
				break;
			case 'B':
				$out = $weight-$this->data['wmarkup_weight'];
				break;
			case 'C':
				$out = $this->data['wmarkup_weight'];
				break;				
			case 'D':
				$out = $weight-$this->data['wmarkup_weight_offset'];
				break;
		}
		return $out;
	}
	
    private function getHat() {
        $hat = (float) Registry::get('settings.AutoPricing.christmass_hat');
        return $hat;
    }

    public function round_to($number, $increments) {
        $increments = 1 / $increments;
        return (round($number * $increments) / $increments);
    }

}
