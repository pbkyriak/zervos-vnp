<?php

namespace Slx\PU\Core;
use Slx\Logger\Logger;

/**
 * Description of ProductDisabler
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ProductDisabler {
    private $supplierId;
    private $bareliCategoryId;
	
    public function __construct($supplierId) {
        $this->supplierId = $supplierId;
		$this->bareliCategoryId = db_get_field("SELECT new_products_category_id FROM ?:suppliers WHERE supplier_id=?i", $this->supplierId);
    }
    
    public function setup() {
        $this->collectSupplierProductIds($this->supplierId);
        //$this->clearOutdatedSupplierCatalogRecords($this->supplierId, time());
    }
    
    private function collectSupplierProductIds($supplierId) {
        db_query("DELETE FROM ?:sup_ids WHERE supplier_id=?i", $supplierId);
        db_query("INSERT INTO ?:sup_ids SELECT object_id, supplier_id 
	    	FROM ?:supplier_links WHERE supplier_id=?i  AND object_type=?s", $supplierId, 'P');
    }
/*
    private function clearOutdatedSupplierCatalogRecords($supplierId, $beginTimestamp) {
        //diagrafei ap to table ths enopoihshs timvn ,stock oti afora ton trexvn supplier h den einai pleon xronika egkyro
        Logger::getInstance()->log(sprintf ("Delete not time valid ean products supp=%s time=%s",$supplierId, $beginTimestamp));
        db_query("DELETE FROM ?:supplier_catalog_data WHERE supplier_id=?i OR valid_timestamp<?i", $supplierId, $beginTimestamp);
    }
*/
    /**
     * Removes from db cscart_sup_ids the given product id, so it will not
     * be markup as not updated any more. 
     * @param int $productId
     */
    public function remoreFromToDisableList($productId) {
        db_query("DELETE FROM cscart_sup_ids WHERE product_Id=".$productId);
    }
	
    public function disableNotUpdatedProducts() {
        $supplierId = $this->supplierId;
        Logger::getInstance()->log("Disable the product which wasn't updated");
			db_query("update ?:products set shop_feed_avail_opt=9, amount=666, status='D' 
					where 
						product_id in (select bad.product_id from ?:sup_ids bad where supplier_id=?i)
						and skip_updater!='Y'
					",$supplierId);

		//db_query("update cscart_products p, cscart_product_prices pp set p.status='H' where (p.product_id=pp.product_id and pp.lower_limit=1) and p.my_supplier_id=?i and pp.price<60 and p.skip_updater!='Y'",$supplierId);
		
        Logger::getInstance()->log("disabling not update supplier products DONE");
        Logger::getInstance()->log("disabling bareli products");
		$this->disableBareliProducts();
		Logger::getInstance()->log("disabling bareli products DONE");
    }

	private function disableBareliProducts() {
		db_query("update ?:products set status='D' where product_id in ( select product_id from ?:products_categories where link_type='M' and category_id=?i)", $this->bareliCategoryId);
	}
	
    public function disableNotUpdatedProductsForced() {
        $supplierId = $this->supplierId;
        Logger::getInstance()->log("Disable the product which wasn't updated");
			db_query("update ?:products set shop_feed_avail_opt=9, status='D' 
					where 
						product_id in (select bad.product_id from ?:sup_ids bad where supplier_id=?i)
					",$supplierId);
        Logger::getInstance()->log("disabling not update supplier products forced DONE");
    }

}
