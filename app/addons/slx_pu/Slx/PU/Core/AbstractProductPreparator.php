<?php

namespace Slx\PU\Core;

use Slx\PU\PricingRules\PricingRules;
use Slx\PU\PricingRules\SupplierDefaultPriceMarkuper;

/**
 * Description of AbstractProductPreparator
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractProductPreparator {

    protected $supplierId;
    protected $defMarkaper;

    public function __construct($supplierId) {
        $this->supplierId = $supplierId;
        $this->defMarkaper = new SupplierDefaultPriceMarkuper($this->supplierId);
    }

    /**
     * Matches product to db using product code.
     * If you need any other data to be retrieved also
     * add fields and default values to $data (getMatchProductFields)
     * @param Product|type $p
     */
    protected function matchProduct(Product $p) {
        $data = $this->getMatchProductFields($p);
        $selectFields = implode(',', array_keys($data));
        $cs_product_id = $p->get('cart_product_id');
        if($cs_product_id==-1) {    // match by code and ean
            /*
            $row = db_get_row(
                "SELECT " . $selectFields . " FROM ?:products WHERE (product_code=?s or ean=?s) and my_supplier_id=?i",
                $p->get('product_code'),
                $p->get('ean'),
                $this->supplierId
            );
            */
			if(!empty($p->get('ean'))) {
				$row = db_get_row(
					"SELECT " . $selectFields . " FROM ?:products WHERE (product_code=?s or ean=?s) ",
					$p->get('product_code'),
					$p->get('ean')
				);
			}
			else {
				$row = db_get_row(
					"SELECT " . $selectFields . " FROM ?:products WHERE product_code=?s ",
					$p->get('product_code')
				);
			}
        }
        else {  // match by cs-cart-product-id
            $row = db_get_row(
                "SELECT " . $selectFields . " FROM ?:products WHERE product_id=?i",
                $cs_product_id
            );
        }
        if ($row) {
            foreach ($data as $key => $v) {
                if (isset($row[$key])) {
                    $data[$key] = $row[$key];
                }
            }
        }
        foreach ($data as $key => $v) {
            $p->set($key, $v);
        }
    }

    abstract protected function getMatchProductFields(Product $p);

    /**
     * Matches category_id. Checks if category id existing in db.
     *
     * @param Product|type $p
     */
    protected function matchCategory(Product $p) {
		$catId = db_get_field("SELECT category_id FROM ?:categories WHERE category_id=?i", $p->get('category_id'));
        if ($catId) {
            $p->set('category_id', $catId);
        } else {
            $p->set('category_id', -1);
        }
    }

    /**
     * Price markup.
     * Firsts tries to use category pricing rules,
     * if none then uses markup rules from supplier.
     *
     * @param Product|type $p
     */
    protected function markupPrice(Product $p) {
        $pr = PricingRules::create($p->get('category_id'), $this->supplierId);
        $price = $pr->applyToPrice($p->get('purchase_price'));
        if ($price == 0) {
            $price = $this->defMarkaper->calcPriceForUserGroup($p->get('purchase_price'));
        }
        $price = $this->defMarkaper->calcWeightSurcharge($price, $p->get('weight'));
        $price = $this->defMarkaper->round_to($price, 0.1);
        $p->set('price', $price);
    }

}
