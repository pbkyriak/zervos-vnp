<?php

namespace Slx\PU\Core;

/**
 * Description of Product
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Product {
    
    private $attributes;
    
    public function __construct() {
        $this->attributes = array();
    }
    
    public function set($attr, $val) {
        $this->attributes[$attr] = $val;
    }
    public function add($attr, $val) {
        if(!isset($this->attributes[$attr])) {
            $this->attributes[$attr] = array();
        }
        $this->attributes[$attr][] = $val;
    }
    
    public function get($attr) {
        $out = null;
        if(isset($this->attributes[$attr])) {
            $out = $this->attributes[$attr];
        }
        return $out;
    }
    
    public function dump(){
        print_R($this->attributes);
    }
}
