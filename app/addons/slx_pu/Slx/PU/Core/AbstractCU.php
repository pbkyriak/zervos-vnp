<?php

namespace Slx\PU\Core;

use Slx\PU\Core\Traits\DefaultInsertDataTrait;
use Slx\PU\Core\Traits\ImagesTrait;
use Tygh\Registry;
use Slx\Logger\Logger;
/**
 * Parent for Creator and Updater classes.
 * Holds common functionality for product creation and update
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class AbstractCU {
    protected $supplierId;
    protected $vat;

    use DefaultInsertDataTrait;
    use ImagesTrait;

    public function __construct($supplierId) {
        $this->supplierId = $supplierId;
		$this->vat = Registry::get('product_merger.vat_rate')/100+1;
    }
    protected function setCategory($p) {
		$this->updateProductCategory($p->get('product_id'),$p->get('category_id'));
        fn_update_product_count(array($p->get('category_id')));
    }
    
	protected function updateProductCategory($productId, $categoryId) {
        $data = array(
            'product_id' => $productId,
            'category_id' => $categoryId,
            'link_type' => 'M',
        );
		db_query("DELETE FROM ?:products_categories WHERE product_id=?i", $productId);
        db_query("REPLACE INTO ?:products_categories ?e", $data);
	}
	
    protected function setDescriptions($p, $langCode, $updateTitle=true) {
        $data = array(
            'product'           => $p->get('title'),
            'shortname'         => $p->get('title'),
            'short_description' => str_replace('&amp;','&',$p->get('short_description')),
            'full_description'  => str_replace('&amp;','&',$p->get('full_description')),
            'full_description2' => '', //$p->get('full_description'),
            'meta_keywords'     => $p->get('search_words'),
            'meta_description'  => sprintf("%s", $p->get('title')),
            'search_words'      => sprintf("%s,%s",$p->get('product_code'),$p->get('search_words')),
            'page_title'        => sprintf("%s",$p->get('title')),
            'lang_code'         => $langCode,
            'product_id'        => $p->get('product_id'),
        );
        if( !$updateTitle ) {
			$tmp = db_get_row("select product, shortname from ?:product_descriptions where product_id=?i and lang_code=?s", $p->get('product_id'), $langCode);
			if( $tmp ) {
				$data['product'] = $tmp['product'];
				$data['shortname'] = $tmp['shortname'];
			}
        }
        db_query("REPLACE INTO ?:product_descriptions ?e",$data);
    }
    
    protected function setPrice($p) {
        $data = array(
            'product_id' => $p->get('product_id'),
            'usergroup_id' => 0,
            'lower_limit' => 1,
            'price' => $p->get('price'),
        );
        db_query("REPLACE INTO ?:product_prices ?e", $data);
    }
    


    protected function getAvailabilities($p) {
        $data = array(
            'shop_availability'=>1,
            'skroutz_availability'=>1,
            );
    }

    protected function updateSupplierProduct($supplier_id, $productId, $productCode, $xproduct_id) {
        if(!empty($supplier_id) && !empty($productId)) {
            Logger::getInstance()->log(sprintf("Set Supplier productID=%s supplierId=%s", $productId, $supplier_id));
            db_query('DELETE FROM ?:supplier_links WHERE object_type = ?s AND object_id=?i', 'P', $productId);
            db_query("UPDATE ?:products SET my_supplier_id=?i, product_codeC=?s, xproduct_id=?i WHERE product_id=?i", $supplier_id, $productCode,$xproduct_id, $productId);
            db_query('INSERT INTO ?:supplier_links (supplier_id, object_id, object_type) VALUES (?i, ?i, ?s)', $supplier_id, $productId, 'P');
            db_query('DELETE FROM slx_supplier_links WHERE product_id=?i', 'P', $productId);
            db_query('INSERT INTO slx_supplier_links (supplier_id, product_id) VALUES (?i, ?i)', $supplier_id, $productId);
        }
        else {
            Logger::getInstance()->log(sprintf("FAIL Set Supplier productID=%s supplierId=%s", $productId, $supplier_id));
        }
        return true;
    }

}
