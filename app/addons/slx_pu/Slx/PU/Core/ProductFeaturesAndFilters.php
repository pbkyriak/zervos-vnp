<?php

namespace Slx\PU\Core;
use Slx\ProductMerger\Exporter\ProductStream\FeatureExportDefinition;
use Tygh\Registry;
use Slx\Logger\Logger;
/**
 * Description of AbstractProductFeaturesAndFilters
 * @author nsik
 * @author imatz
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ProductFeaturesAndFilters {
    protected $cart_lang_primary;
    protected $cart_lang_secondary;
    protected $product;
    protected $manufacturer_feature_id;
    protected $fDefs;

    public function __construct() {
        $this->cart_lang_primary = 'el'; //Registry::get('pu_cart_lang_primary');
        $this->cart_lang_secondary = 'en'; //Registry::get('pu_cart_lang_secondary');
        $this->manufacturer_feature_id = Registry::get("addons.slx_updater_admin.manufacturerfromfeature");
        $this->fDefs = new FeatureExportDefinition();
    }

    /**
     * 
     * @param ProductRow $product
     */
    public function updateFeaturesAndFilters($product) {
        $this->product = $product;

        if (!empty($this->product->get('brand')) && !empty($this->product->get('product_id'))) {
            if(!empty($this->manufacturer_feature_id)) {
                $this->updateFeatureVariantAndLinkWithProduct($this->product->get('product_id'), $this->manufacturer_feature_id, $this->product->get('brand'));
            }
        }
        $rFeatures = $product->get('features');
        if (is_array($rFeatures)) {
            foreach ($rFeatures as $rFeature => $rFeatureV) {
                if ($featureId = $this->fDefs->getCategFeatureId(str_replace("X-",'',$this->product->get('xcategory_id')), $rFeature)) {
                    if(!empty($featureId)) {
                        $this->updateFeatureVariantAndLinkWithProduct(
                            $this->product->get('product_id'),
                            $featureId,
                            $rFeatureV
                        );
                    }
                }
            }
        }
    }

    public function updateFeatureVariantAndLinkWithProduct($product_id, $feature_id, $variant) {

        $variant_id = $this->getFeatureVariantByTitle($feature_id, $variant);
        //An to fetaure tou kataskeyasti den exei to variant tote prosthiki
        if (empty($variant_id)) {
            $variant_id = $this->createFeatureVariant($feature_id, $variant);
        }

        //elegxos an to proion exei to feature me auto to variant
        $variant_exits_in_product = $this->hasProductFeatureVariant($product_id, $feature_id, $variant_id);
        //To proion den exei to variant tou feature sindedemeno
        if (empty($variant_exits_in_product)) {
            $this->addFeatureVariantToProduct($product_id, $feature_id, $variant_id, $this->cart_lang_primary);
            $this->addFeatureVariantToProduct($product_id, $feature_id, $variant_id, $this->cart_lang_secondary);
        } else {
            $this->fixFeatureVariantToProduct($product_id, $feature_id, $variant_id);
        }
    }

    private function getFeatureVariantByTitle($feature_id, $variant) {
        $variant_id = db_get_field("
            SELECT pfvd.variant_id FROM ?:product_feature_variants AS pfv 
            LEFT JOIN ?:product_feature_variant_descriptions AS pfvd ON pfvd.variant_id=pfv.variant_id
            WHERE pfvd.variant=?s AND pfv.feature_id=?i AND pfvd.lang_code=?s", $variant, $feature_id, $this->cart_lang_primary
        );
        return $variant_id;
    }

    private function createFeatureVariant($feature_id, $variant) {
        //Mono gia brand
        if ($feature_id == $this->manufacturer_feature_id) {
            $insert_data = array(
                'feature_id' => $feature_id,
                'variant_name' => $variant
            );
        } else {
            $insert_data = array(
                'feature_id' => $feature_id
            );
        }
        $variant_id = db_query("INSERT INTO ?:product_feature_variants ?e", $insert_data);
        $this->setVariantDescription($variant_id, $variant, $this->cart_lang_primary);
        $this->setVariantDescription($variant_id, $variant, $this->cart_lang_secondary);
        return $variant_id;
    }

    private function setVariantDescription($variant_id, $variant, $langCode) {
        $insert_data = array(
            'variant_id' => $variant_id,
            'variant' => $variant,
            'lang_code' => $langCode
        );
        db_query("REPLACE INTO ?:product_feature_variant_descriptions ?e", $insert_data);        
    }
    
    private function hasProductFeatureVariant($product_id, $feature_id, $variant_id) {
        $variant_exits_in_product = db_get_field(
                "SELECT COUNT(*) FROM ?:product_features_values WHERE feature_id=?i AND product_id=?i AND variant_id=?i", $feature_id, $product_id, $variant_id
        );

        //An gia kapoio logo to proion exei sto feature 2 variants
        if ($variant_exits_in_product > 1) {
            $feature_type = db_get_field(
                    "SELECT feature_type FROM ?:product_features WHERE feature_id=?i", $feature_id
            );
            if (!empty($feature_type) && $feature_type != "M") {//Den einai feature me dinatotita pollaplwn vriants se ena proion
                //Diagrafei twn dipltipwn eggrafw tou feature sto proion
                db_query(
                        "DELETE FROM ?:product_features_values WHERE feature_id=?i AND product_id=?i", $feature_id, $product_id
                );
                $variant_exits_in_product = 0;
            }
        }
        return $variant_exits_in_product;
    }

    private function addFeatureVariantToProduct($product_id, $feature_id, $variant_id, $langCode) {
        //prosthiki gia tin ellinik glossa      
        $insert_data = array(
            'feature_id' => $feature_id,
            'product_id' => $product_id,
            'variant_id' => $variant_id,
            'lang_code' => $langCode
        );
        db_query("REPLACE INTO ?:product_features_values ?e", $insert_data);
    }

    private function fixFeatureVariantToProduct($product_id, $feature_id, $variant_id) {
        //To proion exei variant gia to feature ayto
        $product_feature_variant_id = db_get_field(
                "SELECT variant_id FROM ?:product_features_values WHERE feature_id=?i AND product_id=?i", 
                $feature_id, 
                $product_id
                );
        if ($product_feature_variant_id != $variant_id) {//An to variant gia to feature exei allaksei
            //update tou variant id gia oles tis glwsses tou product
            db_query("UPDATE ?:product_features_values SET variant_id=?i WHERE feature_id=?i AND product_id=?i", 
                    $variant_id, 
                    $feature_id, 
                    $product_id
                    );
        }        
    }
    
    public function addProductFeature($feature_id, $variant_id) {
        $variant_exits_in_product = db_get_field("SELECT COUNT(*) FROM ?:product_features_values WHERE feature_id=?i AND product_id=?i AND variant_id=?i", $feature_id, $this->product->get('product_id'), $variant_id);

        //To proion den exei to variant tou fetaure sindedemeno
        if (empty($variant_exits_in_product)) {
            $this->addProductFeatureValue($this->product->get('product_id'), $feature_id, $variant_id, $this->cart_lang_primary);
            $this->addProductFeatureValue($this->product->get('product_id'), $feature_id, $variant_id, $this->cart_lang_secondary);
        }
    }

    private function addProductFeatureValue($productId, $feature_id, $variant_id, $langCode) {
            $insert_data = array(
                'feature_id' => $feature_id,
                'product_id' => $productId,
                'variant_id' => $variant_id,
                'lang_code' => $langCode
            );
            db_query("INSERT INTO ?:product_features_values ?e", $insert_data);
        
    }

}
