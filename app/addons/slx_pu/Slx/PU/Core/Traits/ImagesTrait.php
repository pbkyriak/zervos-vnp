<?php

namespace Slx\PU\Core\Traits;

use Tygh\Registry;
use Slx\Logger\Logger;

trait ImagesTrait {
    protected function removeProductImages($productId) {
        fn_delete_image_pairs($productId, 'product');
    }

    protected function getProductImageCount($productId) {
        return db_get_field("select count(*) as cnt from ?:images_links where object_type='product' and object_id=?i", $productId);
    }

    protected function addProductImages($productId, $images) {
        $linkType = 'M';
        if(!$images) {
            return;
        }
        foreach($images as $image) {
            $image = $this->prepareImage($image);
            if( $image ) {
                $this->updateImagePair($productId, $image, $linkType);
                @unlink(Registry::get("product_merger.imres.tmp_dir").basename($image));
                if( $this->getProductImageCount($productId)!=0 ) {
                    $linkType='A';
                }
            }
        }
    }

    private function updateImagePair($productId, $imgsrc, $linkType = "M", $object = "product") {
        $detailed = array();
        $im = fn_get_url_data($imgsrc);

        $detailed[] = $im;
        $pair_data[] = array(
            "pair_id" => null,
            "type" => $linkType,
            "object_id" => $productId,
            "image_alt" => '',
            "detailed_alt" => '',
        );

        fn_update_image_pairs(array(), $detailed, $pair_data, $productId, $object);
        unset($im);
        unset($detailed);
        unset($pair_data);
    }

    private function prepareImage($url) {
        $imresDir = DIR_ROOT.'/app/addons/slx_pu/bin';
        $imresTmpDir = Registry::get("product_merger.imres.tmp_dir");

        $out = false;
        $fn = preg_replace("/[^a-zA-Z0-9\.]/", "_", basename($url));
        $file = $imresTmpDir . 'dn-' . $fn;
        $resizedFile = $imresTmpDir . $fn;
        $srcUrl = $url;
        //$url = 'https://www.ibm.com/cloud-computing/images/cloud-videologo-image-new.png';
        $dstUrl = Registry::get('config.current_location') . Registry::get("product_merger.imres.uri").$fn;
        $cmd = sprintf('%s/imres.sh "%s" "%s" "%s"', $imresDir, $file, $url, $resizedFile);
        Logger::getInstance()->log($cmd);
        $output = array();
        $out = exec($cmd, $output);
        if( file_exists($resizedFile) ) {
            $out = $dstUrl;
            Logger::getInstance()->log($dstUrl);
        }

        return $out;
    }

}