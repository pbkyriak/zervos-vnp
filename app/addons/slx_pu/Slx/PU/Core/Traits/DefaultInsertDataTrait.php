<?php

namespace Slx\PU\Core\Traits;


trait DefaultInsertDataTrait {
    /**
     * Return array with default values for insert product statement
     *
     * @return array
     */
    protected function getDefaultInsertData() {
        $data = array(
            'product_type' =>           'P',
            'list_price' =>             0,
            'weight' =>                 0,
            'length' =>                 0,
            'width' =>                  0,
            'height' =>                 0,
            'low_avail_limit' =>        0,
            'shipping_freight' =>       0,
            'timestamp' =>              strtotime(date("Y-m-d")),
            'usergroup_ids' =>          '0',
            'is_edp' => 		'N',
            'edp_shipping' => 		'N',
            'unlimited_download' =>     'N',
            'tracking' => 		'B',
            'free_shipping' => 		'N',
            'feature_comparison' =>     'N',
            'zero_price_action' => 	'R',
            'is_pbp' => 		'N',
            'is_op' => 			'N',
            'is_oper' => 		'N',
            'is_returnable' => 		'N',
            'return_period' => 		10,
            'avail_since' => 		0,
            'localization' => 		'',
            'min_qty' => 		0,
            'max_qty' => 		0,
            'qty_step' => 		0,
            'list_qty_count' => 	0,
            'age_verification' => 	'N',
            'age_limit' => 		0,
            'options_type' => 		'P',
            'exceptions_type' => 	'F',
            'details_layout' => 	'default',
            'shipping_params' => 	'a:5:{s:16:"min_items_in_box";i:0;s:16:"max_items_in_box";i:0;s:10:"box_length";i:0;s:9:"box_width";i:0;s:10:"box_height";i:0;}',
            'tax_ids' =>                '',
            'status' =>                 'H',
            'amount' =>                 0,
            'product_code' =>           '',
            'company_id' =>             1,
            'my_supplier_id' =>         $this->supplierId,
            'price_buy' =>              0,
        );
        return $data;
    }

}