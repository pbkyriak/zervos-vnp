<?php

namespace Slx\PU\Core;

use Slx\PU\Core\ProductFeaturesAndFilters;
use Slx\PU\Core\ProductDisabler;
use Slx\PU\InputValidator;
use Slx\PU\ProductValidator;
use Slx\PU\Creator;
use Slx\PU\Updater;
use Slx\Logger\Logger;
use Slx\PU\ProductPreparator;
use Slx\PU\PricingRules\CachePricingRules;
use Slx\PU\Category;
use Slx\PU\XProductStatus;

/**
 * Description of AbstractWorker
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractWorker {
    protected $inputVal;
    protected $productVal;
    protected $creator;
    protected $updater;
    protected $supplierId;
    protected $preparator;
    protected $disabler;
    protected $filterer;
    private $productCnt;

    private function commonPrepare() {
        printf("Cache pricing rules...\n");
        $cachePR = new CachePricingRules();
        $cachePR->run();
        printf("Index categories...\n");
        $catIndexer = new Category\IndexCategories();
        $catIndexer->run();
        XProductStatus::resetAll();
    }

    public function setup($supplierId) {
        $this->supplierId = $supplierId;
        $this->inputVal = new InputValidator($this->supplierId);
        $this->productVal = new ProductValidator($this->supplierId);
        $this->preparator = new ProductPreparator($this->supplierId);
        $this->creator = new Creator($this->supplierId);
        $this->updater = new Updater($this->supplierId);
        $this->disabler = new ProductDisabler($this->supplierId);
        $this->filterer = new ProductFeaturesAndFilters();        
    }
    
    public function run($parser) {
        $this->productCnt=0;
        if( $parser->open() ) {
            $t1= time();
            $this->commonPrepare();
            while( $supplier = $parser->nextSupplier() ) {
                Logger::getInstance()->log(sprintf("\n>> supplier found %s \n", $supplier));
                $supplierId = $this->getSupplierByName($supplier);
                Logger::getInstance()->log(sprintf(">> supplier id %s \n", $supplierId));
                if( $supplierId ) {
                    $this->runSupplier($supplierId, $parser);
                }
				else {
					$this->disableSupplierProducts($supplier);
				}
            }
            Logger::getInstance()->log(sprintf("upd time: %s for %s products \n", time()-$t1, $this->productCnt));
        }
        $parser->close();
	return $this->productCnt;
    }
    
    private function runSupplier($supplierId, $parser) {
        printf("Setup...\n");
        $this->setup($supplierId);
        printf("Prepare...\n");
        $this->prepare();
        printf("Loop...\n");
        while( $p = $parser->next() ) {
            printf("R");
            if($this->isValidInputData($p)) {
				printf("V");
                $this->prepareProduct($p);
                if($this->isValidProduct($p)) {
                    $this->productCnt++;
                    Logger::getInstance()->log(sprintf("%s. ", $this->productCnt));
                    $this->persistProduct($p);
                }
            }
        }
        $this->finish();
    }
    
    /*
     * validate raw input data
     * 
     */
    protected function isValidInputData($p) {
        return $this->inputVal->isValid($p);
    }

    /**
     * validate product data, ie category matched or has description
     * @param Product $p
     * @return boolean
     */
    protected function isValidProduct($p) {
        return $this->productVal->isValid($p);
    }
    
    protected function prepareProduct($p) {
        $this->preparator->prepare($p);
    }
           
    protected function persistProduct($p) {
        if($p->get('product_id')>0) {
            printf("U ");
            $this->updater->update($p);
            $this->disabler->remoreFromToDisableList($p->get('product_id'));
			$this->filterer->updateFeaturesAndFilters($p);
        }
        else {
           printf("C ");
           $this->creator->update($p);
		   $this->filterer->updateFeaturesAndFilters($p);
        }        
        
    }

    private function getSupplierByName($supplier) {
        return db_get_field("SELECT supplier_id from ?:suppliers Where name=?s", $supplier);
    }
	
    private function isSupplierActive($supplier) {
        $status =
            db_get_field("select status from ?:suppliers where name=?s", $supplier);
        return $status == 'A';
    }

	private function disableSupplierProducts($supplier) {
		return false;
		$supplierId = db_get_field("SELECT supplier_id from ?:suppliers Where name=?s and status='D'", $supplier);
		if($supplierId) {
			$this->setup($supplierId);
			$this->disabler->setup();
			$this->disabler->disableNotUpdatedProductsForced();
		}
	}
}
