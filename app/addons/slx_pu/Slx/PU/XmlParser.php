<?php

namespace Slx\PU;
use Slx\PU\Core\Product;

/**
 * Description of XmlParser
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class XmlParser {

    private $inFile;
    private $inXml = null;
    private $prodCnt = 0;
	const PRODUCT_TAG = 'Product';
	const IMAGE_TAG = 'image';


    public function __construct($inFile) {
        $this->inFile = $inFile;
        $this->inXml = null;
        printf("%s\n\n", __CLASS__);
    }

    public function open() {
        die('out parser');
        $out = false;
        $this->inXml = new \XMLReader();
		try {
			//if ($this->inXml->open($this->inFile, 'windows-1253', LIBXML_NOERROR | LIBXML_NOWARNING | 1)) {
			if ($this->inXml->open($this->inFile, 'utf-8', LIBXML_NOERROR | LIBXML_NOWARNING | 1)) {
				$out = true;
				$this->prodCnt = 0;
			}
		}
		catch(Exception $ex) {
			$out = false;
		}
        return $out;
    }

    public function close() {
        $this->inXml->close();
    }
    
    public function next() {
        
        while ($this->inXml->read()) { //start reading.
            if ($this->inXml->nodeType == \XMLReader::ELEMENT) { //only opening tags.
                $tag = $this->inXml->name; //make $tag contain the name of the tag
                if( $tag==self::PRODUCT_TAG ) {
                    $this->prodCnt++;
                    return $this->parseProduct();
                }
            }
        }
        return null;
    }

    private function parseProduct() {
        $product = new Product();
        while ($this->inXml->read()) {
            $type = $this->inXml->nodeType;
            $tag = $this->inXml->name;
            if($type == \XMLReader::END_ELEMENT && $tag==self::PRODUCT_TAG) {
                return $product;
            }
            if( $type==\XMLReader::ELEMENT ) {
                $field = $this->tagToProductMapping($tag);
				printf("field=%s\n", $field);
                if($field) {
                    if( $field==self::IMAGE_TAG ) {
                        $product->add($field, $this->inXml->readString());
                    }
                    elseif( $field==self::FEATURES_TAG ) {
                        $fff = (string)$this->inXml->readString();
                        printf("%s\n",$fff);
                        $ff = @unserialize($fff);
                        if(!is_array($ff)) {
                            $ff = [];
                        }
                        $product->add($field, $ff);
                    }
                    else {
                        $product->set($field, $this->inXml->readString());
                    }
                }
            }
        }
        return null;
    }
    
    /**
     * xml tag to product field mapping.
     * An allaksei onoma tag sto xml allazoyme tin aristeri meria (to key)
     * @param string $tag
     * @return string
     */
    private function tagToProductMapping($tag) {
        // tag -> product field
        $map = array(
            'category_id'           =>'category_id',
            'product_code'          =>'product_code',
            'purchase_price'        =>'purchase_price',
            'amount'                =>'amount',
            'part_number'           =>'part_number',
            'ean'                   =>'ean',
            'title'                 =>'title',
            'short_description'      =>'short_description',
            'full_description'       =>'full_description',
            'search_words'          =>'search_words',
            'seo_name'              =>'seo_name',
            'brand'                 =>'brand',
            'image'                 =>'image',
            'shop_availability'     =>'shop_availability',
            'skroutz_availability'  =>'skroutz_availability',
            'status'                =>'status',
            'weight'                =>'weight',
            'free_shipping'         =>'free_shipping',
            'Supplier'              =>'Supplier',
            'features'              =>'features',
        );
        $out = '';
        if( isset($map[$tag]) ) {
            $out = $map[$tag];
        }
        return $out;
    }
}

