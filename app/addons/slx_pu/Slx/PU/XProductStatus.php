<?php
/**
 * @copyright 2018 Kartpay.com.
 * @author Panos <panos@kartpay.com>
 * Date: 20/3/2018
 * Time: 9:11 πμ
 */

namespace Slx\PU;


class XProductStatus {

    public static function resetAll() {
        db_query("update slx_product set pu_result_id=0");
    }

    public static function update($xproductId, $statusId) {
        db_query("update slx_product set pu_result_id=?i where id=?i", $statusId, $xproductId);
    }
}