<?php

namespace Slx\PU;

use Slx\Logger\Logger;
use Tygh\Registry;

/**
 * Description of ProductValidator
 *
 * @author Andreas <an@weblive.gr>
 * @author Panos <panos@salix.gr>
 *
 */
class ProductValidator {

    protected $supplierId;
    protected $minAmount;
    protected $minPrice;
    protected $maxPrice;

    public function __construct($supplierId) {
        $this->supplierId = $supplierId;
		$this->vat = Registry::get('product_merger.vat_rate')/100+1;
        $r = db_get_row("SELECT stock_limit, price_limit, max_price_limit FROM ?:suppliers WHERE supplier_id=?i", $this->supplierId);
        $this->minAmount = $r['stock_limit'];
        $this->minPrice = $r['price_limit'];
        $this->maxPrice = $r['max_price_limit'];
    }

    public function isValid($product) {
        $out = true;
        Logger::getInstance()->log(sprintf("==============================\n"));
        Logger::getInstance()->log(sprintf("product code = %s\n", $product->get("product_code")));
        if ($product->get('category_id') < 1) {
            Logger::getInstance()->log('failed validation: no category');
            XProductStatus::update($product->get('x_product_id'), 101);
            return false;
        }
        if ($product->get('amount') < $this->minAmount) {
            Logger::getInstance()->log(sprintf('failed validation: bellow amount - %s < %s', $product->get('amount'), $this->minAmount));
            XProductStatus::update($product->get('x_product_id'), 102);
            return false;
        }
        if( ($product->get('purchase_price') < $this->minPrice) || ($product->get('purchase_price') > $this->maxPrice) ) {
            Logger::getInstance()->log(
                sprintf(
                'failed validation: %s out of price limits  %s<%s || %s>%s',
                $this->supplierId,
                $product->get('purchase_price'),
                $this->minPrice,
                $product->get('purchase_price'),
                $this->maxPrice
                )
            );
            XProductStatus::update($product->get('x_product_id'), 103);
            return false;
        }

        if ($product->get('product_id') <= 0 && $product->get('status') == 'D') {
            Logger::getInstance()->log('failed validation: no product id and status=D');
            XProductStatus::update($product->get('x_product_id'), 104);
            return false;
        }
        Logger::getInstance()->log(
                sprintf(
                        "after product validation amount=%s price=%s cat=%s\n", $product->get("amount"), $product->get("price"), $product->get("category_id")
        ));
        return $out;
    }

}
