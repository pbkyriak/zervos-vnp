<?php

namespace Slx\PU\Category;

use Tygh\Registry;
use Slx\Logger\Logger;
/**
 * Kanei indexing tis katigories tou cscart me tis katigories twn suppliers
 * Apo ton pinaka category kai apo to pedio supplier_category_name (sindeei tin katigoria tou supplier me tin katigoria sto site)
 * dimiourgei pinaka opou gia kathe zeygari store category kai supplier category iparxei mia eggrafi
 * To exei kanei giati anti na dosei ston admin pinaka na ta pernaei tou edwse ena input field kai ta pernaei me komata
 * TODO Na ginei pinaka to perasma twn supplier categories
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class IndexCategories {

    public static function getInstance() {
        return new IndexCategories();
    }

    public function run() {
        Logger::getInstance()->log("Indexing categories");
        $this->reset();
        $categories = db_get_array("SELECT category_id, supplier_category_name FROM ?:categories");

        foreach ($categories as $category) {
            $this->index($category);
        }
        Logger::getInstance()->log("Indexing categories DONE");
    }

    private function index($category) {
        if (empty($category['supplier_category_name'])) {
            return;
        }
        $names = explode(',', $category['supplier_category_name']);
        foreach ($names as $name) {
            db_query("INSERT INTO ?:categories_supplier_matching (category_id, supplier_category_name) VALUES (?i, ?s)", $category['category_id'], $name);
        }
    }

    private function reset() {
        db_query("TRUNCATE TABLE ?:categories_supplier_matching");
    }

}
