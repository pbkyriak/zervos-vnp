<?php

namespace Slx\PU\Category;

use Tygh\Registry;
use Slx\Logger\Logger;
/**
 * Description of ProductsCategoriesTotalCount
 *
 * @author Ioannis Matziaris <imatzgr@gmail.com>
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ProductsCategoriesTotalCount {

    public function update() {
        $root_categories = db_get_fields("SELECT category_id FROM ?:categories WHERE parent_id=0 AND status='A'");
        if (!empty($root_categories)) {
            $this->updateProductTotalCount($root_categories);
        }
        Logger::getInstance()->log("Products count in active Categories has been updated");
    }

    public function updateProductTotalCount($category_ids) {
        foreach ($category_ids as $category_id) {
            $subcategories = fn_get_categories_tree($category_id);
            if( $subcategories ) {
                $this->updateProductTotalCountPerCategory($category_id, $subcategories);
            }
        }
    }

    function updateProductTotalCountPerCategory($category_id, $subcategories) {
        $product_count = db_get_field("SELECT COUNT(pc.product_id) FROM ?:products_categories AS pc 
		LEFT JOIN ?:products AS p ON p.product_id=pc.product_id WHERE pc.category_id = ?i AND p.status='A'", $category_id);
        if (!empty($subcategories)) {
            foreach ($subcategories as $sub) {
                if ($sub["status"] == "A") {
                    $_subcategories = array();
                    if (isset($sub["subcategories"]) && !empty($sub["subcategories"])) {
                        $_subcategories = $sub["subcategories"];
                    }
                    $_product_count = $this->updateProductTotalCountPerCategory($sub["category_id"], $_subcategories);
                    $product_count+=$_product_count;
                }
            }
        }
        db_query("UPDATE ?:categories SET product_total_count = ?i WHERE category_id = ?i", $product_count, $category_id);
        return($product_count);
    }

}
