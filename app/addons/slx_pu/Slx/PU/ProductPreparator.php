<?php

namespace Slx\PU;

use Slx\PU\Core\AbstractProductPreparator;

/**
 * Prepares product object before update. 
 * Normalize, match codes, replace texts all this has to be done here.
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ProductPreparator extends AbstractProductPreparator {

    protected $defaultSupplierAmount;
    protected $freeShippingCategories;
    protected $supplierShopFeedAvail;
    protected $supplierSkroutzFeedAvail;
	protected $newProductsCategory=0;
	protected $shippmentSurcharges = [];
	protected $additionalWeights = [];
	
    public function __construct($supplierId) {
        parent::__construct($supplierId);
        // add any constructor needed statements
        $this->getSupplierDefaultAmount();
		$this->getCategoriesAdditionalWeights();
    }

    private function getSupplierDefaultAmount() {
        $row = db_get_row("SELECT default_amount,shop_availability,skroutz_availability,new_products_category_id FROM ?:suppliers WHERE supplier_id=?i", $this->supplierId);
        $this->defaultSupplierAmount = $row['default_amount'];
        $this->supplierShopFeedAvail = $row['shop_availability'];
        $this->supplierSkroutzFeedAvail = $row['skroutz_availability'];
		$this->newProductsCategory = $row['new_products_category_id'];
    }

	private function getCategoriesAdditionalWeights() {
		$this->shippmentSurcharges = db_get_hash_single_array("select category_id, shipping_freight_surcharge from ?:categories where shipping_freight_surcharge!=0", ['category_id', 'shipping_freight_surcharge']);
		$this->additionalWeights = db_get_hash_single_array("select category_id, additional_weigth from ?:categories where additional_weigth!=0", ['category_id', 'additional_weigth']);
	}
	
    /**
     * this method is called before product object is persisted.
     * any data normallization or handling has to be done here.
     * Default:
     *  - get productId matching product code
     *  - check if category id exists
     *  - markup price
     * 
     */
    public function prepare(Core\Product $p) {
        $this->matchProduct($p);
        $this->matchCategory($p);   // in abstract class
        //$p->set('weight', round($p->get('weight') / 1000, 2));
        $this->fixWeight($p);       // prostheto varos se kathgoria
		$this->fixShippingFreight($p);
        $this->markupPrice($p);     // in abstract class
        // add any method calls you need here
        $this->setStock($p);
        $this->fixTitle($p);
		$this->setNewProdCategory($p);
    }

    /**
     * Matches product to db using product code.
     * If you need any other data to be retrieved also
     * add fields and default values to $data.
     * This is called from MatchProduct to get fields,
     * so do not remore return statement at the end
     * @param Core\Product|type $p
     * @return array
     */
    protected function getMatchProductFields(Core\Product $p) {
        $data = array(
            'product_id' => -1, // do not change or remove!!
            'pricelist_updater_skip_skroutz' => 'N',
            'skip_updater' => 'N',
        );
        // nothing to change bellow this line
        return $data;
    }
	private function isInStock($am) {
		$out = false;
		if(is_numeric($am) ) {
			$out = intVal($am)>0;
		}
		else {
			$out = ($am=='available');
		}
		return $out;
	}

    // add any more methods needed below
    protected function setStock(Core\Product $p) {
		$am = $p->get('amount');
		if ($this->isInStock($am)) {
			//$p->set('shop_feed_avail_opt', 0);
			$p->set('shop_feed_avail_opt', $this->supplierShopFeedAvail);
			$p->set('skroutz_availability', $this->supplierSkroutzFeedAvail);
			$p->set('amount', is_numeric($am) ? intVal($am) : $this->defaultSupplierAmount);
			//$p->set('amount',$this->defaultSupplierAmount);
		} else {
			$p->set('amount', 0);
			$p->set('shop_feed_avail_opt', 9);
			$p->set('skroutz_availability', 9);
		}
    }

    protected function fixWeight(Core\Product $p) {
		if(isset($this->additionalWeights[$p->get('category_id')])) {
			$p->set('weight', $p->get('weight') + $this->additionalWeights[$p->get('category_id')]);
		}
    }

	protected function fixShippingFreight($p) {
		$p->set('shipping_freight',0);
		if(isset($this->shippmentSurcharges[$p->get('category_id')])) {
			$p->set('shipping_freight',$this->shippmentSurcharges[$p->get('category_id')]);
		}
	}
	
    /**
     * overrides AbstractProductPreparator::matchCategory.
     * Patent expects categoryId to be cscart_categories.category_id
     * This matches with supplier category names using category index table
     *
     * @param Core\Product|type $p
     */
    protected function matchCategory(Core\Product $p) {
        // add supplierID_ prefix to category_id
        $p->set('category_id', sprintf("%s",  $p->get('category_id')));
        $this->matchCategoryBySupplierMatchString($p);
    }

    /**
     * Matches category with supplier category names using category index table
     * @param Core\Product|type $p
     */
    private function matchCategoryBySupplierMatchString(Core\Product $p) {
        $p->set('xcategory_id',$p->get('category_id'));
        $catId = db_get_field("SELECT category_id FROM ?:categories_supplier_matching WHERE supplier_category_name=?s", $p->get('category_id'));
        if ($catId) {
            $p->set('category_id', $catId);
        } else {
            $p->set('category_id', -1);
        }
    }

    private function fixTitle($p) {
        $title = $p->get('title');
        $title = ForeignWordFixer::replaceForeignWords($title);
        $title = preg_replace(array('/\sEU$/i', '/\sDE$/i'), '', $title);
		
        $p->set('title', trim($title));
    }

	
    protected function setNewProdCategory($p) {
        $p->set('new_products_category_id', $this->newProductsCategory);
		if( $p->get('category_id')<1 ) {
			$p->set('category_id', $this->newProductsCategory);
		}
		if( $p->get('category_id')==$this->newProductsCategory) {
			$p->set('status','H');
		}
    }
/*
	protected function replaceForeignWords($text) {

        $trans = array(
            'swartz' => 'black',            'weiss' => 'white',            'gelb' => 'yellow',            'grün' => 'green',
            'Seiten' => '',            ', Grafikkarte' => '',            ', Festplatte' => '',            ', Prozessor' => '',
            'Prozessor' => 'Processor',			'für' => 'for',			'T-Mobile' => '',			', Handy' => '',
			'Kopfhörer' => 'headphone',			', Speicherkarte' => '',			', Blu-ray-Rohlinge' => '',			', Spielsitz' => '',
			', Resttonerbehälter' => '',			', DVD-Rohlinge' => '',			', Gehäuselüfter' => '',			', Arbeitsspeicher' => '',
			', CPU-Kühler' => 'CPU cooler',			', Einbaurahmen' => '',			', Gehäuselüfter' => '',			', Beschriftungsgerät' => '',
			', Netzwerkkamera' => '',			', Laufwerksgehäuse' => '',			', Multifunktionsdrucker' => '',			'Bildtrommeleinheit' => '',
			'-Gehäuse' => '',			'Gehäuse' => '',			', Schalteinsatz' => '',			', Wandhalterung' => '',
			'Adapterkabel' => '',			', Kühlschrank' => '',			'Rucksack' => '',			'Schutzkontakt' => '',
			', Wechselrahmen' => '',			', Bürste' => '',			', PC-Lautsprecher' => '',			', Schutzhülle' => '',
			', Ersatzteil' => '',			', Maus' => ', Mouse',			', Standfuß' => '',			'Standfüße' => '',
			', Halterung' => '',			'-Lampe' => '-Lamp',			'Einbaurahmen' => '',			'Geräte ohne' => '',
			'Stecker' => '',			', Tastatur' => '',			'Mikrofon' => 'microphone',			'Schienen' => '',
			'Netzteil' => '',			', Kühlung' => '',			', Objektiv' => '',			', Laufwerksgehäuse' => '',
			', Fitnesstracker' => '',			', Epiliergerät' => '',			', Terrestrischer Receiver' => '',			', Farblaserdrucker' => '',
			', Tintenstrahldrucker' => '',			', Aufsteckbürste' => '',			', Fernbedienung' => '',			', Netzwerkadapter' => '',
			', Wasserkühlung' => '',			', Überwachungskamera' => '',			'Tastenverschlüsselung' => '',			', papier' => '',
			', LED-Drucker' => '',			', Spielfigur' => '',			'Modul' => '',			'Kabel' => '',
			', Kabel-Receiver' => '',			'Etikettenrolle' => '',			', Schriftband' => '',			'Tür-/Fensterkontakt' => '',
			', Öffnungsmelder' => '',			', Überspannungsschutz' => '',			', Lenkrad' => '',			', Verlängerungskabel' => '',
			', Spannungsregler' => '',			', Laserdrucker' => '',			', Lautsprecher' => '',			', Grafiktablett' => '',
			'Netzwerkkabel' => '',			', Tasche' => '',			'Tasche' => '',			'RDX-Laufwerk' => '',
			', Steckdosenadapter' => '',			', Lautsprecher' => '',			', LED-Fernseher' => '',			', Hochdruckreiniger' => '',
			', Hochdruckreiniger' => '',			', Öffnungsmelder' => '',			', Terrestrischer Receiver' => '',			', Farblaserdrucker' => '',
			', Fotopapier' => '',			', Baustrahler' => '',			', Erweiterungsmodul' => '',
			', Rauchmelder' => '',			'for Wandmontage' => '',			'l-Kabel BNC  auf BNC ' => '',			', RDX-Laufwerk' => '',
			', Spielfigur' => '',			', Befestigung/Montage' => '',			', Rolloleinwand' => '',			', Slotblende' => '',
			', Wasserkühlung' => '',			', Einzugsscanner' => '',			', LED-Fernseher' => '',			', Telefonanlage' => '',
			', Staubfilter' => '',			', Farblaserdrucker' => '',			'Doppelpack' => '',			', Lautsprecher' => '',
			', Öffnungsmelder' => '',			', Geschicklichkeitsspiel' => '',			', Fahrradaufbewahrung' => '',			', Überwachungskamera' => '',
			', Kartenleser' => '',			', Heizungsthermostat' => '',			', Komplett-PC' => '',			'Großformatdrucker' => '',
			', Steuermodul' => '',			', Fotodrucker' => '',			', Reinigungsmittel' => '',			'-Schränke' => '',
			', Abisolier-Zange' => '',			'mit Ethernet Kabel gewinkelt' => '',			'Buchse, Verlängerungskabel' => '',			'Buchse' => '',
			', Verlängerungskabel' => '',			', Bewegungsmelder' => '',			', Steuereinheit' => '',			'Direktanschlusskupferkabel' => '',
			', Drohne' => '',
        );

        $words = explode(' ', $text);
        array_walk($words, function(&$item) use ($trans) {
            $lt = strtolower($item);
            if (isset($trans[$lt])) {
                $item = $trans[$lt];
            }
        });

        return trim(implode(' ', $words));
    }
*/
}
