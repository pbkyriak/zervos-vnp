<?php

namespace Slx\PU\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;
use Tygh\Registry;
use Slx\PU\Worker;
use Slx\PU\Parser\XmlParser;
use Slx\Category;

class ImportCommand  extends Command {

    private $output;
    private $supplier;

    protected function configure() {
        $this
            ->setName('pu:import')
            ->setDescription('PU: import data.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        printf("\nStart Of :%s\n\n",$this->getName());
        Logger::getInstance()->log('Start Of :'.$this->getName());

        $cart_language='el';
        $t1 = microtime();

        $upderDir = Registry::get('product_merger.pu_files_dir');
        if( !file_exists($upderDir) ) {
            fn_mkdir($upderDir,0777);
        }
        $histDir = $upderDir.'history/';
        if( !file_exists($histDir) ) {
            fn_mkdir($histDir,0777);
        }
        $hist = $histDir.date('Y-m-d-H-i-s').'-Merger.xml';
        $fn = $upderDir.'/merger.xml';
        $fn2 = $upderDir.'/Working-Products.xml';
        if( file_exists($fn) ) {
            if( file_exists($fn2) ) {
                Logger::getInstance()->log("maybe updater is running");
                die(100);
            }
            rename($fn, $fn2);
        }else {
            Logger::getInstance()->log("no products file");
            die(101);
        }
        Logger::getInstance()->log(sprintf("Data file %s", $fn));
        Logger::getInstance()->log(sprintf("Working file %s", $fn2));
        $t1 = time();
        $updater = new Worker();
        $parser = new XmlParser($fn2);
        $updater->run($parser);
        rename($fn2, $hist);
        Logger::getInstance()->log(sprintf("moved to history %s", $hist));
        Logger::getInstance()->log(sprintf("exec time: %s", time()-$t1));
        Logger::getInstance()->log('End Of :'.$this->getName());
        printf("\nEnd Of :".$this->getName());
        printf("\n\n");
    }


}