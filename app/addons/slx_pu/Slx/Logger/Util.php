<?php

namespace Slx\Logger;

/**
 * Description of Util
 * Added Jul 2015
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Util {
    /**
     * Clean logs folder of old files
     * Keeps past 10 days
     * 
     * @param int $days         Number of days to keep
     * @param string $histPath
     */
    public static function cleanOldFiles($histPath, $days) {
        if (file_exists($histPath)) {
            foreach (new \DirectoryIterator($histPath) as $fileInfo) {
                if ($fileInfo->isDot()) {
                continue;
                }
                if (time() - $fileInfo->getCTime() >= $days*24*3600 ) { 
                    unlink($fileInfo->getRealPath());
                }
            }
        }
    }
}
