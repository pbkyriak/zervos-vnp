<?php

namespace Slx\Logger;

use Slx\Logger\Util;
use Tygh\Registry;
/**
 * Description of Logger
 * Added Jul 2015
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Logger {

    private static  $instance = null;

    private $handle;
    private $type='file';
    
    private function __construct() {
        $histPath = Registry::get('product_merger.pu_files_dir'). 'logs';
        if( !file_exists($histPath) ) {
            fn_mkdir($histPath,0777);
        }

        $this->type = 'file';
        if( $this->type=='file' ) {
            Util::cleanOldFiles($histPath, 10);
            $fn = sprintf($histPath.'/updaters_executions_%s.log',date("Y_m_d", time()));
            $this->handle = fopen($fn, 'a+');
        }
        $this->log('START '.date("H:i:s m/d/Y", time()).' '.  str_repeat('-', 74));
    }
    
    /**
     * 
     * @return \Slx\Logger\Logger
     */
    public static function getInstance() {
        if (!static::$instance) {
            static::$instance = new Logger();
        }
        return static::$instance;
    }

    public function __destruct() {
        $this->log('END   '.date("H:i:s m/d/Y", time()).' '.  str_repeat('-', 74));
        if($this->handle) {
            fclose($this->handle);
        }
    }
    
    public function log($msg, $addNewLine=true) {
        if( $this->type=='file' && $this->handle ) {
            fwrite($this->handle,date("m/d H:i:s", time()).' : '. $msg.($addNewLine ? "\n" : ''));
            fflush($this->handle);
        }
        elseif( $this->type=='html' ) {
            printf(($addNewLine ? "%s<br />" : '%s'), $msg);
        }
        elseif( $this->type=='screen' ) {
            printf(($addNewLine ? "%s\n" : '%s'), $msg);
        }
    }
    
}
