<?php

namespace Tygh\Shippings\Services;

/**
 * Description of Elta
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Jun 22, 2017
 */
class Elta {
    /**
     * Returns shipping service information
     * @return array information
     */
    public static function getInfo()
    {
        return array(
            'name' => __('carrier_elta'),
            'tracking_url' => 'http://www.elta.gr/en-us/personal/tracktrace.aspx'
        );
    }

}
