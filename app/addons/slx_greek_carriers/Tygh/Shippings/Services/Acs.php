<?php
namespace Tygh\Shippings\Services;

use Tygh\Shippings\IService;
use Tygh\Http;


/**
 * Description of Acs
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Jun 22, 2017
 */
class Acs {
    /**
     * Returns shipping service information
     * @return array information
     */
    public static function getInfo()
    {
        return array(
            'name' => __('carrier_acs'),
            'tracking_url' => 'https://www.acscourier.net/el/track-and-trace?p_p_id=ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-4&p_p_col_pos=1&p_p_col_count=2&_ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet_javax.portlet.action=trackTrace&generalCode=%s'
        );
    }

}
