<?php
namespace Tygh\Shippings\Services;

use Tygh\Shippings\IService;
use Tygh\Http;


/**
 * Description of Acs
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Jun 22, 2017
 */
class Vnp {
    /**
     * Returns shipping service information
     * @return array information
     */
    public static function getInfo()
    {
        return array(
            'name' => __('carrier_vnp'),
            'tracking_url' => ''
        );
    }

}
