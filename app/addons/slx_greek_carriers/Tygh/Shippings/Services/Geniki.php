<?php

namespace Tygh\Shippings\Services;

/**
 * Description of Geniki
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Jun 22, 2017
 */
class Geniki {
    /**
     * Returns shipping service information
     * @return array information
     */
    public static function getInfo()
    {
        return array(
            'name' => __('carrier_geniki'),
            'tracking_url' => 'http://www.taxydromiki.com/track/%s'
        );
    }

}
