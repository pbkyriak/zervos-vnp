<?php

namespace Tygh\Shippings\Services;

/**
 * Description of Speedex
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Jun 22, 2017
 */
class Speedex {
    /**
     * Returns shipping service information
     * @return array information
     */
    public static function getInfo()
    {
        return array(
            'name' => __('carrier_speedex'),
            'tracking_url' => 'http://www.speedex.gr/pelates/isapohi.asp?voucher_code=%s'
        );
    }

}
