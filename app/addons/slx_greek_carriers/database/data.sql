REPLACE INTO cscart_shipping_services (service_id, STATUS, module,CODE,sp_file) VALUES (500, 'A', 'acs', '1', '');
REPLACE INTO cscart_shipping_service_descriptions (service_id, description, lang_code) VALUES (500,'GR - ACS courier', 'el');

REPLACE INTO cscart_shipping_services (service_id, STATUS, module,CODE,sp_file) VALUES (501, 'A', 'geniki', '1', '');
REPLACE INTO cscart_shipping_service_descriptions (service_id, description, lang_code) VALUES (501,'GR - Γενική Ταχυδρομική', 'el');

REPLACE INTO cscart_shipping_services (service_id, STATUS, module,CODE,sp_file) VALUES (502, 'A', 'speedex', '1', '');
REPLACE INTO cscart_shipping_service_descriptions (service_id, description, lang_code) VALUES (502,'GR - Speedex courier', 'el');

REPLACE INTO cscart_shipping_services (service_id, STATUS, module,CODE,sp_file) VALUES (503, 'A', 'elta', '1', '');
REPLACE INTO cscart_shipping_service_descriptions (service_id, description, lang_code) VALUES (503,'GR - ELTA courier', 'el');
REPLACE INTO cscart_shipping_service_descriptions (service_id, description, lang_code) VALUES (503,'GR - Hellenic post', 'en');

REPLACE INTO cscart_shipping_services (service_id, STATUS, module,CODE,sp_file) VALUES (504, 'A', 'vnp', '1', '');
REPLACE INTO cscart_shipping_service_descriptions (service_id, description, lang_code) VALUES (504,'GR - VNP courier', 'el');

REPLACE INTO cscart_shipping_services (service_id, STATUS, module,CODE,sp_file) VALUES (505, 'A', 'easy', '1', '');
REPLACE INTO cscart_shipping_service_descriptions (service_id, description, lang_code) VALUES (505,'GR - EASY mail', 'el');