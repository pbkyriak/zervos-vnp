<?php

// if allow == true all modes in controller are allowed, no changes needed.
if (!(isset($schema['shopfeed']['allow']) && $schema['shopfeed']['allow'] === true)) {
    $schema['shopfeed']['allow']['run'] = true;
}

return $schema;