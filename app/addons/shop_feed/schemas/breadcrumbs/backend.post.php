<?php
/**
 * @author Panos Kyriakakis <panos at salix.gr>
 */

$schema['shopfeed.updateopts'] = array (
    array (
        'title' => 'shopfeed_feeds',
        'link' => 'shopfeed.manage'
    )
);

$schema['shopfeed.update'] = array (
    array (
        'title' => 'shopfeed_feeds',
        'link' => 'shopfeed.manage'
    )
);

$schema['shopfeed.manage'] = array (
    array (
        'title' => 'shopfeed_log',
        'link' => 'shopfeed.log'
    )
);

return $schema;
