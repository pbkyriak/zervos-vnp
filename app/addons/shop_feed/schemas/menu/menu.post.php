<?php

$schema['central']['products']['items']['shopfeed'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'shopfeed.log',
    'position' => 500,
);

return $schema;
