<?php
/*
 *************************************************************** 
               _     _ _                               _ _      
 __      _____| |__ | (_)_   _____     _     ___  __ _| (_)_  __
 \ \ /\ / / _ \ '_ \| | \ \ / / _ \  _| |_  / __|/ _` | | \ \/ /
  \ V  V /  __/ |_) | | |\ V /  __/ |_   _| \__ \ (_| | | |>  < 
   \_/\_/ \___|_.__/|_|_| \_/ \___|   |_|   |___/\__,_|_|_/_/\_\
                                                                
 ***************************************************************
 * shopFeed is a product of mind and effords of Panos Kyriakakis.
 * (c) 2010-2015 Panos Kyriakakis
 * 
 * I thank all my partners that sold this for me and transfered their clients
 * comments and suggestions to make it better.
 * 
 * For any inquires please contact weblive.gr 210 2519282.
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 */

use Tygh\Registry;
use Tygh\ShopFeed\Admin\ShopFeedFeedAdmin;
use Tygh\ShopFeed\DataExporters\ShopFeedExporterFactory;
use Tygh\Languages\Languages;

/* HOOKS */

/**
 * Hook for get_product_fields, adds extra fields in product fetching
 * 
 */
function fn_shop_feed_get_product_fields(&$fields) {
    $fields[] = 
        array(
            'name' => '[data][manufacturer]',
            'text' => __('manufacturer')
        );
    $fields[] = 
        array(
            'name' => '[data][ean]',
            'text' => __('ean')
        );
    $fields[] = 
        array(
            'name' => '[data][mpn_code]',
            'text' => __('mpn_code')
        );
}

/**
 * Hook for get_products, to add field exclude_from_shopfeed in fields and sortings
 * so they can be shown in products manage page in backend
 * 
 */
function fn_shop_feed_get_products($params, &$fields, &$sortings, &$condition, &$join, &$sorting, &$group_by, $lang_code) {

	$fields[] = 'products.exclude_from_shopfeed';
	$sortings['exclude_from_shopfeed'] = 'products.exclude_from_shopfeed';
	if(!empty($params['availability'])) {
		$condition .= db_quote(' AND products.shop_feed_avail_opt = ?i', $params['availability']);
	}
}

/**
 * Hook for update_category_pre, serialize field data exclude_from_shopfeed (array) 
 * to string so it can be stored to database
 * 
 */
function fn_shop_feed_update_category_pre( &$category_data, $category_id, $lang_code) {
    
    if( isset($category_data['fake_shopfeed']) ) {   // dirty trick to know if is editing category
        if (isset($category_data['exclude_from_shopfeed'])) {
            $category_data['exclude_from_shopfeed'] = serialize($category_data['exclude_from_shopfeed']);
        }
        else {
            $category_data['exclude_from_shopfeed'] = serialize(array());
        }
    }
}

/**
 * Hook for get_category_data_post, unserialize exclude_from_shopfeed field
 * 
 */
function fn_shop_feed_get_category_data_post( $category_id, $field_list, $get_main_pair, $skip_company_condition, $lang_code, &$category_data) {
    if (!empty($category_data['exclude_from_shopfeed'])) {
        $category_data['exclude_from_shopfeed'] = unserialize($category_data['exclude_from_shopfeed']);
    } else {
        $category_data['exclude_from_shopfeed'] = array();
    }
}

/**
 * Hook for update_product_pre, serialize field data exclude_from_shopfeed (array) 
 * to string so it can be stored to database
 * 
 */
function fn_shop_feed_update_product_pre(&$product_data, $product_id, $lang_code, $can_update) {
	if( isset($product_data['fake_shopfeed'])) {
	    if (isset($product_data['exclude_from_shopfeed'])) {
	        $product_data['exclude_from_shopfeed'] = serialize($product_data['exclude_from_shopfeed']);
	    } else {
	        $product_data['exclude_from_shopfeed'] = array();
	    }  
	}  
}

/**
 * Hook for get_product_data_post, unserialize exclude_from_shopfeed field
 * 
 */
function fn_shop_feed_get_product_data_post(&$product_data, $auth, $preview, $lang_code) {
    if (!empty($product_data['exclude_from_shopfeed'])) {
        $product_data['exclude_from_shopfeed'] = unserialize($product_data['exclude_from_shopfeed']);
    } else {
        $product_data['exclude_from_shopfeed'] = array();
    }    
}

/* END OF HOOKS */

/**
 * Helper for frontend template
 * 
 * @param int $productId
 * @return string
 */
function shop_feed_get_product_avail_opt($productId) {
	$availOpt = db_get_field("SELECT shop_feed_avail_opt FROM ?:products WHERE product_id=?i", $productId);
	if( $availOpt==0 ) {
		$categoryId = db_get_field("SELECT category_id FROM ?:products_categories WHERE product_id=?i and link_type='M'", $productId);
		$idPath = db_get_field("SELECT id_path FROM ?:categories WHERE category_id=?i", $categoryId);
		if( $idPath ) {
			$rows = db_get_array("select shop_feed_avail_opt from cscart_categories where category_id in (".str_replace('/',',',$idPath).") order by id_path");
			$rows = array_reverse($rows);
			foreach($rows as $row) {
				if( $row['shop_feed_avail_opt']!=0 ) {
					$availOpt=$row['shop_feed_avail_opt'];
					break;
				}
			}
		}
		if( $availOpt==0 ) {
			$availOpt = Registry::get('addons.shop_feed.def_avail');
		}
	}
	return $availOpt;
}

/**
 * Returns info of enabled exporters
 * 
 * @return array
 */
function shop_feed_get_exporters_info() {
    $_exporters = ShopFeedExporterFactory::getExportersInfo();
    $out = array();
    foreach($_exporters as $exporter) {
            $out[$exporter['id']] = $exporter;
    }
    return $out;
}

function shop_feed_get_feeds_info() {
    return ShopFeedFeedAdmin::getFeedsInfo();
}

function shop_feed_get_features_info() {
    return db_get_array("select pf.feature_id, pfd.description from cscart_product_features pf
            left join cscart_product_features_descriptions as pfd on (pf.feature_id=pfd.feature_id and pfd.lang_code='el')
            where pf.feature_code!='' and pf.status='A'");
}

function shop_feed_get_suppliers_info() {
	if( Registry::get('addons.suppliers') ) {
    	return db_get_array("select supplier_id, name from cscart_suppliers where status='A'");
	}
	else {
		return null;
	}
}

/**
 * addon.xml settings variants for feedlanguage
 * 
 * @return array
 */
function fn_settings_variants_addons_shop_feed_feedlanguage() {
    $langs = Languages::getAvailable(AREA, false);
    $out = array();
    foreach($langs as $lang) {
        $out[$lang['lang_code']] = $lang['name'];
    }
    return $out;
}

/**
 * Helper used from mpnfromfeature and manufacturerfromfeature settings
 * @return array
 */
function fn_shop_feed_get_features_for_settings() {
    $out = array('0'=>__('none'));

    $fs = db_get_hash_single_array("select pf.feature_id, pfd.description from cscart_product_features pf
            left join cscart_product_features_descriptions as pfd on (pf.feature_id=pfd.feature_id and pfd.lang_code='el')
            where pf.feature_code='' and pf.status='A'", array('feature_id', 'description'));
    foreach($fs as $k => $v) {
        $out[$k] = $v;
    }
    return $out;    
}
/**
 * addon.xml settings variants for mpnfromfeature setting
 * @return array
 */
function fn_settings_variants_addons_shop_feed_mpnfromfeature() {
    return fn_shop_feed_get_features_for_settings();
}
/**
 * addon.xml settings variants for manufacturerfromfeature setting
 * @return array
 */
function fn_settings_variants_addons_shop_feed_manufacturerfromfeature() {
    return fn_shop_feed_get_features_for_settings();
}

/**
 * Helper for template
 * 
 * @param int $availId
 * @return string
 */
function fn_shop_feed_get_avail_color($availId) {
    return Registry::get('addons.shop_feed.avail_color_'.$availId);
}