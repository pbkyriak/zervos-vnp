<?php
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if( $mode=='m_shopfeed_exclude' ) {
		if (isset($_REQUEST['product_ids'])) {
            $exporters = shop_feed_get_exporters_info();
            $feeds = array();
            foreach($exporters as $exporter) {
                $feeds[$exporter['id']]=$exporter['id'];
            }
            $feeds = serialize($feeds);
			foreach ($_REQUEST['product_ids'] as $v) {
				db_query("UPDATE cscart_products SET exclude_from_shopfeed=?s WHERE product_id=?i",$feeds, $v);
			}
		}
		unset($_SESSION['product_ids']);
		fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('text_products_have_been_shopfeed_excluded'));

	}
	if( $mode=='m_shopfeed_include' ) {
		if (isset($_REQUEST['product_ids'])) {
			foreach ($_REQUEST['product_ids'] as $v) {
				db_query("UPDATE cscart_products SET exclude_from_shopfeed='' WHERE product_id=?i", $v);
			}
		}
		unset($_SESSION['product_ids']);
		fn_set_notification('N', fn_get_lang_var('notice'), fn_get_lang_var('text_products_have_been_shopfeed_included'));

	}
  return;
}

if ($mode == 'update') {
  Registry::set('navigation.tabs.shopfeed', array(
      'title' => fn_get_lang_var('shop_feed'),
      'js' => true
  ));
  Registry::get('view')->assign('exporters', shop_feed_get_feeds_info());

}


