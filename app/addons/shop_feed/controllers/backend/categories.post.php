<?php
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if ($mode == 'update') {
  }
  return;
}

if ($mode == 'update') {
  Registry::set('navigation.tabs.shopfeed', array(
      'title' => fn_get_lang_var('shop_feed'),
      'js' => true
  ));
  Registry::get('view')->assign('exporters', shop_feed_get_feeds_info());
}

