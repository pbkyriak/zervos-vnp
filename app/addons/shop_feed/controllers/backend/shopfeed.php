<?php

/**
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 26 Ιουλ 2014
 */


use Tygh\Registry;
use Tygh\ShopFeed\Admin\ShopFeedMassOptChanger;
use Tygh\ShopFeed\Admin\ShopFeedFeedAdmin;
use Tygh\ShopFeed\ShopFeedRunner;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if( $mode=='update' ) {
        $sst = new ShopFeedFeedAdmin();
        $feedId = $sst->updateFeed($_REQUEST['feed_id'], $_REQUEST['feed_data']);
        
        $suffix = '.manage';
		if (empty($feedId)) {
			$suffix = ".manage";
		} else {
			$suffix = ".update?feed_id=$feedId" . (!empty($_REQUEST['feed_data']['block_id']) ? "&selected_block_id=" . $_REQUEST['feed_data']['block_id'] : "");
		}
    }

    elseif( $mode=='mdelete' ) {
        $tableIds = $_REQUEST['table_ids'];
        $sst = new ShopFeedFeedAdmin();
        $sst->deleteFeeds($tableIds);
        $suffix = '.manage';        
    }
    
    elseif( $mode=='deletelog' ) {
        $tableIds = $_REQUEST['table_ids'];
        foreach($tableIds as $tableId) {
            if(!empty($tableId)) {
                db_query("DELETE FROM ?:shopFeed_log WHERE id=?i", $tableId);
            }
        }
        $suffix = '.log';
    }
    elseif( $mode=='updateopts') {
        $in = $_REQUEST['option_data'];
        $ch = new ShopFeedMassOptChanger();
        $ch->change($in['option_name'], $in['is_shopfeed_option'],DESCR_SL);
        fn_set_notification('N', 'Option Updates', $ch->getMessage());
        $suffix = '.updateopts';
    }
    return array(CONTROLLER_STATUS_OK, "shopfeed$suffix");
}

if( $mode=='log' ) {
	$params = $_REQUEST;

	$params['paginate'] = true;
    if (!empty($params['paginate'])) {
        $params['page'] = empty($params['page']) ? 1 : $params['page'];
        $params['total_items'] = db_get_field("SELECT COUNT(?:shopFeed_log.id) FROM ?:shopFeed_log");
        $params['items_per_page'] = isset($params['items_per_page']) ? $params['items_per_page'] : Registry::get('settings.Appearance.admin_elements_per_page');
        $limit = db_paginate(
            $params['page'], 
            $params['items_per_page']
            );
    }

    $rows = db_get_array("SELECT * FROM ?:shopFeed_log ORDER BY timestamp DESC ?p", $limit);
    Registry::get('view')->assign('search', $params);
    Registry::get('view')->assign('rows', $rows);
   //printf("Use promotions: %s \n", Registry::get('addons.shop_feed.apply_promotions'));
}

elseif( $mode=='manage' ) {
	$params = $_REQUEST;
	$params['paginate'] = true;
    if (!empty($params['paginate'])) {
        $params['page'] = empty($params['page']) ? 1 : $params['page'];
        $params['total_items'] = db_get_field("SELECT COUNT(?:shopFeed_feeds.id) FROM ?:shopFeed_feeds");
        $params['items_per_page'] = isset($params['items_per_page']) ? $params['items_per_page'] : Registry::get('settings.Appearance.admin_elements_per_page');
        $limit = db_paginate(
            $params['page'], 
            $params['items_per_page']
            );
    }

    $rows = db_get_array("SELECT * FROM ?:shopFeed_feeds ORDER BY id ?p", $limit);
    Registry::get('view')->assign('search', $params);
    Registry::get('view')->assign('rows', $rows);    
}

elseif( $mode=='add' ) {
    Registry::get('view')->assign('exporters', shop_feed_get_exporters_info());
    Registry::get('view')->assign('features', shop_feed_get_features_info());
    Registry::get('view')->assign('suppliers', shop_feed_get_suppliers_info());
}

elseif( $mode=='update' ) {
    $sst = new ShopFeedFeedAdmin();
    $data = $sst->getFeedData($_REQUEST['feed_id']);
    Registry::get('view')->assign('exporters', shop_feed_get_exporters_info());
    Registry::get('view')->assign('features', shop_feed_get_features_info());
    Registry::get('view')->assign('suppliers', shop_feed_get_suppliers_info());
    Registry::get('view')->assign('feed_data', $data);
}

elseif( $mode=='delete' ) {
    $sst = new ShopFeedFeedAdmin();
    $sst->deleteFeed($_REQUEST['feed_id']);
    return array(CONTROLLER_STATUS_REDIRECT, "shopfeed.manage");
}
elseif( $mode=='updateopts') {
    
} 
elseif( $mode=='run' ) {
    define('SHOPFEED_VERSION', '15.04.17');
    @ini_set('memory_limit', '256M');
    error_reporting(E_ALL);
    DEFINE('AREA_NAME', 'customer');

    // cscart version check
    if (defined('PRODUCT_TYPE'))
        $type = PRODUCT_TYPE;
    else
        $type = 'PROFESSIONAL';

    list($major, $minor, $build) = explode('.', PRODUCT_VERSION);
    $version = $major + $minor / 10 + $build / 1000;
    if ($version < 4.0) {
        die("Not compatible with versions prior to 4.0\n");
    }

    $cron_password = Registry::get('addons.shop_feed.cron_password');

    if ((!isset($_REQUEST['cron_password']) || $cron_password != $_REQUEST['cron_password']) && (!empty($cron_password))) {
        die(__('access_denied'));
    }
    
    $runner = new ShopFeedRunner($version, $type);
    $runner->run();
   
}