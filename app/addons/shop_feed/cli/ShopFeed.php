<?php
/**
 * @author Panos Kyriakakis <panos at salix.gr>
 */
use Tygh\ShopFeed\ShopFeedRunner;

define('SHOPFEED_VERSION', '15.04.17');
@ini_set('memory_limit', '512M');
error_reporting(E_ALL);

require_once(dirname(__FILE__) . '/ShopFeedCSboot4.php');
printf("bootstraped\n");

//$reflector = new ReflectionClass('Tygh');
//echo $reflector->getFileName();

// cscart version check
if (defined('PRODUCT_TYPE'))
    $type = PRODUCT_TYPE;
else
    $type = 'PROFESSIONAL';

list($major, $minor, $build) = explode('.', PRODUCT_VERSION);
$version = $major + $minor / 10 + $build / 1000;
if ($version < 4.0) {
    die("Not compatible with versions prior to 4.0\n");
}

$runner = new ShopFeedRunner($version, $type);
printf("now will run\n");
$runner->run();
