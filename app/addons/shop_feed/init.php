<?php

/**
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since Aug 4, 2014
 */

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
    'get_product_fields',
    'get_products',
    'update_category_pre',
    'get_category_data_post',
    'update_product_pre',
    'get_product_data_post'
    );