<?php
/**
 * Description of ShopFeedRunner
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
namespace Tygh\ShopFeed;

use Tygh\Registry;
use Tygh\ShopFeed\Admin\ShopFeedFeedAdmin;
use Tygh\ShopFeed\DataCollectors\ShopFeedDataCollectorFactory;
use Tygh\ShopFeed\DataExporters\ShopFeedExporterFactory;

class ShopFeedRunner {
    
    private $coreVersion, $coreType;
    
    public function __construct($coreVersion, $coreType) {
        $this->coreVersion = $coreVersion;
        $this->coreType = $coreType;
    }
    
    public function run() {
        $langCode = Registry::get('addons.shop_feed.feedlanguage');
        // get datacollector
        $dataCollector = ShopFeedDataCollectorFactory::getDataCollector($this->coreVersion, $this->coreType);

        printf("Start: %s\n", date('l dS \o\f F Y h:i:s A', strtotime('now')));
        $time1 = time();
        try {
            $userGroupId = 0;
			/* ama exeis epityxia sto collection, meta gia tis dokimes koveis to collection. */
            $dataCollector->setup(
                $langCode, 
                $userGroupId
            );
            $dataCollector->prepare();
			/* mexri edo */
            $log = "";
            $feedInfos = ShopFeedFeedAdmin::getFeedsInfo();
            foreach($feedInfos as $feedInfo) {
                $feed = ShopFeedExporterFactory::getExporter($feedInfo);
				printf(" %s(%s) \n", $feedInfo['id'], $feedInfo['exporter']);
                $feed->export();
                $log .= sprintf(" %s(%s) ", $feedInfo['id'], $feedInfo['exporter']);
            }
            $log = sprintf('Feeds: %s.',$log);
			
        } catch (Exception $ex) {
            echo $ex->getMessage();
            $log = sprintf("ERROR: %s",$ex->getMessage());
        }
        $log .= sprintf(" Duration: %s sec.", time()-$time1);
        $dataCollector->log($log);
        printf("End: %s\n", date('l dS \o\f F Y h:i:s A', strtotime('now')));
    }
}
