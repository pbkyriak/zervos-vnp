<?php
namespace Tygh\ShopFeed\DataCollectors;
/**
 * Collects product features
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Νοε 2014
 */
use Tygh\Registry;

class ShopFeedProdFeatures
{
    private $langCode;
    
    public function __construct($langCode)
    {
        $this->langCode = $langCode;
    }

    public function get($productId) {
        $features = $this->getFeatures($productId);
        $exFeatures = $this->getExtraFeatures($productId);
        if( $exFeatures ) {
            $features = array_merge($features, $exFeatures);
        }
        return $features;
    }
    
    private function getFeatures($productId) {
        $query = "
			select pf.feature_code k0 ,pfd.description as k, 
			if(pf.feature_type='T', 
			  pfv.value, 
			  if(pf.feature_type in ('S','E'),
			    pfvd.variant,
			  	if( pf.feature_type='M',
			        group_concat(pfvd.variant),
			    	concat(pfd.prefix,' ',pfv.value_int,' ',pfd.suffix)
			    )
			  )
			) as v
			from cscart_product_features_values as pfv
			left join cscart_product_features as pf on (pf.feature_id=pfv.feature_id)
			left join cscart_product_feature_variant_descriptions as pfvd on (pfvd.variant_id=pfv.variant_id and pfvd.lang_code=pfv.lang_code)
			left join cscart_product_features_descriptions as pfd on (pfd.feature_id=pf.feature_id and pfd.lang_code=pfv.lang_code)
			where pfv.product_id=?i and pf.status='A' and pfv.lang_code=?s and pf.feature_code!=''
			group by k0, k
			";
        $features = db_get_hash_single_array($query, array('k0', 'v'), $productId, $this->langCode);
        return $features;        
    }
    
    private function getExtraFeatures($productId) {
        $features = array();
        $fIds = array();
        if( (int)Registry::get('addons.shop_feed.manufacturerfromfeature')!=0) {
            $fIds[] = Registry::get('addons.shop_feed.manufacturerfromfeature');
        }
        if( (int)Registry::get('addons.shop_feed.mpnfromfeature')!=0) {
            $fIds[] = Registry::get('addons.shop_feed.mpnfromfeature');
        }
        if( count($fIds) ) {
            $query = "
                            select concat('sf_',pf.feature_id) k0 ,pfd.description as k, 
                            if(pf.feature_type='T', 
                              pfv.value, 
                              if(pf.feature_type in ('S','E'),
                                pfvd.variant,
                                    if( pf.feature_type='M',
                                    group_concat(pfvd.variant),
                                    concat(pfd.prefix,' ',pfv.value_int,' ',pfd.suffix)
                                )
                              )
                            ) as v
                            from cscart_product_features_values as pfv
                            left join cscart_product_features as pf on (pf.feature_id=pfv.feature_id)
                            left join cscart_product_feature_variant_descriptions as pfvd on (pfvd.variant_id=pfv.variant_id and pfvd.lang_code=pfv.lang_code)
                            left join cscart_product_features_descriptions as pfd on (pfd.feature_id=pf.feature_id and pfd.lang_code=pfv.lang_code)
                            where pfv.product_id=?i and pf.status='A' and pfv.lang_code=?s and pf.feature_id in (?a)
                            group by k0, k
                            ";
            $features = db_get_hash_single_array($query, array('k0', 'v'), $productId, $this->langCode, $fIds);
        }
        return $features;        
    }
}
