<?php
namespace Tygh\ShopFeed\DataCollectors;
/**
 * Gets product price after promotions
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Νοε 2014
 */
class ShopFeedProdPromoPrice
{
    public function getPriceAfterPromotions($productId, $categoryId, $price)
    {
        static $promotions = array();
        $applied_promotions = array();
        $zone = 'catalog';
        $auth = null;
        $cart_products = null;
        $categories = $this->getProductCategories($productId);
        $data = array('product_id' => $productId, 'category_ids' => $categories, 'price' => $price, 'base_price' => $price);

        if (!isset($promotions[$zone])) {
            $params = array(
                'active' => true,
                'expand' => true,
                'zone' => $zone,
                'sort_by' => 'priority',
                'sort_order' => 'asc'
            );
            list($promotions[$zone]) = fn_get_promotions($params);
        }
        if (empty($promotions[$zone])) {
            return $price;
        }

        foreach ($promotions[$zone] as $promotion) {
            // Rule is valid and can be applied
            if (fn_promotion_check($promotion['promotion_id'], $promotion['conditions'], $data, $auth, $cart_products)) {
                if (fn_promotion_apply_bonuses($promotion, $data, $auth, $cart_products)) {
                    $applied_promotions[$promotion['promotion_id']] = $promotion;
                    // Stop processing further rules, if needed
                    if ($promotion['stop'] == 'Y') {
                        break;
                    }
                }
            }
        }
        if (count($applied_promotions)) {
            $price = $data['price'];
        }
        return $price;
    }

    private function getProductCategories($productId)
    {
        $ids = db_get_fields("SELECT category_id FROM cscart_products_categories WHERE product_id=?i",
            $productId);
        return $ids;
    }

}
