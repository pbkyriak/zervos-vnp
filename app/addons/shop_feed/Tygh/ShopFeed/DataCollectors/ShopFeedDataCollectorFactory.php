<?php
namespace Tygh\ShopFeed\DataCollectors;

use Tygh\ShopFeed\DataCollectors\Versions\ShopFeedDataCollector41;

/**
 * Description of ShopFeedDataCollectorFactory
 * Created on 31-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedDataCollectorFactory {

  public static function getDataCollector($version, $type='PROFESSIONAL') {
    if ($version >= 4 && $type=='PROFESSIONAL')
      return new ShopFeedDataCollector41();
    else
      throw new Exception('Not compatible with versions prior to 4.0.0');
  }

}

