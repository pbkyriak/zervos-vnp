<?php
namespace Tygh\ShopFeed\DataCollectors;
use Tygh\Registry;
/**
 * Description of ShopFeedProdImages
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Νοε 2014
 */
class ShopFeedProdImages
{
    
    private $langCode;
    
    public function __construct($langCode)
    {
        $this->langCode = $langCode;
    }
    
    public function getImageLink($productId)
    {
    	$out = '';
        $skin_name = Registry::get('settings.skin_name_' . AREA_NAME);
        $dataType = 'detailed';

        $image_pair = fn_get_image_pairs($productId, 'product', 'M');
        $ifn = $this->findValidImagePath($image_pair, $dataType, $this->langCode);
        if ($ifn) {
        	$out = $this->checkUrl($ifn);
        } else {
            $out = $this->checkUrl(Registry::get('config.no_image_path'));
        }
        return $out;
    }

	private function checkUrl($ifn) {
    	$out = $ifn;
        if(strpos($ifn, 'http://')===false && strpos($ifn, 'https://')===false) {
            $out = 'http://' . Registry::get('config.http_host') . $ifn;
        }
		return $out;
	}

    protected function findValidImagePath($image_pair, $object_type, $lang_code)
    {

        if (isset($image_pair[$object_type]['image_path']) && !isset($image_pair[$object_type]['absolute_path'])) {
            $image_pair[$object_type]['absolute_path'] = DIR_ROOT . str_replace(Registry::get('config.images_path'),
                    '/images/', $image_pair[$object_type]['image_path']);
        }
        if (isset($image_pair[$object_type]['absolute_path']) && is_file($image_pair[$object_type]['absolute_path'])) {
            if (isset($image_pair[$object_type]['is_flash']) && $image_pair[$object_type]['is_flash'])
                return false;
            return $image_pair[$object_type]['image_path'];
        }

        // Try to get the product's image.
        if (!empty($image_pair['image_id'])) {
            $image = fn_get_image($image_pair['image_id'], 'product', 0,
                $lang_code);
            if (!isset($image['absolute_path']))
                $image['absolute_path'] = DIR_ROOT . str_replace(Registry::get('config.images_path'),
                        '/images/', $image['image_path']);
            if (isset($image['absolute_path']) && is_file($image['absolute_path'])) {
                if (isset($image['is_flash']) && $image['is_flash'])
                    return false;
                return $image['image_path'];
            }
        }

        // If everything above failed, try to generate the thumbnail.
        if (!empty($image_pair['detailed_id'])) {
            $image = fn_get_image($image_pair['detailed_id'], 'detailed', 0,
                $lang_code);

            if (isset($image['absolute_path']) && is_file($image['absolute_path'])) {
                if (isset($image['is_flash']) && $image['is_flash']) {
                    return false;
                }
                $image = fn_generate_thumbnail($image['image_path'],
                    Registry::get('settings.Thumbnails.product_details_thumbnail_width'),
                    Registry::get('settings.Thumbnails.product_details_thumbnail_height'),
                    false);
                if (!empty($image)) {
                    return $image;
                }
            }
        }
        return false;
    }
    
}
