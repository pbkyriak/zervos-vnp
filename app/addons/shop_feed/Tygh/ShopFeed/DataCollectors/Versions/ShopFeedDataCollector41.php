<?php

namespace Tygh\ShopFeed\DataCollectors\Versions;

use Tygh\ShopFeed\DataCollectors\AbstractShopFeedDataCollector;
use Tygh\Registry;

class ShopFeedDataCollector41 extends AbstractShopFeedDataCollector {

  protected function getCategoryProductsQuery() {
    $out = "
      select p.product_id, pc.category_id, pd.product, pd.full_description, p.product_code,p.ean as ean,
        if(p.tracking='B', 
            p.amount, 
            (select sum(amount) from ?:product_options_inventory poi where poi.product_id=p.product_id)
        ) as amount,
        p.avail_since, p.usergroup_ids, pp.price, p.skroutz_availability shop_feed_avail_opt, p.product_codeB as mpn_code, p.weight, 
        p.manufacturer, p.ean, p.exclude_from_shopfeed, p.feed_min_qty, p.timestamp, p.updated_timestamp, if(isnull(sc.cost), -1, sc.cost) as scost";
    if( Registry::get('addons.suppliers') ) {
    	$out .= ", sl.supplier_id";
    }
    else {
   		$out .= ", 0 as supplier_id";
    }
    $out .= "
      from ?:products as p
      left join ?:products_categories as pc 
        on (p.product_id=pc.product_id and pc.link_type='M')
      left join ?:product_descriptions as pd
        on (p.product_id=pd.product_id and pd.lang_code='%s')
      left join ?:product_prices as pp on (pp.product_id=p.product_id AND lower_limit=1 AND usergroup_id=%s)
	  left join slx_shopfeed_shipping_costs sc on (p.product_id=sc.product_id)
	  ";
	  if( Registry::get('addons.suppliers') ) {
		  $out .= " left join ?:supplier_links sl on p.product_id=sl.object_id and sl.object_type='P' ";
	  }
	 $out .= "
      where p.status='A' and pc.category_id=%s      
      ";    
	  //printf("%s\n",$out);
      return $out;
  }
  
}