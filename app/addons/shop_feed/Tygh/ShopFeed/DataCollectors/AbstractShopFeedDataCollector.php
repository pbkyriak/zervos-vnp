<?php
/**
 * @author Panos Kyriakakis <panos at salix.gr>
 */
namespace Tygh\ShopFeed\DataCollectors;

use Tygh\ShopFeed\DataCollectors\ShopFeedCategory;
use Tygh\ShopFeed\DataCollectors\ShopFeedProdFeatures;
use Tygh\ShopFeed\DataCollectors\ShopFeedProdOptions;
use Tygh\ShopFeed\DataCollectors\ShopFeedProdImages;
use Tygh\ShopFeed\DataCollectors\ShopFeedProdPromoPrice;
use Tygh\Registry;

abstract class AbstractShopFeedDataCollector
{

    protected $langCode;
    protected $userGroup;
    protected $categories;
    protected $productCount;
    
    public function __construct()
    {
        
    }

    public function setup($langCode, $userGroup)
    {
        $this->langCode = $langCode;
        $this->userGroup = $userGroup;
        $this->categories = array();
        $this->productCount=0;
    }

    public function prepare()
    {
        $this->createDBTable();
        db_query("truncate table slx_shopfeed_shipping_costs");
        $this->fetchCategories();
        $this->updateProducts();
    }

    public function cleanUp()
    {
        db_query("drop table if exists ?:shopFeed_data");
    }

    protected function createDBTable()
    {
        db_query("drop table if exists ?:shopFeed_data");
        db_query("
            CREATE TABLE ?:shopFeed_data (
            `id`  int NOT NULL AUTO_INCREMENT ,
            `uniqueId`  varchar(255) NOT NULL ,
            `name`  mediumtext NOT NULL ,
            `description`  mediumtext NOT NULL ,
            `product_code` varchar(255) NOT NULL,
            `mpn` varchar(255) NOT NULL,
            `manufacturer` varchar(50) NOT NULL,
            `ean` varchar(255),
            `link`  mediumtext NOT NULL ,
            `image`  mediumtext NOT NULL ,
            `category_id` int not null default 0,
            `category`  mediumtext NOT NULL ,
            `price`  decimal(12,2) NOT NULL ,
            `base_price`  decimal(12,2) NOT NULL ,
            `instock`  char(1) NOT NULL ,
            `availability`  varchar(20) NOT NULL ,
            `amount` int not null default 0,
            `sizes` varchar(255),
            exclude_type text,
            features text,
            insert_timestamp int not null default 0,
            updated_timestamp int not null default 0,
			google_category_id int default 0,
			supplier_id int(11) not null default 0,
			`weight` decimal(12,2) NOT NULL DEFAULT '0.00',
			shipping_cost decimal(12,2) default 0,
            PRIMARY KEY (`id`) 
            ) DEFAULT CHARSET=utf8
            ");
        db_query("drop table if exists ?:shopFeed_categories");
        db_query("CREATE TABLE ?:shopFeed_categories 
            (id int not null auto_increment, 
            category_id int not null, 
            title varchar(255) not null, 
            id_path varchar(255) not null, 
            exclude_opt text not null, 
            avail_opt int(1),
            parent_id int,    
            google_category_id int,    
            PRIMARY KEY (`id`) 
            ) DEFAULT CHARSET=utf8
            ");
    }

    protected function fetchCategories()
    {
        $rows = db_get_array("
        select a.category_id, a.id_path, b.category, a.shop_feed_avail_opt, a.parent_id, a.exclude_from_shopfeed, a.google_category_id
        from ?:categories as a
        left join ?:category_descriptions as b on a.category_id=b.category_id
        where b.lang_code=?s AND status='A'
        ORDER BY a.id_path asc", $this->langCode);
        $this->categories = array();
        foreach ($rows as $row) {
            $availOpt = $this->calculateCategoryAvailOpt($row);
            $excludeOpt = $this->calculateCategoryExcludeOpt($row);
            $this->categories[$row['category_id']] = new ShopFeedCategory(
                $row['category_id'], 
                $row['category'], 
                $row['id_path'],
                $availOpt, 
                $excludeOpt,
                $row['parent_id'],
				$this->calculateCategoryGoogleCategoryId($row) //['google_category_id']
            );
            $this->categories[$row['category_id']]->persist();
        }
        // update categories path titles 
        foreach ($this->categories as $cid => $category) {
            $this->categories[$cid]->setIdPathTitle($this->makeCategoryPathTitles($category));            
        }
    }

    protected function calculateCategoryAvailOpt($row) {
        $availOpt = $row['shop_feed_avail_opt'];

        if (isset($this->categories[$row['parent_id']])) {
            if ($this->categories[$row['parent_id']]->getAvailOpt() != 0) {
                $availOpt = $this->categories[$row['parent_id']]->getAvailOpt();
            }
        }
        return $availOpt;
    }

    protected function calculateCategoryGoogleCategoryId($row) {
        $out = $row['google_category_id'];
		if($out==0) {
			$parent_id = $row['parent_id'];
			while (isset($this->categories[$parent_id])) {
				if ($this->categories[$parent_id]->getGoogleCategoryId() != 0) {
					$out = $this->categories[$parent_id]->getGoogleCategoryId();
					break;
				}
				else {
					$parent_id = $this->categories[$parent_id]->getParentId();
				}
			}
		}
        return $out;
    }
    

    
    protected function calculateCategoryExcludeOpt($row) {
        $excludeOpt = (@unserialize($row['exclude_from_shopfeed']) ? unserialize($row['exclude_from_shopfeed']) : array() );
        if (isset($this->categories[$row['parent_id']])) {
            if ( count($excludeOpt)==0 ) { // den exei epilogi na eksaireite
                $parentExcludeOpt = $this->categories[$row['parent_id']]->getExcludeFromShopfeed();
                if ( count($parentExcludeOpt)!=0 ) { // i patriki exei epilogi na eksaireite apo kapoio feed
                    $excludeOpt = $parentExcludeOpt;
                }
            }
        }
        return $excludeOpt;
    }
    
    protected function makeCategoryPathTitles(ShopFeedCategory $category)
    {
        $ids = explode('/', $category->getIdPath());
        $s = array();
        foreach ($ids as $id) {
            if (isset($this->categories[$id]))
                $s[] = $this->categories[$id]->getTitle();
        }
        return implode(' > ', $s);
    }

    protected function updateProducts()
    {
        printf("collecting category product data \n");
        foreach ($this->categories as $cid => $category) {
        	printf("category %s\n", $cid);
            $this->getCategoryProducts($category);
        }
    }

    abstract protected function getCategoryProductsQuery();

    protected function getCategoryProducts(ShopFeedCategory $category)
    {
        $query = sprintf(
            $this->getCategoryProductsQuery(), 
            $this->langCode, 
            $this->userGroup, 
            $category->getId()
            );
        $products = db_get_array($query);
        $featureCollector = new ShopFeedProdFeatures($this->langCode);
        $optCollector = new ShopFeedProdOptions($this->langCode);
        $imageCollector = new ShopFeedProdImages($this->langCode);
                
        if ($products) {
            foreach($products as $row) {
//                print_r($row);
                if($this->isPassingInventoryRule($row)) { 
                    $features = $featureCollector->get($row['product_id']);
                    $row = $this->calculateMPN($row, $features);
                    $row = $this->calculateManufacturer($row, $features);
                    $features = $this->getOptColors($optCollector,$row, $features);
                    $qdata = array(
                        'uniqueId' => $row['product_id'],
                        'name' => $row['product'],
                        'description' => $this->strip_javascript($row['full_description']),
                        'link' => $this->makeProductLink($row),
                        'image' => $imageCollector->getImageLink($row['product_id']),
                        'category_id' => $row['category_id'],
                        'category' => $this->categories[$row['category_id']]->getIdPathTitle(),
                        'base_price' => str_replace('.', '.', $row['price']),
                        'price' => str_replace('.', '.', $this->calculatePrice($category, $row)),
                        'instock' => $this->calculateInStock($row),
                        'availability' => $this->calculateAvailability($category, $row),
                        'product_code' => $row['product_code'],
                        'amount' => $row['amount'],
                        'ean' => $row['ean'],
                        'mpn' => $row['mpn_code'],
                        'manufacturer' => $row['manufacturer'],
                        'sizes' => $optCollector->getOptions($row['product_id'], 'S'),
                        'exclude_type' => $this->calculateExcludeType($row),
                        'features' => serialize(($features ? $features : array())),
                        'insert_timestamp' => $row['timestamp'],
                        'updated_timestamp' => $row['updated_timestamp'],
						'google_category_id' => $this->categories[$row['category_id']]->getGoogleCategoryId(),
                        'supplier_id' => !empty($row['supplier_id']) ? $row['supplier_id'] : 0,
						'weight'=>$row['weight'],
						'scost'=>$row['scost'],
                    );
					$qdata['shipping_cost'] = $this->calculate_shipping_cost($qdata);
                    db_query("INSERT INTO ?:shopFeed_data ?e", $qdata);
                    $this->productCount++;
                }
            }
        }
    }

    /**
     * IF addon setting is that color is not in features it gets product option 
     * with setting that is Color for shopFeed and adds to features array 
     * 'colors' entry with active option variants
     * 
     * @param array $row
     * @param array $features
     * @return array
     */
    private function getOptColors($optCollector, $row, $features) {
        if( Registry::get('addons.shop_feed.colors_from_features')=='N') {
            $colors = $optCollector->getOptions($row['product_id'], 'C');
            if( $colors ) {
                $features['sf_colors'] = $colors;
            }
        }
        return $features;
    }
    
    private function calculateManufacturer($row, &$features) {
        $setting = Registry::get('addons.shop_feed.manufacturerfromfeature');
        
        if( $setting!='0' ) {
            $setting = 'sf_'.$setting;
            if( isset($features[$setting]) && empty($row['manufacturer']) ) {
                $row['manufacturer'] = $features[$setting];
                unset($features[$setting]);
            }
        }
        return $row;        
    }
    
    /**
     * 
     * @param type $row
     * @param type $features
     * @return type
     */
    private function calculateMPN($row, &$features) {
        $setting = Registry::get('addons.shop_feed.mpnfromfeature');
        
        if( $setting!='0' ) {
            $setting = 'sf_'.$setting;
            if( isset($features[$setting]) && empty($row['mpn_code']) ) {
                $row['mpn_code'] = $features[$setting];
                unset($features[$setting]);
            }
        }
        return $row;
    }
    
    /**
     * if inventory_drop is enabled and inventory is bellow feed_min_qty threshold 
     * is not passing rule.
     * If pass the returns TRUE
     * 
     * @param array $row
     * @return boolean
     */
    private function isPassingInventoryRule($row) {
        $pass = true;
        if (Registry::get('addons.shop_feed.inventory_drop') == 'Y' && $row['amount']<=$row['feed_min_qty']) {
            $pass = false;
        }
        return $pass;
    }
    
    private function makeProductLink($row) {
        $link = "products.view&product_id=" . $row['product_id'];
        $out = str_replace('&amp;','&', fn_url($link, 'C', 'current', $this->langCode));
        return $out;
    }
    
    private function calculateExcludeType($row) {
    	$out = ''; 
    	//if(!empty($row['exclude_from_shopfeed'])) {
	        $excludeType0 = (@unserialize($row['exclude_from_shopfeed']) ? unserialize($row['exclude_from_shopfeed']) : array() );
	        $cexcl = $this->categories[$row['category_id']]->getExcludeTotal($excludeType0);
	        if( $cexcl ) {
	        	$out = implode(',',array_keys($cexcl));
	    	}
	    //}
        return $out;
    }
    
    private function calculatePrice($category, $row) {
        $price = $row['price'];

        if (Registry::get('addons.shop_feed.apply_promotions') == 'Y') {
            $promoPriceCollector = new ShopFeedProdPromoPrice();
            $price = $promoPriceCollector->getPriceAfterPromotions($row['product_id'], $category->getId(), $price);
        }
        return $price;
    }
    
    private function calculateInStock($row) {
        $inStock = 'N';
        if($row['avail_since'] == 0 ) {
            if( $row['amount'] ) {
                $inStock = 'Y';
            }
        }
        return $inStock;
    }
    
    private function calculateAvailability($category, $row) {
        $availOpt = 0;
        if ($row['avail_since'] == 0) { // den exei imerominia sto dia8esimo apo
            if (Registry::get('addons.shop_feed.inventory_check') == 'Y' && $row['amount']>0) { // an exei epileksei oti exei apothema na to basei 'Se apothema'
                $availOpt = 1;
            }
            else {
                if ($row['shop_feed_avail_opt'] == 0) { // sto proion exei to default
                    if ($category->getAvailOpt() == 0) {    // stin category exei to default
                        $availOpt = Registry::get('addons.shop_feed.def_avail');
                    } else {    // exei orisei stin katigoria -> bale tis caterory
                        $availOpt = $category->getAvailOpt();
                    }
                } else {    // exei orisei sto proion -> bale toy proiontos
                    $availOpt = $row['shop_feed_avail_opt'];
                }
            }
            $availability = __('shopFeed_avail_opt' . $availOpt, $this->langCode);
        } else {
            $availability = __('shopFeed_avail_opt5', $this->langCode);
        }
        return $availability;
    }

    protected function strip_javascript($filter)
    {

        // realign javascript href to onclick
        $search = array();
        $search[] = '@<script[^>]*?>.*?</script>@si'; // Strip out javascript
        /*
          $search[] = '@<[\/\!]*?[^<>]*?>@si'; 					// Strip out HTML tags
          $search[] = '@<style[^>]*?>.*?</style>@siU'; 	// Strip style tags properly
         */
        $search[] = '@<![\s\S]*?--[ \t\n\r]*>@';      // Strip multi-line comments including CDATA
        $filter = preg_replace($search, '', $filter);

        return $filter;
    }

    public function log($msg) {
        db_query("INSERT INTO ?:shopFeed_log (timestamp, pcount, feeds) VALUES (?i, ?i, ?s)", time(), $this->productCount, $msg);
    }
	
  public function calculate_shipping_cost($row){
	if($row['scost']==-1) {
		$cost = $this->calculate_shipping_cost_real($row);
		db_query("delete from slx_shopfeed_shipping_costs where product_id=?i", $row['uniqueId']);
		$data = [
			'product_id' => $row['uniqueId'],
			'cost' => $cost,
		];
		db_query("insert into slx_shopfeed_shipping_costs ?e", $data);
		return $cost;
	}
	else {
		return $row['scost'];
	}
  }

    private function calculate_shipping_cost_real($row) {
	    $cart = [];
	    $auth = [];
	    $auth['tax_exempt'] = 'Y';
	    $product = [ '123456' => [
		    'product_id'=>$row['uniqueId'],
		    'amount'=>1,
		    'price' => $row['base_price'],
		    ]
	    ];
	    fn_clear_cart($cart);
	    $cart['calculate_shipping'] = true;
        fn_add_product_to_cart($product, $cart, $auth);
        fn_calculate_cart_content($cart, $auth, 'A', true, 'F', true);
	    return $cart['shipping_cost'];  
    }
	
}
