<?php
namespace Tygh\ShopFeed\DataCollectors;
/**
 * Description of ShopFeedProdOptions
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 17 Apr 2015
 */
class ShopFeedProdOptions {

    private $productId, $optionType, $langCode;
    
    public function __construct($langCode) {
        $this->langCode = $langCode;
    }
    
    private function getOptionId() {
        return db_get_field(
            "select pp.option_id from (
                select * from cscart_product_options where product_id=?i
                union (
                    select po.* from cscart_product_global_option_links as pgol left join cscart_product_options as po on (po.option_id=pgol.option_id) where pgol.product_id=?i
                )
            ) as pp where pp.is_shopfeed_option=?s limit 0,1", 
            $this->productId, 
            $this->productId, 
            $this->optionType);
    }
    
    private function getOptionVariants($optionId) {
        $sql = "select group_concat(ovd.variant_name) as sizes
            from ?:product_option_variants ov
            left join ?:product_option_variants_descriptions ovd on (ov.variant_id=ovd.variant_id and ovd.lang_code=?s)
            where ov.status='A' and ov.option_id=?i";
        $out = db_get_field($sql, $this->langCode, $optionId);
        return $out;
    }
    
    public function getOptions($productId, $optionType) {
        $this->productId = $productId;
        $this->optionType = $optionType;
        $out = null;
        $optionId = $this->getOptionId();
        if( $optionId ) {
            $out = $this->getOptionVariants($optionId);
        }
        return $out;
    }
}
