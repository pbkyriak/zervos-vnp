<?php
namespace Tygh\ShopFeed\DataCollectors;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ShopFeedCategory
 * Created on 23-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedCategory
{

    protected $id, $title, $id_path, $idPathTitle, $availOpt, $excludeFromShopfeed, $parentId, $google_category_id;

    public function __construct($id, $title, $id_path, $avail_opt, $exclude_from_shopfeed, $parentId, $google_category_id)
    {
        $this->id = $id;
        $this->title = $title;
        $this->id_path = $id_path;
        $this->idPathTitle = '';
        $this->availOpt = $avail_opt;
        $this->excludeFromShopfeed = $exclude_from_shopfeed;
        $this->parentId = $parentId;
		$this->google_category_id = $google_category_id;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($value)
    {
        $this->id = $value;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($value)
    {
        $this->title = $value;
    }

    public function getIdPath()
    {
        return $this->id_path;
    }

    public function setIdPath($value)
    {
        $this->id_path = $value;
    }

    public function getIdPathTitle()
    {
        return $this->idPathTitle;
    }

    public function setIdPathTitle($value)
    {
        $this->idPathTitle = $value;
    }

    public function getAvailOpt()
    {
        return $this->availOpt;
    }

    public function setAvailOpt($value)
    {
        $this->availOpt = $value;
    }

    public function getExcludeFromShopfeed()
    {
        return $this->excludeFromShopfeed;
    }

    public function setExcludeFromShopfedd($value)
    {
        $this->excludeFromShopfeed = $value;
    }
	
	public function getGoogleCategoryId() {
		return $this->google_category_id;
	}

	public function setGoogleCategoryId($v) {
		$this->google_category_id = $v;
	}
	
    public function getExcludeTotal($prodExcludes)
    {
    	$out = array();
    	if( is_array($prodExcludes) ) {
        	$out = array_unique(array_merge($this->excludeFromShopfeed, $prodExcludes));
    	}
    	else {
    		$out = $this->excludeFromShopfeed;
    	}
    	return $out;
    }
	
	public function getParentId() {
		return $this->parentId;
	}
    public function __toString()
    {
        return sprintf("%s %s %s %s excl=%s", $this->getId(), $this->getTitle(), $this->getIdPath(), $this->getIdPathTitle(), implode(',', $this->excludeFromShopfeed));
    }

    public function persist() {
        $cdata = array(
            'category_id'=> $this->id,
            'title' => $this->title,
            'id_path' => $this->id_path,
            'exclude_opt' => implode(',',$this->excludeFromShopfeed), 
            'avail_opt' => $this->availOpt,
            'parent_id' => $this->parentId,
			'google_category_id' => $this->google_category_id,
        );
        db_query("INSERT INTO ?:shopFeed_categories ?e", $cdata);
    }
}
