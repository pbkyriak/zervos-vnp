<?php
/**
 * Description of AbstractShopFeedExporter
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */

namespace Tygh\ShopFeed\DataExporters;
use Tygh\Registry;

abstract class AbstractShopFeedExporter {
    protected $feedInfo;
    protected $feedId;
    protected $feedFolder, $feedFile;
    protected $outputHandler, $xmlEncoding;
    protected $dbRowCount, $feedRowCount;
    protected $createdTag;
    protected $cleanNonLatin;
    protected $fileExtension = '.xml';
    protected $exportFeatures = false;
	protected $availSups;
	protected $featureReplacements = array();
	    
// $feedId, $feedFolder, $feedFile, $exportFeatures = false) {
// $feedInfo['exporter'], $feedInfo['id'], 'xml_feed', $feedInfo['filename'], $feedInfo['export_features'], $feedInfo[suppliers_to_export]
    
    public function __construct($feedInfo) {
        $this->feedInfo = $feedInfo;
        $feedFolder = Registry::get('addons.shop_feed.feeds_path');
        if( empty($feedFolder)) {
            $feedFolder = 'xml_feed';
        }
        //$feedFolder = 'xml_feed/test';
        $this->feedId = $feedInfo['id'];
        $this->feedFolder = DIR_ROOT . '/' . $feedFolder;
        $this->feedFile = $this->feedFolder . '/' . $feedInfo['filename'] . $this->fileExtension;
        $this->xmlEncoding = "utf-8";
        $this->dbRowCount = 0;
        $this->feedRowCount = 0;
        $this->createdTag = 'created_at';
        $this->cleanNonLatin = false;
        $this->exportFeatures = $feedInfo['export_feature_codes'];
        $this->availSups = $feedInfo['suppliers_to_export'];
		
        if( (int)Registry::get('addons.shop_feed.manufacturerfromfeature')!=0) {
            $this->featureReplacements[Registry::get('addons.shop_feed.manufacturerfromfeature')] = 'manufacturer';
        }
        if( (int)Registry::get('addons.shop_feed.mpnfromfeature')!=0) {
            $this->featureReplacements[Registry::get('addons.shop_feed.mpnfromfeature')] = 'mpn';
        }

        if (!is_dir($this->feedFolder)) {
            clearstatcache();
            mkdir($this->feedFolder, 0777);
        }
        else {
            chmod($this->feedFolder, 0777);
        }
    }

    /**
     *
     * @param boolean $value 
     */
    public function setCleanNonLatin($value) {
        $this->cleanNonLatin = $value;
    }

    protected function openOutput() {
		$out =false;
        $this->outputHandler = fopen($this->feedFile, "w");
		if($this->outputHandler) {
			$this->writeLn('<?xml version="1.0" encoding="' . $this->xmlEncoding . '" ?>');
			$this->writeLn('<mystore>');
			$this->writeLn(sprintf("\t<%s>%s</%s>", $this->createdTag, date("Y-m-d H:i:s"), $this->createdTag));
			$this->writeLn("\t<products>");
			$out =true;
		}
		return $out;
    }

    protected function closeOutout() {
        $this->writeLn("\t</products>");
        $this->writeLn('</mystore>');
        fclose($this->outputHandler);

        if (filesize($this->feedFile) > 10 * 1048576) {
            $zip = new \ZipArchive();
            $res = $zip->open($this->feedFile . '.zip', \ZipArchive::CREATE);
            if ($res === TRUE) {
                $zip->addFile($this->feedFile, str_replace($this->feedFolder . '/', '', $this->feedFile));
                $zip->close();
            }
        }
    }

    protected function write($text) {
        fputs($this->outputHandler, $text);
    }

    protected function writeLn($text) {
        $this->write($text . "\n");
    }

    abstract public function export();

    protected function validateStringData($text) {
        libxml_use_internal_errors(true);
        if ($this->cleanNonLatin)
            $text = preg_replace('/[^\00-\255]+/u', '', $text);
        $sxe = simplexml_load_string(sprintf('<?xml version="1.0" encoding="utf-8"?><description><![CDATA[%s]]></description>', $text));
        $errCount = 0;
        if (!$sxe) {
            $msg = '';
            foreach (libxml_get_errors() as $error) {
                $msg .= "\n\t" . $error->message;
                $errCount++;
            }
        }
        if ($errCount)
            return '';
        else
            return $text;
    }

    protected function getData() {
    	if( Registry::get('addons.suppliers') ) {
    		$sql = db_quote("SELECT * FROM ?:shopFeed_data where not FIND_IN_SET(?s,exclude_type) and supplier_id in(?a) ORDER BY category_id, name", $this->feedId, $this->availSups);
    	}
        else {
        	$sql = db_quote("SELECT * FROM ?:shopFeed_data where not FIND_IN_SET(?s,exclude_type) ORDER BY category_id, name", $this->feedId);
        }
		$results = db_get_array($sql);
        $this->dbRowCount = count($results);
        return $results;
    }

    protected function writeFeatures($row) {
        $features = unserialize($row['features']);
        foreach ($features as $k => $v) {
            if (!empty($k) && !empty($v)) {
                if(in_array($k, $this->exportFeatures) || strpos($k, 'sf_')===0 ) {
                	if( strpos($k, 'sf_')===0 ) {
                    	$k = str_replace('sf_', '', $k);
                    	if( isset($this->featureReplacements[$k]) ) {
                    		$k = $this->featureReplacements[$k];
                    	}
                    	else {
                    		$k = '';
                    	}
                	}
                	if( $k ) {
                    	$this->writeln(sprintf("<%s><![CDATA[%s]]></%s>", $k, $v, $k));
                	}
                }
            }
        }
    }

}
