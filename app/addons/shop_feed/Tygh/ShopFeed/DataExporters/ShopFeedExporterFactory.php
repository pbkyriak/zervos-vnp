<?php
/**
 * @author Panos Kyriakakis <panos at salix.gr>
 */
namespace Tygh\ShopFeed\DataExporters;

use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

/**
 * Description of ShopFeedExporterFactory
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 26 Ιουλ 2014
 */
class ShopFeedExporterFactory
{
    /**
     * Returns instance of exporter with $name
     */
    public static function getExporter($feedInfo) {
        $name = $feedInfo['exporter'];
        $classFile = sprintf("%sShopFeedExport%s.php", 
            dirname(__FILE__) . '/Exporters/',
            ucfirst($name)
            );
        $className = sprintf("Tygh\ShopFeed\DataExporters\Exporters\ShopFeedExport%s", ucfirst($name) );
        if(file_exists($classFile) ) {
            require_once $classFile;
            if( class_exists($className) ) {
                return new $className($feedInfo);
            }
        }
        throw new \Exception('Exporter '.$name.' was not found');
    }
    /**
     * Returns names of existing exporters
     * 
     * @return array
     */
    public static function getExporterNames() {
        $out = array();
        $garbage = array('.', '..', 'ShopFeedExportBase.class.php');
        $exportersDir = dirname(__FILE__) . '/Exporters/';
        $files = scandir($exportersDir);
        foreach($files as $file) {
            if( !in_array($file, $garbage) ) {
                $out[] = strtolower( str_replace(array('ShopFeedExport','.php'), '', $file));
            }
        }
        return $out;
    }
    /**
     * Returns info of ALL exporters
     * 
     * @return array
     */
    public static function getExportersInfo() {
        $names = self::getExporterNames();
        $out = array();
        foreach($names as $name) {
            $out[$name] = array(
                'id'=> $name,
                'title' => 'shopfeed_exporter_'.$name,
            );
        }
        return $out;
    }
}
