<?php
/**
 * Description of ShopFeedBestPrice
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */

namespace Tygh\ShopFeed\DataExporters\Exporters;
use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

class ShopFeedExportBestprice extends AbstractShopFeedExporter {
  
  public function export() {
    $this->createdTag = 'date';
    if(!$this->openOutput()) {
		return false;
	}
    $results = $this->getData();
    if( $results ) {
      foreach($results as $row) {
		  if(empty($row['manufacturer'])) {
			  continue;
		  }

        $this->writeln("<product>");
        $this->writeln( sprintf("<productId>%s</productId>", $row['uniqueId'] ));
        $this->writeln( sprintf("<title><![CDATA[%s]]></title>", $row['name'] ));
        $this->writeln( sprintf("<url><![CDATA[%s]]></url>", $row['link'] ));
        $this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
        $this->writeln( sprintf("<sku><![CDATA[%s]]></sku>", $row['product_code'] ));
        $this->writeln( sprintf("<category_id>%s</category_id>", $row['category_id'] ));
        $this->writeln( sprintf("<category_name><![CDATA[%s]]></category_name>", $row['category'] ));
        $this->writeln( sprintf("<price>%s</price>", $row['price'] ));
        $this->writeln( sprintf("<description><![CDATA[%s]]></description>", $this->validateStringData($row['description']) ));
        $this->writeln( sprintf("<stock><![CDATA[%s]]></stock>", ($row['instock']=='Y' ? '1' : '0') ));
        $this->writeln( sprintf("<availability><![CDATA[%s]]></availability>", $row['availability'] ));
        $this->writeln( sprintf("<manufacturer><![CDATA[%s]]></manufacturer>", $row['manufacturer'] ));
        if( $row['sizes'] ) {
        	$this->writeln( sprintf("<size>%s</size>", $row['sizes'] ));
        }
        $this->writeFeatures($row);
        $this->writeln("</product>");
        $this->feedRowCount++;
      }
    }
    $this->closeOutout();
  }
}

