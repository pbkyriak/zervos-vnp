<?php
/**
 * Description of ShopFeedCsv
 * Created on 29-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
namespace Tygh\ShopFeed\DataExporters\Exporters;
use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

class ShopFeedExportIcap extends AbstractShopFeedExporter {
  
  protected $fileExtension = '.csv';

  protected function openOutput() {
    $this->outputHandler = fopen($this->feedFile, "w");
  }

  protected function closeOutout() {
    fclose($this->outputHandler);
    if (filesize($this->feedFile) > 10 * 1048576) {
      $zip = new ZipArchive();
      $res = $zip->open($this->feedFile . '.zip', ZipArchive::CREATE);
      if ($res === TRUE) {
        $zip->addFile($this->feedFile, str_replace($this->feedFolder . '/', '', $this->feedFile));
        $zip->close();
      }
    }
  }
  
  public function export() {
    
    $this->openOutput();
    $results = $this->getData();
	$row = [
		'ID',
		'ID2',
		'Item title',
		'Final URL',
		'Image URL',
		'Item subtitle',
		'Item description',
		'Item category',
		'Price',
		'Sale price',
	];
	fputcsv($this->outputHandler, $row);
    if( $results ) {
      foreach($results as $row) {
		  $drow = [
				'ID' => $row['uniqueId'],
				'ID2' => $row['product_code'],
				'Item title' => mb_substr(preg_replace('/[^\00-\255]+/u', '', $row['name']),0,25),
				'Final URL' => $row['link'],
				'Image URL' => $row['image'],
				'Item subtitle' => '',
				'Item description' => mb_substr(trim(strip_tags(nl2br($this->validateStringData($row['description'])))),0,25),
				'Item category' => strtr($row['category'], array('>' => '/', '&'=>'-')),
				'Price' => $row['base_price'].' EUR',
				'Sale price' => $row['price'].' EUR',
		  ];
        fputcsv($this->outputHandler, $drow);
        $this->feedRowCount++;
      }
    }
    $this->closeOutout();
  }
}

/*

# Specs: (note rows starting with "# " will be ignored),,,,,,,,,,,
# ID: Required (Primary Key) | String. Any sequence of letters and digits. | The combination of ID and ID2 should be unique across all feeds in your account.,,,,,,,,,,,
# ID2: Optional (Secondary Key) | String. Any sequence of letters and digits. | The combination of ID and ID2 should be unique across all feeds in your account.,,,,,,,,,,,
# Item title: Required | String. Any sequence of letters and digits. | Can be displayed in ad. Recommended maximum length is 25 characters (12 for double-width languages).,,,,,,,,,,,
# Final URL: Recommended; Required if not using Destination URL | Same domain as your website, begins with "http://" or "https://" | The URL of the page in your website that people reach when they click your ad,,,,,,,,,,,,,
# Image URL: Recommended | Format: JPG, JPEG, GIF, or PNG | if JPG, JPEG, or GIF must be used, image must be saved in RGB color code with an ICC profile attached to it. Recommended image size: 300 pixels x 300 pixels. Resolution: 72 dpi. Max file size: 12MB | Image used in ad,,,,,,,,,,,
# Item subtitle: Recommended | String. Any sequence of letters and digits. | Can be displayed in ad. Recommended maximum length is 25 characters (12 for double-width languages).,,,,,,,,,,,
# Item description: Recommended | String. Any sequence of letters and digits. | Can be displayed in ad. Recommended maximum length is 25 characters (12 for double-width languages).,,,,,,,,,,,
# Item category: Recommended | String. Any sequence of letters and digits. | Used to group like items together for recommendation engine,,,,,,,,,,,
# Price: Recommended | Number followed by the currency code, ISO 4217 standard. Use "." as the decimal mark. | Price shown in ad,,,,,,,,,,,
# Sale price: Recommended | Number followed by the currency code, ISO 4217 standard. Use "." as the decimal mark. | If used, Sales price shown in ad, Price struck out or omitted in ad,,,,,,,,,,,


'ID'
'ID2'
'Item title'
'Final URL'
'Image URL'
'Item subtitle'
'Item description'
'Item category'
'Price'
'Sale price'


*/