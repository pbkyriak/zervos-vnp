<?php
/**
 * Description of ShopFeedCsv
 * Created on 29-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
namespace Tygh\ShopFeed\DataExporters\Exporters;
use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

class ShopFeedExportCsv extends AbstractShopFeedExporter {
  
  protected $fileExtension = '.csv';

  protected function openOutput() {
    $this->outputHandler = fopen($this->feedFile, "w");
  }

  protected function closeOutout() {
    fclose($this->outputHandler);
    if (filesize($this->feedFile) > 10 * 1048576) {
      $zip = new ZipArchive();
      $res = $zip->open($this->feedFile . '.zip', ZipArchive::CREATE);
      if ($res === TRUE) {
        $zip->addFile($this->feedFile, str_replace($this->feedFolder . '/', '', $this->feedFile));
        $zip->close();
      }
    }
  }
  
  public function export() {
    
    $this->openOutput();
    $results = $this->getData();
    if( $results ) {
      foreach($results as $row) {
        fputcsv($this->outputHandler, $row);
        $this->feedRowCount++;
      }
    }
    $this->closeOutout();
  }
}

