<?php
/**
 * Description of ShopFeedExportFacebook
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
namespace Tygh\ShopFeed\DataExporters\Exporters;
use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

class ShopFeedExportFacebook extends AbstractShopFeedExporter {
  
  public function export() {
    
    if(!$this->openOutput()) {
		printf("count not open output\n");
		return false;
	}

    $results = $this->getData();
    if( $results ) {
		  foreach($results as $row) {
				$str = str_replace('&', '', $row['description']);
				$str2 = str_replace('&', '', $row['name']);
				$descr = $this->validateStringData(strip_tags($str));
				if($row['manufacturer']==' ' || $row['manufacturer']==''){
				    $manu = 'vnp';
				}
				else{
				    $manu = $row['manufacturer'];
				}
				$this->writeLn("<item>");
				$this->writeLn( sprintf("<g:id>%s</g:id>", $row['uniqueId'] ));
				$this->writeLn( sprintf("<g:title><![CDATA[%s]]></g:title>", strip_tags($str2) ));
				$this->writeLn( sprintf("<g:description><![CDATA[%s]]></g:description>", $descr=='' ? $str2 : $descr));				
				$this->writeLn( sprintf("<g:google_product_category>%s</g:google_product_category>", !empty($row['google_category_id']) ? $row['google_category_id'] : 222 )); // google category code 1604=apparel
				$this->writeLn( sprintf("<g:product_type>%s</g:product_type>", strtr($row['category'], array('>' => '&gt;', '&'=>'&amp;')) ));
				$this->writeLn( sprintf("<g:link><![CDATA[%s]]></g:link>", $row['link'] ));
				$this->writeLn( sprintf("<g:image_link><![CDATA[%s]]></g:image_link>", $row['image'] ));
				$this->writeLn( sprintf("<g:condition>%s</g:condition>", 'new' ));
				$this->writeLn( sprintf("<g:availability>%s</g:availability>", $row['amount']>=1 ? 'in stock' : 'out of stock'));
				$list_price = isset($row['list_price']) ? $row['list_price'] : 0;
				$this->writeLn( sprintf("<g:price>%s EUR</g:price>", $list_price>0 ? $list_price : $row['price'] ));
				$this->writeLn( sprintf("<g:brand><![CDATA[%s]]></g:brand>", $manu));	
				if($row['list_price']>0){
					$this->writeLn( sprintf("<g:sale_price>%s EUR</g:sale_price>",  $row['price'] ));	
				}
				$this->writeFeatures($row);
				/*
				$this->writeLn("<g:shipping>");				
				$this->writeLn( sprintf("<g:country>GR</g:country>"));				
				$this->writeLn( sprintf("<g:service>Standard</g:service>"));				
				$this->writeLn( sprintf("<g:shipping_weight>Standard</g:shipping_weight>"));				
				$this->writeLn( sprintf("<g:shipping_size>Standard</g:shipping_size>"));				
				$this->writeLn( sprintf("<g:price>0.00 EUR</g:price>"));				
				$this->writeLn("</g:shipping>");									
				*/
				$this->writeLn("</item>");
				$this->feedRowCount++;
	   } 
    }
    $this->closeOutout();
  }
  
  protected function writeFeatures($row) {
 		if( $this->exportFeatures ) {
	 		$features = unserialize($row['features']);
	        foreach($features as $k => $v) {
	            if( !empty($k) && !empty($v) && $k=="itemtype") { //na fernei mono itemtype gia facebook
	                $this->writeLn(sprintf("<g:%s><![CDATA[%s]]></g:%s>", "item_group_id", $v, "item_group_id"));
	            }
	        }
	    }
 	} 
  
   protected function openOutput() {
        if($this->outputHandler = fopen($this->feedFile, "w")) {
			$this->writeLn('<?xml version="1.0" encoding="' . $this->xmlEncoding . '" ?>');
			$this->writeLn('<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">');
			$this->writeLn('<channel>');
			$this->writeLn('<title>www.vnp.gr - Online Store</title>');
			$this->writeLn('<link>https://www.vnp.gr</link>');
			$this->writeLn('<description>This is the product feed of vnp.gr</description>');
			return true;
		}
		return false;
    }

    protected function closeOutout() {
        $this->writeLn("\t</channel>");
        $this->writeLn('</rss>');
        fclose($this->outputHandler);

        if (filesize($this->feedFile) > 10 * 1048576) {
            $zip = new \ZipArchive();
            $res = $zip->open($this->feedFile . '.zip', ZipArchive::CREATE);
            if ($res === TRUE) {
                $zip->addFile($this->feedFile, str_replace($this->feedFolder . '/', '', $this->feedFile));
                $zip->close();
            }
        }
    }
}
