<?php
/**
 * Description of ShopFeedSkroutz
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
namespace Tygh\ShopFeed\DataExporters\Exporters;
use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

class ShopFeedExportContact extends AbstractShopFeedExporter {
  
  public function export() {
    
    $this->openOutput();
    $results = $this->getData();
    if( $results ) {
      foreach($results as $row) {
        $this->writeln("<product>");
        $this->writeln( sprintf("<id>%s</id>", $row['uniqueId'] ));
        $this->writeln( sprintf("<name><![CDATA[%s]]></name>", $row['name'] ));
        $this->writeln( sprintf("<link><![CDATA[%s]]></link>", $row['link'] ));
        $this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
        $this->writeln( sprintf("<sku><![CDATA[%s]]></sku>", $row['product_code'] ));
        $this->writeln( sprintf("<mpn><![CDATA[%s]]></mpn>", $row['product_code'] ));
        $this->writeln( sprintf("<category id=\"%s\"><![CDATA[%s]]></category>", $row['category_id'], $row['category'] ));
        $this->writeln( sprintf("<baseprice>%s</baseprice>", $row['base_price'] ));
        $this->writeln( sprintf("<price>%s</price>", $row['price'] ));
        $this->writeln( sprintf("<description><![CDATA[%s]]></description>", $this->validateStringData($row['description']) ));
        $this->writeln( sprintf("<instock>%s</instock>", $row['instock'] ));
        $this->writeln( sprintf("<availability>%s</availability>", $row['availability'] ));
        $this->writeln( sprintf("<manufacturer><![CDATA[%s]]></manufacturer>", $row['manufacturer'] ));
        if( $row['sizes'] ) {
        	$this->writeln( sprintf("<size>%s</size>", $row['sizes'] ));
        }
        $this->writeFeatures($row);
        $this->writeln("</product>");
        $this->feedRowCount++;
      }
    }
    $this->closeOutout();
  }
}

