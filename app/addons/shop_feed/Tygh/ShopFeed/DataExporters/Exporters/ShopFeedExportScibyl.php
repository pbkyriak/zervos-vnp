<?php
/**
 * Description of ShopFeedSkroutz
 * Created on 24-8-2011
 * @author Panos Kyriakakis <panos@salix.gr>
 */
namespace Tygh\ShopFeed\DataExporters\Exporters;
use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

class ShopFeedExportScibyl extends AbstractShopFeedExporter {
  
  public function export() {
    
    if(!$this->openOutput()) {
		return false;
	}
    $results = $this->getData();
    if( $results ) {
	  
      foreach($results as $row) {
		  if(empty($row['manufacturer'])) {
			  printf("Empty manu %s\n", $row['uniqueId']);
			  continue;
		  }
        $this->writeln("<product>");
        $this->writeln( sprintf("<unique_id>%s</unique_id>", $row['uniqueId'] ));
        $this->writeln( sprintf("<title><![CDATA[%s]]></title>", $row['name'] ));
        $this->writeln( sprintf("<url><![CDATA[%s]]></url>", $row['link'] ));
        $this->writeln( sprintf("<image><![CDATA[%s]]></image>", $row['image'] ));
        $this->writeln( sprintf("<ean><![CDATA[%s]]></ean>", $row['ean'] ));
        $this->writeln( sprintf("<mpn><![CDATA[%s]]></mpn>", $row['mpn'] ));
        $this->writeln( sprintf("<category><![CDATA[%s]]></category>",  $row['category'] ));
      //  $this->writeln( sprintf("<category_id><![CDATA[%s]]></category_id>", $row['category_id'] ));
        $this->writeln( sprintf("<price_with_vat>%s</price_with_vat>", $row['price'] ));
      //  $this->writeln( sprintf("<in_stock>%s</in_stock>", $row['instock']=='Υ' ? 'true' : 'false' ));
        $this->writeln( sprintf("<manufacturer><![CDATA[%s]]></manufacturer>", $row['manufacturer'] )); 
        $this->writeln("</product>");
        $this->feedRowCount++;
      }
    }
    $this->closeOutout();
  }
  
    protected function openOutput() {
		$out =false;
        $this->outputHandler = fopen($this->feedFile, "w");
		if($this->outputHandler) {
			$this->writeLn('<?xml version="1.0" encoding="' . $this->xmlEncoding . '" ?>');
			$this->writeLn('<store>');
			//$this->writeLn(sprintf("\t<%s>%s</%s>", $this->createdTag, date("Y-m-d H:i:s"), $this->createdTag));
			$this->writeLn("\t<products>");
			$out =true;
		}
		return $out;
    }

    protected function closeOutout() {
        $this->writeLn("\t</products>");
        $this->writeLn('</store>');
        fclose($this->outputHandler);

        if (filesize($this->feedFile) > 10 * 1048576) {
            $zip = new \ZipArchive();
            $res = $zip->open($this->feedFile . '.zip', \ZipArchive::CREATE);
            if ($res === TRUE) {
                $zip->addFile($this->feedFile, str_replace($this->feedFolder . '/', '', $this->feedFile));
                $zip->close();
            }
        }
    }
  
}
