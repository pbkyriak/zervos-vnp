<?php
/**
 * Description of ShopFeedExportCbproducts
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 3 Νοε 2014
 */

namespace Tygh\ShopFeed\DataExporters\Exporters;
use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

class ShopFeedExportCbproducts extends AbstractShopFeedExporter
{
   
    protected function openOutput()
    {
        $this->outputHandler = fopen($this->feedFile, "w");
        $this->writeLn('<?xml version="1.0" encoding="' . $this->xmlEncoding . '" ?>');
        $this->writeLn("<stored_products>");
    }

    protected function closeOutout()
    {
        $this->writeLn('</stored_products>');
        fclose($this->outputHandler);
    }

    public function export()
    {
        $this->openOutput();
        $results = $this->getData();
        if ($results) {
            foreach ($results as $row) {
                //if(!empty($row['ean'])) {
                    $features = unserialize($row['features']);
                    $this->writeln("<product>");
                    $this->writeln(sprintf("<title><![CDATA[%s]]></title>", $row['name']));
                    $this->writeln(sprintf("<short_description><![CDATA[%s]]></short_description>", $this->validateStringData($row['description']) ));
                    $this->writeln(sprintf("<description><![CDATA[%s]]></description>", $this->validateStringData($row['description']) ));
                    $this->writeln(sprintf("<url><![CDATA[%s]]></url>", $row['image']));
                    $this->writeln(sprintf("<published_date><![CDATA[%s]]></published_date>", date('Y/m/d', $row['insert_timestamp'])));
                    if( $row['updated_timestamp'] ) {
                        $this->writeln(sprintf("<last_modified><![CDATA[%s]]></last_modified>", date('Y/m/d', $row['updated_timestamp'])));
                    }
                    $this->writeln(sprintf("<price><![CDATA[%s]]></price>", $row['price']));
                    if(!empty($row['mpn'])) {
                        $this->writeln(sprintf("<isbn><![CDATA[%s]]></isbn>", $row['mpn']));
                    }
                    $this->writeln(sprintf("<barcode><![CDATA[%s]]></barcode>", $row['ean']));
                    $this->writeln(sprintf("<cat_id><![CDATA[%s]]></cat_id>", $row['category_id']));
                    $this->writeln(sprintf("<InStock>%s</InStock>", 'Y'));
                    foreach($features as $k => $v) {
                        if( !empty($k) && !empty($v) ) {
                            $this->writeln(sprintf("<%s><![CDATA[%s]]></%s>", $k, $v, $k));
                        }
                    }
                    $this->writeln("</product>");
                    $this->feedRowCount++;
                //}
            }
        }
        $this->closeOutout();
    }
}
