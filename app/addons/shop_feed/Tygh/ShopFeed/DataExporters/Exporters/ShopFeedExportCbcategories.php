<?php
/**
 * Exports Categories xml file for CosmoteBooks.gr
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Νοε 2014
 */

namespace Tygh\ShopFeed\DataExporters\Exporters;
use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

class ShopFeedExportCbcategories extends AbstractShopFeedExporter
{

    protected function getData()
    {
        $results = db_get_array("SELECT * FROM ?:shopFeed_categories where not FIND_IN_SET(?s,exclude_opt) ORDER BY id_path, title", $this->feedId);
        $this->dbRowCount = count($results);
        return $results;
    }

    protected function openOutput()
    {
        $this->outputHandler = fopen($this->feedFile, "w");
        $this->writeLn('<?xml version="1.0" encoding="' . $this->xmlEncoding . '" ?>');
        $this->writeLn("<stored_categories>");
    }

    protected function closeOutout()
    {
        $this->writeLn('</stored_categories>');
        fclose($this->outputHandler);
    }

    public function export()
    {
        $this->openOutput();
        $results = $this->getData();
        if ($results) {
            foreach ($results as $row) {
                $this->writeln("<category>");
                $this->writeln(sprintf("<CatId>%s</CatId>", $row['category_id']));
                $this->writeln(sprintf("<Parent_CatId>%s</Parent_CatId>", $row['parent_id']));
                $this->writeln(sprintf("<Cat_Name><![CDATA[%s]]></Cat_Name>", $row['title']));
                $this->writeln(sprintf("<Published>%s</Published>", 1));
                $this->writeln("</category>");
                $this->feedRowCount++;
            }
        }
        $this->closeOutout();
    }

}
