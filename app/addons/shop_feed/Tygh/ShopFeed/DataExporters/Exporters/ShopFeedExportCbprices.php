<?php
/**
 * Description of ShopFeedExportCbprices
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Νοε 2014
 */

namespace Tygh\ShopFeed\DataExporters\Exporters;
use Tygh\ShopFeed\DataExporters\AbstractShopFeedExporter;

class ShopFeedExportCbprices extends AbstractShopFeedExporter
{
    
    protected function openOutput()
    {
        $this->outputHandler = fopen($this->feedFile, "w");
        $this->writeLn('<?xml version="1.0" encoding="' . $this->xmlEncoding . '" ?>');
        $this->writeLn("<stored_products>");
    }

    protected function closeOutout()
    {
        $this->writeLn('</stored_products>');
        fclose($this->outputHandler);
    }

    protected function getData()
    {
        $results = db_get_array("SELECT * FROM ?:shopFeedData where not FIND_IN_SET(?s,exclude_type) ORDER BY category_id, name", $this->feedId);
        $this->dbRowCount = count($results);
        return $results;
    }

    public function export()
    {
        $this->openOutput();
        $results = $this->getData();
        if ($results) {
            foreach ($results as $row) {
                if(!empty($row['ean'])) {
                    $this->writeln("<product>");
                    if(!empty($row['mpn'])) {
                        $this->writeln(sprintf("<isbn>%s</isbn>", $row['mpn']));
                    }
                    /* hack for Stormy */
                    else {
                        $this->writeln(sprintf("<isbn>%s</isbn>", $row['ean']));
                    }
                    /* end of Stormy hack */
                    $this->writeln(sprintf("<barcode>%s</barcode>", $row['ean']));
                    $this->writeln(sprintf("<price>%s</price>", $row['price']));
                    $this->writeln(sprintf("<InStock>%s</InStock>", 'Y'));
                    $this->writeln("</product>");
                    $this->feedRowCount++;
                }
            }
        }
        $this->closeOutout();
    }

}
