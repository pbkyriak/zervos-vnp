<?php
namespace Tygh\ShopFeed\Admin;
/**
 * Description of ShopFeedFeedAdmin
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 12 Οκτ 2014
 */
class ShopFeedFeedAdmin {

    public function updateFeed($feedId, $feedData) {
        if( !isset($feedData['export_features']) ) {
            $feedData['export_features']= array(); 
        }
        $feedData['export_features'] = serialize($feedData['export_features']);
        if( !isset($feedData['suppliers_to_export']) ) {
            $feedData['suppliers_to_export']= array(); 
        }
        $feedData['suppliers_to_export'] = serialize($feedData['suppliers_to_export']);
        if( $feedId ) {
            db_query("UPDATE ?:shopFeed_feeds SET feed=?s, exporter=?s, filename=?s, title=?s, export_features=?s, suppliers_to_export=?s WHERE id=?i", $feedData['feed'], $feedData['exporter'], $feedData['filename'], $feedData['title'], $feedData['export_features'], $feedData['suppliers_to_export'], $feedId);
        } 
        else {
            $feedId = db_query("INSERT INTO ?:shopFeed_feeds (feed, exporter, filename, title, export_features, suppliers_to_export) VALUES (?s, ?s, ?s, ?s, ?s, ?s)", $feedData['feed'], $feedData['exporter'], $feedData['filename'], $feedData['title'], $feedData['export_features'], $feedData['suppliers_to_export']);
        }
        return $feedId;
    }
    
    public function getFeedData($feedId) {
        $data = db_get_row("SELECT * FROM ?:shopFeed_feeds WHERE id=?i", $feedId);
        $data['export_features'] = unserialize($data['export_features']);
        $data['suppliers_to_export'] = unserialize($data['suppliers_to_export']);
        return $data;
    }
    
    public function deleteFeed($feedId) {
        db_query("DELETE FROM ?:shopFeed_feeds WHERE id=?i", $feedId);
    }

    
    public function deleteFeeds($feedIds) {
        foreach($feedIds as $feedId) {
            $this->deleteFeed($feedId);
        }
    }
    
    public static function getFeedsInfo() {
        $out = array();
        $rows = db_get_array("SELECT feed, exporter, filename, title, export_features, suppliers_to_export FROM ?:shopFeed_feeds");
        foreach($rows as $row) {
        	$exportFeatures = @unserialize($row['export_features']) ? unserialize($row['export_features']) : array();
            $out[$row['feed']] = array(
                'id'=>$row['feed'], 
                'exporter'=>$row['exporter'], 
                'title'=>$row['title'], 
                'filename'=>$row['filename'], 
                'export_features'=> $exportFeatures ,
                'export_feature_codes'=> self::getFeatureCodes($exportFeatures),
                'suppliers_to_export'=> @unserialize($row['suppliers_to_export']) ? unserialize($row['suppliers_to_export']) : array(),
                );
        }
        return $out;
    }
    
    private static function getFeatureCodes($fIds) {
        $out = array();
        if( count($fIds) ) {
            $out = db_get_fields("SELECT feature_code FROM ?:product_features WHERE feature_id in (?a)", $fIds);
        }
        return $out;
    }
}

