<?php
namespace Tygh\ShopFeed\Admin;
/**
 * Description of ShopFeedMassOptChanger
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ShopFeedMassOptChanger {
    
    private $optName, $optType;
    private $matchedOpts=0;
    private $langCode;
    
    
    public function change($optName, $optType, $langCode) {
        $this->optName = $optName;
        $this->optType = $optType;
        $this->langCode = $langCode;
        $this->matchedOpts = 0;
        $opts = $this->getMatchingOpts();
        if( $opts ) {
            $this->matchedOpts = count($opts);
            $this->setType($opts);
        }
    }
    
    public function getMessage() {
        $msg = sprintf("<br />Option: %s <br />Type: %s<br />", $this->optName, $this->optType);
        $msg .= sprintf("Matched %s<br />",$this->matchedOpts);
        return $msg;
    }
    
    private function getMatchingOpts() {
        $rows = db_get_hash_array("select po.option_id, pod.option_name, po.is_shopfeed_option
            from cscart_product_options as po
            left join cscart_product_options_descriptions as pod on (po.option_id=pod.option_id and lang_code=?s)
            where pod.option_name=?s and po.status='A'",
                'option_id',
                $this->langCode,
                $this->optName
                );
        return $rows;
    }
    
    private function setType($opts) {
        $optIds = array_keys($opts);
        db_query("UPDATE ?:product_options set is_shopfeed_option=?s WHERE option_id in(?a)", $this->optType, $optIds);
    }
    
}
