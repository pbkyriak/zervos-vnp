<?php

/**
 *
 * @author Panos Kartpay <info@kartpay.com>
 * @since Sep 10, 2017
 */
use Tygh\Slx\Periods\PeriodsHelper;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

if ($mode == 'humanize') {
    $data = $_REQUEST['data'];
    unset($data[0]);
    $p = PeriodsHelper::humanizePeriods($data);
    if (count($p)) {
        $html = '<ul>';
        foreach ($p as $pi) {
            $html .= '<li>' . $pi . '</li>';
        }
        $html .= '</ul>';
    }
    $out = ['a' => $html];
    header("Content-type: application/json; charset=utf-8");
    echo json_encode($out);
    die();
}