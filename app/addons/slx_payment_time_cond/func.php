<?php

use Tygh\Slx\Periods\PeriodsHelper;

/**
 * ΠΡΟΣΟΧΗ! ΕΞΑΡΤΗΣΗ ΜΕ ΤΟ slx_paymentcond
 *
 * Επειδή είναι διαφορετικά τα hook στο 4.2 για να μην επαναλάβω τις βαρβαρότητες του slx_paymentcond
 * κόλλησα στο func.php sthn fn_slx_paymentcond_get_payment_methods να φέρνει και τα 2 πεδία και
 * να κάλεί την fn_slx_payment_time_cond_get_payments_post (δεν έχει hook sthn 4.2, αλλά σε επόμενες
 * εκδόσεις) για να κάνει τα unserialize
 *
 * @author Panos Kartpay <info@kartpay.com>
 * @since Sep 10, 2017
 */

function fn_slx_payment_time_cond_get_payments_post($params, &$payments) {
    foreach($payments as $p => $payment) {
        //fn_print_r($payment['time_cond']);
		if(is_array($payment) ) {
			if(!isset($payment['time_cond'])) {
				$payments[$p]['time_cond'] = PeriodsHelper::getEmptyTable();
			}
			else {
				$payments[$p]['time_cond'] = unserialize($payment['time_cond']);
				$payments[$p]['time_cond'] = PeriodsHelper::validateTable($payments[$p]['time_cond']);
			}
			$payments[$p]['time_cond_human'] = PeriodsHelper::humanizePeriods($payments[$p]['time_cond']);
		}
    }
}
function fn_slx_payment_time_cond_summary_get_payment_method($payment_id, &$payment) {
    if(!isset($payment['time_cond'])) {
        $payment['time_cond'] = PeriodsHelper::getEmptyTable();
    }
    else {
		if(!is_array($payment['time_cond']) ) {
        $payment['time_cond'] = unserialize($payment['time_cond']);
		}
        $payment['time_cond'] = PeriodsHelper::validateTable($payment['time_cond']);
    }
    $payment['time_cond_human'] = PeriodsHelper::humanizePeriods($payment['time_cond']);
}

function fn_slx_payment_time_cond_update_payment_pre(&$payment_data, $payment_id, $lang_code, $certificate_file, $certificates_dir) {
    if(isset($payment_data['time_cond'])) {
        $payment_data['time_cond']=PeriodsHelper::validateTable($payment_data['time_cond']);
    }
    else {
        $payment_data['time_cond']=PeriodsHelper::getEmptyTable();
    }
    $payment_data['time_cond'] = serialize($payment_data['time_cond']);
}


function fn_slx_payment_time_cond_prepare_checkout_payment_methods($cart, $auth, &$payment_groups) {
    if (!empty($payment_groups)) {
        $d = new DateTime();
        $d->setTimestamp(TIME);
        $testDay = $d->format('N');
        $testHour = $d->format('H');
        $total = !empty($cart['total']) ? $cart['total'] :0;
		//printf("Cart total=%s<br />", $total);
        foreach ($payment_groups as $tab_id => $payments) {
            foreach ($payments as $paymentId => $payment_data) {
				//var_dump($payment_data);
                if($payment_data['apply_time_cond']!='Y') {
                    continue;
                }
                $good = false;
                $tr= new \Tygh\Slx\Periods\TransTableToPeriods();
                $offPeriods = $tr->trans($payment_data['time_cond']);
                $good = $offPeriods->isInPeriod($testDay, $testHour);
                if (!$good) {
                    //fn_set_notification("W", "PCT", $paymentId);
                    unset($payment_groups[$tab_id][$paymentId]);
                    if (empty($payment_groups[$tab_id])) {
                        unset($payment_groups[$tab_id]);
                    }
                }
            }
        }
        ksort($payment_groups);
    }
    return true;
}

function fn_slx_payment_time_cond_checkout_select_default_payment_method(&$cart, &$payment_methods, &$completed_steps) {
    $pids = array();
    foreach ($payment_methods as $tab_id => $payments) {
        foreach ($payments as $paymentId => $payment_data) {
            $pids[] = $paymentId;
        }
    }
    if ($cart['payment_id'] == 0 || !in_array($cart['payment_id'], $pids)) {
        if ($pids) {
            $cart['payment_id'] = $pids[0];
        }
    }
    return true;
}
