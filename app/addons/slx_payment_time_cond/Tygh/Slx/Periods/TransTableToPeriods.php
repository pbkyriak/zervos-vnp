<?php

namespace Tygh\Slx\Periods;

use Tygh\Slx\Periods\Period;
use Tygh\Slx\Periods\Periods;

/**
 * Transforms a period table to a Periods collection.
 * Array index is week day [1,7], each element is two elements array
 * first element is starting hour (undefined=-1) and second element
 * is ending time (undefined=-1).
 *
 * Example of periods array:
 * $periods = [
 *    1 => [-1, 8],
 *    2 => [-1, -1],
 *    3 => [8, -1],
 *    4 => [-1, 16],
 *    5 => [-1, -1],
 *    6 => [16, -1],
 *    7 => [-1, -1],
 * ];
 *
 * The above array defines two periods,
 *  1. starting from Saturday (row 6) on 16:00 ending on Monday (row 1) on 08:00
 *  2. starting from Wed (row 3) on 08:00 ending on Tue (row 4) on 16:00
 *
 * That structure is easily implemented in an HTML form for user input and
 * easily serialized-unserialized for storing to a single database field.
 *
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 9, 2017
 */
class TransTableToPeriods {

    private $offPeriods;
    private $inPeriod;
    private $currPeriod;

    private function isOnlyEndRow($row) {
        return ($row[0] == -1 && $row[1] != -1);
    }

    private function isOnlyStartRow($row) {
        return ($row[0] != -1 && $row[1] == -1);
    }

    private function isStartEndRow($row) {
        return ($row[0] != -1 && $row[1] != -1);
    }

    private function addPeriod($sD = 0, $sH = -1, $eD = 0, $eH = -1) {
        $p = new Period();
        $p->startDay = $sD;
        $p->startHour = $sH;
        $p->endDay = $eD;
        $p->endHour = $eH;
        $this->currPeriod = $this->offPeriods->count();
        $this->offPeriods[$this->currPeriod] = $p;
        return $this->currPeriod;
    }

    private function startPeriod($sD, $sH) {
        $this->inPeriod = true;
        return $this->addPeriod($sD, $sH);
    }

    private function endPeriod($eD, $eH) {
        if ($this->inPeriod) {
            $this->offPeriods[$this->currPeriod]->endDay = $eD;
            $this->offPeriods[$this->currPeriod]->endHour = $eH;
        } else {
            $this->currPeriod = $this->addPeriod(0, -1, $eD, $eH);
        }
        $this->inPeriod = false;
        return $this->currPeriod;
    }

    private function fixForOverWeekPeriod() {
        if ($this->offPeriods->count() > 1) {
            if ($this->offPeriods[0]->isOnlyEnd() && $this->offPeriods[$this->currPeriod]->isOnlyStart()) {
                $this->offPeriods[0]->startDay = $this->offPeriods[$this->currPeriod]->startDay;
                $this->offPeriods[0]->startHour = $this->offPeriods[$this->currPeriod]->startHour;
                unset($this->offPeriods[$this->currPeriod]);
                end($this->offPeriods);
                $this->currPeriod = key($this->offPeriods);
            }
        }
    }

    public function checkForUnclosedPeriods() {
        foreach ($this->offPeriods as $i => $p) {
            if ($p->isOpen()) {
                printf("ERROR %s \n", $p);
            }
        }
    }

    public function trans($table) {
        $this->offPeriods = new Periods();
        $this->currPeriod = -1;
        $this->inPeriod = false;
        foreach ($table as $wd => $row) {
            if ($this->isOnlyEndRow($row)) {
                $this->endPeriod($wd, $row[1]);
            }
            if ($this->isStartEndRow($row)) {
                $this->startPeriod($wd, $row[0]);
                $this->endPeriod($wd, $row[1]);
            }
            if ($this->isOnlyStartRow($row)) {
                $this->startPeriod($wd, $row[0]);
            }
        }
        $this->fixForOverWeekPeriod();
        return $this->offPeriods;
    }

}
