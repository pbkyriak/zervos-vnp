<?php

namespace Tygh\Slx\Periods;

/**
 * A collection of Period objects
 * An array holds the data but is never directly accessed from
 * outsiders ArrayAccess and Iterator are implemented... maybe too much
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 9, 2017
 */
class Periods implements \ArrayAccess, \Iterator {

    protected $_data = array();

    // implementation of ArrayAccess interface

    public function offsetExists($offset) {
        return array_key_exists($offset, $this->_data);
    }

    public function offsetGet($offset) {
        return $this->_data[$offset];
    }

    public function offsetSet($offset, $value) {
        $this->_data[$offset] = $value;
    }

    public function offsetUnset($offset) {
        unset($this->_data[$offset]);
    }

    // implementation of Iterator interface

    public function current() {
        return current($this->_data);
    }

    public function key() {
        return key($this->_data);
    }

    public function next() {
        next($this->_data);
    }

    public function rewind() {
        reset($this->_data);
    }

    public function valid() {
        return isset($this->_data[$this->key()]);
    }

    public function count() {
        return count($this->_data);
    }

    /**
     * Checks if day and hour are in any of the periods in the collection
     *
     * @param integer $d    // day of week Monday=1... Sunday=7
     * @param integer $h    // 0..24
     */
    public function isInPeriod($d, $h) {
        $out = false;
        foreach($this->_data as $p) {
            if($p->isInPeriod($d,$h)) {
                $out = true;
                break;
            }
        }
        return $out;
    }

    /**
     * Dump periods in human readable format
     */
    public function dump() {
        $out = '';
        $out .= sprintf("--------------------------------\n");
        $out .= sprintf("Periods\n");
        $out .= sprintf("--------------------------------\n");
        foreach ($this->_data as $i => $p) {
            $out .= sprintf("[%s] %s\n", $i, $p);
        }
        $out .= sprintf("--------------------------------\n");
        return $out;
    }

    /**
     * Returns an array with the periods, can be used to create new Periods
     * using from array method.
     *
     * Example:
     * $arr = $offPeriods->toArray();
     * $newOffPeriods = new Periods();
     * $newOffPeriods->fromArray($arr);
     * $newOffPeriods->dump();
     *
     * @return array
     */
    public function toArray() {
        $out = [];
        foreach ($this->_data as $i => $p) {
           $out[] = [
               $p->startDay,
               $p->startHour,
               $p->endDay,
               $p->endHour,
           ];
        }
        return $out;
    }

    /**
     * Used to populate periods from an array of raw periods
     * exported using toArray() method.
     *
     * Example:
     * $arr = unserialize('a:2:{i:0;a:4:{i:0;i:6;i:1;i:16;i:2;i:1;i:3;i:8;}i:1;a:4:{i:0;i:3;i:1;i:8;i:2;i:4;i:3;i:16;}}');
     * $newOffPeriods = new Periods();
     * $newOffPeriods->fromArray($arr);
     * $newOffPeriods->dump();
     *
     * @param array $arr
     */
    public function fromArray($arr) {
        foreach($arr as $r) {
            $p = new Period();
            $p->startDay = $r[0];
            $p->startHour = $r[1];
            $p->endDay = $r[2];
            $p->endHour = $r[3];
            $this->_data[] = $p;
        }
    }
}