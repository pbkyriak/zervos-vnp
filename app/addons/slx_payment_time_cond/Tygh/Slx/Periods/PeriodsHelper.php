<?php

namespace Tygh\Slx\Periods;
use Tygh\Slx\Periods\TransTableToPeriods;
/**
 * Description of PeriodsHelper
 *
 * @author Panos Kartpay <info@kartpay.com>
 * @since Sep 10, 2017
 */
class PeriodsHelper {

    public static function getEmptyTable() {
        return [
            1 => [-1, -1],
            2 => [-1, -1],
            3 => [-1, -1],
            4 => [-1, -1],
            5 => [-1, -1],
            6 => [-1, -1],
            7 => [-1, -1],
        ];
    }

    /**
     * Checks if table has the required structure and values, if not
     * returns the EmptyTable.
     *
     * @param array $table
     */
    public static function validateTable($table) {
        $failed = false;
        if (is_array($table)) {
            foreach($table as $k=>$v) {
                if(!is_array($v) ) {
                    $failed = true;
                }
                else {
                    if(count($v)!=2) {
                        $failed=true;
                    }
                    else {
                        if(array_keys($v)!=[0,1]) {
                            $failed = true;
                        }
                    }
                }
            }
            if(array_keys($table)!= range(1, 7)) {
                $failed=true;
            }
        }
        else {
            $failed=true;
        }
        if (!$failed) {
            return $table;
        }
        return PeriodsHelper::getEmptyTable();
    }

    public static function humanizePeriods($table) {
        $tr = new TransTableToPeriods();
        $p = $tr->trans($table);
        $harr = $p->toArray();
        $out = [];
        foreach($harr as $r) {
            $out[] = __("time_cond_human", [
                '[sday]' => __("weekday_".($r[0]<7 ? $r[0] : 0)),
                '[stime]' => $r[1],
                '[eday]' => __("weekday_".($r[2]<7 ? $r[2] : 0)),
                '[etime]' => $r[3],
            ]);
        }
        return $out;
    }
    //Starting from [sday] on [stime]:00, ends on [eday] at [etime]:00
}
