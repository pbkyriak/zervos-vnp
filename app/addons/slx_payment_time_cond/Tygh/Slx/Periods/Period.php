<?php
namespace Tygh\Slx\Periods;

/**
 * Holds a single period.
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Sep 9, 2017
 */
class Period {

    public $startDay = 0;       // 0 if undefined, range 1-7 (Mon to Sun)
    public $startHour = -1;     // -1 if undefined, range 0-23
    public $endDay = 0;         // 0 if undefined, range 1-7 (Mon to Sun)
    public $endHour = -1;       // -1 if undefined, range 0-23

    /**
     * True if start or end not defined.
     *
     * @return boolean
     */
    public function isOpen() {
        $out = false;
        if (($this->startDay == 0 && $this->startHour == -1) || ($this->endDay == 0 && $this->endHour == -1)) {
            $out = true;
        }
        return $out;
    }

    /**
     * True if only start day and time are defined.
     *
     * @return boolean
     */
    public function isOnlyStart() {
        $out = false;
        if (($this->startDay != 0 && $this->startHour != -1) && ($this->endDay == 0 && $this->endHour == -1)) {
            $out = true;
        }
        return $out;
    }

    /**
     * True if only end day and time are defined.
     *
     * @return boolean
     */
    public function isOnlyEnd() {
        $out = false;
        if (($this->startDay == 0 && $this->startHour == -1) && ($this->endDay != 0 && $this->endHour != -1)) {
            $out = true;
        }
        return $out;
    }

    public function __toString() {
        return sprintf("Start D=%s H=%02d, End D=%s H=%02d", $this->startDay, $this->startHour, $this->endDay, $this->endHour);
    }

    /**
     * Converts period to range of week days.
     *
     * For example
     *  - if period is Mon -> Fri returns [1,2,3,4,5]
     *  - if period is Sat -> Mon returns [6,7,1]   (cross week period)
     *
     * @return array
     */
    private function getDates() {
        $out = false;
        if($this->startDay>$this->endDay) { // cross week
            $out = array_filter(array_merge(range($this->startDay,7), range(1,$this->endDay)));
        }
        else {
            $out = range($this->startDay, $this->endDay);
        }
        return $out;
    }

    /**
     * Returns time range of the dates in $dates array.
     * The idea is that dates are in sequence 1st, 2nd... for example
     * if period starts on Saturday 16:00 and ends on Monday 08:00 $dates array
     * has 6,7,1 (0: Sat, 1: Sun, 2: Mon). Taking array keys adding 1 we have 1,2,3
     * Then convert time to /100s as fraction to first and last day,
     * we get 1.16 and 3.08, applying the same transformation to test day/time
     * it will be either in the range [1.16, 3.08] and we call it IN, else
     * call it OUT.
     *
     * For example testing Sunday 14:00 the transformation is:
     * 1. get the index of the day in the period -> 1
     * 2. add 1 -> 1+1=2
     * 3. 2 + 14/100 = 2.14
     * Our time range is [1.16, 3.08], test value (2.14) is in range -> IN
     *
     * Testing Monday 09:00:
     * 1. get the index of the day in the period -> 2
     * 2. add 1 -> 2+1=3
     * 3. 3 + 9/100 = 3.09
     * Our time range is [1.16, 3.08], test value (3.09) is out of range -> OUT
     *
     * Testing Friday 09:00:
     * 1. get the index of the day in the period -> False -> OUT
     *
     * @param array $dates
     * @return array    Two elements. First element start time, second element end time
     */
    private function getTimesRange($dates) {
        $range = [];
        $range[] = 1+$this->startHour/100;
        $range[] = count($dates)+$this->endHour/100;
        return $range;
    }

    /**
     * Checks if $d, $h is in the range of that period
     *
     * @param integer $d
     * @param integer $h
     * @return boolean
     */
    public function isInPeriod($d, $h) {
        $out = false;
        $dates = $this->getDates();
        $dIdx = array_search($d, $dates);
        if( $dIdx!==false ){
            list($tI, $tF)=$this->getTimesRange($dates);
            $tH = $dIdx+1+$h/100;
            if( $tI<=$tH && $tH<=$tF) {
                $out=true;
            }
        }
        return $out;
    }
}
