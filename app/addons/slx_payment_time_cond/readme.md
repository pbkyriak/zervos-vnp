
Example usage of Periods package:
---------------------------------

use Tygh\Slx\Periods\TransTableToPeriods;

// Sample user defined table of periods
$periods = [
    1 => [-1, 8],
    2 => [-1, -1],
    3 => [8, -1],
    4 => [-1, 16],
    5 => [-1, -1],
    6 => [16, -1],
    7 => [-1, -1],
];

// transform user table to Periods collection
$tr = new TransTableToPeriods();
$offPeriods = $tr->trans($periods);
$offPeriods->dump();

// get current week day and hour from cs-cart's time constant (time zone defined in settings is applied)
$d = new DateTime();
$d->setTimestamp(TIME);
$testDay = $d->format('N');
$testHour = $d->format('H');
printf("Now = %s  %s %s\n", $d->format("d-m-Y H:i"), $testDay, $testHour);

// test if in any of the defined periods
if( $offPeriods->isInPeriod($testDay, $testHour) ) {
    printf("testing d=%s h=%s = %s\n", $testDay,$testHour,'YES');
}
else {
    printf("testing d=%s h=%s = %s\n", $testDay,$testHour,'NO');
}

