<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Panos Kartpay <info@kartpay.com>
 * @since Sep 10, 2017
 */

fn_register_hooks(
        'summary_get_payment_method',
        'update_payment_pre',
        'get_payments_post',
        'prepare_checkout_payment_methods'
        );
