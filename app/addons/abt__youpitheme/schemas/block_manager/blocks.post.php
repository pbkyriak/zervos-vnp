<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
$schema['banners']['templates']['addons/abt__youpitheme/blocks/abt__yt_banner_carousel_combined.tpl'] = array (
'settings' => array (
'margin' => array (
'option_name' => 'abt__yt.option.margin',
'type' => 'input',
'default_value' => '0'
),
'height' => array (
'option_name' => 'abt__yt.option.height',
'type' => 'input',
'default_value' => '400px'
),
'navigation' => array (
'type' => 'selectbox',
'values' => array (
'N' => 'none',
'D' => 'dots',
'P' => 'pages',
'A' => 'arrows'
),
'default_value' => 'D'
),
'delay' => array (
'type' => 'input',
'default_value' => '3'
),
),
);
$schema['banners']['templates']['addons/abt__youpitheme/blocks/abt__yt_banner_combined.tpl'] = array (
'settings' => array (
'margin' => array (
'option_name' => 'abt__yt.option.margin',
'type' => 'input',
'default_value' => '0'
),
'height' => array (
'option_name' => 'abt__yt.option.height',
'type' => 'input',
'default_value' => '400px'
),
),
);
return $schema;
