<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
$schema['blocks/products/abt__yt_products_multicolumns_with_banners.tpl'] = array (
'settings' => array(
'additional_data' => array(
'type' => 'template',
'template' => 'addons/abt__youpitheme/views/abt__yt/components/block_banners.tpl',
'default_value' => 2
),
),
'bulk_modifier' => array (
'fn_gather_additional_products_data' => array (
'products' => '#this',
'params' => array (
'get_icon' => true,
'get_detailed' => true,
'get_options' => true,
'get_additional' => true,
),
),
),
);
$schema['blocks/products/abt__yt_products_scroller_advanced_with_banners.tpl'] = array (
'settings' => array(
'show_price' => array (
'type' => 'checkbox',
'default_value' => 'Y'
),
'enable_quick_view' => array (
'type' => 'checkbox',
'default_value' => 'N'
),
'not_scroll_automatically' => array (
'type' => 'checkbox',
'default_value' => 'N'
),
'scroll_per_page' => array (
'type' => 'checkbox',
'default_value' => 'N'
),
'speed' => array (
'type' => 'input',
'default_value' => 400
),
'pause_delay' => array (
'type' => 'input',
'default_value' => 3
),
'item_quantity' => array (
'type' => 'input',
'default_value' => 5
),
'thumbnail_width' => array (
'type' => 'input',
'default_value' => 80
),
'outside_navigation' => array (
'type' => 'checkbox',
'default_value' => 'Y'
),
'additional_data' => array(
'type' => 'template',
'template' => 'addons/abt__youpitheme/views/abt__yt/components/block_banners_without_x2.tpl',
'default_value' => 2
),
),
'bulk_modifier' => array (
'fn_gather_additional_products_data' => array (
'products' => '#this',
'params' => array (
'get_icon' => true,
'get_detailed' => true,
'get_options' => true,
'get_additional' => true,
),
),
),
);
$tmpls = array(
'blocks/products/products_scroller.tpl',
'blocks/products/products_multicolumns.tpl',
'blocks/products/abt__yt_products_multicolumns_with_banners.tpl',
'blocks/products/abt__yt_products_scroller_advanced_with_banners.tpl',
);
if (!empty($tmpls)){
foreach ($tmpls as $tmpl) {
$schema[$tmpl]['bulk_modifier']['fn_abt__yt_add_products_features_list'] = array('products' => '#this',);
}
}
$schema['blocks/menu/dropdown_horizontal_abt__yt_mwi.tpl'] = array (
'settings' => array (
'abt_menu_long_names' => array(
'type' => 'checkbox',
'default_value' => 'N'
),
'abt_menu_long_names_max_width' => array (
'type' => 'input',
'default_value' => '100'
),
'dropdown_second_level_elements' => array (
'type' => 'input',
'default_value' => '12'
),
'abt_menu_icon_items' => array(
'type' => 'checkbox',
'default_value' => 'N'
),
'dropdown_third_level_elements' => array (
'type' => 'input',
'default_value' => '5'
),
),
);
$schema['blocks/menu/dropdown_vertical_abt__yt_mwi.tpl'] = array (
'settings' => array (
'dropdown_second_level_elements' => array (
'type' => 'input',
'default_value' => '12'
),
'abt_menu_icon_items' => array(
'type' => 'checkbox',
'default_value' => 'N'
),
'dropdown_third_level_elements' => array (
'type' => 'input',
'default_value' => '6'
),
'no_hidden_elements_second_level_view' => array(
'type' => 'input',
'default_value' => '5'
),
'elements_per_column_third_level_view' => array(
'type' => 'input',
'default_value' => '10'
),
'abt_menu_ajax_load' => array(
'type' => 'checkbox',
'default_value' => 'N'
),
),
);
return $schema;