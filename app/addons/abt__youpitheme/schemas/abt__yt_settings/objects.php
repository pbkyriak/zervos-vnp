<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
$schema = array (
array (
'section' => 'general',
'position' => 100,
'items' => array (
array (
'name' => 'brand_feature_id',
'type' => 'input',
'class' => 'input-small',
'position' => 100,
'value' => 18,
),
array (
'name' => 'load_more_products',
'type' => 'checkbox',
'position' => 200,
'value' => 'Y',
),
array (
'name' => 'view_cat_subcategories',
'type' => 'checkbox',
'position' => 300,
'value' => 'N',
),
array (
'name' => 'use_scroller_for_menu',
'type' => 'checkbox',
'position' => 400,
'value' => 'Y',
),
array (
'name' => 'menu_min_height',
'type' => 'input',
'class' => 'input-small',
'position' => 500,
'value' => 450,
),
),
),
array (
'section' => 'product_list',
'position' => 200,
'items' => array (
array (
'name' => 'height_list_prblock',
'type' => 'input',
'class' => 'input-small',
'position' => 50,
'value' => '440',
),
array (
'name' => 'grid_list_descr',
'type' => 'selectbox',
'class' => 'input-big',
'position' => 100,
'value' => 'features',
'variants' => array (
'none',
'description',
'features',
),
),
array (
'name' => 'max_features',
'type' => 'selectbox',
'class' => 'input-small',
'position' => 200,
'value' => '4',
'variants' => array (
'1',
'2',
'3',
'4',
),
'variants_as_language_variable' => 'N',
),
array (
'name' => 'show_sku',
'type' => 'checkbox',
'position' => 300,
'value' => 'N',
),
array (
'name' => 'show_amount',
'type' => 'checkbox',
'position' => 400,
'value' => 'Y',
),
array (
'name' => 'show_brand',
'type' => 'selectbox',
'class' => 'input-big',
'position' => 500,
'value' => 'none',
'variants' => array (
'none',
'as_text',
'as_logo',
),
),
array (
'name' => 'show_qty',
'type' => 'checkbox',
'position' => 600,
'value' => 'N',
),
array (
'name' => 'show_buttons',
'type' => 'checkbox',
'position' => 700,
'value' => 'Y',
),
array (
'name' => 'show_qty_discounts',
'type' => 'checkbox',
'position' => 800,
'value' => 'N',
),
),
),
array (
'section' => 'products',
'position' => 300,
'items' => array (
array (
'name' => 'id_block_con_right_col',
'type' => 'input',
'class' => 'input-small',
'position' => 200,
'value' => '75',
),
array (
'name' => 'tab_content_view_height',
'type' => 'input',
'class' => 'input-small',
'position' => 300,
'value' => '250',
),
),
),
);
return $schema;
