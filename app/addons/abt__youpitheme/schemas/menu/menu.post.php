<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
if (!defined('BOOTSTRAP')) {die('Access denied');}
use Tygh\Registry;
$schema['central']['ab__addons']['items']['abt__youpitheme'] = array(
'attrs' => array('class'=>'is-addon'),
'href' => 'abt__yt.help',
'position' => 2,
'subitems' => array(
'abt__yt.settings' => array(
'attrs' => array(
'class'=>'is-addon'
),
'href' => 'abt__yt.settings',
'position' => 100
),
'abt__yt.less_settings' => array(
'attrs' => array(
'class'=>'is-addon'
),
'href' => 'abt__yt.less_settings',
'position' => 200,
),
'abt__yt.microdata' => array(
'attrs' => array(
'class'=>'is-addon'
),
'href' => 'abt__yt.microdata',
'position' => 300,
),
'abt__yt.help' => array(
'attrs' => array(
'class'=>'is-addon'
),
'href' => 'abt__yt.help',
'position' => 400,
),
'abt__yt.demodata' => array(
'attrs' => array(
'class'=>'is-addon'
),
'href' => 'abt__yt.demodata',
'position' => 500,
),
),
);
$schema['central']['products']['items']['abt__yt_buy_together.generate'] = array(
'attrs' => array(
'class'=>'is-addon'
),
'href' => 'abt__yt_buy_together.generate',
'position' => 700,
);
$schema['central']['products']['items']['abt__yt_buy_together.manage'] = array(
'attrs' => array(
'class'=>'is-addon'
),
'href' => 'abt__yt_buy_together.manage',
'position' => 800,
);
return $schema;
