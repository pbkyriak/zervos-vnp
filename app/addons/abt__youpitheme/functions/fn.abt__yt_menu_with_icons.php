<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
function fn_abt__youpitheme_update_static_data (&$data, $param_id, $condition, $section, $lang_code){
if (Registry::get('runtime.mode') == 'update') {
$data['abt__yt_mwi__text'] = $_POST['static_data']['abt__yt_mwi__text'];
fn_attach_image_pairs('abt__yt_mwi__icon', 'abt__yt_mwi__icon', $param_id, $lang_code);
}
}
function fn_abt__youpitheme_get_static_data ($params, &$fields, $condition, $sorting, $lang_code){
$fields[] = '?:static_data_descriptions.abt__yt_mwi__desc';
$fields[] = '?:static_data_descriptions.abt__yt_mwi__text';
$fields[] = '?:static_data_descriptions.abt__yt_mwi__label';
$fields[] = 'sd.abt__yt_mwi__status';
$fields[] = 'sd.abt__yt_mwi__text_position';
$fields[] = 'sd.abt__yt_mwi__dropdown';
$fields[] = 'sd.abt__yt_mwi__label_color';
$fields[] = 'sd.abt__yt_mwi__label_background';
}
function fn_abt__youpitheme_top_menu_form_post (&$top_menu, $level, $active){
static $abt__yt_mwi_icon_get = 'N';
static $abt__yt_mwi_icon_ids = array();
if ($abt__yt_mwi_icon_get == 'N'){
$abt__yt_mwi_icon_get = 'Y';
$abt__yt_mwi_icon_ids = db_get_fields("SELECT object_id FROM ?:images_links WHERE object_type = 'abt__yt_mwi__icon'");
}
if ($abt__yt_mwi_icon_get == 'Y'
and !empty($abt__yt_mwi_icon_ids) and is_array($abt__yt_mwi_icon_ids)
and !empty($top_menu) and is_array($top_menu))
{
$ids = array();
foreach ($top_menu as $i => $m){
if (in_array($i, $abt__yt_mwi_icon_ids) and !empty($m['abt__yt_mwi__status']) and $m['abt__yt_mwi__status'] == 'Y'){
$ids[] = $i;
}
}
if (!empty($ids) and is_array($ids)){
$images = fn_get_image_pairs($ids, 'abt__yt_mwi__icon', 'M', true, false);
foreach ($images as $i => $image){
$img = array_shift($image);
$top_menu[$i]['abt__yt_mwi__icon'] = $img['icon'][fn_get_storefront_protocol() . '_image_path'];
}
}
}
}
function fn_abt__yt_ajax_menu_save ($data, $id, $lang_code = DESCR_SL){
static $init_cache = false;
$cache_name = 'abt__yt_am';
$key = $id . '_' . $lang_code;
if (!$init_cache) {
$init_cache = true;
Registry::registerCache($cache_name, array('static_data', 'static_data_descriptions'), Registry::cacheLevel('static'), true);
}
Registry::set($cache_name . '.' . $key , $data);
}
function fn_abt__yt_ajax_menu_get ($key){
static $init_cache = false;
$cache_name = 'abt__yt_am';
if (!$init_cache) {
$init_cache = true;
Registry::registerCache($cache_name, array('static_data', 'static_data_descriptions'), Registry::cacheLevel('static'), true);
}
static $data;
if (empty($data)){
$data = Registry::get($cache_name);
}
return isset($data[$key]) ? $data[$key] : '';
}
