<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
function fn_abt__youpitheme_dispatch_assign_template (){
Registry::set('settings.abt__yt', fn_get_abt__yt_settings());
}
function fn_get_abt__yt_settings($type = 'general', $full_info = false, $style = ''){
static $settings;
$lang_code = DESCR_SL;
$company_id = fn_get_runtime_company_id();
$cache_prefix = 'abt__youpitheme';
$key = "_{$lang_code}_" . md5('settings' . $type . ($full_info ? 'true' : 'false') . $style);
$cache_tables = array('abt__yt_settings', 'abt__yt_less_settings');
if (fn_allowed_for('MULTIVENDOR')) {
$cache_tables[] = 'companies';
}
Registry::registerCache(
array($cache_prefix, $key),
$cache_tables,
Registry::cacheLevel('static'),
true
);
if (empty($settings[$key])){
if ($cache = Registry::get($key)) {
$settings[$key] = $cache;
} else {
$schema_settings = fn_get_schema('abt__yt_settings', $type == 'general' ? 'objects' : 'less_objects');
usort($schema_settings, function ($a, $b){
if ($a['position'] > $b['position']) return 1;
elseif ($a['position'] < $b['position']) return -1;
else return strcasecmp($a['section'], $b['section']);
});
foreach ($schema_settings as &$schema_setting) {
usort($schema_setting['items'], function ($a, $b){
if ($a['position'] > $b['position']) return 1;
elseif ($a['position'] < $b['position']) return -1;
else return strcasecmp($a['name'], $b['name']);
});
}
if ($type == 'general'){
$db_settings = db_get_hash_multi_array("SELECT * FROM ?:abt__yt_settings WHERE lang_code = ?s ?p",
array("section", "name"), $lang_code, fn_get_company_condition('?:abt__yt_settings.company_id'));
}else{
$db_settings = db_get_hash_multi_array("SELECT * FROM ?:abt__yt_less_settings WHERE style = ?s ?p",
array("section", "name"), $style, fn_get_company_condition('?:abt__yt_less_settings.company_id'));
}
foreach ($schema_settings as $section) {
if (!empty($section['items']) and is_array($section['items'])){
$items = array();
foreach ($section['items'] as $i) {
if ($full_info){
$items[$i['name']] = $i;
if (!empty($db_settings[$section['section']][$i['name']])){
$items[$i['name']]['value'] = $db_settings[$section['section']][$i['name']]['value'];
}elseif ($type == 'less' and !empty($i['value_styles'][$style])){
$items[$i['name']]['value'] = $i['value_styles'][$style];
}
}else{
$items[$i['name']] = $i['value'];
if (!empty($db_settings[$section['section']][$i['name']])){
$items[$i['name']] = $db_settings[$section['section']][$i['name']]['value'];
}elseif ($type == 'less' and !empty($i['value_styles'][$style])){
$items[$i['name']] = $i['value_styles'][$style];
}
}
}
if (!empty($items)){
$settings[$key][$section['section']] = $items;
}
}
}
Registry::set($key, $settings[$key]);
}
}
return $settings[$key];
}
function fn_update_abt__yt_settings($data, $type = 'general', $style = ''){
$lang_code = DESCR_SL;
$company_id = fn_get_runtime_company_id();
$settings = fn_get_abt__yt_settings($type, true, $style);
foreach ($settings as $section => $items) {
foreach ($items as $name => $item) {
$v = (isset($data[$section][$name]) ? $data[$section][$name] : $item['value']);
if ($type == 'general'){
$d = array(
'section' => $section,
'name' => $name,
'company_id' => $company_id,
'lang_code' => $lang_code,
'value' => $v,
);
$n = db_get_field("SELECT name FROM ?:abt__yt_settings WHERE section = ?s AND name = ?s ?p LIMIT 1", $section, $name, fn_get_company_condition('?:abt__yt_settings.company_id'));
if (empty($n)){
foreach (fn_get_translation_languages() as $d['lang_code'] => $l) {
db_query('INSERT INTO ?:abt__yt_settings ?e', $d);
}
}else{
db_query("REPLACE INTO ?:abt__yt_settings ?e", $d);
}
$multilanguage = (!empty($item['multilanguage']) and $item['multilanguage'] == 'Y') ? 'Y' : 'N';
if ($multilanguage == 'N'){
db_query("UPDATE ?:abt__yt_settings SET value = ?s WHERE section = ?s AND name = ?s ?p", $v, $section, $name, fn_get_company_condition('?:abt__yt_settings.company_id'));
}
}elseif ($type == 'less'){
$d = array(
'style' => $style,
'section' => $section,
'name' => $name,
'company_id' => $company_id,
'value' => $v,
);
db_query("REPLACE INTO ?:abt__yt_less_settings ?e", $d);
$less_file = Registry::get('config.dir.design_frontend') . 'abt__youpitheme/styles/data/' . $style;
if (file_exists($less_file)){
$less = fn_get_contents($less_file);
if (!empty($less)){
if ($item['type'] == 'checkbox'){
$v = ($v == 'Y') ? 'true' : 'false';
}
$v .= (!empty($item['suffix'])) ? $item['suffix'] : '';
$less_var = '@abt__yt_' . $section . '_' . $name . ': ' . $v . ";\n";
if (preg_match('/@abt__yt_' . $section . '_' . $name . ':.*?;/m', $less)) {
$less = preg_replace('/(*ANYCRLF)@abt__yt_' . $section . '_' . $name . ':.*?;$/m', str_replace("\n", '', $less_var), $less);
} else {
$less .= $less_var;
}
fn_put_contents($less_file, $less);
}
}
}
}
}
return true;
}
