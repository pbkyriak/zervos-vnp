<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
function fn_abt__youpitheme_render_blocks(&$grid, &$block, $that, &$content){
if (AREA == 'C' and $grid['ab__show_in_tabs'] == 'Y' and $block['status'] == 'A') {
$block['tab_id'] = 'ab__grid_tab_' . $grid['grid_id'] . '_' . $block['block_id'];
$block['ab__use_ajax'] = $grid['ab__use_ajax'];
$tab_data = array('title' => $block['name']);
if ($grid['ab__use_ajax'] == 'Y') {
$tab_data['href'] = 'abt__yt_grid_tabs.load?block_id=' . $block['block_id'];
$tab_data['ajax'] = true;
$tab_data['ab__grid_tabs'] = true;
$block['first'] = empty($content);
} else {
$tab_data['js'] = true;
}
Registry::set('navigation.tabs.' . $block['tab_id'], $tab_data);
Registry::set('navigation.ab__grid_tab', 'Y');
}
}
function fn_abt__youpitheme_render_block_content_after($block_schema, $block, &$block_content){
if (AREA == 'C' and !empty($block['tab_id']) && !empty($block_content) && !defined('AJAX_REQUEST')) {
if ($block['ab__use_ajax'] != 'Y' || !empty($block['first'])) {
$block_content = '<div id="content_' . $block['tab_id'] . '">' . $block_content . '</div>';
} else {
$block_content = '<div id="content_' . $block['tab_id'] . '"><span></span></div>';
}
}
}
