<?php
/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
use Tygh\Registry;
if (!defined('BOOTSTRAP')) { die('Access denied'); }
foreach (glob(Registry::get("config.dir.addons") . "/abt__youpitheme/functions/*.php") as $functions) require_once $functions;
function fn_abt__yt_install (){
$objects = array(
array( "t" => "?:bm_grids",
"i" => array(
array("n" => "extended", "p" => "char(1) NOT NULL DEFAULT '0'",),
),
),
array( "t" => "?:bm_grids",
"i" => array(
array("n" => "ab__show_in_tabs", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "ab__use_ajax", "p" => "char(1) NOT NULL DEFAULT 'N'",),
),
),
array( "t" => "?:product_tabs",
"i" => array(
array("n" => "abt__yt__hide_content", "p" => "char(1) NOT NULL DEFAULT 'N'",),
),
),
array( "t" => "?:banners",
"i" => array(
array("n" => "abt__yt_use_avail_period", "p" => "char(1) NOT NULL DEFAULT 'N'", "add_sql" => array("ALTER TABLE ?:banners CHANGE `type` `type` CHAR(20) NOT NULL DEFAULT 'G'")),
array("n" => "abt__yt_avail_from", "p" => "int(11) NOT NULL DEFAULT '0'",),
array("n" => "abt__yt_avail_till", "p" => "int(11) NOT NULL DEFAULT '0'",),
array("n" => "abt__yt_button_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_button_text_color", "p" => "varchar(11) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_button_text_color_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_button_color", "p" => "varchar(11) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_button_color_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_title_font_size", "p" => "varchar(7) NOT NULL DEFAULT '18px'",),
array("n" => "abt__yt_title_color", "p" => "varchar(11) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_title_color_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_title_font_weight", "p" => "varchar(4) NOT NULL DEFAULT '300'",),
array("n" => "abt__yt_title_tag", "p" => "enum('div','h1','h2','h3') NOT NULL DEFAULT 'div'",),
array("n" => "abt__yt_title_shadow", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_description_font_size", "p" => "varchar(7) NOT NULL DEFAULT '13px'",),
array("n" => "abt__yt_description_color", "p" => "varchar(11) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_description_color_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_description_bg_color", "p" => "varchar(11) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_description_bg_color_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_object", "p" => "enum('image','video') NOT NULL DEFAULT 'image'",),
array("n" => "abt__yt_background_color", "p" => "varchar(11) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_background_color_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_class", "p" => "varchar(100) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_color_scheme", "p" => "enum('light','dark') NOT NULL DEFAULT 'light'",),
array("n" => "abt__yt_content_valign", "p" => "enum('top','center','bottom') NOT NULL DEFAULT 'center'",),
array("n" => "abt__yt_content_align", "p" => "enum('left','center','right') NOT NULL DEFAULT 'center'",),
array("n" => "abt__yt_content_full_width", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_content_bg", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_padding", "p" => "varchar(27) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_how_to_open", "p" => "enum('in_this_window','in_new_window','in_popup') NOT NULL DEFAULT 'in_this_window'",),
array("n" => "abt__yt_data_type", "p" => "enum('url','blog','promotion') NOT NULL DEFAULT 'url'",),
array("n" => "abt__yt_page_id", "p" => "int(11) NOT NULL DEFAULT '0'",),
array("n" => "abt__yt_youtube_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_youtube_autoplay", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_youtube_loop", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_youtube_hide_controls", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_product_list_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_product_list", "p" => "mediumtext",),
array("n" => "abt__yt_promotion_id", "p" => "int(11) NOT NULL DEFAULT '0'",),
array("n" => "abt__yt_countdown_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
),
),
array( "t" => "?:banner_descriptions",
"i" => array(
array("n" => "abt__yt_button_text", "p" => "varchar(50) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_title", "p" => "varchar(255) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_url", "p" => "varchar(255) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_description", "p" => "mediumtext",),
array("n" => "abt__yt_youtube_id", "p" => "varchar(15) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_youtube_playlist", "p" => "varchar(300)",),
array("n" => "abt__yt_product_list_title", "p" => "varchar(255) NOT NULL DEFAULT ''",),
),
),
array( "t" => "?:bm_blocks",
"i" => array(
array("n" => "abt__yt_banners_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_banner_max_position", "p" => "int(3) NOT NULL DEFAULT '100'",),
),
),
array( "t" => "?:categories",
"i" => array(
array("n" => "abt__yt_banners_use", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_banner_max_position", "p" => "int(3) NOT NULL DEFAULT '100'",),
),
),
array( "t" => "?:static_data",
"i" => array(
array("n" => "abt__yt_mwi__status", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_mwi__text_position", "p" => "varchar(32) NOT NULL DEFAULT 'bottom'",),
array("n" => "abt__yt_mwi__dropdown", "p" => "char(1) NOT NULL DEFAULT 'N'",),
array("n" => "abt__yt_mwi__label_color", "p" => "varchar(11) NOT NULL DEFAULT ''",),
array("n" => "abt__yt_mwi__label_background", "p" => "varchar(11) NOT NULL DEFAULT ''",),
),
),
array( "t" => "?:static_data_descriptions",
"i" => array(
array("n" => "abt__yt_mwi__desc", "p" => "mediumtext",),
array("n" => "abt__yt_mwi__text", "p" => "mediumtext",),
array("n" => "abt__yt_mwi__label", "p" => "varchar(100) NOT NULL DEFAULT ''",),
),
),
);
if (!empty($objects) and is_array($objects)){
foreach ($objects as $o){
$fields = db_get_fields('DESCRIBE ' . $o['t']);
if (!empty($fields) and is_array($fields)){
if (!empty($o['i']) and is_array($o['i'])){
foreach ($o['i'] as $f) {
if (!in_array($f['n'], $fields)){
db_query("ALTER TABLE ?p ADD ?p ?p", $o['t'], $f['n'], $f['p']);
if (!empty($f['add_sql']) and is_array($f['add_sql'])){
foreach ($f['add_sql'] as $sql) db_query($sql);
}
}
}
}
if (!empty($o['indexes']) and is_array($o['indexes'])){
foreach ($f['indexes'] as $index => $keys){
$existing_indexes = db_get_array("SHOW INDEX FROM " . $o['t'] . " WHERE key_name = ?s", $index);
if (empty($existing_indexes) and !empty($keys)){
db_query("ALTER TABLE ?p ADD INDEX ?p (?p)", $o['t'], $index, $keys);
}
}
}
}
}
}
db_query('ALTER TABLE ?:banner_descriptions MODIFY COLUMN abt__yt_youtube_playlist varchar(300)');
if (Registry::get('addons.buy_together.status') == 'A') {
db_query('ALTER TABLE ?:buy_together_descriptions MODIFY COLUMN name varchar(255)');
}
}
function fn_abt__youpitheme_get_products_post(&$products, $params, $lang_code){
if (AREA == "C" and Registry::get("addons.discussion.status") == "A" and empty($params['get_conditions']) and $products){
$company_cond = "";
if (Registry::ifGet('addons.discussion.product_share_discussion', 'N') == 'N') {
$company_cond = fn_get_discussion_company_condition('?:discussion.company_id');
}
$posts = db_get_hash_single_array("SELECT p.product_id, ifnull(count(dp.post_id),0) as discussion_amount_posts
FROM ?:discussion
INNER JOIN ?:products as p ON (?:discussion.object_id = p.product_id)
INNER JOIN ?:discussion_posts as dp ON (?:discussion.thread_id = dp.thread_id AND ?:discussion.object_type = 'P' ?p)
WHERE dp.status = 'A' and p.product_id in (?n)
GROUP BY p.product_id", array('product_id', 'discussion_amount_posts'), $company_cond, array_keys($products));
foreach ($products as $p_id => $p) {
$products[$p_id]['discussion_amount_posts'] = !empty($posts[$p_id]) ? $posts[$p_id] : 0;
}
}
}
function fn_abt__youpitheme_get_products($params, &$fields, $sortings, $condition, &$join, $sorting, $group_by, $lang_code, $having){
$settings = fn_get_abt__yt_settings();
$auth = & Tygh::$app['session']['auth'];
if (AREA == 'C' && $settings['product_list']['show_qty_discounts'] == 'Y') {
$join .= db_quote(' LEFT JOIN ?:product_prices AS opt_prices ON opt_prices.product_id = products.product_id AND opt_prices.lower_limit > 1 AND opt_prices.usergroup_id IN (?n)', $auth['usergroup_ids']);
$fields[] = ' (opt_prices.product_id IS NOT NULL) AS ab__is_qty_discount';
}
}
function fn_abt__youpitheme_get_pages($params, &$join, $condition, &$fields, $group_by, &$sortings, $lang_code){
if (!empty($params['page_type']) && $params['page_type'] == PAGE_TYPE_BLOG) {
$fields[] = "ifnull(?:banner_descriptions.abt__yt_youtube_id, '') as abt__yt_youtube_id";
$fields[] = "ifnull(?:banners.abt__yt_product_list, '') as abt__yt_product_list";
$join .= " LEFT JOIN ?:banners ON (?:banners.abt__yt_page_id = ?:pages.page_id
AND ?:banners.abt__yt_page_id > 0
AND ?:banners.status = 'A'
AND ?:banners.abt__yt_data_type = 'blog'
AND (
?:banners.abt__yt_youtube_use = 'Y'
OR (?:banners.abt__yt_product_list_use = 'Y' AND trim(?:banners.abt__yt_product_list) != '')
)
AND (
?:banners.abt__yt_use_avail_period = 'N'
OR
(
?:banners.abt__yt_use_avail_period = 'Y'
AND ?:banners.abt__yt_avail_from <= " . TIME . "
AND ?:banners.abt__yt_avail_till >= " . TIME . "
)
)
)";
$join .= db_quote(" LEFT JOIN ?:banner_descriptions ON (?:banner_descriptions.banner_id = ?:banners.banner_id
AND
?:banner_descriptions.lang_code = ?s
)", $lang_code);
}
}
function fn_abt__youpitheme_update_addon_status_post($addon, $status, $show_notification, $on_install, $allow_unmanaged, $old_status, $scheme){
if ($addon == 'buy_together' && $status == 'A') {
db_query('ALTER TABLE `?:buy_together_descriptions` MODIFY COLUMN `name` VARCHAR(255)');
}
}
