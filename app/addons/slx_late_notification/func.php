<?php
use Tygh\Registry;


function fn_settings_variants_addons_slx_late_notification_status_from() {
	return db_get_hash_single_array("SELECT s.status, sd.description FROM cscart_statuses s LEFT JOIN cscart_status_descriptions sd ON (s.status_id=sd.status_id AND sd.lang_code=?s) WHERE s.type='O'", array('status', 'description'), 'el');

}

function fn_settings_variants_addons_slx_late_notification_status_to() {
	return db_get_hash_single_array("SELECT s.status, sd.description FROM cscart_statuses s LEFT JOIN cscart_status_descriptions sd ON (s.status_id=sd.status_id AND sd.lang_code=?s) WHERE s.type='O'", array('status', 'description'), 'el');
}

function  fn_slx_late_notification_change_order_status($status_to, $status_from, $order_info, $force_notification, $order_statuses, $place_order) {
	if ($status_from == $status_to) {
		return;
	}
	db_query("UPDATE ?:orders SET status_update_timestamp = ?i WHERE order_id = ?i", time(), $order_info['order_id']);
}


function fn_slx_late_notification_get_late_orders() {

	$query = "SELECT order_id FROM cscart_orders WHERE late_notice_send='N' AND STATUS=?s AND status_update_timestamp!=0 AND status_update_timestamp< UNIX_TIMESTAMP()-?i*24*3600";
	$orders  = db_get_fields($query, 
			Registry::get("addons.slx_late_notification.status_from"),
			Registry::get("addons.slx_late_notification.late_days")
			);
	return $orders;
}

function fn_slx_late_notification_notify() {
	return;
	
	$orders = (fn_slx_late_notification_get_late_orders());
	foreach($orders as $order_id) {
		fn_change_order_status($order_id, Registry::get("addons.slx_late_notification.status_to"), '', true);
		db_query("UPDATE ?:orders SET late_notice_send='Y' WHERE order_id = ?i", $order_id);
	}
}

function fn_slx_late_notification_discount(){
    $orders = db_get_fields("SELECT order_id FROM cscart_orders WHERE STATUS='C' AND TIMESTAMP BETWEEN 1451606400 AND 1459382400");
	foreach($orders as $order_id) {
	   fn_change_order_status($order_id, 'J', '', true);
	}
}