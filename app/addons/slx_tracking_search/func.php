<?php

function fn_slx_tracking_search_get_orders($params, $fields, $sortings, &$condition, $join, $group) {

	if (isset($params['tracking_no']) && $params['tracking_no'] != '') {
	    //var_dump($params['tracking_no']);
		$track_no = str_replace('*', '%',trim($params['tracking_no']));
        $order_tracking_ids = db_get_fields("SELECT distinct si.order_id FROM ?:shipment_items as si left join ?:shipments as s on (s.shipment_id=si.shipment_id) WHERE s.tracking_number LIKE ?s" ,  $track_no );
		if( $order_tracking_ids ) {
			$condition .= db_quote(" AND ?:orders.order_id in (?n)", $order_tracking_ids);
		}
    }
	if (isset($params['phone_no']) && $params['phone_no'] != '') {
	    //var_dump($params['phone_no']);
		$phone_no = str_replace('*', '%',trim($params['phone_no']));
        $order_phone_no = db_get_fields("SELECT order_id FROM ?:orders WHERE phone LIKE ?s" ,  $phone_no );
		if( $order_phone_no ) {
			$condition .= db_quote(" AND ?:orders.order_id in (?n)", $order_phone_no);
		}
    }

}
