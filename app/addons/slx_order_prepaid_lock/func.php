<?php
use Tygh\Registry;

function fn_settings_variants_addons_slx_order_prepaid_lock_status_from() {
	return db_get_hash_single_array("SELECT s.status, concat(sd.description,' (', s.status,')') as description FROM cscart_statuses s LEFT JOIN cscart_status_descriptions sd ON (s.status_id=sd.status_id AND sd.lang_code=?s) WHERE s.type='O'", array('status', 'description'), 'el');
}

function fn_slx_order_prepaid_lock_change_order_status(&$status_to, $status_from, $order_info, $force_notification, $order_statuses, $place_order) {
	//$statuses = explode(',',Registry::get('addons.slx_order_prepaid_lock.status_from'));
	$stat1 = Registry::get('addons.slx_order_prepaid_lock.status_from');
	if(!is_array($stat1)) {
	    $stat1 = [];
	}
	$statuses = array_keys($stat1);
	if( !in_array($status_from, $statuses) ) {
		return;
	}
	if( $order_info['payment_mode']=='U' ) {
		fn_set_notification('W', 'Status change', __('prepaid_not_set'));
		$status_to = $status_from;
	}
	elseif( $order_info['payment_mode']=='Y' && (float)$order_info['prepayed_amount']==0 ) {
		fn_set_notification('W', 'Status change', __('prepaid_not_set_amount'));
		$status_to = $status_from;		
	}
}

function fn_slx_order_prepaid_lock_get_orders($params, $fields, $sortings, &$condition, &$join, $group) {

	if(!empty($params['is_prepaid']) ) {
		$condition .= db_quote(" AND ?:orders.payment_mode='Y'");
	}
	
	//
	if(!empty($params['supplier_id'])) {
        if (empty($params['p_ids']) && empty($params['product_view_id'])) {
            $join .= " LEFT JOIN ?:order_details ON ?:order_details.order_id = ?:orders.order_id";
        }
		$join .= " LEFT JOIN ?:supplier_links ON ?:order_details.product_id = ?:supplier_links.object_id and ?:supplier_links.object_type='P'";
		$condition .= " AND ?:supplier_links.supplier_id=".$params['supplier_id'];
	}
	//

}

function fn_slx_order_prepaid_lock_get_orders_totals($paid_statuses, $join, $condition, $group) {
	$prepaid_total = db_get_field("SELECT sum(t.prepayed_amount) FROM ( SELECT prepayed_amount FROM ?:orders $join WHERE 1 $condition $group) as t");
	Registry::get('view')->assign('prepaid_total', $prepaid_total);
}