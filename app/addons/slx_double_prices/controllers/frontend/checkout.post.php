<?php

use Tygh\Registry;
use Tygh\Storage;
use Tygh\Session;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_enable_checkout_mode();

fn_define('ORDERS_TIMEOUT', 60);

// Cart is empty, create it
if (empty($_SESSION['cart'])) {
    fn_clear_cart($_SESSION['cart']);
}

$cart = & $_SESSION['cart'];


if($mode=='checkout' || $mode='update_steps' ) {
	///if( !empty($cart['user_data']['user_id']) ) {
		$is_timologio = isset($_REQUEST['user_data']['fields'][59]) 
						? $_REQUEST['user_data']['fields'][59] 
						: (isset($cart['user_data']['fields'][59]) 
							? $cart['user_data']['fields'][59]
							: 10
							);
		
			if( $is_timologio==9 ) {
				$cnt =0 ;
				foreach($cart['products'] as $item => $product) {
					if(fn_slx_douple_prices_is_product_category_invoiced($product['product_id'])) {
						if(empty($cart['products'][$item]['invoiced'])) {			
							$cart['products'][$item]['price'] = fn_slx_douple_prices_get_invoiced_price($cart['products'][$item]['price'],$product['product_id']);
							$cart['products'][$item]['base_price'] = fn_slx_douple_prices_get_invoiced_price($cart['products'][$item]['base_price'],$product['product_id']);
							$cart['products'][$item]['display_price'] = fn_slx_douple_prices_get_invoiced_price($cart['products'][$item]['display_price'],$product['product_id']);
							$cart['products'][$item]['stored_price'] = 'Y';
							$cart['products'][$item]['regular_price'] = $cart['products'][$item]['price'];
							$cart['products'][$item]['invoiced'] = 'Y';
							$cnt++;
						}
					}
				}
				$cart['recalculate'] = true;
				fn_calculate_cart_content($cart, $auth, 'A', true, 'F', true);
				$cart['payment_surcharge'] = 0;
				if (!empty($cart['payment_id']) ) {
					fn_update_payment_surcharge($cart, $auth);
				}
				if($cnt!=0 ) {
					fn_set_notification("N", __("notice"), __("invoice_prices_applied"));
				}
			}
			else {
				$cnt =0;
				foreach($cart['products'] as $item => $product) {
					if(!empty($cart['products'][$item]['invoiced'])) {			
						$cart['products'][$item]['stored_price'] = 'N';
						unset($cart['products'][$item]['regular_price']);
						unset($cart['products'][$item]['invoiced']);
						$cnt++;
					}
				}
				$cart['recalculate'] = true;
				fn_calculate_cart_content($cart, $auth, 'A', true, 'F', true);
				
				$cart['payment_surcharge'] = 0;
				if (!empty($cart['payment_id']) ) {
					fn_update_payment_surcharge($cart, $auth);
				}
				if($cnt!=0 ) {
					fn_set_notification("N", __("notice"), __("invoice_prices_removed"));
				}
			}
	
	//}
}
