<?php
namespace Tygh\Slx\DoublePrices;
use Tygh\Registry;
class DouplePriceCategories {
	private static $instance=null;
	
	private $categories = [
		1052 => 5,  // smartphones
		2267 => 1,  // laptop
		2197 => 5,  // tablets
	];

	
	public $lastCheckedCID = false;
	public $lastCheckedPID = false;
	
	public static function getInstance() {
		if(!is_object(self::$instance) ) {
			self::$instance = new DouplePriceCategories();
		}
		return self::$instance;
	}
	
	public function getCategoryIds() {
		return array_keys($this->categories);
	}
	
	public function getCategoryFactor($catId) {
		//printf("fc=%s",$catId);
		if(isset($this->categories[$catId])) {
			return $this->categories[$catId];
		}
		return Registry::get('addons.slx_double_prices.price_multiplier');
	}

	public function IsProductCategoryInvoiced($product_id) {
		
		if( $this->lastCheckedPID == $product_id ) {
			if($this->lastCheckedCID!==false) {
				return true;
			}
			return false;
		}
		
		$id_path = db_get_field("select b.id_path from cscart_products_categories a left join cscart_categories b on a.category_id=b.category_id where product_id=?i and a.link_type='M'", $product_id);
		$ids = explode('/',$id_path);
		$out = false;
		$targets = array_keys($this->categories);
		$sect = array_intersect($targets,$ids);
		if($sect) {
			$this->lastCheckedCID = reset($sect);
			
			$this->lastCheckedPID = $product_id;
			$out = true;
		}
		else {
			$this->lastCheckedCID = false;
			$this->lastCheckedPID = $product_id;
		}
		//printf("lci=%s\n",$this->lastCheckedCID);
		return $out;
	}
}