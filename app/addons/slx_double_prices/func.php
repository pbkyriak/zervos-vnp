<?php
/**
 * (c) 2010-2015 Panos Kyriakakis
 * (c) 2010-2015 Andreas Morfopoulos
 * 
 * For any inquires please contact weblive.gr 210 2519282.
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 * @author Andreas Morfopoulos <an at weblive.gr>
 */
use Tygh\Registry;
use Tygh\Slx\DoublePrices\DouplePriceCategories;

function fn_slx_double_prices_multiplier ($price, $product_id) {
	/*
	$factor = Registry::get('addons.slx_double_prices.price_multiplier');
	if(fn_slx_douple_prices_is_product_category_invoiced($productId)) {
		$factor = 5;
	}
    
	*/
	$ddp = DouplePriceCategories::getInstance();
	$ddp->IsProductCategoryInvoiced($product_id);
	$factor = $ddp->getCategoryFactor($ddp->lastCheckedCID);
//	$price = round($price * (1+(float) $factor / 100),2);   // me pososto %
	$price = round($price + $factor,2);    // me poso
    return $price; 
}

function fn_slx_douple_prices_is_product_category_invoiced($product_id) {
	$ddp = DouplePriceCategories::getInstance();
	$out = $ddp->IsProductCategoryInvoiced($product_id);
	/**
	$id_path = db_get_field("select b.id_path from cscart_products_categories a left join cscart_categories b on a.category_id=b.category_id where product_id=?i and a.link_type='M'", $product_id);
	$ids = explode('/',$id_path);
	$out = false;
	$targets = ['1052', '2267','2197'];
	if(array_intersect($targets,$ids)) {
		$out = true;
	}
	*/
	return $out;
}
/*

sto step_one.tpl grammes 77,80 allaksan gia na paizei to ajax me to profile.

*/
function fn_slx_douple_prices_get_invoiced_price($price, $productId) {
	$price = round(fn_slx_double_prices_multiplier($price, $productId)/1.24,2);
	return $price;
}

