<?php

/**
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Αυγ 2014
 */

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

$cart = & $_SESSION['cart'];

if ($mode == 'checkout') {
    if (!empty($_REQUEST['payment_id'])) {
        $period = fn_get_min_installments($_REQUEST['payment_id']);
        
        if (!empty($_REQUEST['period'])) {
            $period = (int)$_REQUEST['period'];
        }
        $cart['payment_period'] = $period;
    }
}
