<?php

/**
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Αυγ 2014
 */

use Tygh\Http;
use Tygh\Registry;
use Tygh\Database;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    //
    // Update payment method
    //
    if ($mode == 'update') {
        if( isset($_REQUEST['insts_data']) ) {
            fn_update_payment_installments($_REQUEST['payment_id'],$_REQUEST['insts_data']);
        }
    }

    return array(CONTROLLER_STATUS_OK, "payments.manage");
}
else {
    //fn_settings_variants_addons_product_block_payment_id();
}
function fn_update_payment_installments($payment_id, $insts_data) {
    $data = [];
    foreach ($insts_data as $k => $_data) {
        $row = [
        'installments' => intval($_data['installments']),
        'p_interest' => floatval($_data['p_interest']),
        'a_interest' => floatval($_data['a_interest']),
        'amount_from' => floatval($_data['amount_from']),
        ];
        if($row['installments']>0) {
            $data[$row['installments']] = $row;
        }
    }
    $processor_data = fn_get_processor_data($payment_id);
    $processor_data['processor_params']['installments'] = $data;
    $processor_params = serialize($processor_data['processor_params']);
    db_query("update ?:payments set processor_params=?s where payment_id=?i", $processor_params, $payment_id);
}