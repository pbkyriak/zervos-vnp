<?php

/**
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 1 Αυγ 2014
 * @since 16 Mar 2018 refactorings for 4.6+ series
 */


function fn_settings_variants_addons_slx_payments_product_block_payment_id() {
    $params = array(
        'simple' => true,
        'lang_code' => CART_LANGUAGE,
    );
    $payments = fn_get_payments($params);

    return $payments;

}

function fn_slx_payments_calculate_cart(&$cart, $cart_products, $auth, $calculate_shipping, $calculate_taxes, $apply_cart_promotions) {
    if( !isset($cart['payment_id']) ) {
        return true;
    }
    $user_data = $auth;
    if(!isset($cart['interest'])) {
        $cart['interest'] = 0;
    }
    $cart['interest']=0;
    $payment_params = slx_payments_get_payment_installments($cart['payment_id']);
    if ( count($payment_params)>0 ) {
        $period = 1;
        if(isset($cart['payment_period']) ) {
            $period = (int)$cart['payment_period'];
        }
        $cart['interest'] = fn_check_installment(
            (float) $cart['total'],
            $period,
            $cart['payment_id']
        );
        //fn_save_cart_content($cart, $user_data['user_id']);
    }
    $cart['total'] = $cart['total'] + $cart['interest'];
//        printf("period=%s interest=%s total=%s<br />",$cart['payment_period'],$cart['interest'],$cart['total']);

}

function fn_slx_payments_form_cart($order_info, &$cart) {
	$cart['payment_period'] = isset($order_info['payment_period']) ? $order_info['payment_period'] : 1;
	$cart['interest'] = isset($order_info['interest']) ? $order_info['interest'] : 0;
}

function fn_slx_payments_update_cart_by_data_post(&$cart, $new_cart_data, $auth) {
	$cart['payment_period'] = isset($new_cart_data['payment_period']) ? $new_cart_data['payment_period'] : 1;
	$cart['interest'] = isset($new_cart_data['interest']) ? $new_cart_data['interest'] : 0;
	$cart['total'] = $cart['total'] + $cart['interest'];
}

function slx_payments_get_payment_installments($payment_id) {
    $processor_data = fn_get_processor_data($payment_id);
    $installments = [];
    if(isset($processor_data['processor_params']['installments'])) {
        if(is_array($processor_data['processor_params']['installments'])) {
            $installments = $processor_data['processor_params']['installments'];
        }
    }
    return $installments;
}

function fn_get_min_installments($payment_id) {
    $out = 1;
    $installments = slx_payments_get_payment_installments($payment_id);
    $first = reset($installments);
    if(is_array($first)) {
        if(isset($first['installments'])) {
            $out = $first['installments'];
        }
    }
    return $out;
}

function fn_check_installmnets($total = 0, $payment_id)  {
    $total = (float)$total;
    $processor_data = fn_get_processor_data($payment_id);
    $installments = slx_payments_get_payment_installments($payment_id);
    $out = [];
    foreach($installments as $inst) {
        if($inst['amount_from']<=$total) {
            $out[] = $inst['installments'];
        }
    }
    return $out;
	//
	//return db_get_fields("SELECT installments FROM ?:installments WHERE payment_id=?i AND (`amount_from` <= $total ) ORDER BY installments ASC ", $payment_id);
}

function fn_check_installment($total = 0, $installment, $payment_id) {
	if( is_array($payment_id) )
		$payment_id = $payment_id['payment_id'];
    $processor_data = fn_get_processor_data($payment_id);
    $installments = slx_payments_get_payment_installments($payment_id);
    $_insts = false;
    foreach($installments as $inst) {
        if($inst['amount_from']<=$total && $inst['installments']==$installment) {
            $_insts = $inst;
            break;
        }
    }
	$_interest = 0;

	if (!empty($_insts['amount_from']) ) {
		if (floatval($_insts['a_interest'])) {
			$_interest += $_insts['a_interest'];
		}
		if (floatval($_insts['p_interest'])) {
			$_interest += fn_format_price($total * $_insts['p_interest'] / 100);
		}
	} 
	return $_interest;
}

function slx_payments_get_modirum_urls() {
    return [
        'Eurobank Test' => 'https://euro.test.modirum.com/vpos/shophandlermpi',
        'Eurobank Live' => 'https://euro.live.modirum.com/vpos/shophandlermpi',
        'Alphabank Test' => 'https://alpha.test.modirum.com/vpos/shophandlermpi',
        'Alphabank Live' => 'https://alpha.live.modirum.com/vpos/shophandlermpi',
    ];
}
function slx_payments_get_modirum_banks() {
    return [
        'Eurobank' => 'Eurobank',
        'Alpha Bank' => 'Alpha Bank',
    ];
}
