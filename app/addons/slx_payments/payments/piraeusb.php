<?php

if (!defined('AREA')) {
    die('Access denied');
}

require_once(DIR_LIB . 'nusoap/nusoap.php');

/*
  [transactionid] => 4358450
  [merchantreference] => 640_2
  [responsecode] => 00
  [responsedescription] => Approved or completed successfully
  [retrievalref] => 000345756257
  [approvalcode] => 153152
  [errorcode] => 0
  [errordescription] =>
  [amount] => 0.10
  [installments] => 0
  [cardtype] =>
  [langid] => 1
  [parameters] =>

  «00», «08», «10», «11» or «16».
 */

if (defined('PAYMENT_NOTIFICATION')) {

    if ($mode == 'notify') {

        $order_id = (strpos($_REQUEST['MerchantReference'], '_')) ? substr($_REQUEST['MerchantReference'],
                0, strpos($_REQUEST['MerchantReference'], '_')) : $_REQUEST['MerchantReference'];
        $order_info = fn_get_order_info($order_id);
        $processor_data = fn_get_payment_method_data($order_info['payment_id']);

        $pp_response['SupportReferenceID'] = $_REQUEST['SupportReferenceID'];
        $pp_response['ResultCode'] = $_REQUEST['ResultCode'];
        $pp_response['ResultDescription'] = $_REQUEST['ResultDescription'];
        $pp_response['StatusFlag'] = $_REQUEST['StatusFlag'];
        $pp_response['ResponseCode'] = $_REQUEST['ResponseCode'];
        $pp_response['ResponseDescription'] = $_REQUEST['ResponseDescription'];
        $pp_response['TransactionDateTime'] = $_REQUEST['TransactionDateTime'];
        $pp_response['TransactionId'] = $_REQUEST['TransactionId'];
        $pp_response['CardType'] = $_REQUEST['CardType'];
        $pp_response['PackageNo'] = $_REQUEST['PackageNo'];
        $pp_response['ApprovalCode'] = $_REQUEST['ApprovalCode'];

        $pp_response['RetrievalRef'] = $_REQUEST['RetrievalRef'];
        $pp_response['AuthStatus'] = $_REQUEST['AuthStatus'];
        $pp_response['Parameters'] = $_REQUEST['Parameters'];

        if ($_REQUEST['ResultCode'] == 0) {
            if ($_REQUEST['StatusFlag'] == 'Success' && in_array($_REQUEST['ResponseCode'],
                    array('00', '08', '10', '11', '16'))) {

                $cart_hashkey_str = $order_info['payment_info']['TranTicket'] . $processor_data['params']['posid'] . $processor_data['params']['acquirerid'] . $_REQUEST['MerchantReference'] . $_REQUEST['ApprovalCode'] . $_REQUEST['Parameters'] . $_REQUEST['ResponseCode'] . $_REQUEST['SupportReferenceID'] . $_REQUEST['AuthStatus'] . $_REQUEST['PackageNo'] . $_REQUEST['StatusFlag'];
                $cart_hashkey = strtoupper(hash('sha256', $cart_hashkey_str));

                if ($cart_hashkey == $_REQUEST['HashKey']) {
                    $pp_response['order_status'] = 'P';
                    $pp_response['reason_text'] = 'Processed';
                } else {
                    $pp_response['order_status'] = 'F';
                    $pp_response['reason_text'] = 'Hash value is incorrect';
                }
            } elseif ($_REQUEST['StatusFlag'] == 'Failure') {
                $pp_response['order_status'] = 'F';
                $pp_response['reason_text'] = 'Failed';
            } elseif ($_REQUEST['StatusFlag'] == 'Asynchronous') {
                $pp_response['order_status'] = 'O';
                $pp_response['reason_text'] = 'Asynchronous';
            }
        } else {
            $pp_response['order_status'] = 'F';
            $pp_response['reason_text'] = $_REQUEST['ResultDescription'];
        }
    }

    if (fn_check_payment_script('piraeusc.php', $order_id)) {
        fn_finish_payment($order_id, $pp_response, false);
    }

    fn_order_placement_routines($order_id);
} else {

    $ticketing_url = "https://paycenter.winbank.gr/services/tickets/issuer.asmx?WSDL";
    $payment_info = $order_info['payment_info'];
    $installments = $processor_data['params']['installments'];
    //var_dump($order_info);
    if (!empty($payment_info['period']) && $payment_info['period'] > 1) {
        $installments = $payment_info['period'];
    }

    $ticketing_data = Array(
        'AcquirerId' => $processor_data['params']['acquirerid'],
        'MerchantId' => $processor_data['params']['merchantid'],
        'PosId' => $processor_data['params']['posid'],
        'Username' => $processor_data['params']['username'],
        'Password' => md5($processor_data['params']['password']),
        'RequestType' => $processor_data['params']['requesttype'],
        'CurrencyCode' => $processor_data['params']['currencycode'],
        'MerchantReference' => (($order_info['repaid']) ? ($order_id . '_' . $order_info['repaid']) : $order_id),
        'Amount' => $order_info['total'],
        'Installments' => $installments,
        'ExpirePreauth' => (($processor_data['params']['requesttype'] == '00') ? $processor_data['params']['expirepreauth'] : '0'),
        'Bnpl' => 0,
        'Parameters' => ''
    );

    $ticketing_soapaction = "http://piraeusbank.gr/paycenter/redirection/IssueNewTicket";

    $ticketing_client = new nusoap_client($ticketing_url, 'wsdl');

    $ticketing_err = $ticketing_client->getError();

    if ($ticketing_err) {
        $pp_response['order_status'] = 'F';
        $pp_response["reason_text"] = 'Constructor error: ' . $ticketing_err . '; Debug: ' . htmlspecialchars($ticketing_client->getDebug(),
                ENT_QUOTES);
    } else {

        $ticketing_soapmsg = $ticketing_client->serializeEnvelope('
<IssueNewTicket xmlns="http://piraeusbank.gr/paycenter/redirection">
	<Request>
		<Username>' . $ticketing_data['Username'] . '</Username>
		<Password>' . $ticketing_data['Password'] . '</Password>
		<MerchantId>' . $ticketing_data['MerchantId'] . '</MerchantId>
		<PosId>' . $ticketing_data['PosId'] . '</PosId>
		<AcquirerId>' . $ticketing_data['AcquirerId'] . '</AcquirerId>
		<MerchantReference>' . $ticketing_data['MerchantReference'] . '</MerchantReference>
		<RequestType>' . $ticketing_data['RequestType'] . '</RequestType>
		<ExpirePreauth>' . $ticketing_data['ExpirePreauth'] . '</ExpirePreauth>
		<Amount>' . $ticketing_data['Amount'] . '</Amount>
		<CurrencyCode>' . $ticketing_data['CurrencyCode'] . '</CurrencyCode>
		<Installments>' . ($ticketing_data['Installments'] + 0) . '</Installments>
		<Bnpl>' . $ticketing_data['Bnpl'] . '</Bnpl>
		<Parameters>' . $ticketing_data['Parameters'] . '</Parameters>
	</Request>
</IssueNewTicket>', '', array(), 'document', 'literal');
//var_dump($ticketing_data);
        $ticketing_result = $ticketing_client->send($ticketing_soapmsg,
            $ticketing_soapaction);

        if ($ticketing_client->fault) {
            $pp_response['order_status'] = 'F';
            $pp_response["reason_text"] = $ticketing_result;
        } else {

            $ticketing_err = $ticketing_client->getError();
            if ($ticketing_err) {
                $pp_response['order_status'] = 'F';
                $pp_response["reason_text"] = $ticketing_err;
            } else {

                $ticketing_result = $ticketing_result['IssueNewTicketResult'];

                if ($ticketing_result['ResultCode'] == 0) {
                    $pp_response = $ticketing_result;
                    fn_update_order_payment_info($order_id, $pp_response);

                    $post_url = 'https://paycenter.winbank.gr/redirection/pay.aspx';

                    $post_data = Array(
                        'AcquirerId' => $processor_data['params']['acquirerid'],
                        'MerchantId' => $processor_data['params']['merchantid'],
                        'PosId' => $processor_data['params']['posid'],
                        'User' => $processor_data['params']['username'],
                        'LanguageCode' => $processor_data['params']['languagecode'],
                        'MerchantReference' => (($order_info['repaid']) ? ($order_id . '_' . $order_info['repaid']) : $order_id),
                        'ParamBackLink' => ""
                    );

                    fn_create_payment_form(
                        $post_url,
                        $postData, 
                        'WinBank',
                        false
                    );
                    exit;
                } else {
                    $pp_response = $ticketing_result;
                    $pp_response['order_status'] = 'F';
                    $pp_response["reason_text"] = $ticketing_result['ResultDescription'];
                }
            }
        }
    }
}
