<?php
use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if (defined('PAYMENT_NOTIFICATION')) {

    $orderId = 0;
	if(!empty($_REQUEST['orderid'])) {
		$tok = explode('opn',$_REQUEST['orderid']);
		$orderId = (int)$tok[0];
	}
    if ($orderId) {
        $pp_response = array();
        $pp_response['order_status'] = 'F';
        $pp_response['reason_text'] = __('text_transaction_declined');
        /* build digest */
        $post_data = array();
        
        $post_data_values = array(
            'mid',
            'orderid',
            'status',
            'orderAmount',
            'currency',
            'paymentTotal',
            'riskScore',
            'payMethod',
            'txId',
            'paymentRef'
        );

        foreach ($post_data_values as $post_data_value) {
            if (isset($_REQUEST[$post_data_value])) {
                $post_data[] = $_REQUEST[$post_data_value];
            }
        }

		$digest = base64_encode(sha1(implode('', $post_data) . $processor_data['processor_params']['shared_secret'], true));
        $postedDigest = $_REQUEST['digest'];
        $digestMatch = ( $digest==$postedDigest);
        $digestMatch = true; // !!!
        /* end build digest */
        
        if ($mode == 'success' && $digestMatch ) {
            $pp_response['order_status'] = 'O';
            $pp_response['reason_text'] = __('order_id') . '-' . $_REQUEST['orderid'];
        } 
        elseif ($mode == 'failed') {
            $pp_response['order_status'] = 'D';
            $pp_response['reason_text'] = __('payment_failed');
        }

        if (fn_check_payment_script('modirumb.php', $orderId)) {
            fn_finish_payment($orderId, $pp_response, false);
            fn_order_placement_routines('route', $orderId);
        }
    }
    else {
        fn_set_notification('E', __('error'), __('connection_error'));
        fn_redirect('checkout.checkout');
    }
} else {
    
    $postData = array();
    //var_dump($processor_data);die(123);
    $postData['mid']=$processor_data['processor_params']['merchantid'];
	
    $languages = array( 'el', 'EL' );
	if (in_array( DESCR_SL, $languages )) {
		$postData['lang'] = 'el';
	}
	else {
		$postData['lang'] = 'en';
	}
    
    $postData['deviceCategory']='0';
        
    $postData['orderid'] = sprintf("%sopn%s",$order_id, rand());
    $postData['orderDesc'] = $processor_data['processor_params']['payment_details'] .' '. sprintf("%s - %s", 'Order', $order_id);

    $postData['orderAmount'] = str_replace('.', ',', $order_info["total"]);
    $postData['currency'] = $processor_data['processor_params']['currency'];
    
    $postData['payerEmail']=$order_info['email'];
        
	//$period = $order_info['payment_info']['payment_period'];
	$period = $order_info['payment_period'];
	//var_dump($order_info['payment_method']['installments']);
	if (!empty($period) && $period > 1) {
        $postData['extInstallmentoffset'] = '0';
		$postData['extInstallmentperiod'] = $period;
	}

    if(!empty($processor_data['processor_params']['css_gateway']) ) {
        $postData['cssUrl'] = $processor_data['processor_params']['css_gateway'];
    }
    $sid = session_id();
	$postData['confirmUrl'] = fn_url("payment_notification.success?payment=modirumb&orderid=".$postData['orderid'], AREA, 'current');
	$postData['cancelUrl'] = fn_url("payment_notification.failed?payment=modirumb&orderid=".$postData['orderid'], AREA, 'current');
	
    $password = $processor_data['processor_params']['sharedSecretKey'];
    $cargo = implode("", $postData);
    $postData['digest'] = base64_encode( sha1( $cargo.$password, true ) );
//fn_print_r($postData);
//var_dump($order_info);
//die(1);
    fn_create_payment_form(
        $processor_data['processor_params']['url'],
        $postData,
        $processor_data['processor_params']['bank_name'],
        false
        );
    exit();
}
