<?php
/**
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 24, 2016
 */
use Tygh\Registry;

require_once(dirname(__FILE__).'/../../lib/DonaldApi.php');

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if( $mode=='product' ) {
        $da = new DonaldApi(Registry::get("addons.idefix.dapi_key"));
        $action = $da->processProductPost();
        if( $action ) {
            if( $action->getAction()=='update' ) {  // update price
                $productId = db_get_field("select product_id from ?:products where product_id=?i", $action->getProductId());
                if( $productId ) {
    				db_query(
    						"UPDATE cscart_product_prices SET price=?i WHERE product_id=?i", 
    						$action->getNPrice(), 
    						$action->getProductId()
    						);
    			    $da->responseOK();
			    }
			    else {
			        $da->responseMyError("Product not found");
			    }
            }
		    else {
		        $da->responseMyError("Unknown action");
		    }            
        }
        else {
            $da->responseError();
        }
        die();
    }
}
