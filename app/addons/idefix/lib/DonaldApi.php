<?php

require_once dirname(__FILE__) . '/DonaldUpdateAction.php';

/**
 * Description of DonaldApi
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Dec 24, 2016
 */
class DonaldApi {

    private $sApiKey = '';  // the key for donald to access shop
    private $error = '';

    public function __construct($sApiKey) {
        $this->sApiKey = $sApiKey;
    }

    public function processProductPost() {
        $out = false;
        try {
            $data = json_decode(file_get_contents('php://input'), true);
        } catch (Exception $ex) {
            $this->error = $ex->getMessage();
            return false;
        }

        // check api_key
        if ($this->checkSApiKey($data)) {
            $action = isset($data['action']) ? $data['action'] : false;
            if ($action) {
                switch ($action) {
                    case 'update':
                        $out = $this->updateAction($data);
                        break;
                    default:
                        $out = false;
                        $this->error = 'Unknown action';
                }
            } else {
                $this->error = "Bad action ";
            }
        }
        return $out;
    }

    public function getError() {
        return $this->error;
    }

    private function checkSApiKey($data) {
        $out = false;
        if (isset($data['api_key'])) {
            if ($data['api_key'] == $this->sApiKey) {
                $out = true;
            }
            else {
            	$this->error = "API key error ";
            }
        }
        else {
        	$this->error = "No api key in request ";
        }
        return $out;
    }

    private function updateAction($data) {
        $out = false;
        if (isset($data['product_id']) && !empty($data['product_id']) &&
                isset($data['f_price']) && is_numeric($data['f_price']) &&
                isset($data['o_price']) && is_numeric($data['o_price']) &&
                isset($data['o_pos']) && is_numeric($data['o_pos']) &&
                isset($data['n_price']) && is_numeric($data['n_price'])
        ) {
            $out = new DonaldUpdateAction($data['product_id'], $data['f_price'], $data['o_price'], $data['o_pos'], $data['n_price']);
        } else {
            $this->error = 'Wrong parameters';
        }
        return $out;
    }

    public function responseOK() {
        $out = ['status' => 'ok'];
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($out);
    }

    public function responseError() {
        $out = ['status' => 'error', 'error'=>$this->getError() ];
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($out);
    }

    public function responseMyError($error) {
        $out = ['status' => 'error', 'error'=>$error ];
        header("Content-type: application/json; charset=utf-8");
        echo json_encode($out);
    }


}
