<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) {
    die('Access denied');
}

$files_dir = Registry::get("config.dir.var").'updater_files/';
$pm_files_dir = $files_dir.'merger/';
$pu_files_dir = $files_dir;

Registry::set('product_merger', [
    'pu_files_dir' => $pu_files_dir,
    'supplier_files_dir' => $pm_files_dir,
    'pricing_rules_dir' => $pu_files_dir.'SupplierPricingRules',

    'merger_out_fn' =>  $pu_files_dir.'merger.xml',

    'vat_rate' => 24,

    'telepart' => [
        'fn' => $pm_files_dir . 'telepart.csv',
        'url' => 'https://www.telepart.com/en/mytelepart/mypricelistcsv/D101237/15343',
    ],

    'wave' => [
        'fn' => $pm_files_dir . 'wave.csv',
        'user' => '26151430',
        'pass' => '6392884a',
        'url' => 'http://export.wave-distribution.de/redirect.jsp?exportFormat=CSV&exportEncoding=utf-8&compressed=0&cat=-9969929',
    ],

    'apide' => [
        'fn' => $pm_files_dir . 'pricelist_api.csv',
        'url' => 'http://pricelist.api.de/pricelist/?cid=160408&cpw=1067705&shop=API&action=FULL&attachmentFilename=pricelist_api.csv',
    ],

	'mpmobile' => [
		'fn' => $pm_files_dir . 'MPSproducts.csv',
		//'url' => 'https://mpsmobile.de/team_csvgenerator.php?XTCsid=sjirlne9nhohcj1lrg19limdq1',
		'url' => 'https://mpsmobile.de/team_csvgenerator.php?email_address=ioanniszervos@gmail.com&password=32154&action=process',
	],
	'eglobal' => [
		'fn' => $pm_files_dir . 'eglobal.csv',
		//'url' => 'http://51.15.130.253/out-eglobalcentral.csv',
		'url' => 'http://feed.eglobalcentral.com/CEU/reseller_eu.csv',
	],
	
    'kosatec' => [
        'fn' => $pm_files_dir . 'kosatec.csv',
        'url' => 'http://data.kosatec.de/3971352/8b3bc554c4e774a6842f280e97cc97e3/preisliste.csv',
    ],

    'icecat' => [
        'api_username' => 'evercommobiles',
        'api_password' => '6392884xX',
        'tmp_dir' => $pu_files_dir.'tmp/icecat/',
    ],

    'imres' => [
        'tmp_dir' => $pu_files_dir.'tmp/imres/',
        'uri' => '/var/updater_files/tmp/imres/',
    ],
]);

Registry::set('config.product_merger', Registry::get('product_merger'));