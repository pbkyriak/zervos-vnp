<?php

function fn_slx_product_merger_delete_product_post($product_id, $product_deleted) {
	if($product_deleted) {
		$xproductId = db_get_field("select id from slx_product where cproduct_id=?i", $product_id);
		if($xproductId) {
			db_query("update slx_product set cproduct_id=0 where id=?i", $xproductId);
		}
	}
}

function slx_product_merger_unlink_xproduct($product_id, $xproduct_id) {
	if(empty($product_id) || empty($xproduct_id)) {
		return;
	}
	db_query("update slx_product set cproduct_id=0, istatus=1 where id=?i", $xproduct_id);
	db_query("update cscart_products set xproduct_id=0 where product_id=?i", $product_id);
}

function slx_product_merger_unlink_and_block_xproduct($product_id, $xproduct_id) {
	if(empty($product_id) || empty($xproduct_id)) {
		return;
	}
	db_query("update slx_product set cproduct_id=0, blocked=1 where id=?i", $xproduct_id);
	db_query("update cscart_products set xproduct_id=0 where product_id=?i", $product_id);
}

function slx_product_merger_unblock_xproduct($xproduct_id) {
	if(empty($product_id)) {
		return;
	}
	db_query("update slx_product set cproduct_id=0, blocked=0, istatus=1 where id=?i", $xproduct_id);
}

function slx_product_merger_set_mismatch_checked_xproduct($xproduct_id, $check) {
	if(empty($xproduct_id) || (!in_array($check,[0,1]))) {
		return;
	}

	db_query("update slx_product set mismatch_checked=?i where id=?i",$check, $xproduct_id);
}