<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if($mode=='manage') {

    $params = $_REQUEST;

    $xp = new \Slx\ProductMerger\Lists\SProducts();
    list($sproducts, $search) = $xp->get($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
    $suppliers = db_get_fields("select distinct supplier from slx_supplier_product");
    Tygh::$app['view']->assign('sproducts', $sproducts);
    Tygh::$app['view']->assign('suppliers', $suppliers);
    Tygh::$app['view']->assign('search', $search);
}
elseif($mode=='details') {
    $id = $_REQUEST['id'];
    $sproduct = db_get_row("select * from slx_supplier_product where id=?i", $id);
    $sproduct['eans'] = db_get_fields("select ean from slx_supplier_product_ean where sproduct_id=?i", $id);
    Tygh::$app['view']->assign('sproduct', $sproduct);

    $xproduct = false;
    $xproductId = db_get_field("select xproduct_id from slx_product_supplier_product where sproduct_id=?i", $id);
    if($xproductId) {
        $xproduct = db_get_row("select * from slx_product where id=?i", $xproductId);
    }
    Tygh::$app['view']->assign('xproduct', $xproduct);

    $tabs = array (
        'general' => array (
            'title' => __('general'),
            'js' => true
        ),
        'sproduct' => array (
            'title' => __('something'),
            'js' => true
        ),
        'iproduct' => array(
            'title' => __('something'),
            'js' => true
        ),
    );
    Registry::set('navigation.tabs', $tabs);
}