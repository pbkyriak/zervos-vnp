<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if($mode=='manage') {

    $params = $_REQUEST;

    $xp = new \Slx\ProductMerger\Lists\PendingIProducts();
    list($iproducts, $search) = $xp->get($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
    $brands = db_get_fields("select distinct brand from slx_icecat_product order by brand");
    Tygh::$app['view']->assign('iproducts', $iproducts);
    Tygh::$app['view']->assign('search', $search);
}
