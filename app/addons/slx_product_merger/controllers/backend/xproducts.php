<?php



use Tygh\Registry;



if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if($mode=='unshit') {
		$productId = $_REQUEST['pid'];
		$inXproductId = $_REQUEST['inxid'];
		
		$xproducts = db_get_fields("select id from slx_product where cproduct_id=?i", $productId);
		//fn_print_r($xproducts);
		foreach($xproducts as $xid) {
			if($xid!=$inXproductId) {
				db_query("update slx_product set cproduct_id=?i, istatus=?i where id=?i", 0, 1, $xid);
				//echo db_quote("update slx_product set cproduct_id=?i, istatus=?i where id=?i", 0, 1, $xid);
			}
		}
		db_query("update slx_product set cproduct_id=?i, istatus=?i where id=?i", $productId, 5, $inXproductId);
		db_query("update ?:products set xproduct_id=?i where product_id=?i", $inXproductId, $productId);
		
		fn_set_notification("N", "unshit", sprintf("unshited %s", $productId));
		return array(CONTROLLER_STATUS_REDIRECT,"xproducts.shit");
	}
	elseif($mode=='unlink') {
		$product_id= $_REQUEST['cid'];
		$xproduct_id= $_REQUEST['xid'];
		slx_product_merger_unlink_xproduct($product_id, $xproduct_id);
		return array(CONTROLLER_STATUS_REDIRECT,"products.update?product_id=".$product_id."&selected_section=addons");
	}
	elseif($mode=='unlink_and_block') {
		$product_id= $_REQUEST['cid'];
		$xproduct_id= $_REQUEST['xid'];
		slx_product_merger_unlink_and_block_xproduct($product_id, $xproduct_id);
		return array(CONTROLLER_STATUS_REDIRECT,"products.update?product_id=".$product_id."&selected_section=addons");
	}
	elseif($mode=='unblock') {
		$xproduct_id= $_REQUEST['xid'];
		slx_product_merger_unblock_xproduct($xproduct_id);
		return array(CONTROLLER_STATUS_REDIRECT,"xproducts.details&id=".$xproduct_id);
	}
	elseif($mode=='mismatchecked') {
		$xproduct_id=$_REQUEST['xid'];
		$check = $_REQUEST['check'];
		slx_product_merger_set_mismatch_checked_xproduct($xproduct_id, $check);
		return array(CONTROLLER_STATUS_REDIRECT,"xproducts.details&id=".$xproduct_id);
	}
}

if($mode=='manage') {



    $params = $_REQUEST;



    $xp = new \Slx\ProductMerger\Lists\XProducts();

    list($xproducts, $search) = $xp->get($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);

    Tygh::$app['view']->assign('xproducts', $xproducts);

    Tygh::$app['view']->assign('search', $search);

}


elseif($mode=='details') {

    $xproduct = db_get_row("select * from slx_product where id=?i", $_REQUEST['id']);

    $xproduct['sproducts'] = db_get_array("select * from slx_supplier_product where id in (select sproduct_id from slx_product_supplier_product where xproduct_id=?i)", $_REQUEST['id']);

    foreach($xproduct['sproducts'] as $k => $v) {

        $eans = db_get_fields("select distinct ean from slx_supplier_product_ean where sproduct_id=?i", $v['id']);

        $xproduct['sproducts'][$k]['eans'] = implode(', ',$eans);

    }

    Tygh::$app['view']->assign('xproduct', $xproduct);



    $ip = new \Slx\ProductMerger\Icecat\IcecatProductLoader();

    $iproduct = $ip->loadById($xproduct['iproduct_id'], 'el');

    Tygh::$app['view']->assign('iproduct', $iproduct);



    $tabs = array (

        'general' => array (

            'title' => __('general'),

            'js' => true

        ),

        'sproduct' => array (

            'title' => __('sproducts'),

            'js' => true

        ),

        'iproduct' => array(

            'title' => __('iproduct'),

            'js' => true

        ),

    );

    Registry::set('navigation.tabs', $tabs);

}

elseif($mode=='overview') {



    $si = new \Slx\Util\SuppliersMergerInfo();

    $suppliers = $si->getSuppliersInfo();

    Tygh::$app['view']->assign('suppliers', $suppliers );

    Tygh::$app['view']->assign('iproducts', $si->getIProductsStatus() );

    Tygh::$app['view']->assign('sproducts', $si->getSProductsStatus() );

    Tygh::$app['view']->assign('xproducts', $si->getXProductsStatus() );

    Tygh::$app['view']->assign('cproducts', $si->getCsProductsStatus() );

    //Tygh::$app['view']->assign('cproducts', [] );

}

elseif($mode=='overview2') {

    $si = new \Slx\Util\SuppliersMergerInfo();

    Tygh::$app['view']->assign('cproducts', $si->getCsProductsStatus() );

}
elseif($mode=='shit') {
	$rows = db_get_array("SELECT cproduct_id, GROUP_CONCAT(id) ids, count(id) as cnt FROM `slx_product` 
				where cproduct_id!=0
				group by cproduct_id
			having cnt>1
			");
			
	Tygh::$app['view']->assign('shit_count', count($rows) );
	if($rows) {
		$row = reset($rows);
		$xIds = explode(',',$row['ids']);
		$shit = [];
		
		$shit['cproduct'] = db_get_row("select p.product_id, p.xproduct_id, pd.product from cscart_products p left join cscart_product_descriptions pd on p.product_id=pd.product_id and pd.lang_code='el' where p.product_id=?i ", $row['cproduct_id']);
		$shit['xproudcts'] = db_get_array("select id, title from slx_product where id in (?a)", $xIds);
		Tygh::$app['view']->assign('shit', $shit );
	}
}
elseif($mode=='bad_rel') {
	$sql = "
SELECT p.product_id, pd.product, xp.title xtitle, GROUP_CONCAT(sp.title SEPARATOR '$$') stitles
FROM cscart_products p
LEFT JOIN cscart_product_descriptions pd ON p.product_id=pd.product_id AND pd.lang_code='el'
LEFT JOIN slx_product xp ON p.xproduct_id=xp.id
LEFT JOIN slx_product_supplier_product psp ON xp.id=psp.xproduct_id
LEFT JOIN slx_supplier_product sp ON sp.id=psp.sproduct_id
WHERE p.xproduct_id!=0 AND pd.product!=xp.title and xp.mismatch_checked=0 and xp.blocked=0
GROUP BY p.product_id, pd.product, xp.title 
	";
	$params = $_REQUEST;
	$default_params = array(
		'page' => 1,
		'items_per_page' => 10
		);

	$params = array_merge($default_params, $params);
	
	$rows = db_get_array("$sql");
	$results = [];
	foreach($rows as $row) {
		$titles = explode('$$', $row['stitles']);
		$matched = false;
		$testTitle = str_replace(' ','',strtolower($row['product']));
		foreach($titles as $title) {
			$check = str_replace(' ','',strtolower($title));
			if($check==$testTitle) {
				$matched=true;
			}
		}
		if(!$matched) {
			$results[] = $row;
		}
	}

	$params['total_items'] = count($results);
	$limit = '';
	if (!empty($params['items_per_page'])) {
		$limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
		$limit = str_replace('LIMIT','',$limit);
		$lms = explode(',',$limit);
		$results = array_slice($results, $lms[0], $lms[1]);
	}

    Tygh::$app['view']->assign('results', $results);
    Tygh::$app['view']->assign('search', $params);

}