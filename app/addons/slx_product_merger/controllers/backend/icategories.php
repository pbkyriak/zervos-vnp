<?php

use Tygh\Registry;
use Slx\ProductMerger\Lists\ICategories;

if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if($mode=='unlink') {
        $icategory_id = $_REQUEST['icategory_id'];
        $category_id = $_REQUEST['category_id'];
        if(!empty($icategory_id) && ! empty($category_id)) {
            $xp = new \Slx\ProductMerger\Lists\ICategories();
            $xp->unlinkICategoryWithCategory($icategory_id, $category_id);
        }
        return array(CONTROLLER_STATUS_OK,"icategories.details?icategory_id=".$icategory_id);
    }
    elseif($mode=='link') {
        $icategory_id = $_REQUEST['icategory_id'];
        $category_id = $_REQUEST['category_id'];
        if(!empty($icategory_id) && ! empty($category_id)) {
            $xp = new \Slx\ProductMerger\Lists\ICategories();
            $xp->linkICategoryWithCategory($icategory_id, $category_id);
        }
        return array(CONTROLLER_STATUS_OK,"icategories.details?icategory_id=".$icategory_id);
    }
}
if($mode=='manage') {
    $params = $_REQUEST;
    $xp = new \Slx\ProductMerger\Lists\ICategories();
    list($icategories, $search) = $xp->get($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
    Tygh::$app['view']->assign('icategories', $icategories);
    Tygh::$app['view']->assign('search', $search);
}
elseif( $mode=='crosslinked') {
    $params = $_REQUEST;
    $xp = new \Slx\ProductMerger\Lists\ICategories();
    list($icategories, $search) = $xp->getCrossLinked($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);

    Tygh::$app['view']->assign('icategories', $icategories);
    Tygh::$app['view']->assign('search', $search);
}
elseif($mode=='details') {
    $icategory_id= $_REQUEST['icategory_id'];
    $xp = new \Slx\ProductMerger\Lists\ICategories();
    $category_data = $xp->getICategory($icategory_id);
    Tygh::$app['view']->assign('category_data', $category_data);
}