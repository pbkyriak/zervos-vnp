<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if($mode=='manage') {

    $params = $_REQUEST;

    $xp = new \Slx\ProductMerger\Lists\IProducts();
    list($iproducts, $search) = $xp->get($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
    $brands = db_get_fields("select distinct brand from slx_icecat_product order by brand");
    Tygh::$app['view']->assign('iproducts', $iproducts);
    Tygh::$app['view']->assign('brands', $brands);
    Tygh::$app['view']->assign('search', $search);
}
elseif($mode=='details') {
    $id = $_REQUEST['iproduct_id'];
    $ip = new \Slx\ProductMerger\Icecat\IcecatProductLoader();
    $iproduct = $ip->loadById($id, 'el');

    Tygh::$app['view']->assign('iproduct', $iproduct);
}