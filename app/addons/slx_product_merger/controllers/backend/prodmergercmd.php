<?php

use Tygh\Registry;
use Tygh\UpgradeCenter\Console\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Application as ConsoleApplication;

if (defined('CONSOLE') && CONSOLE) {
    if ($mode == 'console') {
        printf("\n\nProduct Merger\n");
        printf("--------------\n\n");
        $application = new ConsoleApplication();

        $cmdReg = new \Slx\Util\RegisterCommands();
        $cmdReg->setApplication($application)
            ->setDir(Registry::get('config.dir.addons').'slx_product_merger/Slx/ProductMerger/Command/')
            ->setNamespace('Slx\\ProductMerger\\Command')
            ->register();

        $input = new ArgvInput();
        $output = new ConsoleOutput();

        Registry::set('runtime.console.output', $output);

        $application->run($input, $output);
    }
}