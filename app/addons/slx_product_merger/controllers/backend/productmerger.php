<?php

use Slx\ProductMerger\ManMerger\Possible;

if($mode=='next') {
    $possible = new Possible();

    $p = $possible->getNext();
    foreach($p['results'] as $k => $v) {
        $p['results'][$k]['product'] = db_get_row("SELECT * from slx_supplier_product where id=?i", $v['sproduct_id']);
    }
    Tygh::$app['view']->assign('p',$p);

}