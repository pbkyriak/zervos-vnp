<?php

namespace Slx\ProductMerger\ImportProductStream;


use Slx\ProductMerger\Exception\SupplierStreamNotSupported;

/**
 * Creates import product stream for suppliers that go through merger.
 * To add new Import stream, create a class named XXXXProductStream.php in Suppliers folder and implement ImportProductStreamInterface.
 * Create and enable in admin panel a supplier named XXXX and you are done.
 *
 * @package Slx\ProductMerger\ImportProductStream
 */
class ImportProductStreamFactory {

    /**
     * @param $supplier
     * @return null|ImportProductStreamInterface
     * @throws SupplierStreamNotSupported
     */
    public static function getStream($supplier) {
        $stream = null;

        $streams = self::getSupplierStreams();
        if(isset($streams[$supplier])) {
            $stream = new $streams[$supplier];
        }
        else {
            throw new SupplierStreamNotSupported();
        }
        return $stream;
    }

    /**
     * Return supported supplier streams
     *
     * @return array
     */
    public static function getSupportedSuppliers() {
        $streamMethods = self::getSupplierStreams();
        return array_keys($streamMethods);
    }

    public static function getSupplierStreams() {
        $out = [];
        $dir = __DIR__.'/Supplier';
        $ns = '\\Slx\\ProductMerger\\ImportProductStream\\Supplier\\';
        foreach(glob($dir.'/*ProductStream.php') as $fn) {
            $fname = sprintf("%s\n", $fn);
            $class = basename($fn, '.php');
            $supplier = strtolower(str_replace('ProductStream','',$class));
            $fqn = $ns . $class;
            $r = new \ReflectionClass($fqn);
            if($r->implementsInterface('\\Slx\\ProductMerger\\ImportProductStream\\ImportProductStreamInterface') && !$r->isAbstract()) {
                $supplierId = db_get_field("select supplier_id from ?:suppliers where name=?s", $supplier);
                if($supplierId) {
                    $out[$supplier] = $fqn;
                }
                else {
                    //printf("\nClass found for supplier '%s' but supplier is not found in cscart_suppliers\n", $supplier);
                }
            }
        }
        return $out;
    }
}