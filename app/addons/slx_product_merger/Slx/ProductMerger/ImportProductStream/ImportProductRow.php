<?php

namespace Slx\ProductMerger\ImportProductStream;


use Slx\ProductMerger\Exception\ImportProductFieldNotExistsException;

class ImportProductRow implements \ArrayAccess {
    private $supplier;
    private $pcode;
    private $availability;
    private $category;
    private $price;
    private $title;
    private $product_id;
    private $istatus;
    private $ean;

    public function offsetExists($offset) {
        if(property_exists($this, $offset)) {
            return $this->$offset;
        }
        return false;
    }

    public function offsetGet($offset) {
        if(property_exists($this, $offset)) {
            return $this->$offset;
        }
        else {
            throw new ImportProductFieldNotExistsException(sprintf("ImportProductRow does not have field named '%s'", $offset));
        }
    }

    public function offsetSet($offset, $value) {
        if(property_exists($this, $offset)) {
            $this->$offset = $value;
        }
        else {
            throw new ImportProductFieldNotExistsException(sprintf("ImportProductRow does not have field named '%s'", $offset));
        }
    }

    public function offsetUnset($offset) {
        return true;
    }

    public function toArray() {
        $refl = new \ReflectionClass(self::class);
        $properties = $refl->getProperties(\ReflectionProperty::IS_PRIVATE);
        $out = [];
        foreach($properties as $property) {
            $name = $property->name;
            $out[$name] = $this->$name;
        }
        return $out;
    }
}
