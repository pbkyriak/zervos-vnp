<?php

namespace Slx\ProductMerger\ImportProductStream;

use Slx\ProductMerger\Exception\EndOfStreamException;
use Tygh\Registry;

abstract class AbstractCsvStream implements ImportProductStreamInterface {

    protected $fname;
    protected $delimeter;
    protected $handle;
    protected $lnCnt;
    protected $supplier;

    public function open() {
        $out = false;
        $fn = sprintf("%s%s",Registry::get("product_merger.supplier_files_dir"), $this->fname);
        printf("CSV file: %s\n",$fn);
        if(!file_exists($fn)) {
            printf("File not found!");
            return false;
        }
        $this->handle = fopen( $fn, 'r');
        if ($this->handle) {
            $out = true;
            $this->lnCnt=0;
        }
        return $out;
    }

    public function next() {
        if(!$this->handle ) {
            throw new EndOfStreamException();
        }
        while (($row = fgetcsv($this->handle, 1000, $this->delimeter))) {
            $this->lnCnt++;
            if ($this->lnCnt == 1) {
                continue;
            }
            return $this->mapFields($row);
        }
        throw new EndOfStreamException();
    }

    public function close() {
        if( $this->handle ) {
            fclose($this->handle);
        }
    }

    abstract protected function mapFields($row);

}