<?php
namespace Slx\ProductMerger\ImportProductStream;

interface ImportProductStreamInterface {

    public function open();
    public function next();
    public function close();

}