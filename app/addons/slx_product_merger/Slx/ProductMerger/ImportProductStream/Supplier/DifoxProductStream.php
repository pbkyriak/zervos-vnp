<?php

namespace Slx\ProductMerger\ImportProductStream\Supplier;

use Slx\ProductMerger\ImportProductStream\AbstractCsvStream;
use Slx\ProductMerger\ImportProductStream\ImportProductRow;

class DifoxProductStream extends AbstractCsvStream {

    public function __construct() {
        $this->delimeter = ';';
        $this->fname = 'difox.csv';
        $this->supplier = 'difox';
    }

    protected function mapFields($row) {
        $out = new ImportProductRow();

        $out['supplier'] = $this->supplier;
        $out['pcode'] = $row[2];
        $out['availability'] = $row[14] == 'available' ? 10 : 0;
        $out['category'] = $row[0];
        $out['price'] = (float)$row[12];
        $out['title'] = preg_replace('!\s+!', ' ',$row[3]);
        $out['product_id'] = 0;
        $out['istatus'] = 0;
        $eans = array_slice($row, 6, 5);
        array_walk($eans, 'trim');
        $out['ean'] = array_filter($eans, function ($item) {
            return !empty($item) && strlen($item) == 13;
        });
        return $out;

    }
}