<?php

namespace Slx\ProductMerger\ImportProductStream\Supplier;

use Slx\ProductMerger\ImportProductStream\AbstractCsvStream;
use Slx\ProductMerger\ImportProductStream\ImportProductRow;

class TelepartProductStream  extends AbstractCsvStream {

    public function __construct() {
        $this->delimeter = ';';
        $this->fname = 'telepart.csv';
        $this->supplier = 'telepart';
    }

    protected function mapFields($row) {
        $out = new ImportProductRow();

        $out['supplier'] = $this->supplier;
        $out['pcode'] = $row[0];
        $out['availability'] = is_numeric($row[10]) ? 10 : 0;
        $out['category'] = $row[1];
        $out['price'] = (float)$row[6];
        $out['title'] = preg_replace('!\s+!', ' ',$row[3]);
        $out['product_id'] = 0;
        $out['istatus'] = 0;
        $eans = explode(',', $row[13]);
        array_walk($eans, 'trim');
        foreach($eans as $i => $v) {
            $v = trim($v);
            if(strlen($v)==12) {
                $v = '0' . $v;
            }
            $eans[$i] = $v;
        }
        $out['ean'] = array_filter($eans, function ($item) {
            return !empty($item) && strlen($item) == 13;
        });
        return $out;

    }
}

/*
0 Articlenumber;
1 Type;
2 Manufacturer;
3 Model;
4 Modeldescription;
5 Languages;
6 UnitPrice;
7 IsDiscontinued;
8 IsNew;
9 IsSpecialPrice;
10 Stock;
11 DeliveryQuantity;
12 DeliveryDay;
13 EAN;
14 IsVIPPrice;
15 TimeStamp
 */