<?php

namespace Slx\ProductMerger\ImportProductStream\Supplier;

use Slx\ProductMerger\ImportProductStream\AbstractCsvStream;
use Slx\ProductMerger\ImportProductStream\ImportProductRow;

class ApideProductStream extends AbstractCsvStream {

    public function __construct() {
        $this->delimeter = ';';
        $this->fname = 'pricelist_api.csv';
        $this->supplier = 'apide';
    }
// sku;stock;currency;price;title;manufacturer;msku;ean;egId;lwg1;availability;weight;width;depth;height;externalStock
    protected function mapFields($row) {
        $out = new ImportProductRow();

        $out['supplier'] = $this->supplier;
        $out['pcode'] = $row[0];
        $stock = ((int)$row[1]!==0 ? (int)$row[1] : 0);
        if($row[10]!='B') {
            $stock = 0;
        }
        $out['availability'] = $stock;
        $out['category'] = $row[9];
        $out['price'] = (float)$row[3];
        $out['title'] = preg_replace('!\s+!', ' ',$row[4]);
        $out['product_id'] = 0;
        $out['istatus'] = 0;
        $out['ean'] = [$row[7]];

        //printf("code=%s c=%s p=%s s=%s\n",$out['pcode'],$out['category'], $out['price'], $out['availability']);
        return $out;

    }
}
