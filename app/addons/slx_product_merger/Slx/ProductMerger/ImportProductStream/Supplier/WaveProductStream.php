<?php

namespace Slx\ProductMerger\ImportProductStream\Supplier;


use Slx\ProductMerger\ImportProductStream\AbstractCsvStream;
use Slx\ProductMerger\ImportProductStream\ImportProductRow;

class WaveProductStream  extends AbstractCsvStream {

    public function __construct() {
        $this->delimeter = "\t";
        $this->fname = 'wave.csv';
        $this->supplier = 'wave';
    }

    protected function mapFields($row) {
        $out = new ImportProductRow();

        $out['supplier'] = $this->supplier;
        $out['pcode'] = $row[0];
        $out['availability'] = ($row[12]=='Auf Lager' ? 10 : 0);
        $out['category'] = $row[11];
        $out['price'] = (float)$row[4];
        $out['title'] = preg_replace('!\s+!', ' ',$row[1].' '.$row[2]);
        $out['product_id'] = 0;
        $out['istatus'] = 0;
        $eans = explode(',', $row[14]);
        array_walk($eans, 'trim');
        $out['ean'] = array_filter($eans, function ($item) {
            return !empty($item) && strlen($item) == 13;
        });
        return $out;

    }
}

/*
 *
        $out = array(
            'supplier' => $this->supplier,
            'pcode' => $row[0],
            'availability' => ($row[12]=='Auf Lager' ? 10 : 0),
            'category' => $row[11],
            'price' => (float)$row[4],
            'title' => $row[2],
            'product_id' => 0,
            'istatus' => 0,
            'ean' => array($row[14]),
        );
ArtNo	Manufacturer	Name	Addition	Price (EUR)	Scale	ScalePrice (EUR)	Col1	Col2	Col3	Col4	Category	Availability	DeliveryCondition	EAN	Manufacturer Id
 */