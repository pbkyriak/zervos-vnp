<?php

namespace Slx\ProductMerger\ImportProductStream\Supplier;

use Slx\ProductMerger\ImportProductStream\AbstractCsvStream;
use Slx\ProductMerger\ImportProductStream\ImportProductRow;

class KosatecProductStream  extends AbstractCsvStream {

    public function __construct() {
        $this->delimeter = ';';
        $this->fname = 'kosatec.csv';
        $this->supplier = 'kosatec';
    }

    protected function mapFields($row) {
        $out = new ImportProductRow();

        $out['supplier'] = $this->supplier;
        $out['pcode'] = $row[0];
        $out['availability'] = is_numeric($row[9]) ? $row[9] : 0;
        $out['category'] = $row[14];
        $out['price'] = (float)$row[6];
        $out['title'] = preg_replace('!\s+!', ' ',$row[2]);
        $out['product_id'] = 0;
        $out['istatus'] = 0;
        $out['ean'] = [$row[5]];
        return $out;

    }
}
/*

0 artnr;
1 herstnr;
2 artname;
3 hersteller;
4 hersturl;
5 ean;
6 hek;
7 vkbrutto;
8 verfuegbar;
9 menge;
10 eta;
11 indate;
12 gewicht;
13 eol;
14 kat1;kat2;kat3;kat4;kat5;kat6
1000378;2L5201U;KVM Modules & Accessories KVM Cable USB 1,2 m, Aten 2L-5201U ATEN;ATEN INTERNATIONAL CO. LTD.;;4710423772540;4,76;10,12;;0;;2013-07-02;0;0;
Kabel & Adapter;Data-/KVM-Switch;KVM-Switch;;;
*/