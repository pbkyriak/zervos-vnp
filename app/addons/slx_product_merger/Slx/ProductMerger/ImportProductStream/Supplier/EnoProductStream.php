<?php

namespace Slx\ProductMerger\ImportProductStream\Supplier;

use Slx\ProductMerger\ImportProductStream\ImportProductRow;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamInterface;
use Slx\ProductMerger\Exception\EndOfStreamException;
use Tygh\Registry;

class EnoProductStream  implements ImportProductStreamInterface {

    protected $fname;
    protected $handle;
    protected $lnCnt;
    protected $supplier;
    
    public function __construct() {
        $this->fname = 'eno.xml';
        $this->supplier = 'eno';
    }
    
    public function open() {
        $out = false;
        $fn = sprintf("%s%s",Registry::get("product_merger.supplier_files_dir"), $this->fname);
        printf("XML file: %s\n",$fn);
        if(!file_exists($fn)) {
            printf("File not found!");
            return false;
        }
        $this->handle = $reader = \XMLReader::open($fn);
        if ($this->handle) {
            $out = true;
        }
        return $out;
    }

    public function next() {
        if(!$this->handle ) {
            throw new EndOfStreamException();
        }
        
        while($this->handle->read()) {
            //printf("EL TYPE= %s \n", $this->handle->nodeType);
            if($this->handle->nodeType == \XMLReader::ELEMENT && $this->handle->name=='Artikel') {
                $product = $this->processProduct();
                if($product) {
                    return $product;
                }
            }
        }
        
        throw new EndOfStreamException();
    }

    public function close() {
        if( $this->handle ) {
            $this->handle->close();
        }
    }
    
    private function processProduct() {
        $product = new ImportProductRow();
        $row = [];
        //printf("product start\n");
        while($this->handle->read()) {
            if($this->handle->nodeType == \XMLReader::END_ELEMENT && $this->handle->name=='Artikel') {
                $product = $this->mapFields($row);
                //printf("product end\n");
                break;
            }
            if($this->handle->nodeType == \XMLReader::ELEMENT) {
                //printf("EL TYPE= %s %s=%s \n", $this->handle->nodeType,$this->handle->name, $this->handle->readString());
                $row[$this->handle->name] = $this->handle->readString();
            }
            
        }
        return $product;
    }
    
    private function mapFields($row) {
        $out = new ImportProductRow();
        if(intval($row['Bestand'])==0 || empty($row['EAN'])) {
            return false;
        }
        $out['supplier'] = $this->supplier;
        $out['pcode'] = $row['Artikelnummer'];
        $out['availability'] = intval($row['Bestand']);
        $out['category'] = $row['Haupt-WG'].'='.$row['WG-Nummer'];
        $out['price'] = (float)$row['HEK']; // or UVP
        $out['title'] = $row['Bezeichnung'];
        $out['product_id'] = 0;
        $out['istatus'] = 0;
        $out['ean'] = [$row['EAN']];
        
        return $out;
    }
}
