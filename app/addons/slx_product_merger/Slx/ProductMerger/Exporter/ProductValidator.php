<?php

namespace Slx\ProductMerger\Exporter;

use Slx\ProductMerger\Exporter\Traits\TextSanitizationTrait;
use Slx\Logger\Logger;
use Slx\ProductMerger\Icecat\IcecatCategoryIdHelper;

/**
 * Add any validation checks required here.
 * Methods must:
 * - be named isValidXxxx
 * - declared protected and
 * - have only one argument $productData.
 *
 * All Validations must be valid (like `and` operator).
 * Methods are called in alphabetical order.
 *
 * @package Slx\ProductMerger\Exporter
 */
class ProductValidator extends AbstractProductValidator {

    protected $supCategories=null;

    use TextSanitizationTrait;

    protected function isValidDescription($productData) {
        return !$this->isGermanText($productData['short_description']) && !empty($productData['full_description']);
    }

    protected function isValidAmount($productData) {
        $sizeFlag = $productData['amount']>0;
        return $sizeFlag;
    }

    protected function isValidSize($productData) {
        $sizeFlag = ($productData['width'] != 0 && $productData['height'] != 0 && $productData['depth'] != 0 && $productData['width'] != 0);
        //return $sizeFlag;
        return true;
    }
	
    protected function isValidCategory($productData) {
        if(is_null($this->supCategories)) {
            $this->supCategories = db_get_fields("select supplier_category_name from ?:categories_supplier_matching order by supplier_category_name");
        }
        $out = false;
		if($productData['x_product_id']==0) {
			$mycategory = $productData['category_id'];
		}
		else {
			$mycategory=IcecatCategoryIdHelper::idToName($productData['category_id']);
		}
		if(in_array($mycategory, $this->supCategories)) {
			$out = true;
		}
		else {
			Logger::getInstance()->log(sprintf("%s failed category: %s\n",$productData['product_code'], $mycategory));
		}
        return $out;
        //return true;
	}		
	
	protected function isValidBrand($productData) {		
	
	$invalidBrands = ['Manfrotto'];		
	
	$out = true;		
	
	if(in_array($productData['brand'], $invalidBrands)) {

	$out = false;		
	
	}		
	
	return $out;	
	
	}
/*
    protected function isValidFeatures($productData) {
        $out = true;
        if(count($productData['features'])==0) {
            $out = false;
        }
        return $out;
    }
*/
}


