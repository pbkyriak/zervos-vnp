<?php

namespace Slx\ProductMerger\Exporter;

use Slx\ProductMerger\Icecat\IcecatCategoryIdHelper;
/**
 * Class ProductFixer
 * Fixer methods must:
 * - named fixXxxx
 * - be 'protected' and
 * - have only one argument with parameter $productData passed by reference so modifications can be done.
 *
 * Methods are called in alphabetical order.
 *
 * @package Slx\ProductMerger\Exporter
 */
class ProductFixer extends AbstractProductFixer {

    protected function fixWeigth(&$productData) {
		if(in_array($productData['Supplier'],['mpmobile','eglobal']))
			return;
		$w = floatVal($productData['weight']);
		$w = round($w/1000,2);
		if($w<0.5) {
			$w = 0.5;
		}		
        $productData['weight'] = $w;
    }

    protected function fixCategoryId(&$productData) {
		if(in_array($productData['Supplier'],['mpmobile','eglobal']))
			return;
        $productData['category_id'] = IcecatCategoryIdHelper::idToName($productData['category_id']);
    }
	
    /*
	protected function fixTitleWave(&$productData) {
		if($productData['Supplier']=='wave' && !in_array($productData["category_id"],['X-1893','X-119'])) {
			//$title = substr($productData["short_description"],0, strpos($productData["short_description"],'. '));
			printf("T=%s | %s\n ",$productData['title'],$productData["cotitle"]);
			
			$productData['title'] = $productData['cotitle'];
		}
	}
	*/
	
}