<?php

namespace Slx\ProductMerger\Exporter;

use Slx\ProductMerger\Exporter\Traits\TextSanitizationTrait;
use Slx\Logger\Logger;

class AbstractProductValidator {

    protected $methods;

    public function __construct() {
        $this->methods = $this->getValidationMethods();
        //print_r($this->methods);
    }

    /**
     * @param $productData
     * @return bool True is good, False is bad
     */
    public function isProductValid($productData) {
        $out = true;
        foreach($this->methods as $method) {
            if(!$this->$method($productData)) {
                Logger::getInstance()->log(sprintf("%s failed %s\n",$productData['product_code'], $method));
                $out = false;
                break;
            }
        }
        return $out;
    }


    protected function getValidationMethods() {
        $out = [];
        $refl = new \ReflectionClass($this);
        foreach( $refl->getMethods() as $reflMeth) {
            if( strpos($reflMeth->getName(), 'isValid')===0 ) {
                if($reflMeth->isProtected() ) {
                    if ($reflMeth->getNumberOfRequiredParameters() == 1) {
                        $out[] = $reflMeth->getName();
                    }
                    else {
                        printf("Method '%s::%s' to be used as validator must have only one argument.\n",
                               $reflMeth->getDeclaringClass()->getName(),
                               $reflMeth->getName()
                        );
                    }
                }
                else {
                    printf("Method '%s::%s' to be used as validator must be protected\n",
                           $reflMeth->getDeclaringClass()->getName(),
                           $reflMeth->getName()
                    );
                }
            }
        }
        sort($out);
        return $out;
    }

}