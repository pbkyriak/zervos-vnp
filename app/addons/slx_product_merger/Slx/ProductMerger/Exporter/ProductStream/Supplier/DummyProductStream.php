<?php

namespace Slx\ProductMerger\Exporter\ProductStream\Supplier;


use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exporter\ProductStream\StreamProduct;
use Slx\ProductMerger\Exporter\ProductStream\SupplierProductStreamInterface;

/**
 * Dummy stream for demonstration of bypass supplier stream.
 *
 * Class DummyProductStream
 * @package Slx\ProductMerger\Exporter\ProductStream\Supplier
 */
class DummyProductStream implements SupplierProductStreamInterface {

    private $counter = 0;
    public function open() {
        $counter = 0;
        return true;
    }

    public function next() {
        $product = null;
        $supplier = 'dummy';
        if($this->counter<3) {
            $product = new StreamProduct();
            $product['category_id'] = '123';
            $product['product_code'] = 'DUMMY-'.$this->counter;
            $product['purchase_price'] = 1;
            $product['amount'] = 1;
            $product['part_number'] = $product['product_code'];
            $product['ean'] = $product['product_code'];
            $product['title'] = $product['product_code'];
            $product['short_description'] = $product['product_code'];
            $product['full_description'] = $product['product_code'];
            $product['search_words'] = $product['product_code'];
            $product['seo_name'] = $product['product_code'];
            $product['brand'] = '';
            $product['image'] = [];
            $product['shop_availability'] = 0;
            $product['skroutz_availability'] = 0;
            $product['status'] = 'A';
            $product['weight'] = 0;
            $product['height'] = 0;
            $product['width'] = 0;
            $product['depth'] = 0;
            $product['free_shipping'] = 'N';
            $product['cart_product_id'] = -1;
            $product['x_product_id'] = 0;
            $product['Supplier'] = $supplier;
        }
        else {
            throw new EndOfStreamException();
        }
        $this->counter++;
        return $product;
    }

    public function close() {
        return true;
    }

}