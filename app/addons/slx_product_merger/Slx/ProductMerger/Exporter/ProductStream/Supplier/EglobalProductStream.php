<?php

namespace Slx\ProductMerger\Exporter\ProductStream\Supplier;

use Tygh\Registry;
use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exporter\ProductStream\StreamProduct;
use Slx\ProductMerger\Exporter\ProductStream\SupplierProductStreamInterface;

/**
 * Dummy stream for demonstration of bypass supplier stream.
 *
 * Class EglobalProductStream
 * @package Slx\ProductMerger\Exporter\ProductStream\Supplier
 */
class EglobalProductStream implements SupplierProductStreamInterface {

    private $counter = 0;
	private $handle = null;
	private $categories;
	private $supplierShopFeedAvail, $supplierShopFeedAvailSK;
	
    public function open() {
		$fn = Registry::get('product_merger.eglobal.fn');
		printf("%s Reading: %s\n", __METHOD__, $fn);
        $this->counter = 0;
		$this->categories = [];
		$this->supplierShopFeedAvail = db_get_field("select shop_availability from ?:suppliers where name=?s", 'eglobal');
		$this->supplierShopFeedAvailSK = db_get_field("select skroutz_availability from ?:suppliers where name=?s", 'eglobal');
		if( ($this->handle=fopen($fn, "r")) ) {
			return true;
		}
        return false;
    }
// 0 SKU,1 ProductName,2 ProductPrice,3 Image,4 ProductDescription,5 Image2,6 Image3,7 Category,8 Link,9 LightWeight
    public function next() {
        $product = null;
        $supplier = 'eglobal';
		while($data = fgetcsv($this->handle, 10000, ',')) {
			$egcat = $this->extractCategory($data[7]);
			$cat = 'EG-'.$egcat;
			if(!in_array($cat, $this->categories)) {
				$this->categories[] = $cat;
			}
			break;
		}
		if($data) {
			//printf("%s. cat=%s\n", $this->counter, $cat);
			$pcode = 'EG-'.$data[0];
			$product = new StreamProduct();
			$product['category_id'] = $cat;
			$product['product_code'] = $pcode;
			$product['purchase_price'] = $this->cleanPriceField($data[2]);
			$product['amount'] = 5;
			$product['part_number'] = $pcode;
			$product['ean'] = '';
			$product['title'] = preg_replace("/[^a-zA-Z0-9()\s\w]/",'',$data[1]);
			$product['short_description'] = '';
			$product['full_description'] = $data[4];
			$product['search_words'] = $product['product_code'];
			$product['seo_name'] = $product['title'];
			$product['brand'] = reset(explode(' ',$product['title']));
			$product['image'] = [$data[3]];
			$product['shop_availability'] = $this->supplierShopFeedAvail;
			$product['skroutz_availability'] = $this->supplierShopFeedAvailSK;
			$product['status'] = 'A';
			$product['weight'] = 0.5;
			$product['height'] = 0;
			$product['width'] = 0;
			$product['depth'] = 0;
			$product['free_shipping'] = 'N';
			$product['cart_product_id'] = -1;
			$product['x_product_id'] = 0;
			$product['Supplier'] = $supplier;

		}
		else {
			throw new EndOfStreamException();
		}

        $this->counter++;
        return $product;
    }

	private function cleanPriceField($txt) {
		$out = $txt;
		//$out = str_replace("�", '',$txt);
		//$out = str_replace(',', '', $txt);
		$out = floatVal($out);
		return $out;
	}
	private function extractCategory($cat) {
		$out = '';
		$parts = explode('>',$cat);
		if(count($parts)>=2) {
			$out=  trim($parts[1]);
		}
		return $out;
	}
    public function close() {
		if($this->handle) {
			fclose($this->handle);
		}
		print_r($this->categories);
        return true;
    }

}