<?php

namespace Slx\ProductMerger\Exporter\ProductStream\Supplier;

use Tygh\Registry;
use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exporter\ProductStream\StreamProduct;
use Slx\ProductMerger\Exporter\ProductStream\SupplierProductStreamInterface;

/**
 * Dummy stream for demonstration of bypass supplier stream.
 *
 * Class TeleparthandmadeProductStream
 * @package Slx\ProductMerger\Exporter\ProductStream\Supplier
 */
class MpmobileProductStream implements SupplierProductStreamInterface {

    private $counter = 0;
	private $handle = null;
	private $categories;
	private $supplierShopFeedAvail, $supplierShopFeedAvailSK;
	
    public function open() {
		$fn = Registry::get('product_merger.mpmobile.fn');
		printf("%s Reading: %s\n", __METHOD__, $fn);
        $this->counter = 0;
		$this->categories = [];
		$this->supplierShopFeedAvail = db_get_field("select shop_availability from ?:suppliers where name=?s", 'mpmobile');
		$this->supplierShopFeedAvailSK = db_get_field("select skroutz_availability from ?:suppliers where name=?s", 'mpmobile');
		if( ($this->handle=fopen($fn, "r")) ) {
			$dummy = fgetcsv($this->handle, 1000, ';');	// grammi epikefalidon petama
			return true;
		}
        return false;
    }

    public function next() {
        $product = null;
        $supplier = 'mpmobile';
		while($data = fgetcsv($this->handle, 1000, ';')) {
			if($data[0]==1) {
				if(strpos($data[18],'#Spare Parts#')>0 && $data[38]==1) {
					$cats = explode('#~#', $data[18]);
					if(count($cats)>3) {
						$brand = $cats[1];
						$brandc = 'MP-SPARES-'.strtoupper($brand);
						break;
					}
				}
				elseif(strpos($data[18],'#Bluetooth#')>0 && $data[38]==1) {
					$cats = explode('#~#', $data[18]);
					if(count($cats)>2) {
						$brand = $cats[1];
						$brandc = 'MP-'.strtoupper($brand);	// => MP-BLUETOOTH
						break;
					}
				}
			}
		}
			
        if($data) {
			
			$images = [];
			$images[] = sprintf("https://mpsmobile.de/images/product_images/original_images/%s", $data[32]);
			for($i=42; $i<=46; $i++) {
				if(!empty($data[$i])) {
					$images[] = sprintf("https://mpsmobile.de/images/product_images/original_images/%s", $data[$i]);
				}
			}
			
			//$brand = $cats[1];
			//$brandc = 'MP-SPARES-'.strtoupper($brand);
			if(!in_array($brandc, $this->categories)) {
				$this->categories[] = $brandc;
			}
			$pcode = 'MPSP-'.$data[25];
            $product = new StreamProduct();
            $product['category_id'] = $brandc;
            $product['product_code'] = $pcode;
            $product['purchase_price'] = floatVal($data[33]);
            $product['amount'] = $data[38]==1 ? 5 : 0;
            $product['part_number'] = !empty($data[27]) ? $data[27] : $data[31];
            $product['ean'] = $data[26];
            $product['title'] = preg_replace("/[^a-zA-Z0-9()\s\w]/",'',$data[48]);
            $product['short_description'] = $data[49];
            $product['full_description'] = $data[49];
            $product['search_words'] = $product['product_code'];
            $product['seo_name'] = $product['title'];
            $product['brand'] = !empty($data[22]) ? $data[22] : 'OEM';
            $product['image'] = $images;
            $product['shop_availability'] = $this->supplierShopFeedAvail;
            $product['skroutz_availability'] = $this->supplierShopFeedAvailSK;
            $product['status'] = 'A';
            $product['weight'] = $data[37];
            $product['height'] = 0;
            $product['width'] = 0;
            $product['depth'] = 0;
            $product['free_shipping'] = 'N';
            $product['cart_product_id'] = -1;
            $product['x_product_id'] = 0;
            $product['Supplier'] = $supplier;
        }
        else {
            throw new EndOfStreamException();
        }
        $this->counter++;
        return $product;
    }

    public function close() {
		if($this->handle) {
			fclose($this->handle);
		}
		print_r($this->categories);
        return true;
    }

}