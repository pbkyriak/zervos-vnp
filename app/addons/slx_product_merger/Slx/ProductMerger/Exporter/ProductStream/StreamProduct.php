<?php

namespace Slx\ProductMerger\Exporter\ProductStream;

class StreamProduct implements \ArrayAccess, \Iterator {

    private $data;
    private $keys;
    public function __construct() {
        $this->data = array(
            'category_id' => 0,
            'product_code' => '',
            'purchase_price' => '',
            'amount' => '',
            'part_number' => '',
            'ean' => '',
            'title' => '',						'cotitle'=>'',
            'short_description' => '',
            'full_description' => '',
            'search_words' => '',
            'seo_name' => '',
            'brand' => '',
            'image' => '',
            'shop_availability' => 0,
            'skroutz_availability' => 0,
            'status' => 'A',
            'weight' => '',
            'height' => '',
            'width' => '',
            'depth' => '',
            'free_shipping' => 'N',
            'cart_product_id' => '',
            'x_product_id' => '',
            'Supplier' => '',
            'features' => [],
        );
        $this->keys = array_keys($this->data);
    }

    public function offsetExists($offset) {
        return isset($this->data[$offset]);
    }

    public function offsetGet($offset) {
        if( in_array($offset,$this->keys) ) {
            return $this->data[$offset];
        }
        else {
            throw new \OutOfBoundsException(sprintf("\nOffset='%s' not found!!\n Available keys: \n %s \n", $offset, implode(', ',$this->keys)));
        }
    }

    public function offsetSet($offset, $value) {
        if( in_array($offset,$this->keys) ) {
            $this->data[$offset]=$value;
        }
        else {
            throw new \OutOfBoundsException(sprintf("\nOffset='%s' not found!!\n Available keys: \n %s \n", $offset, implode(', ',$this->keys)));
        }
    }

    public function offsetUnset($offset) {
        throw new \Exception('you cannot unset items');
    }

    public function current() {
        return current($this->data);
    }

    public function next() {
        return next($this->data);
    }

    public function key() {
        return key($this->data);
    }

    public function valid() {
        return key($this->data) !== null;
    }

    public function rewind() {
        reset($this->data);
    }

}