<?php

namespace Slx\ProductMerger\Exporter\ProductStream;


interface SupplierProductStreamInterface {

    public function open();
    public function next() ;
    public function close();

}