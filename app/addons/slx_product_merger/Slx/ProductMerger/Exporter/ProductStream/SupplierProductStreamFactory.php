<?php

namespace Slx\ProductMerger\Exporter\ProductStream;

use Slx\ProductMerger\Exception\SupplierStreamNotSupported;
use Slx\ProductMerger\Exporter\ProductStream\Supplier\DummyProductStream;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;

/**
 * Class SupplierProductStreamFactory. For suppliers using merger nothing has to be done.
 * For byPass suppliers product stream has to be created for each one of them in folder Suppliers.
 * Naming convension XXXXProductStream where XXXX is lower cased supplier name as in cscart_suppliers.name.
 * Also the class must implement SupplierProductStreamInterface.
 *
 * @package Slx\ProductMerger\Exporter\ProductStream
 */
class SupplierProductStreamFactory {

    public static function getStream($supplier) {
        // get merger suppliers (merger suppliers use MergerProductStream for export)
        $mergerSuppliers = ImportProductStreamFactory::getSupportedSuppliers();
        $bypassSuppliers = self::getSupplierStreams();
        $stream = null;        if( in_array($supplier,$mergerSuppliers) ) {
            $stream = new MergerProductStream($supplier);
        }
        elseif(in_array($supplier, array_keys($bypassSuppliers))) {  // bypass suppliers
            $stream = new $bypassSuppliers[$supplier]();
        }
        if($stream==null) {
            throw new SupplierStreamNotSupported($supplier);
        }
        return $stream;
    }

    public static function getSupplierStreams() {
        $out = [];
        $dir = __DIR__.'/Supplier';
        $ns = '\\Slx\\ProductMerger\\Exporter\\ProductStream\\Supplier\\';
        foreach(glob($dir.'/*ProductStream.php') as $fn) {
            $fname = sprintf("%s\n", $fn);
            $class = basename($fn, '.php');
            $supplier = strtolower(str_replace('ProductStream','',$class));
            $fqn = $ns . $class;
            $r = new \ReflectionClass($fqn);
            if($r->implementsInterface('\\Slx\\ProductMerger\\Exporter\\ProductStream\\SupplierProductStreamInterface') && !$r->isAbstract()) {
                $supplierId = db_get_field("select supplier_id from ?:suppliers where name=?s", $supplier);
                if($supplierId) {
                    $out[$supplier] = $fqn;
                }
                else {
                    //printf("\nClass found for supplier '%s' but supplier is not found in cscart_suppliers\n", $supplier);
                }
            }
        }
        return $out;
    }
}