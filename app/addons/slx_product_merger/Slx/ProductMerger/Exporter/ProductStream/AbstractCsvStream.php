<?php

namespace Slx\ProductMerger\Exporter\ProductStream;

use Slx\ProductMerger\Exception\EndOfStreamException;
use Tygh\Registry;

abstract class AbstractCsvStream implements SupplierProductStreamInterface {

    protected $fname;
    protected $delimeter;
    protected $supplier;

    protected $handle;
    protected $lnCnt;

    public function open() {
        if(empty($this->fname) || empty($this->delimeter) || empty($this->supplier)) {
            throw new \Exception("Product stream not setuped");
        }
        $out = false;
        $fn = sprintf("%supdater_files/%s",Registry::get("config.dir.var"), $this->fname);
        $this->handle = fopen( $fn, 'r');
        if ($this->handle) {
            $out = true;
            $this->lnCnt=0;
        }
        return $out;
    }

    public function next(): StreamProduct {
        if(!$this->handle ) {
            throw new EndOfStreamException();
        }
        while (($row = fgetcsv($this->handle, 1000, $this->delimeter))) {
            $this->lnCnt++;
            if ($this->lnCnt == 1) {
                continue;
            }
            return $this->mapFields($row);
        }
        throw new EndOfStreamException();
    }

    public function close() {
        if( $this->handle ) {
            fclose($this->handle);
        }
    }

    /**
     * Converts $row to StreamProduct. $row is raw data read from csv file.
     *
     * @param $row
     * @return StreamProduct
     */
    abstract protected function mapFields($row) : StreamProduct;

}