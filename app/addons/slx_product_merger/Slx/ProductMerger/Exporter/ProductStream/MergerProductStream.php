<?php

namespace Slx\ProductMerger\Exporter\ProductStream;

use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exporter\Traits\SupplierCodePrefixesTrait;
use Slx\ProductMerger\Exporter\Traits\TextSanitizationTrait;
use Slx\ProductMerger\Icecat\IcecatProductLoader;
use Slx\ProductMerger\Icecat\IcecatProduct;

class MergerProductStream implements SupplierProductStreamInterface {

    use TextSanitizationTrait;
    use SupplierCodePrefixesTrait;

    private $supplier;
    private $supplierShopFeedAvail = 0;
    private $productIds;
    private $pos;
    private $itemCount;
    private $loader;

    public function __construct($supplier) {
        $this->supplier = $supplier;
        $this->supplierShopFeedAvail = db_get_field("select shop_availability from ?:suppliers where name=?s", $this->supplier);
        printf("supplier %s avail = %s\n",$this->supplier, $this->supplierShopFeedAvail);
        $this->productIds = [];
        $this->featureDefs = new FeatureExportDefinition();
    }

    public function open() {
        $this->productIds = db_get_fields(
            "SELECT p.id FROM slx_product p
             LEFT JOIN slx_supplier_product sp ON sp.id=p.eproduct_id
             WHERE p.eproduct_id!=0 AND blocked=0 AND sp.supplier=?s",
            $this->supplier
        );
        $this->pos = 0;
        $this->itemCount = count($this->productIds);
        $this->loader = new IcecatProductLoader();
        return ($this->itemCount>0);
    }

    public function next() {
        $out = null;
        if($this->pos<$this->itemCount) {
            $id = $this->productIds[$this->pos];
            $out = $this->processProduct($this->supplier, $id);
            $this->pos++;
        }
        else {
            throw new EndOfStreamException();
        }
        return $out;
    }

    public function close() {
        $this->productIds = [];
    }


    private function processProduct($supplier,$productId) {
        // load product data
        $xproduct = db_get_row("SELECT * FROM slx_product WHERE id=?i", $productId);
        $sproduct = db_get_row("SELECT price, availability, pcode FROM slx_supplier_product WHERE id=?i", $xproduct['eproduct_id']);
        $iproduct = $this->loader->loadById($xproduct['iproduct_id'], 'el');
        // format product data
        $xmldata = $this->formatProduct($supplier, $xproduct, $sproduct, $iproduct);
        return $xmldata;
    }


    private function formatProduct($supplier,$xproduct, $sproduct, IcecatProduct $iproduct) {

        $out = false;
        $short = $this->sanitizeShortDescription($iproduct->getDescription());
        //$title = $this->replaceForeignWords($xproduct['title']);
        $title = $xproduct['title'];
        $descr = $iproduct->getDescription();

        $data = new StreamProduct();
        $data['category_id'] = $iproduct->getCategoryId();
        $data['product_code'] = sprintf("%s%s", $this->supCodePrefixes[$supplier], $sproduct['pcode']);
        $data['purchase_price'] = $sproduct['price'];
        $data['amount'] = $sproduct['availability'];
        $data['part_number'] = $iproduct->getMpc();
        $data['ean'] = $xproduct['mean'];
        $data['title'] = $title;
        $data['cotitle'] = $iproduct->getCompositeTitle();
        $data['short_description'] = $short;
        $data['full_description'] = $descr;
        $data['search_words'] = $title;
        $data['seo_name'] = $title;
        $data['brand'] = $iproduct->getBrand();
        $data['image'] = $iproduct->getImages();
        $data['shop_availability'] = $this->supplierShopFeedAvail;
        $data['skroutz_availability'] = $this->supplierShopFeedAvail;
        $data['status'] = 'A';
        $data['weight'] = $iproduct->getWeight();
        $data['height'] = $iproduct->getDimH();
        $data['width'] = $iproduct->getDimW();
        $data['depth'] = $iproduct->getDimD();
        $data['free_shipping'] = 'N';
        $data['cart_product_id'] = $xproduct['cproduct_id'];
        $data['x_product_id'] = $xproduct['id'];
        $data['Supplier'] = $supplier;
        $data['features'] = $this->filterFeatures($iproduct);
        return $data;
    }

    private function filterFeatures($iproduct) {
        $out = [];
        if($this->featureDefs->categNeedsFeatures($iproduct->getCategoryId())) {
            $features = $iproduct->getFeatures();
            foreach($features as $feature) {
                if( ($fname=$this->featureDefs->isCategFeature($iproduct->getCategoryId(), $feature[0])) ) {
                    $out [$fname] = trim(sprintf("%s%s", $feature[1], $feature[2]));
                }
            }
        }
        return $out;
    }
}