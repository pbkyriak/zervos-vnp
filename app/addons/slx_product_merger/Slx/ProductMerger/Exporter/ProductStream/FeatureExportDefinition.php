<?php

namespace Slx\ProductMerger\Exporter\ProductStream;

/*
 * To format einai:
 *  $this->def['ena_string_otinanai_gia_na_katalabainoyme_emeis_ti_einai'] = [
 *      [ ta id ton icecat catigorion sta proionta ton opoion 8a eksax8oun ta features ],
 *      [
 *          feature_id => [ 'titlos feature opos einai ston icecat', 'titlos feature opos eiani ston icecat', ''],
 *          ...
 *
 *      ]
 *   ]
 */
class FeatureExportDefinition {

    private $def = [];

    private $categWithDef = [];

    public function __construct() {
        $this->def['mobiles'] = [
            [119, 1893],
            [
                1615=>['Μέγεθος οθόνης','Display diagonal'],
                1535=>['Επεξεργαστής αριθμό των πυρήνων','Processor cores'],
                1540=>['Εσωτερική μνήμη RAM','Internal memory'],
                1618=>['Ανάλυση οπίσθιας κάμερας (αριθμητικά)','Rear camera resolution (numeric)'],
            ]
        ];

        $this->def['tablets'] = [
            [897,2315],
            [
                1703=>['Μέγεθος οθόνης','Display diagonal'],
                1535=>['Επεξεργαστής αριθμό των πυρήνων','Processor cores'],
                1706=>['Εσωτερική μνήμη RAM','Internal memory'],
                1541=>['Maximum memory card size',],
            ]
        ];

        $this->def['monitor_and_tvs'] = [
            [1584,222,1441,940,1442,1487,2672,223],
            [
                1649=>['Μέγεθος οθόνης','Display diagonal'],
            ]
        ];


        $this->categWithDef = [];
        foreach ($this->def as $idx => $def) {
            foreach($def[0] as $c) {
                $this->categWithDef[$c] = $idx;
            }
        }

    }

    public function categNeedsFeatures($xCategId) {
        $out = false;
        if(isset($this->categWithDef[$xCategId])) {
            $out = true;
        }
        return $out;
    }

    public function isCategFeature($xCategId, $feature) {
        $out = false;
        if(isset($this->categWithDef[$xCategId])) {
            $idx = $this->categWithDef[$xCategId];
            foreach($this->def[$idx][1] as $fnames) {
                if(in_array($feature, $fnames)) {
                    $out = reset($fnames);
                }
            }
        }
        return $out;
    }

    public function getCategFeatureId($xCategId, $feature) {
        $out = false;
        printf("cid=%s\n",$xCategId);
        if(isset($this->categWithDef[$xCategId])) {
            $idx = $this->categWithDef[$xCategId];
            foreach($this->def[$idx][1] as $featureId => $fnames) {
                if(in_array($feature, $fnames)) {
                    $out = $featureId;
                }
            }
        }
        return $out;
    }
}
