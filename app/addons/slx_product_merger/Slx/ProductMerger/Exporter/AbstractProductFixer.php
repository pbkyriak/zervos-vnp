<?php

namespace Slx\ProductMerger\Exporter;

class AbstractProductFixer {

    protected $methods;

    public function __construct() {
        $this->methods = $this->getFixingMethods();
        //print_r($this->methods);
    }

    /**
     * @param $productData
     * @return bool True is good, False is bad
     */
    public function fixProduct(&$productData) {
        foreach($this->methods as $method) {
            $this->$method($productData);
        }
    }


    protected function getFixingMethods() {
        $out = [];
        $refl = new \ReflectionClass($this);
        foreach( $refl->getMethods() as $reflMeth) {
            if( strpos($reflMeth->getName(), 'fix')===0 ) {
                if($reflMeth->getName()=='fixProduct') {
                    continue;
                }
                if($reflMeth->isProtected()) {
                    if ($reflMeth->getNumberOfRequiredParameters() == 1) {
                        $reflParams = $reflMeth->getParameters();
                        if ($reflParams[0]->isPassedByReference()) {
                            $out[] = $reflMeth->getName();
                        }
                        else {
                            printf("Method '%s::%s' to be used as fixer first parameter '%s' must be passed by reference\n",
                                   $reflMeth->getDeclaringClass()->getName(),
                                   $reflMeth->getName(),
                                   $reflParams[0]->getName()
                            );
                        }
                    }
                    else {
                        printf("Method '%s::%s' to be used as fixer must have only one argument.\n",
                               $reflMeth->getDeclaringClass()->getName(),
                               $reflMeth->getName()
                        );
                    }
                }
                else {
                    printf("Method '%s::%s' to be used as fixer must be protected\n",
                           $reflMeth->getDeclaringClass()->getName(),
                           $reflMeth->getName()
                    );
                }

            }
        }
        sort($out);
        return $out;
    }

}