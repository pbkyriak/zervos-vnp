<?php

namespace Slx\ProductMerger\Exporter\Traits;


trait SupplierCodePrefixesTrait {
    private $supCodePrefixes = [
        'difox' => '11',
        'telepart' => '',
        'wave' => 'WA',
        'apide' => '',
    ];

}