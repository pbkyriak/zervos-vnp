<?php

namespace Slx\ProductMerger\Exporter\Traits;


trait TextSanitizationTrait {

    protected function isGermanText($testString) {

        $commonWords = array(
            "eine", "das", "ist", "du", "ich", "nicht", "die", "es", "und", "Sie", "der", "wir", "zu", "ein", "er", "sie", "mir", "mit", "ja", "wie", "den", "auf", "mich", "dass", "da", "hier", "eine", "wenn", "sind", "von", "dich"
        );

        $words = explode(" ", strtolower($testString));
        $freq = array();
        $freqs = array();
        $total = 0;
        foreach ($words as $word) {
            if (in_array($word, $commonWords)) {
                if (!isset($freqs[$word])) {
                    $freqs[$word] = 0;
                }
                $freqs[$word] ++;
                $total++;
            }
        }
        $out = false;
        if ($total / count($words) > 0.05 && count($freqs) / $total > 0.05) {
            $out = true;
        }
        return $out;
    }

    protected function replaceForeignWords($text) {

        $trans = array(
            'swartz' => '',
            'weiss' => '',
        );

        $words = explode(' ', $text);
        array_walk($words, function(&$item) use ($trans) {
            $lt = strtolower($item);
            if (isset($trans[$lt])) {
                $item = $trans[$lt];
            }
        });

        return trim(implode(' ', $words));
    }

    protected function sanitizeShortDescription($text) {
        $short = preg_replace(
            "/&#?[a-z0-9]{2,8};/i",
            "",
            strip_tags(
                str_replace(
                    '&amp;',
                    '&',
                    $text
                )
            )
        );
        $short = strlen($short) > 150 ? substr($short, 0, 150) . '...' : $short;
        return $short;
    }
}