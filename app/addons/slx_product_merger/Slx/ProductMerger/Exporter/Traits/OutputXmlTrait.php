<?php

namespace Slx\ProductMerger\Exporter\Traits;

use Tygh\Registry;trait OutputXmlTrait {

    protected $handle;

    private function openFile() {

	$out = true;

	$outfn = Registry::get('product_merger.merger_out_fn');

	$this->handle = fopen($outfn, "w+");

	if(!$this->handle) {

	Logger::getInstance()->log("Failed to open output file. ".$outfn); 

	$out = false;

	}

	else {

	$this->writelnFile("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");

	$this->writelnFile("<Products>");

	} 

	return $out;

    }

    private function closeFile() {

	if($this->handle) {

	$this->writelnFile("</Products>");

	fclose($this->handle);

	}

    }

    private function writelnFile($text, $tabs=0) {

	if($tabs) {

	fwrite($this->handle, str_repeat("\t", $tabs));

	}

	fwrite($this->handle, $text."\n");

    }

    private function writeInfo($data) {

	$this->writelnFile("<Product>",2);

	foreach ($data as $k => $v) {

	if($k=='features') {

	$this->writelnFile(sprintf("<%s><![CDATA[%s]]></%s>", $k, serialize($v), $k),3);

	}

	elseif (is_array($v)) { 

	foreach ($v as $vv) {

	$this->writelnFile(sprintf("<%s><![CDATA[%s]]></%s>", $k, $vv, $k),3);

	}

	} 
	
	else {

	if (in_array($k, array('short_description', 'full_description'))) {

	$n1 = base64_encode( $v);

	} 
	
	else {

	$n1 = $v;

	}

	$this->writelnFile(sprintf("<%s><![CDATA[%s]]></%s>", $k, $n1, $k),3);

	} 

	} 

	$this->writelnFile("</Product>",2);

    }
	
	}
	