<?php

namespace Slx\ProductMerger\ManMerger;


class Possible {

    public function getNext() {
        $out = [];
        $start=0;
        while(count($out)==0) {
            $product = db_get_row("SELECT * FROM slx_supplier_product WHERE istatus=1 LIMIT ?i,1",$start);//and id=313
            if ($product) {
                $title = db_get_field("SELECT title FROM slx_supplier_product_words WHERE sproduct_id=?i", $product['id']);
                if ($title) {
                    $results = $this->getMatchingTitles($title, $product['id'], $product['price'], 0.75);
                    if(count($results)>3) {
                        $out = [
                            'product' => $product,
                            'results' => $results,
                        ];
                        break;
                    }
                }
            }
            else {
                break;
            }
            $start++;
        }
        return $out;
    }

    public function getAll() {
        $out = [];
        $products = db_get_array("SELECT * FROM slx_supplier_product WHERE istatus=1");
        foreach($products as $product) {
            $title = db_get_field("SELECT title FROM slx_supplier_product_words WHERE sproduct_id=?i", $product['id']);
            if ($title) {
                $results = $this->getMatchingTitles($title, $product['id'], $product['price'], 0.75);
                if(count($results)>3) {
                    $out[] = $product['id'];
                }
            }
        }
        return $out;
    }

    private function dropFalsePositives($productId, $results) {
        foreach($results as $k => $v) {
            if($this->isFalsePositive($productId,$v['sproduct_id'])) {
                //printf("FP id=%s\n", $v['sproduct_id']);
                unset($results[$k]);
            }
        }
        return $results;
    }

    private function hasAlternative($word1, $word2) {
        if(strlen($word1)<4) {
            $word1 = str_pad($word1, 4,'0',STR_PAD_LEFT);
        }
        if(strlen($word2)<4) {
            $word2 = str_pad($word2, 4,'0',STR_PAD_LEFT);
        }
        $test = sprintf("+\"%s\" +\"%s\"", $word1, $word2);
        $alts = db_get_field("SELECT words FROM slx_product_match_alternative_words WHERE MATCH(words) AGAINST(?s IN BOOLEAN MODE)",$test);
        if($alts) {
            return true;
        }
        return false;
    }

    private function isFalsePositive($word1, $word2) {
        if(strlen($word1)<4) {
            $word1 = str_pad($word1, 4,'0',STR_PAD_LEFT);
        }
        if(strlen($word2)<4) {
            $word2 = str_pad($word2, 4,'0',STR_PAD_LEFT);
        }
        $test = sprintf("+\"%s\" +\"%s\"", $word1, $word2);

        $alts = db_get_field("SELECT sproduct_ids FROM slx_product_match_false_positive WHERE MATCH(sproduct_ids) AGAINST(?s IN BOOLEAN MODE)",$test);
        if($alts) {
            return true;
        }
        return false;
    }

    private function getMatchingTitles($title,$productId, $price, $tolerance) {
        $out = [];
        $sql = db_quote(
            "SELECT sproduct_id,title,price,MATCH(title) AGAINST(?s IN BOOLEAN MODE) AS scoreF 
                     FROM slx_supplier_product_words 
                     WHERE 
                        sproduct_id!=?i
                        and abs(?d-price)/price< 0.15
                        and MATCH(title) AGAINST(?s IN BOOLEAN MODE)
                     HAVING scoreF>2 limit 0,20
                     ",
            $title,
            $productId,
            $price,
            $title
        );
        $results = db_get_array($sql);
        if($results) {

            $results = $this->dropFalsePositives($productId,$results);
            $control_parts = explode(" ", $title);
            foreach ($results as $result) {
                $test_parts = explode(' ', $result['title']);
                $same_parts = array_intersect($control_parts, $test_parts);
                if(count($same_parts)>count($control_parts)*0.6) {
                    //printf("Rtitle:%s\n", $result['title']);
                    //printf("Same parts:\n");
                    //print_r($same_parts);
                    $diff_parts2 = array_diff($control_parts, $test_parts);
                    $diff_parts1 = array_diff($test_parts, $control_parts);
                    //printf("\t%s total: %s same: %s diff: %s-%s\n", $result['sproduct_id'], count($control_parts), count($same_parts), count($diff_parts1), count($diff_parts2));
                    if ($diff_parts1) {
                        foreach($diff_parts1 as $k1 => $v1) {
                            foreach($diff_parts2 as $k2 => $v2) {
                                if($this->hasAlternative($v1,$v2)) {
                                    $same_parts[] = $v1;
                                    unset($diff_parts2[$k2]);
                                    unset($diff_parts1[$k1]);
                                }
                            }
                        }
                    }
                    if(count($same_parts)/count($control_parts)>=$tolerance) {
                        $result['scoreT'] =count($same_parts)/count($control_parts);
                        $out[] = $result;
                    }
                }
            }
        }
        return $out;
    }
}