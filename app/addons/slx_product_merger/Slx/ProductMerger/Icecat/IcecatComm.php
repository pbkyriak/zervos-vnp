<?php

namespace Slx\ProductMerger\Icecat;
use Tygh\Registry;
/**
 * Description of IcecatComm
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class IcecatComm {
    
    private $api_username;
    private $api_password;
    private $api_lang;

    public function __construct() {
        $this->api_username = Registry::get('product_merger.icecat.api_username');
        $this->api_password = Registry::get('product_merger.icecat.api_password');
        $this->api_lang = 'EL';
    }
    
    public function get($ean, $lang_code) {
        $api_username = $this->api_username;
        $api_password = $this->api_password;
        $api_lang = strtoupper($lang_code);

        // Get the product specifications in XML format
        $context = stream_context_create(array(
            'http' => array(
                'header' => "Authorization: Basic " . base64_encode($api_username . ":" . $api_password)
            )
        ));
        $iurl = 'http://data.icecat.biz/xml_s3/xml_server3.cgi?ean_upc=' . urlencode($ean) . ';lang=' . $api_lang . ';output=productxml';
        //printf("%s\n",$iurl);
        //$data = file_get_contents($iurl, false, $context);
        $fn = sprintf("%s%s.xml",Registry::get("product_merger.icecat.tmp_dir"),$ean);
        $cmd = sprintf("wget -q --http-user=%s --http-password=%s -O %s %s",$this->api_username, $this->api_password, $fn, $iurl);
        //printf("%s\n",$cmd);
        exec($cmd);
        $data = null;
        if(file_exists($fn)) {
            $data = file_get_contents($fn);
            if (empty($data)) {
                $data = null;
            }
            unlink($fn);
        }
        return $data;
    }
}

