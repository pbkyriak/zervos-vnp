<?php

namespace Slx\ProductMerger\Icecat;

use Slx\ProductMerger\Icecat\IcecatProduct;

/**
 * Description of IceCatParser
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class IcecatParser {

    private $featureGroups = array();
    
    public function parse($ean,$xmlstring, $lang_code) {
        $prod = null;
        $this->featureGroups = array();
        $xml = new \SimpleXMLElement($xmlstring);
        $products = $xml->xpath("//Product");
        if ($products) {
            $xmlProd = $products[0];
            $prod = new IcecatProduct();
            $prod->setLangCode($lang_code);
            $prod->setEan($ean);
            $prod->setStatus($xmlProd['Code']);
            if ($prod->getStatus() == -1) {
                $prod->setErrorMsg($xmlProd['ErrorMessage']);
            } else {
                $prod->setIceId($xmlProd['ID']);
                $prod->setMpc($xmlProd['Prod_id']);
                $prod->setTitle($xmlProd['Name']);
                $this->getEan($xml, $prod);
                $this->getImages($xml, $prod);
                $this->getFeatures($xml, $prod);
                $this->getLongSummary($xml, $prod);
                $this->parseFeatureGroups($xml,$prod);
                $this->parseSupplier($xml, $prod);
                $this->parseCategoryName($xml, $prod);
            }
        }
        return $prod;
    }

    private function getEan($xml, $prod) {
        $items = $xml->xpath("//EANCode");
        if ($items) {
            foreach ($items as $item) {
                $prod->addEan((string) $item['EAN']);
            }
        }        
    }
    
    private function getImages($xml, $prod) {
        $images = $xml->xpath("//ProductGallery/ProductPicture");
        if ($images) {
            foreach ($images as $image) {
                $prod->addImage((string) $image['Pic']);
            }
        }
    }

    private function getFeatures($xml, $prod) {
        $xfeatures = $xml->xpath("//ProductFeature");
        foreach ($xfeatures as $xfeature) {
            $name = '';
            $sign = '';
            $value = $xfeature['Value'];
            $fgId = $xfeature['CategoryFeatureGroup_ID'];
            $xsigns = $xfeature->xpath(".//Feature/Measure//Sign");
            if ($xsigns) {
                foreach ($xsigns as $xsign) {
                    $sign = (string) $xsign;
                }
            }
            $name = '';
            $xname = $xfeature->xpath(".//Name");
            if ($xname) {
                $name = (string) $xname[0]['Value'];
            }
            $prod->addFeature($name, $value, $sign, $fgId);
        }
    }

    private function parseFeatureGroups($xml, $prod) {
        $xfeatures = $xml->xpath("//CategoryFeatureGroup");
        foreach ($xfeatures as $xfeature) {
            $name = '';
            $fgId = $xfeature['ID'];
            $xname = $xfeature->xpath(".//Name");
            if ($xname) {
                $name = (string) $xname[0]['Value'];
            }
            $prod->addFeatureGroup($name, (string)$fgId);
        }        
    }
    
    private function getLongSummary($xml, $prod) {
        $summaries = $xml->xpath("//SummaryDescription/LongSummaryDescription");
        if( $summaries ) {
            $prod->setDescription((string)$summaries[0]);
        }
    }

    private function parseSupplier($xml, $prod) {
        $items = $xml->xpath("Product/Supplier");
        if( $items ) {
            $prod->setBrand((string)$items[0]['Name']);
        }
    }

    private function parseCategoryName($xml, $prod) {
        $items = $xml->xpath("//Category");
        if( $items ) {
            $prod->setCategoryId((int)$items[0]['ID']);
            $xname = $items[0]->xpath(".//Name");
            if ($xname) {
                $prod->setCategory( (string) $xname[0]['Value']);
            }
        }
        
    }
}
