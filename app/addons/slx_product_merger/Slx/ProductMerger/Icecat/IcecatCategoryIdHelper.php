<?php

namespace Slx\ProductMerger\Icecat;


class IcecatCategoryIdHelper {

    public static function idToName($id) {
        return 'X-'.$id;
    }

    public static function nameToId($name) {
        return str_replace('X-', '', $name);
    }
}