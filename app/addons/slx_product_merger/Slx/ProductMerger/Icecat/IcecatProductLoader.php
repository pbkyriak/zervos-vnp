<?php

namespace Slx\ProductMerger\Icecat;

/**
 * Description of IcecatProductLoader
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class IcecatProductLoader {

    private $lang_code;
    /**
     * 
     * @param int $id
     * @param boolean $onlyCheckedImages
     * @return IcecatProduct
     */
    public function loadById($id,$lang_code, $onlyCheckedImages=false) {
        $out = null;
        $this->lang_code = $lang_code;
        $row = db_get_row("SELECT * FROM slx_icecat_product WHERE id=?i and status=1", $id);
        if($row) {
            $out = $this->loadInfo($row, $onlyCheckedImages);
        }
        return $out;
    }
    
    public function loadByPcode($pCode, $onlyCheckedImages=false) {
        $out = null;
        $row = db_get_row("SELECT * FROM slx_icecat_product WHERE pcode=?s and status=1", $pCode);
        if( $row ) {
            $out = $this->loadInfo($row,$onlyCheckedImages);
        }
        return $out;
    }
    
    private function loadInfo($row,$onlyCheckedImages) {
        $out = new IcecatProduct();
        $out->setPcode($row['pcode']);
        $out->setEan($row['ean']);
        $out->setStatus($row['status']);
        $this->loadDescriptions($out, $row['id']);
        $this->loadImages($row['id'], $out,$onlyCheckedImages);
        $this->getDimensions($row['id'], $out);
        $out->setMpc($row['mpc']);
        $out->setBrand($row['brand']);
        $out->setCategory($row['category']);
        $out->setCategoryId($row['category_id']);
        $this->loadFeatures($row['id'], $out);
        return $out;
    }
    
    private function loadImages($productId, $product,$onlyCheckedImages) {
        if( $onlyCheckedImages ) {
            $rows = db_get_array("SELECT * FROM slx_icecat_image WHERE product_id=?i and checked=1", $productId);
        }
        else {
            $rows = db_get_array("SELECT * FROM slx_icecat_image WHERE product_id=?i", $productId);
        }
        foreach($rows as $row) {
            $product->addImage($row['image']);
        }
    }
    
    private function getDimensions($productId, $product) {
        $product->setDimH($this->getDimension($productId, 'Ύψος%'));
        $product->setDimW($this->getDimension($productId, 'Πλάτος%'));
        $product->setDimD($this->getDimension($productId, 'Βάθος%'));
        $product->setWeight($this->getDimension($productId, 'Βάρος%'));
    }
    
    private function getDimension($productId, $fname) {
        $out = 0;
        $row = db_get_row(
                "SELECT max(fvalue) as val FROM slx_icecat_feature where product_id=?i and fname like ?s",
                $productId,
                $fname
                );
        if( $row ) {
            $out = $row['val'];
        }
        return $out;
    }
    
    /**
     * 
     * @param int $productId
     * @param IcecatProduct $product
     */
    private function loadFeatures($productId, $product) {
        $rows = db_get_array("
          SELECT f.fname, f.fvalue, f.fsign, f.fg_id, fg.title fg_title, f.lang_code 
          from slx_icecat_feature f 
          left join slx_icecat_feature_group fg on (f.fg_id=fg.group_id AND f.lang_code=fg.lang_code) 
          WHERE f.product_id=?i  AND f.lang_code=?s
          ", $productId, $this->lang_code);
        foreach($rows as $row) {
            $product->addFeature($row['fname'], $row['fvalue'], $row['fsign'], $row['fg_id']);
            $product->addFeatureGroup($row['fg_title'], $row['fg_id']);
        }
    }

    private function loadDescriptions($product, $productId) {
        $row = db_get_row("select * from slx_icecat_product_description where iproduct_id=?i and lang_code=?s", $productId, $this->lang_code);
        if($row) {
            $product->setTitle($row['title']);
            $product->setDescription($row['overview']);
        }
    }

}
