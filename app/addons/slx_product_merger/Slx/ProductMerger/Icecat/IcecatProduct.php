<?php

namespace Slx\ProductMerger\Icecat;

/**
 * Description of IcecatProduct
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class IcecatProduct {

    private $images = array();
    private $features = array();
    private $fgroups = array();
    private $ean = null;
    private $status = 0;
    private $errorMsg = '';
    private $description = array();
    private $eans = array();
    private $pCode = '';
    private $dimH, $dimD, $dimW, $weight;
    private $mpc='';
    private $brand;
    private $title;
    private $categoryId;
    private $category;
    private $ice_id;
    private $lang_code;

    function getIceId() {
        return $this->ice_id;
    }

    function setIceId($ice_id) {
        $this->ice_id = $ice_id;
    }

    public function setEan($ean) {
        $this->ean = $ean;
    }

    public function getEan() {
        return $this->ean;
    }
    public function setStatus($status) {
        $this->status = $status;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setErrorMsg($status) {
        $this->errorMsg = $status;
    }

    public function getErrorMsg() {
        return $this->errorMsg;
    }

    public function addImage($img) {
        $this->images[] = $img;
    }

    public function addFeature($name, $measure, $sign, $fgId) {
        $this->features[] = array($name, $measure, $sign, $fgId);
    }

    public function getFeatures() {
        return $this->features;
    }
    
    public function addFeatureGroup($name, $fgId) {
        $this->fgroups[$fgId] = array('id'=>$fgId, 'name'=>$name);
    }
    
    public function getFeatureGroups() {
        return $this->fgroups;
    }
    
    public function getImages() {
        return $this->images;
    }
    
    public function setDescription($descr) {
        $this->description = $descr;
    }
    
    public function getDescription() {
        return $this->description;
    }

    public function addEan($ean) {
        $this->eans[] = $ean;
        $this->eans = array_unique($this->eans);
    }
    
    public function getEans() {
        return $this->eans;
    }

    public function setPcode($pcode) {
        $this->pCode = $pcode;
    }
    
    public function getPcode() {
        return $this->pCode;
    }
    
    function getDimH() {
        return $this->dimH;
    }

    function getDimD() {
        return $this->dimD;
    }

    function getDimW() {
        return $this->dimW;
    }

    function getWeight() {
        return $this->weight;
    }

    function setDimH($dimH) {
        $this->dimH = $dimH;
    }

    function setDimD($dimD) {
        $this->dimD = $dimD;
    }

    function setDimW($dimW) {
        $this->dimW = $dimW;
    }

    function setWeight($weight) {
        $this->weight = $weight;
    }

    function getMpc() {
        return $this->mpc;
    }

    function setMpc($mpc) {
        $this->mpc = $mpc;
    }

    public function getBrand() {
        return $this->brand;
    }
    
    public function setBrand($v) {
        $this->brand = $v;
    }

    public function getTitle() {
        return $this->title;
    }
    
    public function setTitle($v) {
        $this->title = $v;
    }
    function getCategoryId() {
        return $this->categoryId;
    }

    function getCategory() {
        return $this->category;
    }

    function setCategoryId($categoryId) {
        $this->categoryId = $categoryId;
    }

    function setCategory($category) {
        $this->category = $category;
    }

    public function getLangCode() {
        return $this->lang_code;
    }

    public function setLangCode($lang_code) {
        $this->lang_code = $lang_code;
    }

	public function getCompositeTitle() {
		$out = substr($this->getDescription(),0 , strpos($this->getDescription(),'. '));
		if ($this->features) {
			$fts = [] ;
			//$feats = array_slice($this->features,0,4);
			$ftCnt=0;
			foreach ($this->features as $feat) {
			    if(strlen($feat[1])<20) {
			        $ft = trim($feat[1] . $feat[2]);
			        if(strlen($ft)>1) {
				        $fts[] = $ft;
				        $ftCnt++;
				    }
			    }
			    if($ftCnt==4) {
			        break;
			    }
			}
			$out .= ' '. implode(' ', $fts);
			$out = trim($out);
		}
		return $out;
	}
	
    public function __toString() {
        $out = sprintf("Status=%s\n", $this->status);
        if ($this->status == -1) {
            $out .= sprintf("ErrorMsg = %s\n", $this->errorMsg);
        } else {
            if ($this->images) {
                $out .= "Images\n";
                foreach ($this->images as $img) {
                    $out .= sprintf("\t%s\n", $img);
                }
            }
            if ($this->features) {
                $out .= "features\n";
                foreach ($this->features as $feat) {
                    $out .= sprintf("\t%s=%s %s\n", $feat[0], $feat[1], $feat[2]);
                }
            }
        }
        return $out;
    }

}
