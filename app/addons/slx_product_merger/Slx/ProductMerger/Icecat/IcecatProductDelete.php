<?php

namespace Slx\ProductMerger\Icecat;

/**
 * Description of IcecatProductDelete
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 29 Νοε 2015
 */
class IcecatProductDelete {

    public function deleteById($id) {
        db_query('DELETE FROM slx_icecat_product WHERE id=?i', $id);
        db_query('DELETE FROM slx_icecat_product_description WHERE iproduct_id=?i', $id);
        db_query('DELETE FROM slx_icecat_image WHERE product_id=?i', $id);
        db_query('DELETE FROM slx_icecat_ean WHERE product_id=?i', $id);
        db_query('DELETE FROM slx_icecat_feature WHERE product_id=?i', $id);
        db_query('DELETE FROM slx_supplier_product WHERE product_id=?i', $id);
    }

    public function deleteByEan($ean) {
        $out = true;
        try {
            $id = db_get_field("SELECT id from slx_icecat_product WHERE ean=?s", $ean);
            if ($id) {
                $this->deleteById($id);
            }
        } catch (\Exception $ex) {
            $out = false;
        }
        return $out;
    }

}
