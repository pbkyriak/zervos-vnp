<?php

namespace Slx\ProductMerger\Icecat;

use Slx\ProductMerger\Icecat\IcecatProduct;
/**
 * Description of IcecatProductPersist
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class IcecatProductPersist {

    private $deleter;

    public function __construct() {
        $this->deleter = new IcecatProductDelete();
    }
        
    public function persist($products) {
        $persisted = false;
        db_query("start transaction");
        $lang_codes = array_keys($products);
        $lang_code = reset($lang_codes);
        $product = $products[$lang_code];
        $this->deleter->deleteByEan($product->getEan());
        $productId = $this->persistProduct($products, $lang_code);
        if($productId) {
            if( $this->persistFeatures($productId, $products) ) {
                if( $this->persistImages($productId, $product) ) {
                    if( $this->persistEans($productId, $product) ) {
                        $persisted = true;
                    }
                }
            }
        }
        if( $persisted ) {
            db_query("commit");
        }
        else {
            db_query("rollback");
        }
        return $productId;
    }
    
    private function persistProduct($products, $lang_code) {
        $product = $products[$lang_code];
        $lang_codes = array_keys($products);
        $data = array(
            'ice_id' => $product->getIceId(),
            'pcode' => $product->getPcode(),
            'status' => $product->getStatus(),
            'lastchecked' => time(),
            'ean' => $product->getEan(),
            'overview_el' => $product->getDescription('el'),
            'overview_en' => '',
            'mpc' => $product->getMpc(),
            'brand' => $product->getBrand(),
            'title' => $product->getTitle(),
            'category' => $product->getCategory(),
            'category_id' => $product->getCategoryId(),
        );
        try {
            $productId = db_query("INSERT INTO slx_icecat_product ?e", $data);
            foreach( $lang_codes as $lang_code ) {
                $datad = [
                    'title' => $products[$lang_code]->getTitle(),
                    'overview' => $products[$lang_code]->getDescription(),
                    'iproduct_id' => $productId,
                    'lang_code' => $lang_code,
                ];
                db_query("REPLACE INTO slx_icecat_product_description ?e", $datad);
            }
        }
        catch(\Exception $ex) {
            printf("Exception!! %s\n", $ex->getMessage());
            return false;
        }
        return $productId;
    }
    
    private function persistFeatures($productId, $products) {
        $out = true;
        $lang_codes = array_keys($products);
        foreach ($lang_codes as $lang_code) {
            $product = $products[$lang_code];
            foreach ($product->getFeatures() as $pFeature) {
                $data = array(
                    'product_id' => $productId,
                    'fname' => $pFeature[0],
                    'fvalue' => $pFeature[1],
                    'fsign' => $pFeature[2],
                    'fg_id' => $pFeature[3],
                    'lang_code' => $lang_code,
                );

                try {
                    db_query("INSERT INTO slx_icecat_feature ?e", $data);
                }
                catch (Exception $ex) {
                    printf("Exception!! %s\n", $ex->getMessage());
                }
            }
            $this->persistFeatureGroups($product->getFeatureGroups(), $lang_code);
        }
        return $out;
    }
    
    private function persistEans($productId, $product) {
        $out = true;
        $stored = 0;
        $eans = array_filter($product->getEans());
        foreach($eans as $item) {
            $data = array(
                'product_id' => $productId,
                'ean' => $item,
            );
            try {
                db_query("INSERT INTO slx_icecat_ean ?e", $data);
                $stored++;
            }
            catch (\Exception $ex) {
                printf("Exception!! %s\n", $ex->getMessage());
            }
        }
        if($stored==0) {
            $out = false;
        }
        return $out;
    }
    
    private function persistImages($productId, $product) {
        $out = true;
        foreach($product->getImages() as $pImage) {
            $data = array(
                'product_id' => $productId,
                'image' => $pImage,
                'checked' => 0,
            );
            if( $this->urlExists($pImage)) {
                $data['checked']=1;
            }
            try {
                db_query("INSERT INTO slx_icecat_image ?e", $data);
            }
            catch (\Exception $ex) {
                printf("Exception!! %s\n", $ex->getMessage());
            }
        }
        return $out;
    }
    
    private function urlExists($url, array $failCodeList = array(404)) {

        $exists = false;
        try {
            $handle = curl_init($url);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($handle, CURLOPT_HEADER, true);
            curl_setopt($handle, CURLOPT_NOBODY, true);
            curl_setopt($handle, CURLOPT_USERAGENT, true);
            $headers = curl_exec($handle);
            curl_close($handle);
        }
        catch(\Exception $ex) {
            return false;
        }
        if (empty($failCodeList) or ! is_array($failCodeList)) {
            $failCodeList = array(404);
        }
        if (!empty($headers)) {
            $exists = true;
            $headers = explode(PHP_EOL, $headers);
            foreach ($failCodeList as $code) {
                if (is_numeric($code) and strpos($headers[0], strval($code)) !== false) {
                    $exists = false;
                    break;
                }
            }
        }
        return $exists;
    }
    
    public function persistFeatureGroups($featureGroups, $lang_code) {
        foreach($featureGroups as $featureGroup) {
            $sql = "REPLACE INTO slx_icecat_feature_group (group_id, title, lang_code) VALUES (?i, ?s, ?s)";
            try {
                db_query($sql, $featureGroup['id'],$featureGroup['name'], $lang_code);
            }
            catch (\Exception $ex) {
                printf("Exception!! %s\n", $ex->getMessage());
            }
        }
    }
}
