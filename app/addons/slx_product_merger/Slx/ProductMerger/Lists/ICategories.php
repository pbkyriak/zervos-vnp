<?php

namespace Slx\ProductMerger\Lists;

use Slx\ProductMerger\Icecat\IcecatCategoryIdHelper;

class ICategories {

    public function get($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE) {
        // Set default values to input params
        $default_params = array(
            'page' => 1,
            'items_per_page' => $items_per_page
        );

        $params = array_merge($default_params, $params);

        $condition = 'WHERE 1';

        if (!empty($params['title'])) {
            $condition .= db_quote(" AND category LIKE ?l", str_replace('*', '%', $params['title']));
        }
        if (!empty($params['icid'])) {
            $condition .= db_quote(" AND category_id=?s", $params['icid']);
        }

        $select = "SELECT category_id,GROUP_CONCAT(DISTINCT category) titles, COUNT(*) AS cnt FROM slx_icecat_product $condition GROUP BY category_id ORDER BY category_id";

        if (!empty($params['items_per_page'])) {
            $params['total_items'] = db_get_field("
                    SELECT COUNT(category_id)
                    FROM ($select) as a
                    ");
            $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        }

        $results = db_get_array(
            $select . " $limit"
        );

        if($results) {
            $icIds = [];
            foreach ($results as $result) {
                $icIds[]= IcecatCategoryIdHelper::idToName($result['category_id']);
            }
            $cIds = db_get_array("
                select m.category_id ccategory_id, m.supplier_category_name, c.category as ccategory 
                from ?:categories_supplier_matching m 
                left join ?:category_descriptions c on c.category_id=m.category_id and c.lang_code=?s 
                where supplier_category_name in (?a)
                ",
                $lang_code,
                $icIds
            );
            foreach ($results as $idx => $result) {
                $n = IcecatCategoryIdHelper::idToName($result['category_id']);
                foreach($cIds as $cRow) {
                    if($cRow['supplier_category_name']==$n) {
                        if (!isset($results[$idx]['ccategory_id'])) {
                            $results[$idx]['ccategory_id'] = [];
                            $results[$idx]['ccategory'] = [];
                        }
                        $results[$idx]['ccategory_id'][] = $cRow['ccategory_id'];
                        $results[$idx]['ccategory'][] = $cRow['ccategory'];
                    }
                }
            }
        }
        return array($results, $params);
    }

    public function getCrossLinked($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE) {
        // Set default values to input params
        $default_params = array(
            'page' => 1,
            'items_per_page' => $items_per_page
        );

        $params = array_merge($default_params, $params);
        $select = "select supplier_category_name, count(category_id) cnt from cscart_categories_supplier_matching group by supplier_category_name having cnt>1 order by supplier_category_name";

        if (!empty($params['items_per_page'])) {
            $params['total_items'] = db_get_field("
                    SELECT COUNT(supplier_category_name)
                    FROM ($select) as a
                    ");
            $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        }
        $results = db_get_array(
            $select . " $limit"
        );
        foreach ($results as $idx => $result) {
            $results[$idx]['icategory_id'] = IcecatCategoryIdHelper::nameToId($result['supplier_category_name']);
        }
        return array($results, $params);
    }

    public function getICategory($icId) {
        $name = IcecatCategoryIdHelper::idToName($icId);
        $category_data = db_get_row("SELECT category_id,GROUP_CONCAT(DISTINCT category) titles, COUNT(*) AS cnt FROM slx_icecat_product WHERE category_id=?i", $icId);
        $category_data['linked_categories'] = db_get_array(
            "select m.category_id, cd.category from cscart_categories_supplier_matching m left join cscart_category_descriptions cd on cd.category_id=m.category_id and cd.lang_code='el' where m.supplier_category_name=?s",
            $name
        );
        return $category_data;
    }

    public function linkICategoryWithCategory($icategory_id, $category_id) {
        $supplier_category_name = db_get_field("select supplier_category_name from ?:categories where category_id=?s", $category_id);
        $catNames = array_filter(explode(",",$supplier_category_name));

        $iName = IcecatCategoryIdHelper::idToName($icategory_id);
        if(!in_array($iName, $catNames)) {
            $catNames[] = $iName;
        }
        db_query("update ?:categories set supplier_category_name=?s where category_id=?i", implode(',',$catNames), $category_id);
        db_query("delete from ?:categories_supplier_matching where category_id=?i and supplier_category_name=?s", $category_id, $iName);
        db_query("INSERT INTO ?:categories_supplier_matching (category_id, supplier_category_name) VALUES (?i, ?s)", $category_id, $iName);
    }

    public function unlinkICategoryWithCategory($icategory_id, $category_id) {
        $supplier_category_name = db_get_field("select supplier_category_name from ?:categories where category_id=?s", $category_id);
        $catNames = array_filter(explode(",",$supplier_category_name));

        $iName = IcecatCategoryIdHelper::idToName($icategory_id);
        if(($idx=array_search($iName, $catNames))!==false) {
            unset($catNames[$idx]);
        }
        db_query("update ?:categories set supplier_category_name=?s where category_id=?i", implode(',',$catNames), $category_id);
        db_query("delete from ?:categories_supplier_matching where category_id=?i and supplier_category_name=?s", $category_id, $iName);
    }

}