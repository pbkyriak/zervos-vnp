<?php

namespace Slx\ProductMerger\Lists;


use Slx\ProductMerger\Icecat\IcecatProductLoader;

class PendingIProducts {

    public function get($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE) {
        // Set default values to input params
        $default_params = array(
            'page' => 1,
            'items_per_page' => $items_per_page
        );

        $params = array_merge($default_params, $params);

        // Unset all SQL variables
        $fields = $joins = array();
        $condition = $sorting = $limit = $group = '';

        $fields = array(
            'a.id',
            'a.ean',
            'a.lastchecked',
            'a.source',
            'a.pcode',
        );

        $condition = 'WHERE 1';

        if (!empty($params['ean'])) {
            $condition .= db_quote(" AND a.ean = ?s", $params['ean']);
        }
        if (!empty($params['source'])) {
            $condition .= db_quote(" AND a.source=?s", $params['source']);
        }
        if (!empty($params['pcode'])) {
            $condition .= db_quote(" AND a.pcode = ?s", $params['pcode']);
        }
        if (!empty($params['checked'])) {
            if($params['checked']=='no') {
                $condition .= db_quote(" AND a.lastchecked=0");
            }
            elseif($params['checked']=='yes') {
                $condition .= db_quote(" AND a.lastchecked!=0");
            }
        }
        if (!empty($params['pcode'])) {
            //$condition .= db_quote(" AND a.title LIKE ?l", '%' . $params['title'] . '%');
            $condition .= db_quote(" AND a.pcode=?s", $params['pcode']);
        }

        $group = '';
        $sorting = "ORDER BY a.pcode, a.id";

        if (!empty($params['items_per_page'])) {
            $params['total_items'] = db_get_field("
                    SELECT COUNT(DISTINCT a.id)
                    FROM slx_icecat_product_pending AS a
                    $condition");
            $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        }

        $results = db_get_array(
            "SELECT " . implode(', ', $fields) . " FROM slx_icecat_product_pending as a " .
            implode(' ', $joins) .
            " $condition $group $sorting $limit"
        );
        return array($results, $params);
    }

}