<?php

namespace Slx\ProductMerger\Lists;


use Slx\ProductMerger\Icecat\IcecatProductLoader;

class IProducts {

    public function get($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE) {
        // Set default values to input params
        $default_params = array(
            'page' => 1,
            'items_per_page' => $items_per_page
        );

        $params = array_merge($default_params, $params);

        // Unset all SQL variables
        $fields = $joins = array();
        $condition = $sorting = $limit = $group = '';

        $fields = array(
            'a.id',
            'a.title',
            'a.brand',
            'a.mpc',
            'a.ean',
            'a.category',
        );

        $condition = 'WHERE 1';

        if (!empty($params['mean'])) {
            $condition .= db_quote(" AND a.ean = ?s", $params['mean']);
        }
        if (!empty($params['mpc'])) {
            $condition .= db_quote(" AND a.mpc LIKE ?l", str_replace('*', '%', $params['mpc']));
        }
        if (!empty($params['brand'])) {
            $condition .= db_quote(" AND a.brand = ?s", $params['brand']);
        }
        if (!empty($params['title'])) {
            $condition .= db_quote(" AND a.title LIKE ?l", str_replace('*', '%', $params['title']));
        }
        if (!empty($params['cid'])) {
            $condition .= db_quote(" AND a.category_id = ?s", $params['cid']);
        }
        $group = '';
        $sorting = "ORDER BY a.title, a.id";

        if (!empty($params['items_per_page'])) {
            $params['total_items'] = db_get_field("
                    SELECT COUNT(DISTINCT a.id)
                    FROM slx_icecat_product AS a
                    $condition");
            $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        }

        $results = db_get_array(
            "SELECT " . implode(', ', $fields) . " FROM slx_icecat_product as a " .
            implode(' ', $joins) .
            " $condition $group $sorting $limit"
        );
        return array($results, $params);
    }

    public function getProduct($id, $lang_code) {
        $loader = new IcecatProductLoader();
        return $loader->loadById($id, $lang_code);
    }
}