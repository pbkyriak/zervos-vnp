<?php

namespace Slx\ProductMerger\Lists;


class XProducts {

    public function get($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE) {
        // Set default values to input params
        $default_params = array(
            'page' => 1,
            'items_per_page' => $items_per_page
        );

        $params = array_merge($default_params, $params);

        // Unset all SQL variables
        $fields = $joins = array();
        $condition = $sorting = $limit = $group = '';

        $fields = array(
            'a.id',
            'a.title',
            'a.mean',
            'a.istatus',
        );

        $condition = 'WHERE 1';

        if (!empty($params['mean'])) {
            $condition .= db_quote(" AND a.mean = ?s", $params['mean']);
        }
        if (!empty($params['title'])) {
            //$condition .= db_quote(" AND a.title LIKE ?l", '%' . $params['title'] . '%');
            $condition .= db_quote(" AND a.title LIKE ?l", str_replace('*', '%', $params['title']));
        }
        if (!empty($params['iid'])) {
            $condition .= db_quote(" AND a.iproduct_id = ?i", $params['iid']);
        }
        if (!empty($params['istatus'])) {
            $condition .= db_quote(" AND a.istatus = ?i", $params['istatus']);
        }
        if (!empty($params['pu_result_id'])) {
            if($params['pu_result_id']==-1) {
                $condition .= db_quote(" AND a.pu_result_id!=0");
            }
            elseif($params['pu_result_id']==-2) {
                $condition .= db_quote(" AND a.pu_result_id=0");
            }
            else {
                $condition .= db_quote(" AND a.pu_result_id = ?i", $params['pu_result_id']);
            }
        }
        if (!empty($params['csrelated'])) {
            if($params['csrelated']=='Y') {
                $condition .= db_quote(" AND a.cproduct_id!=0");
            }
            elseif($params['csrelated']=='N') {
                $condition .= db_quote(" AND a.cproduct_id=0");
            }
        }        if (!empty($params['blocked'])) {            if($params['blocked']=='Y') {                $condition .= db_quote(" AND a.blocked!=0");            }            elseif($params['blocked']=='N') {                $condition .= db_quote(" AND a.blocked=0");            }        }
        if(!empty($params['icid'])) {
            $condition .= db_quote(" AND b.category_id=?i",$params['icid']);
            $joins[] = 'LEFT JOIN slx_icecat_product b ON a.iproduct_id=b.id';
        }
        if (!empty($params['pcode'])) {
            $condition .= db_quote(" AND b.pcode = ?s", $params['pcode']);
            $joins[] = " left join slx_product_supplier_product psp on psp.xproduct_id=a.id ";
            $joins[] = " left join slx_supplier_product b on psp.sproduct_id=b.id";
            
        }

        $group = '';
        $sorting = "ORDER BY a.title, a.id";

        if (!empty($params['items_per_page'])) {
            $params['total_items'] = db_get_field("
                    SELECT COUNT(DISTINCT a.id)
                    FROM slx_product AS a ".
                    implode(' ', $joins) .
                    " $condition");
            $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        }

        $results = db_get_array(
            "SELECT " . implode(', ', $fields) . " FROM slx_product as a " .
            implode(' ', $joins) .
            " $condition $group $sorting $limit"
        );
        return array($results, $params);
    }
}