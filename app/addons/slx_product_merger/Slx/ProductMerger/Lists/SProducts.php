<?php

namespace Slx\ProductMerger\Lists;


class SProducts {

    public function get($params = array(), $items_per_page = 0, $lang_code = CART_LANGUAGE) {
        // Set default values to input params
        $default_params = array(
            'page' => 1,
            'items_per_page' => $items_per_page
        );

        $params = array_merge($default_params, $params);

        // Unset all SQL variables
        $fields = $joins = array();
        $condition = $sorting = $limit = $group = '';

        $fields = array(
            'a.id',
            'a.supplier',
            'a.pcode',
            'a.title',
            'a.availability',
            'a.price',
            'a.mean',
            'a.istatus',
        );

        $condition = 'WHERE 1';

        if (!empty($params['mean'])) {
            $condition .= db_quote(" AND a.mean = ?s", $params['mean']);
        }
        if (!empty($params['supplier'])) {
            //$condition .= db_quote(" AND a.title LIKE ?l", '%' . $params['title'] . '%');
            $condition .= db_quote(" AND a.supplier=?s", $params['supplier']);
        }
        if (!empty($params['title'])) {
            //$condition .= db_quote(" AND a.title LIKE ?l", '%' . $params['title'] . '%');
            $condition .= db_quote(" AND a.title LIKE ?l", str_replace('*', '%', $params['title']));
        }
        if (!empty($params['pcode'])) {
            //$condition .= db_quote(" AND a.title LIKE ?l", '%' . $params['title'] . '%');
            $condition .= db_quote(" AND a.pcode=?s", $params['pcode']);
        }
        if (!empty($params['istatus'])) {
            $condition .= db_quote(" AND a.istatus = ?i", $params['istatus']);
        }

        $group = '';
        $sorting = "ORDER BY a.title, a.id";

        if (!empty($params['items_per_page'])) {
            $params['total_items'] = db_get_field("
                    SELECT COUNT(DISTINCT a.id)
                    FROM slx_supplier_product AS a
                    $condition");
            $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        }

        $results = db_get_array(
            "SELECT " . implode(', ', $fields) . " FROM slx_supplier_product as a " .
            implode(' ', $joins) .
            " $condition $group $sorting $limit"
        );
        return array($results, $params);
    }
}