<?php

namespace Slx\ProductMerger\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class DeleteProductsCommand extends Command {

    private $output;

    protected function configure() {
        $this
            ->setName('merger:delete-products')
            ->setDescription('Deletes products that are not matched to some criteria.')
            ->addOption('force', null,InputOption::VALUE_NONE, 'required to make delete work')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        $this->output->writeln($this->getDescription());
        if(!$input->getOption('force')) {
            $this->output->writeln("<error>Add force switch to make it work</error>");
            return false;
        }
        // ask for confirmation
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action?', false);
        if (!$helper->ask($input, $output, $question)) {
            return;
        }

        //$this->deleteNonX();
        //$this->deleteNonSup();
        //$this->deleteDeadSup();
        $this->output->writeln("\n--END--\n");

    }

    private function deleteNonX() {
        $this->output->writeln("<info>Deleting products that are from suppliers 10,28,11,13 and do not match with x-products.</info>");
        $productIds = db_get_fields(
            "select p.product_id from cscart_products p 
            left join cscart_supplier_links sl on sl.object_id=p.product_id and sl.object_type='P' 
            where sl.supplier_id in (10,28,11,13) and p.xproduct_id=0 and status in ('D','H')"
        );
        $this->delete($productIds);
    }

    private function deleteNonSup() {
        $this->output->writeln("<info>Deleting products with no supplier link</info>");
        $productIds = db_get_fields(
            "
            select product_id from cscart_products p left join cscart_supplier_links sl on sl.object_id=p.product_id and sl.object_type='P' where isnull(sl.supplier_id);
            "
        );
        $this->delete($productIds);
    }

    private function deleteDeadSup() {
        $sIds = [1,2,3,4,5,6,7,8,9,12,14,15,16,18,19,20,21,22,23,24,25,26,27];
        $this->output->writeln("<info>Deleting products that are from suppliers ".implode(',', $sIds)." and do not match with x-products.</info>");
        $productIds = db_get_fields(
            "select p.product_id from cscart_products p 
            left join cscart_supplier_links sl on sl.object_id=p.product_id and sl.object_type='P' 
            where sl.supplier_id in (?a) and status in ('A','D','H')",
            $sIds
        );
        $this->delete($productIds);
    }

    private function delete($productIds) {
        $this->output->writeln(sprintf("Count=%s\n",count($productIds)));

        $cnt = 1;
        foreach($productIds as $productId) {
            printf("%s[%s] ", $cnt++, $productId);
            if($cnt%9==0) printf("\n");
            fn_delete_product($productId);
        }

        $this->output->writeln("<info>end of deletion</info>");
    }
}

//;