<?php

namespace Slx\ProductMerger\Command;


use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exception\SupplierStreamNotSupported;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Slx\ProductMerger\ManMerger\Possible;
use Slx\ProductMerger\SlxProduct;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ProductMatchThreeCommand extends Command {

    private $output;
    private $supplier;

    protected function configure() {
        $this
            ->setName('merger:match-three')
            ->setDescription('Finalize not-matching products with title relevance, the remaining need human clearing.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        printf("%s\n\n", $this->getDescription());
        /*
        $ps = new Possible();
        $all = $ps->getAll();
        db_query("UPDATE slx_supplier_product SET istatus=2 WHERE id in (?a)", $all);
        */
        
        $products = db_get_array("SELECT * FROM slx_supplier_product WHERE istatus=1");
        foreach($products as $product) {
            printf("not matching %s\n", $product['id']);
            $xproduct = new SlxProduct();
            $xproduct->create($product['id'], [], '---',4);
        }
        //db_query("UPDATE slx_supplier_product SET istatus=1 WHERE id in (?a)", $all);
        printf("\nEND\n");
    }

}