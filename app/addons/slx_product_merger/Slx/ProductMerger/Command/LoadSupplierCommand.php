<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exception\SupplierStreamNotSupported;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Slx\Util\SlxProcessIndicator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;

class LoadSupplierCommand extends Command {
    private $output;
    private $supplier;
    private $fltCatType;
    private $fltCatCodes;

    protected function configure() {
        $this->setName('merger:loadsupplier')
             ->setDescription('get data from supplier product file')
             ->addArgument('supplier', InputArgument::REQUIRED, 'Supplier name');
        //$this->config = \Slx\Config\Config::getInstance();
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        printf("%s\n\n", $this->getDescription());
        if (defined("DEBUG_MODE")) {
            $output->writeln("<error>\n\nDEBUG_MODE is enabled. Pls disable debug mode to run this process\n</error>");
            return false;
        }
        $this->supplier = strtolower($input->getArgument('supplier'));
        $supportedSuppliers = ImportProductStreamFactory::getSupportedSuppliers();
        if (!in_array($this->supplier, $supportedSuppliers)) {
            $this->output->writeln(sprintf("Supplier %s not supported. [%s]", $this->supplier, implode(',', $supportedSuppliers)));
            return false;
        }
        try {
            $stream = ImportProductStreamFactory::getStream($this->supplier);
        }
        catch (SupplierStreamNotSupported $ex) {
            $this->output->writeln("No stream class for supplier " . $this->supplier);
        }
        db_query("update slx_supplier_product set price=0, availability=0,upd_timestamp=?i where supplier=?s", time(), $this->supplier);
        if (!$this->isSupplierActive($this->supplier)) {
            $this->output->writeln("The supplier {$this->supplier} is not active, their s-products are set to zero price and stock, nothing more to do.");
            Logger::getInstance()
                  ->log("The supplier {$this->supplier} is not active, their s-products are set to zero price and stock, nothing more to do.");
            return;
        }
        printf("A\n");
        $this->loadSupplierCatoriesFilters($this->supplier);
        printf("B\n");
        if ($stream->open()) {
            $this->output->writeln("Stream opened..");
            $cnt = 0;
            $indicator = new SlxProcessIndicator();
            $indicator->start('Line:');
            try {
                while ($p = $stream->next()) {
                    //$this->output->write('.');
                    $indicator->advance();
                    $cnt++;
                    //$output->write($cnt);
                    $this->processRow($p);
                }
            }
            catch (EndOfStreamException $ex) {
                $indicator->end();
                $this->output->writeln("\nEnd of stream");
                $stream->close();
            }
        }
        else {
            $this->output->writeln(sprintf("Couldnt open product stream for supplier %s", $this->supplier));
        }
    }

    private function processRow($row) {
        Logger::getInstance()
              ->log(sprintf("title=%s\n", $row['title']));
        if (count($row['ean']) == 0) {
            return false;
        }
        if (!$this->isAcceptableCategory($row['category'])) {
            Logger::getInstance()
                  ->log(sprintf("Not acceptable category %s", $row['category']));
            return false;
        }
        if (($spid = $this->supplierProductExists($row['pcode']))) {
            // supplier product already exists, update avail and price
            $this->updateSupplierProduct($row, $spid);
            Logger::getInstance()
                  ->log(sprintf("updated supplier product %s->%s", $row['pcode'], $spid));
        }
        else {  // supplier product does not exist
            $eans = $row['ean'];
            if (($productId = $this->getProductByEans($eans))) { // ean exists
                $this->addSupplierProduct($row, $productId);
                Logger::getInstance()
                      ->log(sprintf("created supplier product %s",
                                    $row['pcode']));
            }
            else {  // add to pending
                $this->addToPending($row);
                Logger::getInstance()
                      ->log(sprintf("added to pending %s", $row['pcode']));
            }
        }
    }

    private function supplierProductExists($pcode) {
        $id = db_get_field("SELECT id FROM slx_supplier_product WHERE pcode=?s and supplier=?s", $pcode, $this->supplier);
        return $id;
    }

    private function getProductByEans($eans) {
        $out = 0;
        $pIds = array();
        foreach ($eans as $ean) {
            $pId = $this->eanExistsInProducts($ean);
            if ($pId) {
                $pIds[] = $pId;
            }
        }
        $pIds =
            array_unique($pIds);
        if (count($pIds) > 1) {
            Logger::getInstance()
                  ->log(sprintf("Eans %s match more than one products %s!!!", implode(',', $eans), implode(',', $pIds)));
        }
        elseif (count($pIds) == 1) {
            $out = $pIds[0];
        }
        return
            $out;
    }

    private function eanExistsInProducts($ean) {
        $pid =
            db_get_field("SELECT product_id FROM slx_icecat_ean WHERE ean=?s", $ean);
        return $pid;
    }

    private function getIceCatProductEans($ipid) {
        $eans
            = db_get_fields("select ean from slx_icecat_ean where product_id=?i", $ipid);
        return $eans;
    }

    private function updateSupplierProduct($row, $spid) {
        $data = array('availability' => $row['availability'],
            'price' => $row['price'], 'upd_timestamp' => time(),);
        db_query("update slx_supplier_product set ?u where id=?i ", $data, $spid);
    }

    private function addSupplierProduct($row, $productId) {
        $data = array(
            'supplier' => $row['supplier'], 'pcode' => $row['pcode'],
            'availability' => $row['availability'], 'category' =>
                $row['category'], 'price' => $row['price'], 'title' =>
                $row['title'], 'product_id' => $productId, 'istatus' =>
                ($productId == 0 ? 2 : 1),);
        $sp_id = db_query("INSERT into slx_supplier_product ?e", $data);
        $eans = array_filter(array_merge($row['ean'], $this->getIceCatProductEans($productId)));
        foreach ($eans as $ean) {
            $data = ['sproduct_id' => $sp_id, 'ean' => trim($ean),];
            db_query("INSERT into slx_supplier_product_ean ?e", $data);
        }
        return $sp_id;
    }

    private function addToPending($row) {
        foreach ($row['ean'] as $ean) {
            $data = array('ean' =>
                $ean,
                //'lastchecked' => 0,
                'pcode' => $row['pcode'],
                'source' => $row['supplier'],
            );
            
            if (!db_get_row("select * from slx_icecat_product_pending where ean=?s", $ean)) {
                db_query("REPLACE INTO slx_icecat_product_pending ?e", $data);
            }
        }
    }

    private function isSupplierActive($supplier) {
        $status =
            db_get_field("select status from ?:suppliers where name=?s", $supplier);
        return $status == 'A';
    }

    public function fitlerCatFilterRow($text) {
        return trim($text);
    }

    private function loadSupplierCatoriesFilters($supplier) {
        $this->fltCatType = 'X';
        $this->fltCatCodes = [];
        $row = db_get_row("select * from ?:suppliers where name=?s", $supplier);
        $this->fltCatType = $row['pm_category_flt_type'];
		$catCodes = preg_split('/$\R?^/m', $row['pm_category_flt_cat_codes']);
        foreach ($catCodes as $text) {
            $this->fltCatCodes[] = $this->fitlerCatFilterRow($text);
        }
        //$this->fltCatCodes = array_filter($this->fltCatCodes, [$this,'fitlerCatFilterRow']);
        printf("%s\n", __METHOD__);
    }

    private function isAcceptableCategory($category) {
        $out = true;
        if ($this->fltCatType == 'X') {
            $out = !in_array($category, $this->fltCatCodes);
        }
        elseif ($this->fltCatType == 'I') {
            $out = in_array($category, $this->fltCatCodes);
        }
        return $out;
    }
}
