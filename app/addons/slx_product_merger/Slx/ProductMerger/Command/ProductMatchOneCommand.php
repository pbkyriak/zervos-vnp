<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exception\SupplierStreamNotSupported;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Slx\ProductMerger\SlxProduct;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ProductMatchOneCommand extends Command {

    private $output;
    private $supplier;

    protected function configure() {
        $this
            ->setName('merger:match-one')
            ->setDescription('match supplier products pass one, Merges using EAN coming from ICECAT.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {

        //db_query("truncate table slx_product");
        //db_query("truncate table slx_product_supplier_product");
        //db_query("update slx_supplier_product set istatus=1 where istatus!=1");
        printf("%s\n\n", $this->getDescription());
        $this->output = $output;
        $xproduct = new SlxProduct();
        $iproductIds = db_get_fields("SELECT product_id, COUNT(*) cnt FROM slx_supplier_product WHERE istatus=1 GROUP BY product_id HAVING cnt>1");
        foreach ($iproductIds as $ipId) {
            $matching = db_get_array("select * from slx_supplier_product where product_id=?i", $ipId);
            if ($matching) {
                //print_r($matching);
                $sproduct = array_pop($matching);
                $eans = db_get_fields("SELECT ean FROM slx_supplier_product_ean WHERE sproduct_id=?i", $sproduct['id']);
                $mean = reset($eans);
                //$this->createSlxProduct($sproduct, $matching, $mean);
                $matchingIds = [];
                foreach($matching as $m) {
                    $matchingIds[] = $m['id'];
                }
                $matchingIds = array_filter($matchingIds);
                $xproduct->create($sproduct['id'], $matchingIds, $mean,9);
                printf("matched %s %s of %s with %s\n", $sproduct['pcode'], $sproduct['title'], $sproduct['supplier'], count($matching));
            }
        }
    }

}