<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exception\SupplierFetcherNotSupported;
use Slx\ProductMerger\ListFetcher\FileFetcherFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;

class FetchSupplierFileCommand extends Command {

    private $output;

    protected function configure() {
        $this
            ->setName('merger:fetch:supplier-file')
            ->setDescription('Fetch remote Supplier data file.')
            ->addArgument('supplier', InputArgument::REQUIRED, 'Supplier name')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        printf("%s\n\n", $this->getDescription());
        Logger::getInstance()->log("Start of : " .$this->getDescription());
        $supplier = strtolower($input->getArgument('supplier'));
        $supportedSuppliers = FileFetcherFactory::getSupportedSuppliers();
        if( !in_array($supplier,$supportedSuppliers)) {
            $this->output->writeln(sprintf("Supplier %s not supported. [%s]", $supplier, implode(',', $supportedSuppliers)));
            return false;
        }
        $output->writeln("Fetching from ".$supplier);
        try {
            $fetcher = FileFetcherFactory::getFetcher($supplier);
            $fetcher->fetch();
        }
        catch(SupplierFetcherNotSupported $ex) {
            $this->output->writeln("No fetcher class for supplier ".$supplier);
        }


        Logger::getInstance()->log("End of process.");
        $this->output->writeln("\n--END--\n");

    }

}

