<?php

namespace Slx\ProductMerger\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;

class FixNoSupplierProductsCommand extends Command {

    private $output;

    protected function configure() {
        $this
            ->setName('merger:fix:nosups')
            ->setDescription('Fix No Supplier linked products');
        ;
    }

    protected function execute_big(InputInterface $input, OutputInterface $output) {/*
        $this->output = $output;
        printf("%s\n\n", $this->getDescription());
        $supplier_id = 10;
        $productIds = db_get_fields("select p.product_id from cscart_products p left join cscart_supplier_links sl on sl.object_id=p.product_id and sl.object_type='P' where isnull(sl.supplier_id)");
        printf("Count=%s\n",count($productIds));
        foreach($productIds as $productId) {
            $row = db_get_row("select * from ?:supplier_links where object_id=?i",$productId);
            if($row) {
                if ($row['object_type'] != 'P') {
                    print_r($row);
                }
                printf("| ");
            }
            else{
                $trow = db_get_row("select product_code, product_codeB, product_codeC,my_supplier_id from cscart_products where product_id=?i", $productId);
                $updProduct = false;
                if($trow['my_supplier_id']==0) {
                    $prefix = substr($trow['product_code'],0,2);
                    if($prefix=='11') {
                        $supplier_id = 10;
                    }
                    elseif($prefix=='WA') {
                        $supplier_id = 13;
                    }
                    elseif($prefix[0]=='V') {
                        $supplier_id = 17;
                    }
                    else {
                        $supplier_id = 0;
                    }
                    $updProduct = true;
                }
                else {
                    $supplier_id = $trow['my_supplier_id'];
                    $updProduct = false;
                }
                if($supplier_id!=0) {
                    if($updProduct) {
                        db_query("UPDATE ?:products SET my_supplier_id=?i WHERE product_id=?i", $supplier_id, $productId);
                    }
                    db_query('INSERT INTO ?:supplier_links (supplier_id, object_id, object_type) VALUES (?i, ?i, ?s)', $supplier_id, $productId, 'P');
                    //printf("update my=%s pid=%s %s\n",$supplier_id, $productId, $updProduct ? ' AND P ' : '');
                }
                else {
                    printf("NO update my=%s pid=%s %s\n",$supplier_id, $productId, $updProduct ? ' AND P ' : '');
                }
            }
        }
        printf("\n\n");
        $this->output->writeln("\n--END--\n");
*/
    }

    protected function execute(InputInterface $input, OutputInterface $output) {        
	$this->output = $output;        
	
	printf("%s\n\n", $this->getDescription());        
	
	$supplier_id = 11;        
	
	$productIds = db_get_fields("select p.product_id from cscart_products p left join cscart_supplier_links sl on sl.object_id=p.product_id and sl.object_type='P' where p.my_supplier_id=11 and isnull(sl.supplier_id)");

	printf("Count=%s\n",count($productIds));

	foreach($productIds as $productId) {

	$row = db_get_row("select * from ?:supplier_links where object_id=?i",$productId);

	if($row) {

	if ($row['object_type'] != 'P') {

	print_r($row);

	}               

	printf("| ");

	}

	else{

	//db_query('INSERT INTO ?:supplier_links (supplier_id, object_id, object_type) VALUES (?i, ?i, ?s)', $supplier_id, $productId, 'P');

	printf("update my=%s pid=%s %s\n",$supplier_id, $productId, $updProduct ? ' AND P ' : '');

	}        
	
	}

	printf("\n\n");

	$this->output->writeln("\n--END--\n");

    }

	
}