<?php

namespace Slx\ProductMerger\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;

class InitBCommand extends Command {

    private $output;

    protected function configure() {
        $this
            ->setName('merger:initB')
            ->setDescription('Product initialization phase B.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        printf("%s\n\n", $this->getDescription());
        Logger::getInstance()->log("Start of process.");
        /*
        */
		$this->fix01();
        Logger::getInstance()->log("End of process.");
        $this->output->writeln("\n--END--\n");

    }

	private function dumpTableStruct() {
        $tables = db_get_fields("show tables like 'slx%'");
        print_r($tables);
        foreach($tables as $table) {
            $struct = db_get_row("show create table $table");
            printf("%s\n\n",$struct['Create Table']);
        }
	}
	
	private function fix01() {
        $rows = db_get_array("SELECT p.product_id, p.my_supplier_id, p.product_code 
                                FROM cscart_products p LEFT JOIN cscart_supplier_links sl ON sl.object_id=p.product_id AND sl.object_type='P' 
                                WHERE ISNULL(sl.supplier_id) and p.my_supplier_id!=0
                                ORDER BY p.product_id;
                                ");
        foreach($rows as $row) {
            if($row['my_supplier_id']!=0) {
                $sid = $row['my_supplier_id'];
				db_query('INSERT INTO ?:supplier_links (supplier_id, object_id, object_type) VALUES (?i, ?i, ?s)', $sid, $row['product_id'], 'P');
            }
        }
	}
	
	private function fix02() {
		$rows = db_get_array("SELECT * FROM `cscart_seo_names` WHERE path='' and type='p'");
		foreach($rows as $row) {
			$object_id = $row['object_id'];
			$object_type = $row['type'];
			$company_id=$row['company_id'];
			$path = fn_get_seo_parent_path($object_id, $object_type, $company_id);
			if(!empty($path)) {
				printf("id=%s path=%s\n", $object_id, $path);
				db_query("update cscart_seo_names set path=?s where object_id=?i and type=?s", $path, $object_id, $object_type);
			}
		}
	}

	private function fix03() {
		$rows = db_get_array("SELECT * FROM `cscart_seo_names` WHERE name like '%-eu'");
		foreach($rows as $row) {
			$object_id = $row['object_id'];
			$object_type = $row['type'];
			$name = $row['name'];
			$name2 = preg_replace(array('/-EU$/i', '/-DE$/i'), '', $name);
			if($name!=$name2) {
				printf("id=%s name=%s\n", $object_id, $name2);
				db_query("update cscart_seo_names set name=?s where object_id=?i and type=?s", $name2, $object_id, $object_type);
			}
		}
	}
	
	private function test() {
		file_put_contents(DIR_ROOT.'/var/updater_files/test.txt', 'psiftis');
	}

	private function fixMissmatched() {
		$fn = DIR_ROOT.'/var/updater_files/mismatched-results-1.csv';
		$handle = fopen($fn, "r");
		if($handle) {
			while($data=fgetcsv($handle, 100, ';')){ 
				
				$productId = $data[1];
				$xproductId = db_get_field("select xproduct_id from cscart_products where product_id=?i",$productId);
				if(!empty($xproductId)) {
					if($data[0]==1) {
						printf("%s %s %s\n", $data[0], $data[1], $xproductId);
						slx_product_merger_set_mismatch_checked_xproduct($xproductId, 1);
					}
					else {
					//	slx_product_merger_unlink_and_block_xproduct($productId, $xproductId);
					}
					
				}
			}
			fclose($handle);
		}
		else {
			printf("not opened\n");
		}
	}
	
	private function findMismatched() {
		/*
		$sql = "
			SELECT p.product_id, pd.product, xp.title xtitle, GROUP_CONCAT(sp.title SEPARATOR '$$') stitles
			FROM cscart_products p
			LEFT JOIN cscart_product_descriptions pd ON p.product_id=pd.product_id AND pd.lang_code='el'
			LEFT JOIN slx_product xp ON p.xproduct_id=xp.id
			LEFT JOIN slx_product_supplier_product psp ON xp.id=psp.xproduct_id
			LEFT JOIN slx_supplier_product sp ON sp.id=psp.sproduct_id
			WHERE p.xproduct_id!=0 AND pd.product!=xp.title and xp.mismatch_checked=0 and xp.blocked=0
			GROUP BY p.product_id, pd.product, xp.title 
		";
		$rows = db_get_array($sql);
		
		foreach($rows as $row) {
			$titles = explode('$$', $row['stitles']);
			$matched = false;
			$testTitle = str_replace(' ','',strtolower($row['product']));
			foreach($titles as $title) {
				$check = str_replace(' ','',strtolower($title));
				if($check==$testTitle) {
					$matched=true;
				}
			}
			if(!$matched) {
				printf("%s\t%s\t%s\n", $row['product_id'], $row['product'], implode("\t", $titles));
			}
		}
		*/
	}
	

	private function fix04() {
        $rows = db_get_array("
        SELECT product_id FROM cscart_products WHERE (product_code LIKE 'V%' OR product_code LIKE 'EG%' OR product_code LIKE 'D%'  OR product_code LIKE 'HE%'  OR product_code LIKE 'MO%') 
                                ");
        foreach($rows as $row) {
            $sid = 17;
            db_query("update cscart_products set my_supplier_id=17 where product_id=?i",$row['product_id']);
            db_query('INSERT INTO ?:supplier_links (supplier_id, object_id, object_type) VALUES (?i, ?i, ?s)', $sid, $row['product_id'], 'P');
        }
	}
}
