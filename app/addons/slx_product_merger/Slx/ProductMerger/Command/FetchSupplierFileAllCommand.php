<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exception\SupplierFetcherNotSupported;
use Slx\ProductMerger\ListFetcher\FileFetcherFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;

class FetchSupplierFileAllCommand  extends Command {

    private $output;

    protected function configure() {
        $this
            ->setName('merger:fetch:supplier-file-all')
            ->setDescription('This command runs fetch:supplier-file foreach active (and supported) supplier.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        printf("%s\n\n", $this->getDescription());
        Logger::getInstance()->log("Start of : " .$this->getDescription());

        $activeSuppliers = $this->getActiveSuppliers();
        $fetchers = FileFetcherFactory::getSupplierFetchers();
        $fetchers = array_keys($fetchers);        		$fetchers = array_intersect($activeSuppliers,$fetchers);		
        $command = $this->getApplication()->find('merger:fetch:supplier-file');

        foreach($fetchers as $supplier) {
            $arguments = array(
                'command' => 'merger:fetch:supplier-file',
                'supplier' => $supplier,
            );

            $cmdInput = new ArrayInput($arguments);
            $returnCode = $command->run($cmdInput, $output);
        }
        Logger::getInstance()->log("End of process.");
        $this->output->writeln("\n--END--\n");

    }

    private function getActiveSuppliers() {
        $names = db_get_fields("select name from ?:suppliers where status=?s", 'A');
        $names= array_map('strtolower',$names );
        return $names;
    }
}