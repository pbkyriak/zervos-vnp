<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exception\SupplierFetcherNotSupported;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Slx\ProductMerger\ListFetcher\FileFetcherFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;

class LoadSupplierAllCommand extends Command {

    private $output;

    protected function configure() {
        $this
            ->setName('merger:loadsupplier-all')
            ->setDescription('This command runs merger:loadsupplier foreach active (and supported) supplier.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        printf("%s\n\n", $this->getDescription());
        Logger::getInstance()->log("Start of : " .$this->getDescription());

        if(defined("DEBUG_MODE")) {
            $output->writeln("<error>\n\nDEBUG_MODE is enabled. Pls disable debug mode to run this process\n</error>");
            return false;
        }
        $activeSuppliers = $this->getActiveSuppliers();
        $mergerSuppliers = ImportProductStreamFactory::getSupportedSuppliers();
        //$mergerSuppliers = array_intersect($activeSuppliers,$mergerSuppliers);

        $command = $this->getApplication()->find('merger:loadsupplier');

        foreach($mergerSuppliers as $supplier) {
            $arguments = array(
                'command' => 'merger:loadsupplier',
                'supplier' => $supplier,
            );

            $cmdInput = new ArrayInput($arguments);
            $returnCode = $command->run($cmdInput, $output);
        }
        Logger::getInstance()->log("End of process.");
        $this->output->writeln("\n--END--\n");

    }

    private function getActiveSuppliers() {
        $names = db_get_fields("select name from ?:suppliers where status=?s", 'A');
        $names= array_map('strtolower',$names );
        return $names;
    }

}