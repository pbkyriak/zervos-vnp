<?php

namespace Slx\ProductMerger\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;

class ProductRankCommand  extends Command {

    private $output;
    private $supplier;

    protected function configure() {
        $this
            ->setName('merger:rank')
            ->setDescription('rank sproduct for each xproduct to decide best supplier.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        printf("%s\n\n", $this->getDescription());
        Logger::getInstance()->log('Start Of :'.$this->getName());
        $t1 = time();
        db_query("update slx_product set eproduct_id=0");

        $xproductIds = db_get_fields("SELECT id FROM slx_product WHERE istatus>=4");
        foreach($xproductIds as $xproductId) {
            $this->processXproduct($xproductId);
        }
        printf("Duration: %s\n", time()-$t1);
        Logger::getInstance()->log('End Of :'.$this->getName());
    }

    private function processXproduct($xproductId) {
        $sproducts = db_get_array(
            "SELECT sp.* FROM slx_product_supplier_product psp
              LEFT JOIN slx_supplier_product sp ON sp.id=psp.sproduct_id AND sp.istatus>=4
              WHERE psp.xproduct_id=?i AND NOT ISNULL(sp.id) AND sp.availability>0
              ORDER BY sp.price ",
            $xproductId
        );
        if(! $sproducts ) { // no supplier products
            $this->setEProduct($xproductId, 0);
            //Logger::getInstance()->log("NO dominant product for ".$xproductId);
        }
        elseif(count($sproducts)==1) {  // just one result
            $this->setEProduct($xproductId, $sproducts[0]['id']);
            Logger::getInstance()->log(sprintf("dominant for %s is the only one %s",$xproductId, $sproducts[0]['id']));
        }
        else {  // more than one
            $eproductId = $this->decideDominantSupplier($xproductId,$sproducts);
            $this->setEProduct($xproductId, $eproductId);
        }
    }

    private function setEProduct($xproductId, $eproductId) {
        db_query("update slx_product set eproduct_id=?i where id=?i", $eproductId, $xproductId);
    }

    private function decideDominantSupplier($xproductId,$sproducts) {
        // for now get cheaper
        $tProduct = reset($sproducts);
        $lowerPrice = $tProduct['price'];
        $eproductId =$tProduct['id'];
        foreach($sproducts as $sproduct) {
            if($sproduct['price']<$lowerPrice) {
                $lowerPrice = $sproduct['price'];
                $eproductId = $sproduct['id'];
            }
        }
        Logger::getInstance()->log(sprintf("dominant for %s is %s with price=%s",$xproductId, $eproductId, $lowerPrice));
        return $eproductId;
    }
}