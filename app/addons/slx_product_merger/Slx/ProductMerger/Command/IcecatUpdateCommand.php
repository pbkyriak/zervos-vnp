<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Icecat\IcecatComm;
use Slx\ProductMerger\Icecat\IcecatParser;
use Slx\ProductMerger\Icecat\IcecatProductPersist;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class IcecatUpdateCommand extends Command {

    private $output;
    private $supplier;
    private $lang_codes = ['el', 'en'];

    protected function configure() {
        $this
            ->setName('merger:icecat-update')
            ->setDescription('get from icecate pendding products')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        $this->output->writeln('Updating from ICECAT pending products.');
        $this->parser = new IcecatParser();
        $this->persister = new IcecatProductPersist();
        //$this->difoxSrv = new DifoxComm();
        $this->icecatSrv = new IcecatComm();
        $offset = 24 * 3600;
        $lnCnt = 0;
        $rows = db_get_array("SELECT * FROM slx_icecat_product_pending WHERE lastchecked<?i",1); // time() - $offset);	// and source='difox'
        foreach($rows as $row) {
            $lnCnt++;
            $output->write(sprintf("Processing row: %s ean=%s source=%s pcode=%s", $row['id'], $row['ean'], $row['source'], $row['pcode']));
            if ($this->processRow($row)) {
                $output->writeln(' UPDATED');
            } else {
                $output->writeln(' X');
            }
        }
        $output->writeln(sprintf("processes %s lines", $lnCnt));

    }

    private function processRow($row) {
        $out = false;
        if (strlen($row['ean']) < 12) {
            $this->output->write(' F:E '.strlen($row['ean']).' ');
            //db_query('delete from slx_icecat_product_pending where id=?i', $row['id']);  // diegrapse to apo ta pending
            return false;
        }
        if ($this->eanExistsInProducts($row['ean'])) {  // to ean yparxei idi se proion
            $this->output->write(' F:A ');
            db_query('delete from slx_icecat_product_pending where id=?i', $row['id']);  // diegrapse to apo ta pending
            $this->output->write(sprintf("ean(s) %s exists in another product", $row['ean']));
        } else {
            if ($this->getInfo($row['pcode'], $row['source'], $row['ean'])) {    // ean bre8oun ta dedomena ston icecat
                $this->output->write(' U ');
                db_query('delete from slx_icecat_product_pending where id=?i', $row['id']);  // diegrapse to apo ta pending
                $out = true;
            } else {  // diaforetika enimerose to lastchecked
                $this->output->write(' F:C ');
                db_query('update slx_icecat_product_pending set lastchecked=?i where id=?i', time(), $row['id']);
            }
        }
        return $out;
    }

    private function getInfo($pCode, $source, $ean) {
        $found = false;

        $products = array_fill_keys($this->lang_codes, null);
        $expected = count($this->lang_codes);
        $foundCnt = 0;
        $iceId = 0;
        foreach($this->lang_codes as $lang_code) {
            if (($xmlstring = $this->icecatSrv->get($ean, $lang_code))) {
                $prod = $this->parser->parse($ean, $xmlstring, $lang_code);
                if ($prod->getStatus() != -1) {
                    $products[$lang_code] = $prod;
                    $iceId = $prod->getIceId();
                    $foundCnt++;
                }
            }
        }
        if($foundCnt==$expected) {
            if ($this->getProductByIceId($iceId) == 0) {
                $productId = $this->persister->persist($products);
                $found = true;
            }
        }
        return $found;
    }

    private function eanExistsInProducts($ean) {
        $id = db_get_field("SELECT product_id FROM slx_icecat_ean WHERE ean=?s", $ean);
        return $id;
    }

    private function getProductByIceId($iceId) {
        $out = db_get_field("SELECT id FROM slx_icecat_product WHERE ice_id=?i", $iceId);
        return $out;
    }
}