<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exception\SupplierStreamNotSupported;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateProductIndexCommand   extends Command {

    private $output;
    private $supplier;

    protected function configure() {
        $this
            ->setName('merger:create-product-index')
            ->setDescription('create product matching title index')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        db_query("truncate table slx_supplier_product_words");

        $products = db_get_array("SELECT id, title, price FROM slx_supplier_product");
        foreach($products as $product) {
            $words = explode(' ',$product['title']);
            array_walk($words, 'trim');
            $words = array_filter($words);
            foreach($words as $k => $v) {
                if(substr($v,-1,1)==',') {
                    $v = substr($v,0, strlen($v)-1);
                }
                $words[$k] = $v;
            }
            if(count($words)==0) {
                continue;
            }
            $final_words = [];
            $skipNext = false;
            for($i=0; $i<count($words); $i++) {
                if($skipNext) {
                    $skipNext = false;
                    continue;
                }
                if(!isset($words[$i])) {
                    continue;
                }
                $cword = strtolower($words[$i]);
                $nword = isset($words[$i+1]) ? strtolower($words[$i+1]) : '';
                				$cword = str_replace(array('+','-','~','*','>','<','"','@',')','(',"'"),'_',$cword);				if($product['id']==27802) {					printf("%s - %s\n", $cword, $nword);				}
                if(is_numeric($cword) && in_array($nword,['gb', 'tb'])) {
                    $cword = $cword.$nword;
                    $skipNext = true;
                }
                elseif($cword=='usb' && in_array(substr($nword,0,1), ['1','2','3'])) {
                    $cword = $cword.$nword;
                    $skipNext = true;
                }
                elseif(substr($cword,-1,1)=='"') {
                    $cword = substr($cword,0, strlen($cword)-1);
                }
                if(strlen($cword)<4) {
                    $cword=str_pad($cword, 4,'_',STR_PAD_LEFT);
                }
                $final_words[] = $cword;
                //printf("%s", $cword);
            }
            //printf("\n");
            if(count($final_words)) {
                $data = [
                    'sproduct_id' => $product['id'],
                    'title' => implode(" ", $final_words),
                    'price' => $product['price'],
                ];
                db_query("INSERT INTO slx_supplier_product_words ?e", $data);
            }
        }
    }

}