<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exporter\Traits\SupplierCodePrefixesTrait;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;
use Slx\ProductMerger\Icecat\IcecatCategoryIdHelper;

class InitACommand extends Command {

    use SupplierCodePrefixesTrait;

    private $output;

    private $categoriesNewRel;

    protected function configure() {
        $this
            ->setName('merger:initA')
            ->setDescription('Product initialization phase A.');
        ;
    }

    /**
     * Foreach X-product get related supplier product codes and icecat category-id,
     * search in cscart_products for product that match to any of those product codes
     * on first matched:
     *      - store cscart_product_id to slx_product
     *      - get cscart_product main category and update supplier_category_matching with icecat category-id
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        printf("%s\n\n", $this->getDescription());
		die('katrougalos');
        Logger::getInstance()->log("Start of process.");
        $this->categoriesNewRel = $this->loadCategoriesNewRel();
        $xproductIds = db_get_fields("select id from slx_product where istatus in (4,9) and cproduct_id=0");
        $output->writeln(sprintf("XProducts to match: %s\n ", count($xproductIds)));
        foreach ($xproductIds as $xproductId) {
            $xproduct = $this->getXProductInfo($xproductId);
            $cproduct = $this->matchCSProdut($xproduct['sup_codes']);
            //print_r($cproduct);
            if($cproduct) {
                $this->updateXProduct($xproductId, $cproduct);
                //$this->updateCategory($xproduct, $cproduct);
                $output->write(". ");
            }
            else {
                $output->write("X ");
            }
        }
        Logger::getInstance()->log("End of process.");
        $this->output->writeln("\n--END--\n");

    }

    private function getXProductInfo($xproductId) {
        $supCodes = db_get_array(
            "SELECT supplier, pcode FROM slx_supplier_product sp LEFT JOIN slx_product_supplier_product psp ON sp.id=psp.sproduct_id WHERE psp.xproduct_id=?i",
            $xproductId
        );
        if($supCodes) {
            foreach($supCodes as $idx => $row) {
                //printf("sup=%s pc=%s\n",$row['supplier'], $this->supCodePrefixes[$row['supplier']]);
                $prefix = (isset($this->supCodePrefixes[$row['supplier']]) ? $this->supCodePrefixes[$row['supplier']] : '');
                $supCodes[$idx]['spcode'] = $prefix . $row['pcode'];
            }
        }
        $iceCategory = db_get_field(
            "SELECT ip.category_id  FROM slx_product p LEFT JOIN slx_icecat_product ip ON p.iproduct_id=ip.id WHERE p.id=?i",
            $xproductId
        );
        return [
            'xproduct_id' => $xproductId,
            'sup_codes' => $supCodes,
            'ice_category_id' => $iceCategory,
            'ice_category' => IcecatCategoryIdHelper::idToName($iceCategory),
        ];
    }

    private function matchCSProdut($supCodes) {
        $out = false;
        foreach($supCodes as $supCode) {
            //printf("Testing supplier = %s code=%s \n", $supCode['supplier'],$supCode['spcode']);
            $rows = db_get_array(
                "SELECT p.product_id, p.product_code, LOWER(s.name) AS supplier, pc.category_id
              FROM cscart_products p 
              LEFT JOIN cscart_supplier_links sl ON p.product_id=sl.object_id AND sl.object_type='P' 
              LEFT JOIN cscart_suppliers s ON sl.supplier_id=s.supplier_id
              LEFT JOIN cscart_products_categories pc ON pc.product_id=p.product_id AND pc.link_type='M'
              WHERE s.name=?s AND p.product_code=?s",			  
              $supCode['supplier'],
                $supCode['spcode']
            );
            //$rows = db_get_array("select p.product_id, sl.supplier_id from cscart_products p LEFT JOIN cscart_supplier_links sl ON p.product_id=sl.object_id AND sl.object_type='P' where p.product_code=?s", $supCode['spcode']);
            if ($rows) {
                $out = reset($rows);
                break;
                //printf("supid=%s\n", $out['supplier_id']);
                //$out = false;
            }
            else {
                //printf("supplier = %s code=%s F\n", $supCode['supplier'],$supCode['spcode']);
            }
        }
        return $out;
    }

    private function loadCategoriesNewRel() {
        $out = db_get_hash_single_array("select category_id, supplier_category_name from ?:categories order by category_id", ['category_id', 'supplier_category_name']);
        foreach($out as $c => $v) {
            $out[$c] = array_filter(explode(',', $v));
        }
        return $out;
    }

    private function updateXProduct($xproductId, $cproduct) {
        db_query(
            "update slx_product set cproduct_id=?i where id=?i",
            $cproduct['product_id'],
            $xproductId
        );
        db_query("update cscart_products set xproduct_id=?i where product_id=?i",$xproductId, $cproduct['product_id']);
    }

    private function updateCategory($xproduct, $cproduct) {
        $categoryId = $cproduct['category_id'];
        if(isset($this->categoriesNewRel[$categoryId])) {
            if(!in_array($xproduct['ice_category'], $this->categoriesNewRel[$cproduct['category_id']])) {
                $this->categoriesNewRel[$categoryId][] = $xproduct['ice_category'];
                db_query(
                    "update ?:categories set supplier_category_name=?s where category_id=?i",
                    implode(',',$this->categoriesNewRel[$categoryId]),
                    $categoryId
                );
            }
        }
    }


}

/*
ALTER TABLE cscart_products ADD COLUMN `product_code_new` VARCHAR(32) DEFAULT ''  NULL;
ALTER TABLE cscart_categories ADD COLUMN `supplier_category_name_new` TEXT NULL;
 */