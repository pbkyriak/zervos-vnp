<?php

namespace Slx\ProductMerger\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class DisableSupplierProductsCommand extends Command {

    private $output;

    protected function configure() {
        $this
            ->setName('merger:disable-supplier-products')
            ->setDescription('Disable supplier products.')
            ->addArgument('supplier_id', InputArgument::REQUIRED, 'supplier id')
            ->addArgument('action', InputArgument::REQUIRED, 'action to take [dis|delete]')
            ->addOption('force', null,InputOption::VALUE_NONE, 'required to make delete work')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        $this->output->writeln($this->getDescription());
        $supplierId = $input->getArgument('supplier_id');
        $supplier = db_get_row("select * from cscart_suppliers where supplier_id=?i", $supplierId);
        if(!$supplier) {
            $this->output->writeln(sprintf("<error>No supplier with id=%s</error>", $supplierId));
            return false;
        }
        $this->output->writeln(sprintf("<info>Supplier: %s</info>\n",$supplier['name']));
        $action = $input->getArgument('action');
        if(!in_array($action,['dis', 'delete'])) {
            $this->output->writeln("<error>not supported action</error>");
            return false;
        }
        if($action=='delete' && !$input->getOption('force')) {
            $this->output->writeln("<error>To run delete add --force switch</error>");
            return false;
        }
        // ask for confirmation
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('Continue with this action?', false);
        if (!$helper->ask($input, $output, $question)) {
            return;
        }
        if($action=='dis') {
            $this->disable($supplierId);
        }
        elseif($action=='delete') {
            $this->delete($supplierId);
        }
    }

    private function disable($supplierId) {
        $this->output->writeln("<error>not implemented yet :)))</error>");
    }

    private function delete($supplierId) {
        $this->output->writeln("<error>not implemented yet :)))</error>");
    }
}