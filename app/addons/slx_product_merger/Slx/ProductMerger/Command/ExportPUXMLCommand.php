<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exception\SupplierStreamNotSupported;
use Slx\ProductMerger\Exporter\ProductFixer;
use Slx\ProductMerger\Exporter\ProductStream\SupplierProductStreamFactory;
use Slx\ProductMerger\Exporter\ProductStream\SupplierProductStreamInterface;
use Slx\ProductMerger\Exporter\Traits\OutputXmlTrait;
use Slx\ProductMerger\Exporter\Traits\TextSanitizationTrait;
use Slx\ProductMerger\Exporter\ProductValidator;
use Slx\ProductMerger\Icecat\IcecatProduct;
use Slx\ProductMerger\Icecat\IcecatProductLoader;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\Logger\Logger;

class ExportPUXMLCommand extends Command {

    private $output;
    private $supplier;
    private $loader;
    private $validator;
    private $fixer;

    use OutputXmlTrait;
    use TextSanitizationTrait;

    protected function configure() {
        $this
            ->setName('merger:export')
            ->setDescription('Export products in xml file to be used for PU import.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        printf("%s\n\n", $this->getDescription());
        Logger::getInstance()->log("Start of export.");
        if(!$this->openFile()) {
            return false;
        }
        $this->loader = new IcecatProductLoader();
        $this->validator = new ProductValidator();
        $this->fixer = new ProductFixer();

        $suppliers = $this->getActiveSuppliers();
//		$suppliers = ['eglobal'];
        foreach($suppliers as $supplier) {
//			if($supplier!='wave') continue;
            try { 
                $stream = SupplierProductStreamFactory::getStream($supplier);
            }
            catch(SupplierStreamNotSupported $ex) {
                printf("No stream for supplier %s \n", $supplier);
                continue;
            }
            if($stream) {
                printf("\nSupplier: %s\n", $supplier);
                $this->writelnFile(sprintf("<SupplierProducts name=\"%s\">", $supplier), 1);
                if($stream->open()) {
                    try {
                        while ($xmldata = $stream->next()) {
                            if ($this->validator->isProductValid($xmldata)) {
                                $this->fixer->fixProduct($xmldata);
                                $this->writeInfo($xmldata);
                                $this->output->write('| ');
                            }
                            else {
                                $this->output->write('X ');
                            }
                        }
                    }
                    catch (EndOfStreamException $ex) {
                        $stream->close();
                    }
                }
                else {
                    Logger::getInstance()->log(sprintf("%s stream not opened\n",$supplier));
                }
                $this->writelnFile(sprintf("</SupplierProducts>"), 1);
            }
            else {
                Logger::getInstance()->log(sprintf("no stream for %s\n", $supplier));
            }
        }
        $disSuppliers = $this->getDisabledSuppliers();
        if($disSuppliers) {
            foreach($disSuppliers as $supplier) {
                $this->writelnFile(sprintf("<SupplierProducts name=\"%s\">", $supplier), 1);
                $this->writelnFile(sprintf("</SupplierProducts>"), 1);
            }
        }
        $this->closeFile();

        Logger::getInstance()->log("End of export.");
        $this->output->writeln("\n--END--\n");
    }
/*
    private function getSuppliersFromData() {
        $rows = db_get_fields("
            SELECT DISTINCT sp.supplier FROM slx_product p
            LEFT JOIN slx_supplier_product sp ON sp.id=p.eproduct_id
            WHERE p.eproduct_id!=0;"
        );
        return $rows;
    }
*/
    private function getActiveSuppliers() {
        $out = db_get_fields("select lower(name) from ?:suppliers where status=?s", 'A');
        //$rows2 = $this->getSuppliersFromData();
        //$out = array_intersect($rows2, $rows1);
        return $out;
    }

    private function getDisabledSuppliers() {
        $suppliers = db_get_fields("select lower(name) from ?:suppliers where status!=?s", 'A');
        $supportedSuppliers = $this->getSupportedSuppliers();
        return array_intersect($suppliers,$supportedSuppliers);
    }

    private function getSupportedSuppliers() {
        $supportedSuppliers = array_merge(
            ImportProductStreamFactory::getSupportedSuppliers(),
            SupplierProductStreamFactory::getSupplierStreams()
        );
        return $supportedSuppliers;
    }
}

