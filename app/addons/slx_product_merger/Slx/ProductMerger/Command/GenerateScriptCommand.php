<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exporter\ProductStream\SupplierProductStreamFactory;
use Slx\ProductMerger\Icecat\IcecatComm;
use Slx\ProductMerger\Icecat\IcecatParser;
use Slx\ProductMerger\Icecat\IcecatProductPersist;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Slx\ProductMerger\ListFetcher\FileFetcherFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateScriptCommand extends Command {

    private $output;

    protected function configure() {
        $this
            ->setName('merger:generate:script')
            ->setDescription('Generate update script')
            ->addArgument('type', InputArgument::REQUIRED, 'Script type to generate [day,mid]')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->output = $output;
        $this->output->writeln($this->getDescription());
        $scriptType = $input->getArgument('type');
        if(!in_array($scriptType,['day','mid'])) {
            $output->writeln(sprintf("Type '%s' not in [day, mid]", $scriptType));
            return false;
        }

        $activeSuppliers = $this->getActiveSuppliers();
        $mergerSuppliers = ImportProductStreamFactory::getSupportedSuppliers();
        $fetchers = FileFetcherFactory::getSupplierFetchers();
        $fetchers = array_keys($fetchers);

        //$mergerSuppliers = array_intersect($activeSuppliers,$mergerSuppliers);
        $fetchers = array_intersect($activeSuppliers,$fetchers);

        if($scriptType=='day') {
            $this->generateDay($mergerSuppliers,$fetchers);
        }
        elseif($scriptType=='mid') {
            $this->generateMid($mergerSuppliers,$fetchers);
        }
        $this->output->writeln('# END #');
    }

    private function generateDay($mergerSuppliers,$fetchers) {
        $this->writeScriptTitle("Day script");
        $this->fetchSuppliersLines($fetchers);
        $this->loadSuppliersLines($mergerSuppliers);
        $this->rankAndExportLines();
        $this->puImportLines();
        $this->output->writeln("");
    }

    private function generateMid($mergerSuppliers,$fetchers) {
        $this->writeScriptTitle("After midnight script");
        $this->updateIcecatLines();
        $this->fetchSuppliersLines($fetchers);
        $this->loadSuppliersLines($mergerSuppliers);
        $this->mergeProductsLines();
        $this->rankAndExportLines();
        $this->puImportLines();
    }

    private function writeScriptTitle($title) {
        $this->output->writeln("#");
        $this->output->writeln('# '.$title);
        $this->output->writeln("#");
    }
    private function fetchSuppliersLines($fetchers) {
        $this->output->writeln("");
        foreach($fetchers as $supplier) {
            $this->output->writeln(sprintf("php admin.php --dispatch=prodmergercmd.console  merger:fetch:supplier-file %s", $supplier));
        }
    }
    private function loadSuppliersLines($mergerSuppliers) {
        $this->output->writeln("");
        foreach($mergerSuppliers as $supplier) {
            $this->output->writeln(sprintf("php admin.php --dispatch=prodmergercmd.console merger:loadsupplier %s", $supplier));
        }
    }

    private function rankAndExportLines() {
        $this->output->writeln("");
        $this->output->writeln("php admin.php --dispatch=prodmergercmd.console merger:rank");
        $this->output->writeln("php admin.php --dispatch=prodmergercmd.console merger:export");

    }
    private function updateIcecatLines() {
        $this->output->writeln("");
        $this->output->writeln("php admin.php --dispatch=prodmergercmd.console merger:icecat-update");
    }

    private function mergeProductsLines() {
        $this->output->writeln("");
        $this->output->writeln("php admin.php --dispatch=prodmergercmd.console merger:match-one");
        $this->output->writeln("php admin.php --dispatch=prodmergercmd.console merger:match-two");
        $this->output->writeln("php admin.php --dispatch=prodmergercmd.console merger:create-product-index");
        $this->output->writeln("php admin.php --dispatch=prodmergercmd.console merger:match-three");
    }

    private function puImportLines() {
        $this->output->writeln("");
        $this->output->writeln("php admin.php --dispatch=pucmd.console pu:import");
    }

    private function getActiveSuppliers() {
        $names = db_get_fields("select name from ?:suppliers where status=?s", 'A');
        $names= array_map('strtolower',$names );
        return $names;
    }
}