<?php


namespace Slx\ProductMerger\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exception\SupplierStreamNotSupported;
use Slx\ProductMerger\ImportProductStream\Supplier\EnoProductStream;

class TestCommand extends Command {

    private $output;

    protected function configure() {
        $this
            ->setName('merger:test')
            ->setDescription('Test nothing.')
            ->addArgument('supplier', InputArgument::REQUIRED, 'Supplier name')
        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $supplier=strtolower($input->getArgument('supplier'));
        $this->output = $output;
		$output->writeln($supplier);
        try {
            $stream = new EnoProductStream();
        }
        catch (SupplierStreamNotSupported $ex) {
            $this->output->writeln("No stream class for supplier " . $this->supplier);
        }

        if($stream->open()) {
            $cnt=0;
            try {
                while($data = $stream->next()) {
                    print_r($data->toArray());
                    //$output->writeln(json_encode($data->toArray()));
                    $cnt++;
                }
            }
            catch(EndOfStreamException $ex) {
                $this->output->writeln("End of stream");
            }
            $this->output->writeln("Products ".$cnt);
            $stream->close();
        }
        else {
            $this->output->writeln("Could not open file");
        }
	}
}