<?php

namespace Slx\ProductMerger\Command;

use Slx\ProductMerger\Exception\EndOfStreamException;
use Slx\ProductMerger\Exception\SupplierStreamNotSupported;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Slx\ProductMerger\SlxProduct;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ProductMatchTwoCommand extends Command {

    private $output;
    private $supplier;

    protected function configure() {
        $this
            ->setName('merger:match-two')
            ->setDescription('match supplier products pass two, Merges using EAN coming from suppliers only.');
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        printf("%s\n\n", $this->getDescription());
        $xproduct = new SlxProduct();
        $this->output = $output;
        $suppliers = db_get_fields("select distinct supplier from slx_supplier_product where istatus=1");
        foreach($suppliers as $supplier) {
            $sproducts = db_get_array("SELECT * FROM slx_supplier_product WHERE istatus=1 and supplier=?s", $supplier);
            $this->output->writeln(sprintf("%s products to process: %s\n", $supplier, count($sproducts)));

            foreach ($sproducts as $sproduct) {

                $eans = db_get_fields("SELECT ean FROM slx_supplier_product_ean WHERE sproduct_id=?i", $sproduct['id']);
                $matching = [];
                $matchingIds = [];
                $mean = reset($eans);
                foreach ($eans as $ean) {
                    $sql = db_quote(
                        "SELECT distinct p.id, p.title, p.price, pe.ean, p.istatus FROM slx_supplier_product AS p
                     LEFT JOIN slx_supplier_product_ean AS pe ON pe.sproduct_id=p.id AND p.supplier!=?s
                     WHERE pe.ean=?s",
                        $sproduct['supplier'],
                        $ean
                    );
                    $tmp = db_get_array($sql);
                    if ($tmp) {
                        $mean = $ean;
                        foreach($tmp as $m) {
                            if(!in_array($m['id'], $matchingIds)) {
                                $matchingIds[] = $m['id'];
                                $matching[] = $m;
                            }
                        }
                    }
                }
                if ($matching) {
                    printf("matched %s %s of %s with %s ", $sproduct['id'], $sproduct['title'], $sproduct['supplier'], count($matchingIds));
                    //if(!$this->tryMerge($sproduct, $matching)) {
                        $xproduct->create($sproduct['product_id'], $matchingIds, $mean,3);
                    //}
                    printf(" \n");
                }
            }
        }
        printf("-----------------\n");
    }

    /**
     * If any of the matching products is already slx-product then merge sproduct to that one
     * @param $sproduct
     * @param $matching
     * @return boolean
     */
    private function tryMerge($sproduct, $matching) {
        $existing = false;
        foreach($matching as $m) {
            if($m['istatus']!=1) {
                $xprId = db_get_field("select xproduct_id from slx_product_supplier_product where sproduct_id=?i", $m['id']);
                if($xprId) {
                    $mean = $m['ean'];
                    $data = [
                        'xproduct_id' => $xprId,
                        'sproduct_id' => $sproduct['id'],
                    ];
                    db_query("INSERT INTO slx_product_supplier_product ?e", $data);
                    db_query("update slx_supplier_product set istatus=3, mean=?s where id=?i", $mean, $sproduct['id']);
                    $existing = true;
                    printf("merged! ");
                    break;
                }
            }
        }
        return $existing;
    }

}