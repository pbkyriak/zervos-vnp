<?php

namespace Slx\ProductMerger;


class SlxProduct {

    /**
     * Create Slx_product along with detail records with associated slx_supplier_products.
     * First checks if any of the associated slx_supplier_products is already attached to
     * any slx_product and if it is, merges $productId with the existing product, else
     * creates a new slx_product.
     *
     * @param $productId int        slx_supplier_product id
     * @param $matchingIds array    the ids of any matching supplier_products
     * @param $mean string          main ean code (the one that lead to merging if any, else the first one of supplier_product
     * @param $istatus int          the istatus value to set at supplier_products
     * @return bool|mixed           returns false, if operation failed, else the id if slx_product created or the one merged to.
     */
    public function create($productId, $matchingIds, $mean, $istatus) {
        $product = db_get_row("SELECT p.*, GROUP_CONCAT(pe.ean) as eans FROM slx_supplier_product p LEFT JOIN slx_supplier_product_ean pe ON p.id=pe.sproduct_id WHERE p.id=?i GROUP BY p.id",$productId);
        if($matchingIds) {
            $matching = db_get_array("SELECT p.*, GROUP_CONCAT(pe.ean) as eans FROM slx_supplier_product p LEFT JOIN slx_supplier_product_ean pe ON p.id=pe.sproduct_id WHERE p.id IN (?a) GROUP BY p.id", $matchingIds);
        }
        else {
            $matching = [];
        }
        $out = $this->tryMerge($product, $matching);        if(!$out) {			$out = $this->matchWithExistingXProduct($product, $matching);						if(!$out) {
				if($mean=='---') {
					$eans = explode(',',$product['eans']);
					if(is_array($eans)) {
						$mean = reset($eans);
					}
				}
				$out = $this->createSlxProduct($product, $matching, $mean, $istatus);			}
        }
        return $out;
    }
	private function matchWithExistingXProduct($sproduct, $matching) {		$out = false;				$xproductId = db_get_field("select id from slx_product where iproduct_id=?i", $sproduct['product_id']);		if($xproductId) {			printf("\tmatched with X %s\n",$xproductId);			$mean = $sproduct['mean'];			$data = [				'xproduct_id' => $xproductId,								'sproduct_id' => $sproduct['id'],							];			db_query("INSERT INTO slx_product_supplier_product ?e", $data);			db_query("update slx_supplier_product set istatus=5, mean=?s where id=?i", $mean, $sproduct['id']);			db_query("update slx_product set istatus=5 where id=?i", $xproductId);						foreach($matching as $m) {				if($m['istatus']==1) {					$data = [						'xproduct_id' => $xproductId,											'sproduct_id' => $m['id'],										];					db_query("INSERT INTO slx_product_supplier_product ?e", $data);				}			}			$existing = $xproductId;			$out = $existing;		}		return $out;	}
    private function createSlxProduct($sproduct, $matching, $mean, $istatus) {
        $data = [
            'title' => $sproduct['title'],
            'mean' => $mean,
            'iproduct_id' => $sproduct['product_id'],
            'istatus' => $istatus,
        ];
        $sid = db_query("INSERT INTO slx_product ?e", $data);
        $matching = array_merge($matching, [$sproduct]);
        foreach($matching as $ma) {
            $data = [
                'xproduct_id' => $sid,
                'sproduct_id' => $ma['id'],
            ];
            //print_r($data);
            db_query("INSERT INTO slx_product_supplier_product ?e", $data);
            db_query("update slx_supplier_product set istatus=?i, mean=?s where id=?i and istatus=1",$istatus, $mean, $ma['id']);
        }
        printf("created! ");
        return $sid;
    }

    /**
     * If any of the matching products is already slx-product then merge sproduct to that one
     * @param $sproduct array   the slx_supplier_product that is for merging
     * @param $matching array   assosiated slx_supplier_products the will check if they are already in a slx_product and will merge $sproduct to that one also
     * @return bool|int         returns false if $sproduct is NOT merged, else slx_product id that it was merged to
     */
    public function tryMerge($sproduct, $matching) {
        $existing = false;        if(empty($sproduct['id'])) {
            return false;
        }
        foreach($matching as $m) {
            if($m['istatus']!=1) {
                $m['ean'] = '';
                $eans = explode(',',$m['eans']);
                if(is_array($eans)) {
                    $m['ean'] = reset($eans);
                }
                $xprId = db_get_field("select xproduct_id from slx_product_supplier_product where sproduct_id=?i", $m['id']);
                if($xprId) {
                    $mean = $m['ean'];
                    $data = [
                        'xproduct_id' => $xprId,
                        'sproduct_id' => $sproduct['id'],
                    ];
                    db_query("INSERT INTO slx_product_supplier_product ?e", $data);
                    db_query("update slx_supplier_product set istatus=3, mean=?s where id=?i", $mean, $sproduct['id']);
                    $existing = $xprId;
                    printf("merged! ");
                    break;
                }
            }
        }
        return $existing;
    }
}