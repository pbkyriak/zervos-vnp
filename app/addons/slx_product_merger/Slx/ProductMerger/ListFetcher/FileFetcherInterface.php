<?php

namespace Slx\ProductMerger\ListFetcher;


interface FileFetcherInterface {

    public function fetch();

}