<?php

namespace Slx\ProductMerger\ListFetcher\Supplier;

use Slx\ProductMerger\ListFetcher\FileFetchHttp;
use Tygh\Registry;
use Slx\ProductMerger\ListFetcher\FileFetcherInterface;

class MpmobileListFetcher implements FileFetcherInterface {

    public function fetch() {
        $fn = Registry::get('product_merger.mpmobile.fn');
        $url = Registry::get('product_merger.mpmobile.url');
        printf("fn=%s\n", $fn);
        printf("url=%s\n", $url);
		$cmd = sprintf('wget -O %s "%s"', $fn, $url);
		exec($cmd);
		return true;
		//return FileFetchHttp::create()->fetch($url, $fn, '', '');
    }

}

