<?php

namespace Slx\ProductMerger\ListFetcher\Supplier;

use Slx\ProductMerger\ListFetcher\FileFetchHttp;
use Tygh\Registry;
use Slx\ProductMerger\ListFetcher\FileFetcherInterface;

class WaveListFetcher implements FileFetcherInterface {

    public function fetch() {
        $fn = Registry::get('product_merger.wave.fn');
        $u = Registry::get('product_merger.wave.user');
        $p = Registry::get('product_merger.wave.pass');
        $url = Registry::get('product_merger.wave.url');
        printf("fn=%s\n u=%s\n p=%s\n", $fn,$u,$p);
        printf("url=%s\n", $url);
		return FileFetchHttp::create()->fetch($url, $fn, $u, $p);
    }

}
