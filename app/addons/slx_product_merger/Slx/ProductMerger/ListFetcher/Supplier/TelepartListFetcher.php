<?php

namespace Slx\ProductMerger\ListFetcher\Supplier;

use Tygh\Registry;
use Slx\ProductMerger\ListFetcher\FileFetcherInterface;

class TelepartListFetcher implements FileFetcherInterface {

    public function fetch() {
        $fn = Registry::get('product_merger.telepart.fn');
        $url = Registry::get('product_merger.telepart.url');
        printf("fn=%s\n url=%s\n", $fn,$url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $store = curl_exec($ch);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        $content = curl_exec($ch);
        if (file_exists($fn)) {
            unlink($fn);
        }
        file_put_contents($fn, $content);
        curl_close($ch);
        return strlen($content)>0 ? true : false;
    }

}
//'https://www.telepart.com/en/mytelepart/mypricelistcsv/D105574/38712';