<?php

namespace Slx\ProductMerger\ListFetcher\Supplier;

use Slx\ProductMerger\ListFetcher\FileFetchHttp;
use Tygh\Registry;
use Slx\ProductMerger\ListFetcher\FileFetcherInterface;

class ApideListFetcher implements FileFetcherInterface {

    public function fetch() {
        $fn = Registry::get('product_merger.apide.fn');
        $url = Registry::get('product_merger.apide.url');
        printf("fn=%s\n", $fn);
        printf("url=%s\n", $url);
		return FileFetchHttp::create()->fetch($url, $fn, '', '');
    }

}

