<?php

namespace Slx\ProductMerger\ListFetcher;

/**
 * Description of FileFetchHttp
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 14 Δεκ 2015
 */
class FileFetchHttp {

    public static function create() {
        return new FileFetchHttp();
    }

    public function fetch($url, $fn, $username = "", $password = "") {
        $out = false;
        $ch = curl_init();
        $userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        $cookieJar = 'cookies.txt';
        curl_setopt($ch, CURLOPT_COOKIEJAR, $cookieJar);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $cookieJar);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        if (!empty($username) && !empty($password)) {
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        }
        $content = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $info = curl_getinfo($ch);
//var_dump($info);
        if ($httpCode == 200) {
            file_put_contents($fn, $content);
            $out = true;
        }
        curl_close($ch);
        return $out;
    }

}
