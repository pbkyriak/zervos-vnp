<?php

namespace Slx\ProductMerger\ListFetcher;


use Slx\ProductMerger\Exception\SupplierFetcherNotSupported;
use Slx\ProductMerger\ListFetcher\Supplier\TelepartListFetcher;
use Slx\ProductMerger\ListFetcher\Supplier\WaveListFetcher;

/**
 * Class FileFetcherFactory: Creates fetcher for given supplier.
 * To add a new fetcher create a class in Supplier folder with class naming convention XXXXListFetcher
 * where XXXX is the supplier name lower case as in cscart_suppliers.name.
 * Also this class MUST implement FileFetcherInterface.
 * Fetchers must collect them self's any required settings or parameters, nothing is passed to their constructor.
 *
 * @package Slx\ProductMerger\ListFetcher
 */
class FileFetcherFactory {

    /**
     * @param $supplier
     * @return null|FileFetcherInterface
     * @throws SupplierFetcherNotSupported
     */
    public static function getFetcher($supplier) {
        $stream = null;

        $streamMethods = self::getSupplierFetchers();
        if(isset($streamMethods[$supplier])) {
            $stream = new $streamMethods[$supplier]();
        }
        else {
            throw new SupplierFetcherNotSupported();
        }
        return $stream;
    }

    /**
     * Return supported supplier fetchers
     *
     * @return array
     */
    public static function getSupportedSuppliers() {
        $fetchers = self::getSupplierFetchers();
        return array_keys($fetchers);
    }

    public static function getSupplierFetchers() {
        $out = [];
        $dir = __DIR__.'/Supplier';
        $ns = '\\Slx\\ProductMerger\\ListFetcher\\Supplier\\';
        foreach(glob($dir.'/*ListFetcher.php') as $fn) {
            $fname = sprintf("%s\n", $fn);
            $class = basename($fn, '.php');
            $supplier = strtolower(str_replace('ListFetcher','',$class));
            $fqn = $ns . $class;
            $r = new \ReflectionClass($fqn);
            if($r->implementsInterface('\\Slx\\ProductMerger\\ListFetcher\\FileFetcherInterface') && !$r->isAbstract()) {
                $supplierId = db_get_field("select supplier_id from ?:suppliers where name=?s", $supplier);
                if($supplierId) {
                    $out[$supplier] = $fqn;
                }
                else {
                    printf("Class found for supplier '%s' but supplier is not found in cscart_suppliers", $supplier);
                }
            }
        }
        return $out;
    }
}