<?php

namespace Slx\Util;


use Slx\ProductMerger\Exporter\ProductStream\SupplierProductStreamFactory;
use Slx\ProductMerger\ImportProductStream\ImportProductStreamFactory;
use Slx\ProductMerger\ListFetcher\FileFetcherFactory;

class SuppliersMergerInfo {

    private function getSuppliersByStatus($status) {
        $names = db_get_fields("select name from ?:suppliers where status=?s", $status);
        $names= array_map('strtolower',$names );
        return $names;
    }

    private function getSupplierProductCount($supplier) {
        $out = db_get_field("select count(*) from slx_supplier_product where supplier=?s",$supplier);
        return $out;
    }
    private function getSupplierPendingProductCount($supplier) {
        $out = db_get_field("select count(*) from slx_icecat_product_pending where source=?s",$supplier);
        return $out;
    }
    private function getSupplierPendingProductCountNotChecked($supplier) {
        $out = db_get_field("select count(*) from slx_icecat_product_pending where lastchecked=0 and source=?s",$supplier);
        return $out;
    }

    public function getSuppliersInfo() {
        $active = $this->getSuppliersByStatus('A');
        $inactive = $this->getSuppliersByStatus('D');
        $all = array_merge($active, $inactive);

        $inStreamers = ImportProductStreamFactory::getSupplierStreams();
        $fetchers = FileFetcherFactory::getSupplierFetchers();
        $byPassers = SupplierProductStreamFactory::getSupplierStreams();

        $out = [];
        foreach($all  as $supplier) {
            $out[$supplier] = [
                'supplier' => $supplier,
                'instreamer' => isset($inStreamers[$supplier]) ? $inStreamers[$supplier] : '',
                'fetcher' => isset($fetchers[$supplier]) ? $fetchers[$supplier] : '',
                'byPasser' => isset($byPassers[$supplier]) ? $byPassers[$supplier] : '',
                'status' => in_array($supplier, $active) ? 'A' : 'D',
                'products' => $this->getSupplierProductCount($supplier),
                'ipproducts' => $this->getSupplierPendingProductCount($supplier),
                'ipproductsnc' => $this->getSupplierPendingProductCountNotChecked($supplier),
            ];

        }
        return $out;
    }

    public function getIProductsStatus() {
        $out = [
            'total' => db_get_field(" select count(*) from slx_icecat_product"),
            'pending' => db_get_field("select count(*) from slx_icecat_product_pending"),
            'pending_not' => db_get_field("select count(*) from slx_icecat_product_pending where lastchecked=0"),
        ];
        return $out;
    }

    public function getSProductsStatus() {
        $out = [
            'total' => db_get_field(" select count(*) from slx_supplier_product"),
            'status1' => db_get_field(" select count(*) from slx_supplier_product where istatus=1"),
            'status4' => db_get_field(" select count(*) from slx_supplier_product where istatus=4"),            'status5' => db_get_field(" select count(*) from slx_supplier_product where istatus=5"),
            'status9' => db_get_field(" select count(*) from slx_supplier_product where istatus=9"),
            'bysupplier' => db_get_array("select supplier, count(*) cnt from slx_supplier_product group by supplier"),
            'bysupplier_upd' => db_get_array("select supplier, count(*) cnt, max(upd_timestamp) tim  from slx_supplier_product where price!=0 and availability!=0 group by supplier"),
        ];
        return $out;
    }

    public function getXProductsStatus() {
        $out = [
            'total' => db_get_field(" select count(*) from slx_product"),
            'status1' => db_get_field(" select count(*) from slx_product where istatus=1"),
            'status4' => db_get_field(" select count(*) from slx_product where istatus=4"),            'status5' => db_get_field(" select count(*) from slx_product where istatus=5"),
            'status9' => db_get_field(" select count(*) from slx_product where istatus=9"),
            'matched' => db_get_field("select count(*) from slx_product where cproduct_id!=0"),
            'dominant' => db_get_field("select count(*) from slx_product where eproduct_id!=0"),
            'pu_result' => db_get_array("select pu_result_id, count(id) cnt from slx_product where pu_result_id!=0 group by pu_result_id"),
        ];
        return $out;
    }

    public function getCsProductsStatus() {
        $out = [
            'total' => db_get_field("select count(*) from cscart_products"),
            'active' => db_get_field("select count(*) from cscart_products where status='A'"),
            'hidden' => db_get_field("select count(*) from cscart_products where status='H'"),
            'disabled' => db_get_field("select count(*) from cscart_products where status='D'"),
            'no_sup_linked' => db_get_field("select count(*) from cscart_products p left join cscart_supplier_links sl on sl.object_id=p.product_id and sl.object_type='P' where isnull(sl.supplier_id)"),
            'suppliers_by_status' => db_get_array("select s.name, p.status, count(*) cnt from cscart_products p left join cscart_supplier_links sl on sl.object_id=p.product_id and sl.object_type='P' left join cscart_suppliers s on sl.supplier_id=s.supplier_id where not isnull(sl.supplier_id) group by s.name, p.status"),
        ];
        return $out;
    }
}