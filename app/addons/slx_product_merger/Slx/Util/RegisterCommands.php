<?php

namespace Slx\Util;


class RegisterCommands {
    private $ns;
    private $dir;
    private $application;

    public function setNamespace($ns) {
        $this->ns = $ns;
        return $this;
    }

    public function setApplication($application) {
        $this->application = $application;
        return $this;
    }

    public function setDir($dir) {
        $this->dir = $dir;
        return $this;
    }

    public function register() {
        foreach(glob($this->dir.'*Command.php') as $fn) {
            $basename = basename($fn,'.php');
            $class = sprintf("%s\\%s", $this->ns, $basename);
            $r = new \ReflectionClass($class);
            if ($r->isSubclassOf('Symfony\\Component\\Console\\Command\\Command') && !$r->isAbstract() && !$r->getConstructor()->getNumberOfRequiredParameters()) {
                $this->application->add($r->newInstance());
            }
        }
    }
}