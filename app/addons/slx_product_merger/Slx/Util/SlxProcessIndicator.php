<?php

namespace Slx\Util;


class SlxProcessIndicator {

    private $step=0;
    private $padLen = 8;
    private $message;

    public function start($message='') {
        $this->padLen = $this->padLen + strlen($message)+1;
        $this->message = $message;
        echo str_pad(' ',$this->padLen);
    }

    public function advance() {
        for($i=0; $i<$this->padLen; $i++) {
            echo "\010";
        }
        printf("%s [%6d]",$this->message, $this->step);
        $this->step++;
    }

    public function end() {
        echo "\n";
    }
}