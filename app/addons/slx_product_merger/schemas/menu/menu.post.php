<?php

$schema['central']['products']['items']['merger'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'xproducts.overview',
    'position' => 700,
    'subitems' => [
        'xproducts' => array(
            'href' => 'xproducts.manage',
            'position' => 100
        ),
        'sproducts' => array(
            'href' => 'sproducts.manage',
            'position' => 110
        ),
        'iproducts' => array(
            'href' => 'iproducts.manage',
            'position' => 120
        ),
        'iproductspending' => array(
            'href' => 'iproductspending.manage',
            'position' => 122
        ),
        'icategories' => array(
            'href' => 'icategories.manage',
            'position' => 130
        ),
        'icategoriescrolllinked' => array(
            'href' => 'icategories.crosslinked',
            'position' => 132
        ),
    ],
);

return $schema;
