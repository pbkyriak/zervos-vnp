CREATE TABLE `slx_icecat_ean` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ean` varchar(15) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ean_uniq` (`ean`),
  KEY `product_ean_idx` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_icecat_feature` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `fname` varchar(255) DEFAULT NULL,
  `fvalue` varchar(255) DEFAULT NULL,
  `fsign` varchar(45) DEFAULT NULL,
  `fg_id` int(11) NOT NULL DEFAULT '0',
  `lang_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fname_idx` (`fname`),
  KEY `productid_idx` (`product_id`),
  KEY `pid_fname_idx` (`product_id`,`fname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_icecat_feature_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `lang_code` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id_unq` (`group_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_icecat_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `checked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_icecat_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pcode` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `lastchecked` int(11) DEFAULT NULL,
  `ean` varchar(15) DEFAULT NULL,
  `overview_el` mediumtext NOT NULL,
  `overview_en` mediumtext NOT NULL,
  `mpc` varchar(45) NOT NULL DEFAULT '',
  `brand` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `category` varchar(255) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `ice_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `iceid_idx` (`ice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_icecat_product_description` (
  `title` varchar(255) DEFAULT NULL,
  `overview` text,
  `lang_code` varchar(2) NOT NULL,
  `iproduct_id` int(11) NOT NULL,
  PRIMARY KEY (`iproduct_id`,`lang_code`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_icecat_product_pending` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ean` varchar(15) DEFAULT NULL,
  `lastchecked` int(11) DEFAULT '0',
  `source` varchar(45) DEFAULT NULL,
  `pcode` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pcode_ean_source_idx` (`ean`,`source`,`pcode`),
  KEY `ean_idx` (`ean`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `cproduct_id` int(11) DEFAULT '0',
  `iproduct_id` int(11) DEFAULT '0',
  `mean` varchar(14) DEFAULT NULL,
  `istatus` int(11) DEFAULT '0',
  `eproduct_id` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_product_match_alternative_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `words` text,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `words_idx` (`words`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `slx_product_match_false_positive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sproduct_ids` text,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `id_idx` (`sproduct_ids`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `slx_product_supplier_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `xproduct_id` int(11) DEFAULT NULL,
  `sproduct_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_supplier_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier` varchar(15) NOT NULL DEFAULT '',
  `pcode` varchar(45) NOT NULL DEFAULT '',
  `availability` int(11) NOT NULL DEFAULT '0',
  `category` varchar(125) NOT NULL DEFAULT '',
  `price` decimal(12,2) NOT NULL DEFAULT '0.00',
  `title` varchar(255) NOT NULL DEFAULT '',
  `product_id` int(11) NOT NULL DEFAULT '0',
  `istatus` int(11) NOT NULL DEFAULT '0',
  `mean` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sup_scode_idx` (`supplier`,`pcode`),
  KEY `pid_idx` (`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_supplier_product_ean` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sproduct_id` int(11) DEFAULT NULL,
  `ean` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ean_idx` (`ean`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

CREATE TABLE `slx_supplier_product_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sproduct_id` int(11) DEFAULT NULL,
  `title` text,
  `price` decimal(12,2) DEFAULT '0.00',
  PRIMARY KEY (`id`),
  FULLTEXT KEY `title_idx` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


ALTER TABLE cscart_products ADD COLUMN xproduct_id INT default 0 not null;
ALTER TABLE cscart_categories CHANGE `supplier_category_name` `supplier_category_name_old` TEXT CHARSET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE cscart_categories ADD COLUMN `supplier_category_name_new` TEXT NULL;
ALTER TABLE cscart_categories CHANGE `supplier_category_name_new` `supplier_category_name` TEXT CHARSET utf8 COLLATE utf8_general_ci NOT NULL;


ALTER TABLE cscart_suppliers ADD COLUMN `pm_category_flt_type` CHAR(1) DEFAULT 'X'  NULL AFTER `wmarkup_amount`, ADD COLUMN `pm_category_flt_cat_codes` TEXT NULL AFTER `pm_category_flt_type`;

-- create index object_id on cscart_supplier_links (object_id, object_type);
-- alter table cscart_supplier_links add primary key (supplier_id, object_id, object_type);