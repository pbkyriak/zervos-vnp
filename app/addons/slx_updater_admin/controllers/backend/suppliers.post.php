<?php
//
// Author Ioannis Matziaris [imatz] - imatzgr@gmail.com - February 2014
//

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	return;
}

if ($mode == 'update' || $mode == 'add') {
	$navigation_tabs=Registry::get('navigation.tabs');
	$navigation_tabs['updaters']=array(
		'title' => __('updaters'),
        'js' => true
	);
	Registry::set('navigation.tabs',$navigation_tabs);
}