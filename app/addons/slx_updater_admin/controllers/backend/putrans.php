<?php

use Tygh\Registry;
use Slx\UpdaterAdmin\Lists\PuTranslations;
 
if (!defined('BOOTSTRAP')) { die('Access denied'); }
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $suffix = ".manage";
    if($mode=='update') {

        $hub_data = $_REQUEST['hub_data'];
        $xp = new PuTranslations();
        $hub_id = $xp->update($_REQUEST['hub_data'], $_REQUEST['trans_id'], DESCR_SL);
        $suffix = ".update?trans_id=".$hub_id;
    }

    elseif($mode=='delete') {
        $transId = $_REQUEST['trans_id'];
        $xp = new PuTranslations();
        $xp->delete($transId);
        $suffix = ".manage";
    }
    return array(CONTROLLER_STATUS_OK, 'putrans'.$suffix );
}
if($mode=='manage') {
    $params = $_REQUEST;
    $xp = new PuTranslations();
    list($hubs, $search) = $xp->get($params, Registry::get('settings.Appearance.admin_elements_per_page'), DESCR_SL);
    Tygh::$app['view']->assign('hubs', $hubs);
    Tygh::$app['view']->assign('search', $search);}
elseif($mode=='update') {
    $xp = new PuTranslations();
    $data = $xp->getType($_REQUEST['trans_id']);
    Tygh::$app['view']->assign('hub_data', $data);
}

