<?php

use Tygh\Registry;

if (!defined('AREA')) {
  die('Access denied');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if( $mode == 'upload_import_file' ) {
    $uploaddir = Registry::get("product_merger.supplier_files_dir");
    $uploadfile = $uploaddir .'/difox.csv';
	
    if (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', __('notice'), __('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"datauploader.upload_import_file");
  }
  if( $mode == 'upload_import_file_eglobal' ) {
    $uploaddir = Registry::get("product_merger.supplier_files_dir");
    $uploadfile = $uploaddir .'/eglobal.csv';
	
    if (move_uploaded_file($_FILES['catalog']['tmp_name'], $uploadfile)) {
      fn_set_notification('N', __('notice'), __('file_uploaded_successfully'));
    }
    return array(CONTROLLER_STATUS_OK,"datauploader.upload_import_file");
  }
  
  
}


if ($mode == 'upload_import_file') {
  fn_add_breadcrumb(__('upload_import_file'));
}

