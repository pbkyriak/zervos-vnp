<?php

namespace Slx\UpdaterAdmin\Lists;

class PuTranslations {


    public function get($params,$items_per_page = 0, $lang_code = CART_LANGUAGE) {
        // Set default values to input params
        $default_params = array(
            'page' => 1,
            'items_per_page' => $items_per_page
        );

        $params = array_merge($default_params, $params);

        // Unset all SQL variables
        $fields = $joins = array();
        $condition = $sorting = $limit = $group = '';

        $fields = array(
            'a.trans_id',
            'a.original',
            'a.translation',
        );

        $condition = 'WHERE 1';

        $group = "";
        $sorting = "ORDER BY a.original";

        if (!empty($params['original'])) {
            $condition .= db_quote(" AND a.original like ?s", str_replace('*', '%',$params['original']));
        }
        if (!empty($params['translation'])) {
            $condition .= db_quote(" AND a.translation like ?s", str_replace('*', '%',$params['translation']));
        }

        if (!empty($params['items_per_page'])) {
            $params['total_items'] = db_get_field("
                    SELECT COUNT(DISTINCT a.trans_id)
                    FROM slx_pu_translations AS a
                    $condition");
            $limit = db_paginate($params['page'], $params['items_per_page'], $params['total_items']);
        }

        $results = db_get_array(
            "SELECT " . implode(', ', $fields) . " FROM slx_pu_translations as a " .
            implode(' ', $joins) .
            " $condition $group $sorting $limit"
        );
        return array($results, $params);
    }

    public function getType($trans_id) {
        $hub_data = db_get_row("select * from slx_pu_translations where trans_id=?i", $trans_id);
        return $hub_data;
    }
    public function update($data, $trans_id) {
        if(empty($trans_id)) {
            $otransid = db_get_field("select trans_id from slx_pu_translations where original=?s", $data['original']);
            if(empty($otransid)) {
                $data['trans_id'] = $trans_id = db_query("INSERT INTO slx_pu_translations ?e", $data);
            }
            else {
                $data['trans_id'] = $trans_id = $otransid;
            }
        }
        else {
            db_query("UPDATE slx_pu_translations SET ?u WHERE trans_id = ?i", $data, $trans_id);
        }
        return $trans_id;
    }

    public function delete($trans_id) {
        if(!empty($trans_id)) {
            db_query("delete from slx_pu_translations where trans_id=?i", $trans_id);
        }
    }
    
    public function getDictionary() {
        $trans = db_get_hash_single_array("select distinct original, translation from slx_pu_translations order by original", ['original', 'translation']);
        return $trans;
    }
}