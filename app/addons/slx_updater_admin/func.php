<?php
/**
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 13 Δεκ 2015
 */

use Slx\ProductMerger\Icecat\IcecatCategoryIdHelper;

/**
 * Helper used from mpnfromfeature and manufacturerfromfeature settings
 * @return array
 */
function fn_slx_updater_admin_get_features_for_settings() {
    $out = array('0'=>__('none'));

    $fs = db_get_hash_single_array("select pf.feature_id, pfd.description from cscart_product_features pf
            left join cscart_product_features_descriptions as pfd on (pf.feature_id=pfd.feature_id and pfd.lang_code='el')
            where pf.feature_code='' and pf.status='A' ", array('feature_id', 'description'));
    //and pf.feature_type='E'
    foreach($fs as $k => $v) {
        $out[$k] = $v;
    }
    return $out;    
}

/**
 * addon.xml settings variants for manufacturerfromfeature setting
 * @return array
 */
function fn_settings_variants_addons_slx_updater_admin_manufacturerfromfeature() {
    return fn_slx_updater_admin_get_features_for_settings();
}

function slx_install_pu_settings() {
    $oid = db_get_field("select object_id from cscart_settings_objects where `name`=?s", 'christmass_hat');
    if($oid) {
        db_query("delete from cscart_settings_objects where object_id=?i", $oid);
        db_query("delete from cscart_settings_descriptions where object_type='O' and object_id=?i", $oid);
    }
    $id=db_query("INSERT INTO cscart_settings_objects
                (edition_type, `name`, section_id, section_tab_id, `type`, `value`, `position`, is_global)
                VALUES ('ROOT', 'christmass_hat', 2, 0, 'I', 0, 45, 'Y');");
    db_query("INSERT INTO cscart_settings_descriptions (object_id, object_type, lang_code, `value`, `tooltip`)
            VALUES (?i, 'O','el', 'Καπέλο τιμών αυτόματων','Καπέλο τιμών στα προιοντα που ενημερώνονται αυτόματα')",
            $id);
    db_query("INSERT INTO cscart_settings_descriptions (object_id, object_type, lang_code, `value`, `tooltip`)
            VALUES (?i, 'O','en', 'Καπέλο τιμών αυτόματων','Καπέλο τιμών στα προιοντα που ενημερώνονται αυτόματα')",
            $id);
}

function fn_slx_updater_admin_update_category_post($category_data, $category_id, $lang_code) {
    $names = explode(',', $category_data['supplier_category_name']);
    db_query("delete from ?:categories_supplier_matching where category_id=?i", $category_id);
    foreach ($names as $name) {
        $cnt = db_get_field("select count(*) from slx_icecat_product where category_id=?s", IcecatCategoryIdHelper::nameToId($name));
        if($cnt==0) {
            fn_set_notification('W', 'XCATEGORY', sprintf("Η κατηγορία %s δεν υπάρχει", $name));
        }
        $cids = db_get_fields("select category_id from ?:categories_supplier_matching where supplier_category_name=?s", $name);
        if($cids) {
            $clinks = [];
            array_walk($cids, function($id, $idx) use (&$clinks){
                $clinks[] = sprintf(
                    "<a href='%s'>%s</a>",
                    fn_url("categories.update?category_id=".$id),
                    $id
                );
            });
            fn_set_notification('W', 'XCATEGORY', 'Category '.$name. ' already set to <br />'. implode(', ', $clinks));
        }
        db_query("INSERT INTO ?:categories_supplier_matching (category_id, supplier_category_name) VALUES (?i, ?s)", $category_id, $name);
    }

}

function fn_slx_updater_admin_get_products_before_select( $params, $join, &$condition, $u_condition, $inventory_join_cond, $sortings, $total, $items_per_page, $lang_code, $having) {
    if(isset($params['xproduct_related'])) {
        if(!empty($params['xproduct_related'])) {
            if($params['xproduct_related']=='N') {
                $condition .= db_quote(" AND products.xproduct_id=0");
            }
            elseif($params['xproduct_related']=='Y') {
                $condition .= db_quote(" AND products.xproduct_id!=0");
            }
        }
    }
}