<?php

$schema['central']['products']['items']['difox_uploader'] = array(
    'attrs' => array(
        'class'=>'is-addon'
    ),
    'href' => 'datauploader.upload_import_file',
    'position' => 600,
);

$schema['central']['products']['items']['putrans'] = array(
    'href' => 'putrans.manage',
    'position' => 610
);

return $schema;
