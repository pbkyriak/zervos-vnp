<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_seo_greeklish_generate_name_pre(&$str, $object_type, $object_id) {
	
	if(!empty($str)) {
		$str = str_replace("α", "a", $str);
		$str = str_replace("Α", "a", $str);
		$str = str_replace("ά", "a", $str);
		$str = str_replace("Ά", "a", $str);
		$str = str_replace("Β", "v", $str);
		$str = str_replace("β", "v", $str);
		$str = str_replace("γ", "g", $str);
		$str = str_replace("Γ", "g", $str);
		$str = str_replace("δ", "d", $str);
		$str = str_replace("Δ", "d", $str);
		$str = str_replace("ε", "e", $str);
		$str = str_replace("Ε", "e", $str);
		$str = str_replace("έ", "e", $str);
		$str = str_replace("Έ", "e", $str);
		$str = str_replace("Ζ", "z", $str);
		$str = str_replace("ζ", "z", $str);
		$str = str_replace("η", "i", $str);
		$str = str_replace("Η", "i", $str);
		$str = str_replace("ή", "i", $str);
		$str = str_replace("Ή", "i", $str);
		$str = str_replace("θ", "th", $str);
		$str = str_replace("Θ", "th", $str);
		$str = str_replace("ι", "i", $str);
		$str = str_replace("Ι", "i", $str);
		$str = str_replace("ί", "i", $str);
		$str = str_replace("Ί", "i", $str);
		$str = str_replace("ϊ", "i", $str);
		$str = str_replace("Ϊ", "i", $str);
		$str = str_replace("κ", "k", $str);
		$str = str_replace("Κ", "k", $str);
		$str = str_replace("λ", "l", $str);
		$str = str_replace("Λ", "l", $str);
		$str = str_replace("μ", "m", $str);
		$str = str_replace("Μ", "m", $str);
		$str = str_replace("ν", "n", $str);
		$str = str_replace("Ν", "n", $str);
		$str = str_replace("ξ", "ks", $str);
		$str = str_replace("Ξ", "ks", $str);
		$str = str_replace("ο", "o", $str);
		$str = str_replace("Ο", "o", $str);
		$str = str_replace("ό", "o", $str);
		$str = str_replace("Ό", "o", $str);
		$str = str_replace("π", "p", $str);
		$str = str_replace("Π", "p", $str);
		$str = str_replace("ρ", "r", $str);
		$str = str_replace("Ρ", "r", $str);
		$str = str_replace("σ", "s", $str);
		$str = str_replace("Σ", "s", $str);
		$str = str_replace("ς", "s", $str);
		$str = str_replace("τ", "t", $str);
		$str = str_replace("Τ", "t", $str);
		$str = str_replace("υ", "u", $str);
		$str = str_replace("Υ", "u", $str);
		$str = str_replace("ύ", "u", $str);
		$str = str_replace("Ύ", "u", $str);
		$str = str_replace("φ", "f", $str);
		$str = str_replace("Φ", "f", $str);
		$str = str_replace("χ", "h", $str);
		$str = str_replace("Χ", "h", $str);
		$str = str_replace("ψ", "ps", $str);
		$str = str_replace("Ψ", "ps", $str);
		$str = str_replace("ω", "o", $str);
		$str = str_replace("Ω", "o", $str);
		$str = str_replace("ώ", "o", $str);
		$str = str_replace("Ώ", "o", $str);
		$str = str_replace("&", "kai", $str);
	}

}

?>