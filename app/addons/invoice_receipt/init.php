<?php
//
// Author Ioannis Matziaris - imatzgr@gmail.com - April 2013
//

if (!defined('BOOTSTRAP')) { die('Access denied'); }

fn_register_hooks(
	'get_profile_fields',
	'get_user_info',
	'checkout_summary',
	'get_payment_methods',
	'prepare_checkout_payment_methods',
	'calculate_cart_taxes_pre',

	//Custom Hooks

	//Twigmo
	'place_order_tiwgmo'
);

?>