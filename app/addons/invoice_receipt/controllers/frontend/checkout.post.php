<?php
//
// Author Ioannis Matziaris - imatzgr@gmail.com - April 2013
//

if (!defined('BOOTSTRAP')) { die('Access denied'); }
use Tygh\Registry;

$cart = & $_SESSION['cart'];

if ($mode == 'checkout') {
	$edit_step = !empty($_REQUEST['edit_step']) ? $_REQUEST['edit_step'] : (!empty($_SESSION['edit_step']) ? $_SESSION['edit_step'] : '');
	if(empty($edit_step)){
		$edit_step=Registry::get('view')->getTemplateVars('edit_step');
	}

	if($edit_step == 'step_two'){
		if(!isset($cart["invoice_or_receipt"])) {
			$cart["invoice_or_receipt"]="R";
		}
		if(!isset($cart["receipt_from_store"])) {
			$cart["receipt_from_store"] = 'N';
		}

		if (!empty($auth['user_id'])) {
			$cart = & $_SESSION['cart'];
			if($cart["invoice_or_receipt"]=='I'){		
				fn_profile_fields_empty_vals("remove",array("B"),$cart['user_data']);
				Registry::get('view')->assign('user_data',$cart['user_data']);
				Registry::get('view')->assign('cart',$cart);
			}
		}	
	}
	if($edit_step == 'step_three'){
		//An epilegei paralavi apo to katastima tote mporei na min epilegei kapoios tropos apostolis
		//Etsi ginete epilogi ana omada proiontwn tou prwtou tropou apostolis		
		if(isset($cart['product_groups'])&&isset($cart['receipt_from_store'])&&$cart['receipt_from_store']=="Y"){
			foreach($cart['product_groups'] as $gindex=>$group_data){
				if(isset($group_data['shippings'])&&!empty($group_data['shippings'])&&
					isset($group_data['chosen_shippings'])&&empty($group_data['chosen_shippings'])){

					$first_shipping=reset($group_data['shippings']);
					$cart['product_groups'][$gindex]['chosen_shippings'][]=$first_shipping;
				}
			}
		}
	}
	if($edit_step == 'step_two'){
		if(!isset($cart["invoice_or_receipt"])) {
			$cart["invoice_or_receipt"]="R";
		}
		if(!isset($cart["receipt_from_store"])) {
			$cart["receipt_from_store"] = 'N';
		}
		//Me tin epilogi paralavis apo katastima prepei na ginete apokripsei olwn twn addresses.
		//An o pelatis exei kena kapoia pedia billing tote den afinei na proxorisei sto epomeno bima
		//Etsi gemizoume ta ipoxrewtika pedia gia na sinexisei
		$user_data=Registry::get('view')->getTemplateVars('user_data');
		$profile_fields=Registry::get('view')->getTemplateVars('profile_fields');
		if($cart["receipt_from_store"]=='Y'){
			fn_profile_fields_empty_vals("set",array("S","B"),$user_data);	
			fn_profile_fields_empty_vals("set",array("S","B"),$cart['user_data']);	
		}else{
			fn_profile_fields_empty_vals("remove",array("S","B"),$user_data);
			fn_profile_fields_empty_vals("remove",array("S","B"),$cart['user_data']);	
		}

		//An erthoume sto checkout tote me epilegmeni apodiksei emfanizei stoixeia timologiou
		if(!isset($cart["invoice_or_receipt"])||$cart["invoice_or_receipt"]=="R"){
			if(Registry::get('settings.General.address_position') == "billing_first"){
				$sec_section="S";
			}else{
				$sec_section="B";
			}
			unset($profile_fields[$sec_section]);
		}

		//An erthoume sto 2o vima me epilegmeno mi paralavi apo katastima kai me apodeiksi
		//Diorthosi oti pedia apostolis idia me pliromis giati to allazei o checkout controller
		if(isset($cart["invoice_or_receipt"])&&$cart["invoice_or_receipt"]=="R"&&
			isset($cart["receipt_from_store"])&&$cart["receipt_from_store"]=="N"){

			$cart['i_ship_to_another']=false;
			$cart['ship_to_another']=false;
		}
		Registry::get('view')->assign('user_data', $user_data);//Enimeroni to 2o vima tou checkout
		Registry::get('view')->assign('cart',$cart);//Enimeroni to block sta deksia me ta stoixeia dieythinsenw
		Registry::get('view')->assign('profile_fields',$profile_fields);
	}
	//Exei mpei kai to step 3 giati to my changes kanei redirect sto step four giati exei ginei aferesei tou step three
	if($edit_step == 'step_four'||$edit_step == 'step_three'){

		//An o pelatis den exei epileksei pralavi apo to katastima tote metavasi to 2o vima
		//Efoswn ta ipoxrewtika pedia profile einai kena meta kai apo aferesi twn -|-

		//An o pelatis den exei epilksei paralavi apo to katastima
		if(!isset($cart["receipt_from_store"]) || $cart["receipt_from_store"]=='N'){
			$user_data=Registry::get('view')->getTemplateVars('user_data');
			$required_fields_validations=true;
			//An exei zitisi apodiksei lianikis tote katharisma pediwn billing
			if($cart["invoice_or_receipt"]=="R"){
				fn_profile_fields_empty_vals("remove",array("S"),$user_data);
				fn_profile_fields_empty_vals("remove",array("S"),$cart['user_data']);
				//Elegxos an ta ipoxrewtika pedia exoun times
				$required_fields_validations=fn_required_fields_validation($user_data,array("S"));
			}else{//An exei zitisi timologio tote katharisma pediwn shipping kai billing
				fn_profile_fields_empty_vals("remove",array("S","B"),$user_data);
				fn_profile_fields_empty_vals("remove",array("S","B"),$cart['user_data']);
				//Elegxos an ta ipoxrewtika pedia exoun times
				$required_fields_validations=fn_required_fields_validation($user_data,array("S","B"));
			}
			//An ta ipoxrewtika pedia den exoun times meta apo aferesi twn -|-
			if(!$required_fields_validations){
				//Metavasi sto 2o vima
				$_SESSION['edit_step'] = "step_two";
				Registry::get('view')->assign('edit_step', "step_two");
			}
			Registry::get('view')->assign('user_data', $user_data);//Enimeroni to 2o vima tou checkout
			Registry::get('view')->assign('cart',$cart);//Enimeroni to block sta deksia me ta stoixeia dieythinsenw

			//Me tin emfanisi tou 2ou vimatos an exei epilegei apodiksei tote aferesei pediwn timologiou
			$profile_fields=Registry::get('view')->getTemplateVars('profile_fields');
			//An erthoume sto checkout tote me epilegmeni apodiksei emfanizei stoixeia timologiou
			if(!isset($cart["invoice_or_receipt"])||$cart["invoice_or_receipt"]=="R"){
				if(Registry::get('settings.General.address_position') == "billing_first"){
					$sec_section="S";
				}else{
					$sec_section="B";
				}
				unset($profile_fields[$sec_section]);
			}
		}	
	}
}