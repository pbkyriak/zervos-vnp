<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }


/*  creation: N.Sikiniotis - 08/02/2013
 *  update:
 *  last update:
 *
 *  I=invoice,R=receipt
 *  stelnv ap'to step_two (shipping billing info) an uelei apodeijh h timologio
 *  default einai R (apodeijh)
 *  to xrhsimopoiv sthn fn_get_profile_fields() toy core/fn.users.php gia na mhn trabaei ta pedia toy billing  sthn apodeijh
 *  redirect (checkout.checkout.guest_checkout) gia na pav sto step_two (poy hdh eimai)
*/

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	if($mode=="update_steps"){
		if(isset($_REQUEST['update_step']) && $_REQUEST['update_step']=='step_two') {
			$cart = & $_SESSION['cart'];	
			

			if($cart["invoice_or_receipt"]=='I') {
				if(isset($_REQUEST['i_some_as_above'])){
					if($_REQUEST['i_some_as_above']=='Y'){
						fn_copy_values_from_shipping_to_billing($_REQUEST['user_data'],$cart);					
					}else{				
					}
				}
			}else{
				fn_copy_values_from_shipping_to_billing($_REQUEST['user_data'],$cart);	
				fn_profile_fields_empty_vals("set",array("B"),$_REQUEST['user_data']);				
			}
		}
	}	
}
if($mode=="select_invoice_type"){	
	$dispatch=explode(".",$_REQUEST['dispatch']);
	$cart = & $_SESSION['cart'];			
	$cart["invoice_or_receipt"]=$dispatch[2];
	
	if(!empty($_REQUEST['user_data'])){
		fn_save_post_user_data_to_cart($cart,$_REQUEST['user_data']);
	}
	
	if($cart["invoice_or_receipt"]=='I'){
		fn_profile_fields_empty_vals("remove",array("B"),$cart['user_data']);
	}else{
		fn_profile_fields_empty_vals("set",array("B"),$cart['user_data']);
	}
	if(!empty($auth['user_id'])){
		fn_save_cart_content($cart, $auth['user_id']);
	}
	
	return array(CONTROLLER_STATUS_REDIRECT, "checkout.checkout?edit_step=step_two&from_step=step_two");
	
}elseif($mode=='receipt_from_store'){
	$dispatch=explode(".",$_REQUEST['dispatch']);
	$cart = & $_SESSION['cart'];

	$cart["receipt_from_store"]=$dispatch[2];
	
	if(!empty($_REQUEST['user_data'])){
		fn_save_post_user_data_to_cart($cart,$_REQUEST['user_data']);
	}
	
	if($cart["receipt_from_store"]=='Y'){
		fn_profile_fields_empty_vals("set",array("S","B"),$cart['user_data']);	
		$cart['i_ship_to_another']=true;
		//Me tin epilogi paralavis apo to katastima an exei epilegei timologio tote prepei na girisei se apodiksei
		$cart["invoice_or_receipt"]="R";
		$cart['chosen_shipping'] = db_get_field("select shipping_id from cscart_shippings where receipt_from_store='Y' and status='A' limit 0,1");
		
	}else{
		fn_profile_fields_empty_vals("remove",array("S","B"),$cart['user_data']);
		$cart['chosen_shipping'] = db_get_field("select shipping_id from cscart_shippings where receipt_from_store='N' and status='A' limit 0,1");
	}
	if(!empty($auth['user_id'])){
		fn_save_cart_content($cart, $auth['user_id']);
	}
	//<nsik> 25/8/14 sbhnv to krathmeno payment_id se periptvsh poy gyrisei apo epilogh plhrvmhs 
	//unset($cart['payment_id']);
	$_REQUEST['next_step']='step_two';
	//return array(CONTROLLER_STATUS_REDIRECT, "checkout.checkout&edit_step=step_two");
	return array(CONTROLLER_STATUS_REDIRECT, 'checkout.checkout?edit_step=step_two&from_step=step_two');
}elseif($mode=="copy_s_address_to_b"){
	$dispatch=explode(".",$_REQUEST['dispatch']);
	$cart = & $_SESSION['cart'];
	$same_as_above=$dispatch[2];
	
	if(!empty($_REQUEST['user_data'])){
		fn_save_post_user_data_to_cart($cart,$_REQUEST['user_data']);
	}
	
	if($same_as_above=='Y' && $cart["receipt_from_store"]=='N'){
		$cart['i_ship_to_another']=false;
		fn_copy_values_from_shipping_to_billing($_REQUEST['user_data'],$cart);
	}else{
		$cart['i_ship_to_another']=true;		
	}
	
	if(!empty($auth['user_id'])){
		fn_save_cart_content($cart, $auth['user_id']);
	}
	
	return array(CONTROLLER_STATUS_REDIRECT, "checkout.checkout?edit_step=step_two&from_step=step_two");
}

if($mode=="update_steps"||$mode=="checkout"){
	$edit_step = !empty($_REQUEST['edit_step']) ? $_REQUEST['edit_step'] : (!empty($_SESSION['edit_step']) ? $_SESSION['edit_step'] : '');
	$cart = & $_SESSION['cart'];

	if(empty($cart["invoice_or_receipt"])){
		$cart["invoice_or_receipt"]="R";
		//Orismos tou ginete epeisis ston post controller mias kai o controller allazei tin timi
		$cart['i_ship_to_another']=false;
		$cart['receipt_from_store']='N';
	}
	
	if($edit_step=='step_two'){//edit tou 2ou vimatos
		if($cart["invoice_or_receipt"]=='R'){//exei epilegei apodeiksi
			fn_copy_values_from_shipping_to_billing($cart['user_data'],$cart);	
			fn_profile_fields_empty_vals("set",array("B"),$cart['user_data']);
		}else{
			if((isset($cart['ship_to_another']) && !$cart['ship_to_another'])
				|| (isset($cart['i_ship_to_another']) && !$cart['i_ship_to_another'])){
					fn_profile_fields_empty_vals("remove",array("B"),$cart['user_data']);
					fn_copy_values_from_shipping_to_billing($cart['user_data'],$cart);					
				}
			
		}
	}

}

if(isset($_REQUEST['user_data'])&&!empty($_REQUEST['user_data'])){
	
	//Emfanizei sta pedia tou 2 vimatos ta stoixeia pou exei dosei o xristis
	//stin forma apo to 1p vima. O xristis exei epileksei agora os episkepteis
	$cart=&$_SESSION['cart'];
	$r_user_data=$_REQUEST['user_data'];
	$user_data=$cart['user_data'];		
	$params=array(
		'S'=>array('firstname'=>'s_firstname','lastname'=>'s_lastname','phone'=>'s_phone'),
		'B'=>array('firstname'=>'b_firstname','lastname'=>'b_lastname','phone'=>'b_phone'),
	);
	
	fn_copy_register_data_to_user_data($user_data,$r_user_data,$params);
	
	if(!isset($cart["invoice_or_receipt"])||$cart["invoice_or_receipt"]=='R'){
		fn_profile_fields_empty_vals("set",array("B"),$cart['user_data']);
	}
	$cart['user_data']=$user_data;
}

?>