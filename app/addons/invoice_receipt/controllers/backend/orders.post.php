<?php

if (!defined('BOOTSTRAP')) { die('Access denied'); }


/*  creation: N.Sikiniotis - 08/02/2013
 *  update:
 *  last update:
 *
 *  an einai apodeijh to pernav sto session gia to katalabei h fn_get_profile_fields kai na mhn ferei pedia billing
*/



if($mode=="details"){	
	
	$_REQUEST['order_id'] = empty($_REQUEST['order_id']) ? 0 : $_REQUEST['order_id'];
	$order_info = fn_get_order_info($_REQUEST['order_id']);
	if ( !empty($order_info['invoice_or_receipt']) ){
		$cart = & $_SESSION['cart'];	
		$cart["invoice_or_receipt"] = ($order_info['invoice_or_receipt']!='I') ? 'R' : 'I';
		$cart["twigmo"] = $order_info['twigmo'];
	}
}
?>