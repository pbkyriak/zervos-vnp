<?php
//
// Author Ioannis Matziaris - imatzgr@gmail.com - April 2013
//

if (!defined('BOOTSTRAP')) { die('Access denied'); }
use Tygh\Registry;
//Xrisi sto 2o vima tou checkout sta: epilogi apodikseis i timologiou, paralavei apo to katastima, idia stoixeia apostolis me timologisi
//Eisagei sta aparetita pedia to -|- wste me tin ipovoli na min ta blepei kena to cscart
//i
//An yparxei sta aparetita pedi to -|- to aferi gia na min to dei o customer otan emfanistoun ta pedia gia enimerosi
function fn_profile_fields_empty_vals($action,$address_types,&$user_data){
	$profile_fields = fn_get_profile_fields('C');
	//unset($user_data['fields']);
	//unset($user_data['b_state_descr']);
	$leave_fields=array("b_state","s_state");
	foreach($address_types as $atindex=>$address_type){
		foreach($profile_fields[$address_type] as $field_id=>$field_data){
			if($field_data['checkout_show']=='Y'&&$field_data['checkout_required']=='Y'){
				if(empty($field_data['field_name'])){					
					if($action=='set'){
						if(empty($user_data['fields'][$field_id])){
							$user_data['fields'][$field_id]=PROFILE_FIELD_EMPTY_CHAR;
						}
					}elseif($action=='remove'){
						if($user_data['fields'][$field_id]==PROFILE_FIELD_EMPTY_CHAR){ 
							unset($user_data['fields'][$field_id]);
						}
					}
				}else{
					if(!in_array($field_data['field_name'],$leave_fields)){
						if($action=='set'){
							if(empty($user_data[$field_data['field_name']])){
								$user_data[$field_data['field_name']]=PROFILE_FIELD_EMPTY_CHAR;
							}
						}elseif($action=='remove'){
							if($user_data[$field_data['field_name']]==PROFILE_FIELD_EMPTY_CHAR){
								$user_data[$field_data['field_name']]='';
							}
						}
					}
				}
			}
		}
	}
}

//Xrisi sto 2o vima tou checkout sta: epilogi apodikseis i timologiou, paralavei apo to katastima, idia stoixeia apostolis me timologisi
//Apothikeyei tis times pou exei dosei o xristis stin cart wste me to load me to ahax na mhn xathoun oi times pou exei dosei, prin akoma apothikeysi to 2o vima
function fn_save_post_user_data_to_cart(&$cart,$user_data){
	$exclude_fields=array('profile_name');
	foreach($user_data as $field_name=>$field_value){
		if(!in_array($field_name,$exclude_fields)){
			$cart['user_data'][$field_name]=$field_value;
		}
	}
}

function fn_get_billing_and_shipping_common_fields(){
	$common_fields=array();;
	$profile_fields = fn_get_profile_fields('C');
	//anazitisi gia ola te pedia tou billing
	foreach($profile_fields['B'] as $field_id=>$field_data){
		if($field_data['checkout_show']=='Y'){//an to pedio billing emfanizetai sto checkout
			if($profile_fields['S'][$field_data['matching_id']]['checkout_show']=='Y'){//an to antistoixeo pedio sto shiping emfanizetai sto checkout
				$common_fields[]='"elm_'.$field_id.'"';
			}
		}
	}
	return(implode(",",$common_fields));
}

function fn_copy_values_from_shipping_to_billing(&$user_data,&$cart=array()){
	$profile_fields = fn_get_profile_fields('C');
	//anazitisi gia ola te pedia tou billing
	foreach($profile_fields['B'] as $field_id=>$field_data){
		if($field_data['checkout_show']=='Y'){//an to pedio billing emfanizetai sto checkout
		    if(isset($profile_fields['S'][$field_data['matching_id']])) {
    			if($profile_fields['S'][$field_data['matching_id']]['checkout_show']=='Y'){//an to antistoixeo pedio sto shiping emfanizetai sto checkout
    				if(!empty($field_data['field_name'])&&$field_data['field_name']!=PROFILE_FIELD_EMPTY_CHAR){
    					$user_data[$field_data['field_name']]=$user_data[$profile_fields['S'][$field_data['matching_id']]['field_name']];
    					if(!empty($cart)){
    						$cart['user_data'][$field_data['field_name']]=$user_data[$field_data['field_name']];
    					}
    				}else{
    					$user_data['fields'][$field_id]=$user_data['fields'][$field_data['matching_id']];
    					if(!empty($cart)){
    						$cart['user_data']['fields'][$field_id]=$user_data['fields'][$field_id];
    					}
    				}
    				
    			}
    		}
		}
	}
}

//Elegxei an to pedio tou prifle yparxei sto checkout
//dexte location {B,S} kai to field_name {name,id}
//An to pedio yparxei sto checkout tote pistrefei true
function fn_profile_field_exits_in_checkout($location,$field_name){
	$profile_fields = fn_get_profile_fields('C');

	foreach($profile_fields[$location] as $field_id=>$field_data){
		if($field_data['checkout_show']=='Y'){
			if(!empty($field_data['field_name'])){
				if($field_name==$field_data['field_name']){
					return(true);
				}
			}else{
				if($field_name==$field_id){
					return(true);
				}				
			}
		}
	}
	
	return(false);
}

//Antigrafei tis times twn pediwn apo tin egggrafi i agora os guest
//sta stoixeia user_data tis cart
function fn_copy_register_data_to_user_data(&$user_data_dest,$user_data_origin,$params){
	foreach($params as $location=>$fields){
		foreach($fields as $org_fields=>$dest_field){
			if(isset($user_data_origin[$org_fields])){
				if(fn_profile_field_exits_in_checkout($location,$dest_field)){
					if(!empty($user_data_origin[$org_fields])&&empty($user_data_dest[$dest_field])){
						$user_data_dest[$dest_field]=$user_data_origin[$org_fields];
					}
				}			
			}
		}
	}
}

//Elegxei an ta apetoumena pedia billing i shipping i kai ta dio exoun sta apetoumena pedia times
function fn_required_fields_validation($user_data,$address_types){
	$profile_fields = fn_get_profile_fields('C');
	$leave_fields=array("b_state","s_state");
	foreach($address_types as $atindex=>$address_type){
		foreach($profile_fields[$address_type] as $field_id=>$field_data){
			if($field_data['checkout_show']=='Y'&&$field_data['checkout_required']=='Y'){
				if(empty($field_data['field_name'])){					
					if(empty($user_data['fields'][$field_id])){
						return false;
					}					
				}else{
					if(!in_array($field_data['field_name'],$leave_fields)){		
						if(empty($user_data[$field_data['field_name']])){
							return false;
						}						
					}
				}
			}
		}
	}
	return true;
}
//
//Hooks
//

function fn_invoice_receipt_get_profile_fields($location, $select, &$condition)
{
	$edit_step = !empty($_REQUEST['edit_step']) ? $_REQUEST['edit_step'] : (!empty($_SESSION['edit_step']) ? $_SESSION['edit_step'] : '');
	if($edit_step=='step_two'){
		if ($location=='O' || $location=='I'){
			$cart =  $_SESSION['cart'];
			if($cart["invoice_or_receipt"]=="R"){
				$condition .=  " AND ?:profile_fields.section = 'S' ";
			}
		}
	}
}

//Kaleite me tin eggrafi tou user
//Me tin klisi se checkout sto customer area
//Antigrafei ta pedia tis formas eggrafis sta stoixeia shipping kai billing ot user
function fn_invoice_receipt_get_user_info(&$user_data){
	$controller = Registry::get('runtime.controller');
	if($controller=='checkout'&&AREA=='C'){
		//Xrisi
		//Sto checkout
		//1) kata tin eggrafi tou customer wste na ginei emfanisi sto 2o vima twn stoixeiwn pou exei dosei sto 1o
		if(!empty($user_data)){
		
			$params=array(
				'S'=>array('firstname'=>'s_firstname','lastname'=>'s_lastname','phone'=>'s_phone'),
				'B'=>array('firstname'=>'b_firstname','lastname'=>'b_lastname','phone'=>'b_phone'),
			);
			
			fn_copy_register_data_to_user_data($user_data,$user_data,$params);
			
			$cart = & $_SESSION['cart'];
			if(!isset($cart["invoice_or_receipt"])||$cart["invoice_or_receipt"]=='R'){
				fn_profile_fields_empty_vals("set",array("B"),$cart['user_data']);
			}
		}
	}
}

//Kaleite sto controllers/customer/checkout.php
//Se mia functions eswteriki tou arxeio: fn_checkout_summary
//
//Den kanoume allagi ston kwdika tis function
//Einai sto mode=checkout akrivos katw apo tin evresi tis timis tou ship_to_another
//I default ipologismos tis timis tou ship_to_another den kanei gia to site
//Edw ginete i evresi tis timis tou ship_to_another me vasi tin allagi tou site gia xrisi addon timoligio i apodeiksei
function fn_invoice_receipt_checkout_summary(&$cart){
	if(isset($cart['i_ship_to_another'])){
		$cart['ship_to_another']=$cart['i_ship_to_another'];
		//unset($cart['i_ship_to_another']);		
	}
	/*else{
		//TODO: n ginete sigkrisi se pedia pou yparxoun sto checkout kai sto shipping all kai sto billing
		if($cart['user_data']['b_address']==$cart['user_data']['s_address'] 
			&& $cart['user_data']['b_city']==$cart['user_data']['s_city'] 
			&& $cart['user_data']['b_state']==$cart['user_data']['s_state'] 
			&& $cart['user_data']['b_country']==$cart['user_data']['s_country'] 
			&& $cart['user_data']['b_zipcode']==$cart['user_data']['s_zipcode'] 
			&& $cart['user_data']['b_phone']==$cart['user_data']['s_phone']){
				$cart['ship_to_another']=false;
		}elseif((empty($cart['user_data']['b_address']) || $cart['user_data']['b_address']==PROFILE_FIELD_EMPTY_CHAR)
			&& (empty($cart['user_data']['b_city'])  || $cart['user_data']['b_city']==PROFILE_FIELD_EMPTY_CHAR)
			&& $cart['user_data']['b_state']==$cart['user_data']['s_state'] 
			&& $cart['user_data']['b_country']==$cart['user_data']['s_country'] 
			&& (empty($cart['user_data']['b_zipcode']) || $cart['user_data']['b_zipcode']==PROFILE_FIELD_EMPTY_CHAR)
			&& (empty($cart['user_data']['b_phone']) || $cart['user_data']['b_phone']==PROFILE_FIELD_EMPTY_CHAR)){
				$cart['ship_to_another']=false;
		}else{			
			$cart['ship_to_another']=true;
		}
	}*/
}

//Prostheti sta stoixeia tropon pliromis kai to pedio receipt_from_store
function fn_invoice_receipt_get_payment_methods(&$payment_methods){
	foreach($payment_methods as $payment_id=>$payment)
		$payment_methods[$payment_id]['receipt_from_store']=db_get_field('SELECT receipt_from_store FROM ?:payments WHERE payment_id=?i',$payment_id);
}

function fn_invoice_receipt_prepare_checkout_payment_methods($cart, $auth, &$payment_groups){

	if(AREA=='C' /*&& CONTROLLER=='checkout'*/){
		if(isset($cart['receipt_from_store'])&&$cart['receipt_from_store']=='Y'){	
		 
			foreach($payment_groups as $tab=>$payment_tab_methods){
				foreach($payment_tab_methods as $payment_id=>$payment){
					if($payment['receipt_from_store']=='N'){					
						unset($payment_groups[$tab][$payment_id]);
						if($cart['payment_id']==$payment_id){
							$cart['payment_id']=0;
						}
					}
				}
			}
			foreach($payment_groups as $tab=>$payment_tab_methods){
				if(empty($payment_tab_methods)){
					unset($payment_groups[$tab]);
				}
			}
		}
	}
	
}

function fn_invoice_receipt_calculate_cart_taxes_pre(&$cart, $cart_products, &$product_groups, $calculate_taxes, $auth){
	
	if(isset($cart['receipt_from_store']) && $cart['receipt_from_store']=='Y'){
		
		$suppliers=Registry::get('view')->getTemplateVars('suppliers',array());		
		if(!empty($suppliers)){
			foreach($suppliers as $sindex=>$supplier){
				foreach($supplier['rates'] as $shipping_id=>$rate){
					$receipt_from_store=db_get_field('SELECT receipt_from_store FROM ?:shippings WHERE shipping_id=?i',$shipping_id);
					if($receipt_from_store=='N'){
						unset($suppliers[$sindex]['rates'][$shipping_id]);
					}
				}
			}
			Registry::get('view')->assign('suppliers', $suppliers);
		}	
		$available_shippings=array();
		if(!empty($product_groups)){
			foreach($product_groups as $no=>$pg ){
				if(!empty ($pg['shippings'])){
					foreach($pg['shippings'] as $shipping_id=>$shipping){
						$receipt_from_store=db_get_field('SELECT receipt_from_store FROM ?:shippings WHERE shipping_id=?i',$shipping_id);
						if($receipt_from_store=='N')	unset($product_groups[$no]['shippings'][$shipping_id]);
						else $available_shippings[]=$shipping_id;
					}	
				}
			}
		}	
		
		if (!empty($cart['shipping'])) 
			foreach( $cart['shipping'] as $id=>$s)
				if (empty($available_shippings) || !in_array($id, $available_shippings)) 
					unset($cart['shipping'][$id]);
		//Iparxei bug
		//An sto vima twn address epileksoume paralavi apo to katastima kai meta allaksoume to eidos tou parastatikou
		//Emfanizei minima oti den iparxoun diathesimi tropoi apostolis gia ta stoixeia dieythinsis
		//Prepei na ofilete sto submit tis formas twn addresses me tin epilogi twn pralavi apo katastim i apodiksei lianikis i timologio
		if (empty($cart['chosen_shipping'])||!empty($product_groups)){
			foreach ($product_groups as $key_group => $group) {
				$cart['chosen_shipping'][$key_group] = key($group['shippings']);
			}
		}
		if (!empty($cart['chosen_shipping'])) 
			foreach( $cart['chosen_shipping'] as $id=>$s)
				if (empty($available_shippings) || !in_array($s, $available_shippings))
					unset($cart['chosen_shipping'][$id]);
	}
}

//
// Cusotm hooks
//

//Twigmo

//Kaleite meta tin apostoli tou order sto twigmo
//Xrisi gia markarisma tis paragkelias oti egine mesa apo twigmo
function fn_invoice_receipt_place_order_tiwgmo($order_id, $action, $order_status, $cart){
	if(!empty($order_id)){
		db_query("UPDATE ?:orders SET twigmo=?s WHERE order_id=?i","Y",$order_id);
		//Exei mpei gia logous parakolouthiseis
		$fp = fopen('./var/twigmo_order.txt', 'a');
		fwrite($fp, date("H:i:s m/d/Y",time())." - Order id: $order_id\n");
		fclose($fp);
	}
}