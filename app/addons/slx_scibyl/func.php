<?php
use Tygh\Registry;

function fn_slx_nosto_get_product_categories_names($categoryId) {
	$names = array();
	$parent_ids = fn_explode('/', db_get_field("SELECT id_path FROM ?:categories WHERE category_id = ?i", $categoryId));
	$cats = fn_get_category_name($parent_ids);
	foreach($cats as $idx => $cat)  {
		$cats[$idx] = str_replace('/', '-', $cat);
	}
	return implode('/', $cats);
}


function fn_slx_nosto_before_dispatch() {
	if($_SESSION['nosto']['notify_nosto']>0) {
		Registry::get('view')->assign('nosto_customer', true);
		$_SESSION['nosto']['notify_nosto']++;
		if($_SESSION['nosto']['notify_nosto']>5) {
			$_SESSION['nosto']['notify_nosto'] = 0;
		}
	}
	else {
		Registry::get('view')->assign('nosto_customer', false);
	}
}

function fn_slx_nosto_update_profile($action, $user_data, $current_user_data) {
	if($action=='add') {
		$_SESSION['nosto']['notify_nosto'] = 1;
	}
}