<?php

/**
 * for 4 series
 */
function fn_slx_paymentcond_prepare_checkout_payment_methods($cart, $auth, &$payment_groups) {
    if (!empty($payment_groups)) {
        $total = !empty($cart['total']) ? $cart['total'] :0;
		//printf("Cart total=%s<br />", $total);
        foreach ($payment_groups as $tab_id => $payments) {
		    //var_dump($payments);
            foreach ($payments as $paymentId => $payment_data) {
                $good = false;
                if($payment_data['active_from']==0 && $payment_data['active_to']==0) {                    
                    $good= true;
                }
                elseif($payment_data['active_from']!=0 && $payment_data['active_to']==0) {
                    if( $payment_data['active_from']<=$total) {
                        $good = true;
                    }
                }
                elseif($payment_data['active_from']==0 && $payment_data['active_to']!=0) {
                    if( $total<=$payment_data['active_to']) {
                        $good = true;
                    }
                }
                elseif($payment_data['active_from']!=0 && $payment_data['active_to']!=0) {
                    if( $payment_data['active_from']<=$total && $total<=$payment_data['active_to']) {
                        $good = true;
                    }
                }
                //printf("%s<%s<%s good=%s<br />",$payment_data['active_from'],$total,$payment_data['active_to'],$good);
                if (!$good) {
                    unset($payment_groups[$tab_id][$paymentId]);
                    if (empty($payment_groups[$tab_id])) {
                        unset($payment_groups[$tab_id]);
                    }
                }
            }
        }
        ksort($payment_groups);
    }
    return true;
}

/**
 * for 4 series
 */
function fn_slx_paymentcond_checkout_select_default_payment_method(&$cart, &$payment_methods, &$completed_steps) {
    $pids = array();
    foreach ($payment_methods as $tab_id => $payments) {
        foreach ($payments as $paymentId => $payment_data) {
            $pids[] = $paymentId;
        }
    }
    if ($cart['payment_id'] == 0 || !in_array($cart['payment_id'], $pids)) {
        if ($pids) {
            $cart['payment_id'] = $pids[0];
        }
    }
    return true;
}

/**
 *	added in order to fetch fields active_from and active_to
 *  required for v. 4.2.4
 */ 
function fn_slx_paymentcond_get_payment_methods(&$payment_methods, $auth, $lang_code = CART_LANGUAGE) {
    $condition = '';
    if (AREA == 'C') {
        $condition .= " AND (" . fn_find_array_in_set($auth['usergroup_ids'], '?:payments.usergroup_ids', true) . ")";
        $condition .= " AND ?:payments.status = 'A' ";
        $condition .= fn_get_localizations_condition('?:payments.localization');
    }

	$q = "
		SELECT 
		  ?:payments.payment_id,
		  ?:payments.template,
		  ?:payments.a_surcharge,
		  ?:payments.p_surcharge,
		  ?:payments.payment_category,
		  ?:payment_descriptions.*,
		  ?:payment_processors.processor,
		  ?:payment_processors.type AS processor_type,
		  ?:payments.active_from, ?:payments.active_to,
		  ?:payments.apply_time_cond, ?:payments.time_cond
		FROM
		  ?:payments 
		  LEFT JOIN ?:payment_descriptions 
			ON ?:payments.payment_id = ?:payment_descriptions.payment_id 
			AND ?:payment_descriptions.lang_code = ?s 
		  LEFT JOIN ?:payment_processors 
			ON ?:payment_processors.processor_id = ?:payments.processor_id 
		WHERE 1 $condition 
		ORDER BY ?:payments.position 
	";
    $payment_methods = db_get_hash_array($q, 'payment_id', $lang_code);
	if( function_exists('fn_slx_payment_time_cond_get_payments_post') ) {
		fn_slx_payment_time_cond_get_payments_post($params, $payment_methods);
	}

}
