<?php


function fn_slx_homepage_products_get_products_before_select( $params, $join, &$condition, $u_condition, $inventory_condition, $sortings, $total, $items_per_page, $lang_code, $having) {
	if( isset($params['shop_availability']) ) {
		$condition .= db_quote(" AND products.shop_feed_avail_opt=1 ");
	}
	
}

function fn_slx_homepage_products_get_products_pre(&$params, $items_per_page, $lang_code){
	if(isset($params['cid'])) {
		if( $params['cid']==3305 ) {
			$params['shop_availability']=1;
			$params['sort_by'] = 'price';
			$params['sort_order'] = 'desc';
			unset($params['cid']);
		}
	}
}