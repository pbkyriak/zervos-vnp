/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
(function (_, $) {
function ab__mcd_close_others (i){
var tab = i.attr('tab_content');
$('.ab__mcd_descs-section-title').each(function(){
var current_tab = $(this).attr('tab_content');
if (current_tab != tab) {
$(this).removeClass('active');
$('.ab__mcd_descs-section-content.' + current_tab).slideUp(200).removeClass('open');
}
});
}
function ab__mcd_open_section (i){
i.addClass('active');
$('.ab__mcd_descs-section-content.' + i.attr('tab_content')).slideDown(200).addClass('open');
}
$(_.doc).on('click', '.ab__mcd_descs-section-title', function (e) {
ab__mcd_open_section($(this));
ab__mcd_close_others($(this));
});
}(Tygh, Tygh.$));
