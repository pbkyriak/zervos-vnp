/*******************************************************************************************
*   ___  _          ______                     _ _                _                        *
*  / _ \| |         | ___ \                   | (_)              | |              © 2017   *
* / /_\ | | _____  _| |_/ /_ __ __ _ _ __   __| |_ _ __   __ _   | |_ ___  __ _ _ __ ___   *
* |  _  | |/ _ \ \/ / ___ \ '__/ _` | '_ \ / _` | | '_ \ / _` |  | __/ _ \/ _` | '_ ` _ \  *
* | | | | |  __/>  <| |_/ / | | (_| | | | | (_| | | | | | (_| |  | ||  __/ (_| | | | | | | *
* \_| |_/_|\___/_/\_\____/|_|  \__,_|_| |_|\__,_|_|_| |_|\__, |  \___\___|\__,_|_| |_| |_| *
*                                                         __/ |                            *
*                                                        |___/                             *
* ---------------------------------------------------------------------------------------- *
* This is commercial software, only users who have purchased a valid license and  accept   *
* to the terms of the License Agreement can install and use this program.                  *
* ---------------------------------------------------------------------------------------- *
* website: https://cs-cart.alexbranding.com                                                *
*   email: info@alexbranding.com                                                           *
*******************************************************************************************/
(function(_, $) {
function fn_abt__yt_next_banner (id, banner_list, lS_key){
var new_banner= 0;
var old_banner = 0;
if(localStorage.getItem(lS_key + id) !== null){
old_banner = localStorage.getItem(lS_key + id);
}
var banners = banner_list.split(',');
if (banners.length == 1){
new_banner = banners[0];
}else if (banners.length > 1){
var curr_pos = banners.indexOf(old_banner);
if (curr_pos == -1) {
new_banner = banners[0];
} else {
banners[banners.length] = banners[0];
new_banner = banners[curr_pos + 1];
}
}
localStorage.setItem(lS_key + id, new_banner);
return new_banner;
}
function fn_abt__yt_init_countdown(e){
e.find('span[data-countdown]').each(function() {
var cd = $(this);
cd.countdown(cd.data('countdown'), function(event) {
cd.html(event.strftime(cd.data('str')));
});
});
}
$.ceEvent('on', 'ce.commoninit', function(context) {
var banners = context.find("div[class*=bnw-x]:not('.loaded')");
if (banners.length){
var lS_key = "abyt_"; // localStorage key
var bs = [];
banners.each(function(){
var t = $(this).children('div');
var obj = t.data('o');
var obj_id = t.data('i');
var obj_banners = t.data('b');
var id = t.attr('id');
bs.push({
id : id,
banner : fn_abt__yt_next_banner(id, obj_banners.toString(), lS_key),
obj_id : obj_id.toString(),
obj : obj,
})
});
if (typeof bs !== 'undefined' && bs !== null) {
$.ceAjax('request', fn_url('abt__yt.get_banner'), {
method: 'post',
data : {banners : bs,},
caching: false,
hidden: true,
callback: function(d) {
if (typeof d.banners_data !== 'undefined' && d.banners_data !== null) {
$.each(d.banners_data, function(key, content){
var b = $('#' + key);
b.html(content).removeClass('pre-load');
fn_abt__yt_init_countdown(b);
});
}
}
});
}
}
});
$(document).on('click', 'div[class*=abyt-in-popup-]', function() {
var bp_container = $(this).find("div.abt-yt-bp-container:not('.loaded')");
if (bp_container.length){
var link = bp_container.data('link');
$.ceAjax('request', fn_url('abt__yt.show_banner_in_popup'), {
method: 'get',
data : {link_id : link,},
hidden: true,
callback: function(response) {
bp_container
.addClass('loaded')
.html(response.data)
.popup({
scrolllock: true,
openelement : '.abyt-in-popup-' + link,
closeelement : '.abyt-closer-' + link,
transition: 'all 0.1s',
color: 'black',
escape: false,
blur: false,
outline: true,
opacity: 0.5,
onclose : function (){
var player = $('#youtube-player-' + link);
if (player.length){
player[0].contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
}
},
})
.popup('show');
$.commonInit(bp_container);
fn_abt__yt_init_countdown(bp_container);
},
});
}
});
$.ceEvent('on', 'dispatch_event_pre', function(e, jelm, processed) {
var link = jelm.is('a') ? jelm : jelm.parent();
if (link.hasClass('cm-ajax') && link.data('caEvent') == 'ce.abt__yt_more_products_callback') {
link.parent().addClass('load');
$("#tygh_container").addClass("loading-more-products");
}
});
$.ceEvent('on', 'ce.abt__yt_more_products_callback', function(response, params) {
$('.' + params.data.result_ids.split(',')[2]).addClass('hidden').css({'display': 'none'});
$('.more-products-link').removeClass('load');
$("#tygh_container").removeClass("loading-more-products");
});
function fn_abt__yt_load_video (elm){
var id = elm.data('banner-youtube-id'), params = elm.data('banner-youtube-params');
elm.addClass('loaded').empty().append($('<iframe>', {
src: "https://www.youtube.com/embed/"+ id +"?" + params,
frameborder: 0,
allowfullscreen: 'true',
allowscriptaccess: 'always',
})
);
}
$(_.doc).on('click', 'a[data-content="video"]:not(.loaded),div.abyt-a-video .box:not(.loaded),img[data-type="youtube-img"]', function(e) {
if ($(e.target).attr('data-banner-youtube-id') || $(e.target).attr('data-type')){
var elm = $(e.target);
if ($(e.target).attr('data-type')) elm = elm.parent();
$(this).addClass('loaded');
fn_abt__yt_load_video(elm);
return false;
}else{
return true;
}
});
$(document).on('click', 'button[type=submit][name^="dispatch[checkout.add"]', function() {
$.ceEvent('on', 'ce.notificationshow', function(notification) {
if (document.documentElement.clientWidth <= 767){
if (notification.find('.ty-ab__ia_joins').length){
notification.addClass('with-ab-ia-joins');
$("html, body").animate({scrollTop: 0}, 400);
}
}
});
});
$.ceEvent('on', 'ce.commoninit', function(context) {
var filter = $('.cm-horizontal-filters'),
container = $('.ypi-filters-container');
if (container.length) {
if (filter.length && !$.contains(container, filter)) {
filter.appendTo(container);
filter.css('visibility', 'visible');
} else if (!filter.length) {
container.addClass('ypi-nofilters');
}
}
});
$(document).ready(function () {
var top_panel = $("#tygh_main_container > .tygh-top-panel"),
header = $('#tygh_main_container > .tygh-header'),
content = $('#tygh_main_container > .tygh-content'),
b = $('body'),
top_panel_height = top_panel.height(),
header_height = header.height(),
height = top_panel_height + header_height,
fixed = 'fixed-top';
$(window).on("resize scroll",function(e){
if (document.documentElement.clientWidth > 767){
var scroll = $(window).scrollTop();
if (scroll >= height && !b.hasClass(fixed)){
content.css('padding-top', top_panel_height + 'px');
b.addClass(fixed);
window.setTimeout(function(){b.addClass('show');}, 50);
}else if (scroll < height && b.hasClass(fixed)){
content.css('padding-top', '');
b.removeClass(fixed + " show");
}
}
});
});
$(document).ready(function () {
if (document.documentElement.clientWidth >= 1124) {
var tabs = $(".ypi-dt-product-tabs"),
p_detail = $(".ty-product-detail"),
b = $('body'),
delta = 50,
sticky = $(".sticky-block"),
sticky_height = 0,
sticky_top = 0,
p_sticky = sticky.parent(),
product_panel_max_height = 300,
fixed = 'fixed-product-form'
;
$(window).on("resize scroll",function(e){
var scroll = $(window).scrollTop();
if (tabs.length){
var product_block = $('.ty-product-block__wrapper > .ypi-product-block__left');
var tabs_height = tabs.height();
if (tabs_height >= product_panel_max_height && product_block.length){
if (
(!b.hasClass(fixed) && scroll + delta > tabs.offset().top && scroll + (sticky_height + sticky_top) < tabs.offset().top + tabs_height)
){
p_sticky.css("height", p_sticky.height());
b.addClass(fixed).addClass('hide-cloudzoom');
p_detail.addClass("fixed-form").addClass('view');
sticky.css("right", product_block.offset().left).css("opacity", 0).stop().fadeTo(300, 1);
sticky_height = sticky.outerHeight();
sticky_top = sticky.position().top;
}else if (
(b.hasClass(fixed) && scroll + delta < tabs.offset().top)
||
(b.hasClass(fixed) && scroll + (sticky_height + sticky_top) > tabs.offset().top + tabs_height)
){
p_sticky.css("height", "");
b.removeClass(fixed).removeClass('hide-cloudzoom');
p_detail.removeClass("fixed-form").removeClass("view");
}
}
}
});
}
});
$(_.doc).on('click', '.ab__show_qty_discounts', function() {
var product_id = $(this).data('caProductId');
if (product_id !== undefined) {
var container = $('#qty_discounts_' + product_id);
if (container.is(':empty')) {
$.ceAjax('request', fn_url('abt__yt.qty_discounts?product_id=' + product_id), {
method: 'get',
result_ids: 'qty_discounts_' + product_id
});
}
container.toggleClass('hidden');
}
});
(function($){
var container,
menu,
wrapper,
show_more_btn,
items,
mobile_menu,
items_widths,
total_width,
total_sections = 1,
current_menu_section = 1;
function show_menu_item(elem) {
elem.removeClass('ab__menu_hidden');
}
function hide_menu_item(elem) {
elem.addClass('ab__menu_hidden');
}
function toggle_menu_items() {
show_more_btn.html(current_menu_section + '/' + total_sections);
var arr,
widths = $.extend(true, [], items_widths),
section;
if (current_menu_section == total_sections) {
arr = $(items.get().reverse());
section = 1;
widths.reverse();
show_more_btn.addClass('last-section');
} else {
arr = items;
section = current_menu_section;
show_more_btn.removeClass('last-section');
}
var sum = 0;
arr.each(function(i) {
sum += widths[i];
if (section > 1 && sum > total_width) {
sum = widths[i];
section -= 1;
show_menu_item($(this));
} else if (section > 1 || sum > total_width) {
hide_menu_item($(this));
} else {
show_menu_item($(this));
}
});
}
function ab__menu_init() {
container = $('.abt_up-menu.extended');
menu = $('.ty-menu__items', container);
wrapper = menu.parent();
show_more_btn = $('.abt_yp_menu-show_more', container);
items = menu.children().filter(':not(.visible-phone)');
mobile_menu = menu.children().filter('.visible-phone');
if (mobile_menu.css('display') == 'none') {
container.addClass('extended');
var sum = 0;
items_widths = [];
total_width = container.innerWidth() - show_more_btn.outerWidth() - 5;
menu.css('visibility', 'hidden');
show_menu_item(items);
items.css('width', 'auto');
items.each(function(i) {
items_widths[i] = $(this).outerWidth();
$(this).css('width', $(this).innerWidth());
sum += items_widths[i];
});
menu.css('width', sum);
total_sections = Math.ceil(sum/total_width);
if (total_sections > 1) {
wrapper.css('width', total_width);
show_more_btn.css('display', 'inline-block');
} else {
show_more_btn.hide();
}
current_menu_section = Math.min(current_menu_section, total_sections);
toggle_menu_items();
menu.css('visibility', 'visible');
show_more_btn.off('click');
show_more_btn.click(function(){
if (current_menu_section == total_sections) {
current_menu_section = 1;
} else {
current_menu_section += 1;
}
toggle_menu_items();
});
} else {
show_menu_item(items);
items.css('width', 'auto');
menu.css('width', 'auto');
wrapper.css('width', 'auto');
container.removeClass('extended');
}
}
$(document).ready(function(){
setTimeout(ab__menu_init, 500);
$(window).resize(function(e){
ab__menu_init();
});
if (window.addEventListener) {
window.addEventListener('orientationchange', function() {
ab__menu_init();
}, false);
}
$.ceEvent('on', 'ce.commoninit', function(context) {
if ($('.abt_up-menu.extended', context).length) {
ab__menu_init();
}
});
});
})($);
}(Tygh, Tygh.$));
