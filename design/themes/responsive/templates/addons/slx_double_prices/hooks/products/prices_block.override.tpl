{* andreas *}
{assign var="show_phone_price" value=true}
{* /andreas *}
{if $product.price|floatval || $product.zero_price_action == "P" || ($hide_add_to_cart_button == "Y" && $product.zero_price_action == "A")}
    {if $show_phone_price}
        <span class="price-phone-cntr">
            <label class="ty-control-group__label21">{__("web_price")}:</label>
            <span class="price{if !$product.price|floatval && !$product.zero_price_action} hidden{/if}" id="line_discounted_phone_price_{$obj_prefix}{$obj_id}">{if $details_page}{/if}{include 
			file="common/price.tpl" value=$product.price span_id="discounted_price_`$obj_prefix``$obj_id`" class="price-num" live_editor_name="product:price:{$product.product_id}" live_editor_phrase=$product.base_price}</span>
        </span>
		<span class="clearfix"></span>		
        <span class="price-normal-cntr">
            <label class="ty-control-group__label2">{__("store_price")}:</label>
            <span class="price{if !$product.price|floatval && !$product.zero_price_action} hidden{/if}" id="line_discounted_price_{$obj_prefix}{$obj_id}">{if $details_page}{/if}{include file="common/price.tpl" 
			value=$product.price|fn_slx_double_prices_multiplier:$product.product_id span_id="discounted_price_`$obj_prefix``$obj_id`" class="price-num1" live_editor_name="product:price:{$product.product_id}" live_editor_phrase=$product.base_price}</span>
			<p>{__("store_price_notes")}</p>
        </span> 
		
		{if fn_slx_douple_prices_is_product_category_invoiced($product.product_id)}
			<span class="price-normal-cntr">
				<label class="ty-control-group__label2">{__("timologio_price")}:</label>
				<span class="price{if !$product.price|floatval && !$product.zero_price_action} hidden{/if}" id="line_discounted_price_{$obj_prefix}{$obj_id}">{if $details_page}{/if}{include file="common/price.tpl" 
				value=$product.price|fn_slx_douple_prices_get_invoiced_price:$product.product_id span_id="discounted_price_`$obj_prefix``$obj_id`" class="price-num1" live_editor_name="product:price:{$product.product_id}" live_editor_phrase=$product.base_price}</span>
				<p>{__("timologio_price_notes")}</p>
			</span> 
		{/if}
				
        <span class="clr"></span>
	{else}
	    <span class="price-phone-cntr">
            <label class="ty-control-group__label2">{__("price")}:</label>
           <span class="price{if !$product.price|floatval && !$product.zero_price_action} hidden{/if}" id="line_discounted_price_{$obj_prefix}{$obj_id}">{if $details_page}{/if}{include file="common/price.tpl" value=$product.price span_id="discounted_price_`$obj_prefix``$obj_id`" class="price-num" live_editor_name="product:price:{$product.product_id}" live_editor_phrase=$product.base_price}</span>	
		</span>
    {/if}
{elseif $product.zero_price_action == "A"}
    {assign var="base_currency" value=$currencies[$smarty.const.CART_PRIMARY_CURRENCY]}
    <span class="price-curency"><span>{__("enter_your_price")}: {if $base_currency.after != "Y"}{$base_currency.symbol}{/if}</span><input class="input-text-short" type="text" size="3" name="product_data[{$obj_id}][price]" value="" />{if $base_currency.after == "Y"}{$base_currency.symbol}{/if}</span>
{elseif $product.zero_price_action == "R"}
    <span class="no-price">{__("contact_us_for_price")}</span>
    {assign var="show_qty" value=false}
{/if}
