{if $addons.hw_subscribe_popup.testmode==Y || $smarty.cookies.hw_subscribe_popup!=1}

<script language="javascript" type="text/javascript">
var _hwspopup = false;
var _hwspopupexpire = {$addons.hw_subscribe_popup.expire};
{literal}
$(function() {
    $('#hw_subscribe_popup').hide(); 
	setTimeout(
	  function() 
	  {
		$('#hw_subscribe_popup').fadeIn( "slow");
	  }, 5000);
	$(window ).scroll(function() {
		if ($(window).scrollTop() > 100 && !_hwspopup){
			$('#hw_subscribe_popup').animate({ bottom: '40px' }, 500, function() {
				_hwspopup = true;
			});
		}
	});
	$('.hw_subscribe_popup_close').click(function(){
		$('#hw_subscribe_popup').fadeOut( "slow");

		//set cookie
		var _hwspopupdate = new Date();
		_hwspopupdate.setDate(_hwspopupdate.getDate() + _hwspopupexpire);
		$.cookie.set('hw_subscribe_popup', 1, _hwspopupdate, '/');
	});
	hw_subscribe_popup_resize();
	$( window ).resize(function() { hw_subscribe_popup_resize(); });
});

function hw_subscribe_popup_resize(){
	if($( window ).width() <= 768 ){
		$('#hw_subscribe_popup').css({ 'right':'0px', 'bottom': '-1px', 'width': '100%' });
		$('#hw_subscribe_popup .hw_subscribe_popup_inner').css({'padding':'30px 20px', 'width':'auto'});
	}else{
		$('#hw_subscribe_popup').css({'right':'40px', 'bottom':'29px', 'width': 'auto' });
		$('#hw_subscribe_popup .hw_subscribe_popup_inner').css({'padding':'30px 50px', 'width':'300px'});
	}
}
</script>
{/literal}
<div id="hw_subscribe_popup">
	<div class="hw_subscribe_popup_inner{if $addons.hw_subscribe_popup.powered!='Y'} hw_subscribe_popup_powered{/if}">
	    <span class="hw_subscribe_popup_close"></span>
	    <form action="{""|fn_url}" method="post" name="subscribe_form">
	        <input type="hidden" name="redirect_url" value="{$config.current_url}" />
	        <input type="hidden" name="newsletter_format" value="2" />
	        <h3>{__("stay_connected")}</h3>
	        <p>{__('stay_connected_notice')}</p>
	        <div class="ty-footer-form-block__form ty-control-group ty-input-append">
	            <label class="cm-required cm-email hidden" for="subscr_email_x2">{__("email")}</label>
	            <input type="text" name="subscribe_email" id="subscr_email_x2" size="20" value="{if $user_info.email}{$user_info.email}{else}{__("enter_email")}{/if}" class="{if !$user_info.email}cm-hint {/if}ty-input-text" />
	            {include file="buttons/go.tpl" but_name="em_subscribers.update" alt=__("go")}
	        </div>
	        {if $addons.hw_subscribe_popup.powered=='Y'}<p class="hw-powered">Powered by <a href="https://www.hungryweb.net/" target="_blank">Hungryweb</a>.</p>{/if}	        
	    </form>
	</div>
</div>
{/if}