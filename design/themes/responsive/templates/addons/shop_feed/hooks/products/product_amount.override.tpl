{* [ShopFeed] *}
{assign var="pavail_opt" value=$product.product_id|shop_feed_get_product_avail_opt}
{assign var="avail_opt" value="shopFeed_avail_opt`$pavail_opt`"}
{assign var="avail_col" value=$pavail_opt|fn_shop_feed_get_avail_color}
<div class="ty-control-group product-list-field">
    <label class="ty-control-group__label">{__("availability")}:</label>
	<span class="ty-control-group__item" id="qty_in_stock_{$obj_prefix}{$obj_id}" style="color:#{$avail_col}">{__($avail_opt)|replace:"Διαθέσιμο":$stock_word}</span>
</div>
{* [/ShopFeed] *}