{if $addons.sd_facebook_pixel.add_to_wish_list_facebook_pixel == 'Y'}
    <script type="text/javascript">
        {if $smarty.const.AJAX_REQUEST}
            /*ajax_rnd_{0|mt_rand:10000000}*/
        {/if}
        (function(_, $) {
            $('#button_wishlist_ajax{$product.product_id}, #button_wishlist_{$product.product_id}').click(function() {
                var milliseconds = new Date().getTime();
                window._fbq.push(['track', 'AddToWishlist', {
                    timestamp: milliseconds
                }]);
            });
        }(Tygh, Tygh.$));
    </script>
{/if}
