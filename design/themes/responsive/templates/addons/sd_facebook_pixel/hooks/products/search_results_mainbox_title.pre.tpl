{if $addons.sd_facebook_pixel.search_facebook_pixel == 'Y'}
    <script type="text/javascript">
        {if $smarty.const.AJAX_REQUEST}
            /*ajax_rnd_{0|mt_rand:10000000}*/
        {/if}
        (function(_, $) {
            fbq('track', 'Search');
        }(Tygh, Tygh.$));
    </script>
{/if}
