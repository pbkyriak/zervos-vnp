{if $smarty.request.pixel_id}
    {assign var="pixel_id" value=$smarty.request.pixel_id}
{else}
    {assign var="pixel_id" value=$addons.sd_facebook_pixel.identifier_facebook_pixel}
{/if}

{if !$smarty.capture.sd_facebook_pixel_head_hook}
    {capture name="sd_facebook_pixel_head_hook"}Y{/capture}
    <script type="text/javascript">
        (function(_, $) {
            {literal}
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
            n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
            document,'script','https://connect.facebook.net/en_US/fbevents.js');
            {/literal}
            fbq('init', '{$pixel_id}');
            fbq('track', 'PageView');
        }(Tygh, Tygh.$));
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id={$pixel_id}&ev=PageView&noscript=1"
    /></noscript>
{/if}