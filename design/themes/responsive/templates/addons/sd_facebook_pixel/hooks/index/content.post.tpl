{if $addons.sd_facebook_pixel.complete_registration_facebook_pixel == 'Y'}
    {script src="js/addons/sd_facebook_pixel/func.js"}
{/if}

{if $addons.sd_facebook_pixel.initiate_checkout_facebook_pixel == 'Y' && $cart_products && $runtime.controller == 'checkout' && $runtime.mode == 'checkout'}

    <script type="text/javascript">
        var pixel_id = '{$smarty.request.pixel_id}',
            el,
            Initcount = 0,
            user_id = '{$auth.user_id}',
            user_data = '{$user_data.firstname}';

        var Initiate = function() {
            if (!pixel_id) {
                {foreach from=$identifiers_facebook_pixel item=item}
                    {if $item.identifier_facebook_pixel && $item.company_id}
                        el = document.createElement("iframe");
                        document.body.appendChild(el);
                        el.id = 'iframe';
                        el.style.width = "1px";
                        el.style.height = "1px";
                        el.style.position = 'absolute';
                        el.style.left = '-9999px';
                        el.src = fn_url('checkout.checkout?pixel_id={$item.identifier_facebook_pixel}&companyid={$item.company_id}');
                    {/if}
                {/foreach}
            }
            el = $('#checkout_steps').get(0);
            if (typeof el != 'undefined') {
                {foreach from=$cart_products item="product_data"}
                    {if !$smarty.request.pixel_id || $product_data.company_id == $smarty.request.companyid}
                        fbq('track', 'InitiateCheckout', {
                            value: {math equation="{$product_data.price} / {$currencies[$secondary_currency].coefficient}" format="%.{$currencies[$secondary_currency].decimals}f"},
                            currency: '{$secondary_currency}',
                            content_name: '{$product_data.product|escape:javascript nofilter}',
                            content_category: '{$product_data.main_category}'
                        });
                    {/if}
                {/foreach}
            }
        }
        {if $addons.sd_facebook_pixel.track_initiate_checkout == "only_after_auth"}
            $.ceEvent('on', 'ce.formajaxpost_step_two_billing_address', function(response_data,params, response_text) {
                if (response_data !== undefined  && Initcount == 0) {
                    Initiate();
                    Initcount++;
                }
            });

            $.ceEvent('on', 'ce.formajaxpost_step_one_register_form', function(response_data) {
                if (response_data !== undefined && Initcount == 0) {
                    var register_at_checkout = response_data["notifications"];
                    for(key in register_at_checkout) {
                        if (register_at_checkout[key]["type"] == "N") {
                            Initiate();
                            Initcount++;
                        }
                    }
                }
            });

            if (Initcount == 0 && (user_id != 0 || user_data)) {
                Initiate();
                Initcount++;
            }
        {elseif $addons.sd_facebook_pixel.track_initiate_checkout == "all_stages"}
            Initiate();
        {/if}
    </script>
{/if}