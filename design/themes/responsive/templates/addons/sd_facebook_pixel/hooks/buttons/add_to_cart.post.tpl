{if $addons.sd_facebook_pixel.add_to_cart_facebook_pixel == 'Y'}
    <script type="text/javascript">
        {if $smarty.const.AJAX_REQUEST}
            /*ajax_rnd_{0|mt_rand:10000000}*/
        {/if}
        (function(_, $) {
            $('#button_cart_ajax{$product.product_id}, #button_cart_{$product.product_id}').click(function() {
                var milliseconds = new Date().getTime();
                window._fbq.push(['track', 'AddToCart', {
                    {if $addons.sd_facebook_pixel.account_facebook_pixel == 'business'}
                    content_type: 'product',
                    content_ids: ['{$product.product_id}'],
                    {/if}
                    timestamp: milliseconds
                }]);
            });
        }(Tygh, Tygh.$));
    </script>
{/if}
