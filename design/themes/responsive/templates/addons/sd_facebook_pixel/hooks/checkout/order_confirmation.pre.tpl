{if $addons.sd_facebook_pixel.purchase_facebook_pixel == 'Y' || $addons.sd_facebook_pixel.add_payment_info_facebook_pixel == 'Y'}
    <script type="text/javascript">
        var pixel_id = '{$smarty.request.pixel_id}';
        if (!pixel_id) {
            {foreach from=$identifiers_facebook_pixel item=item}
                {if $item.identifier_facebook_pixel && $item.company_id}
                    var el = document.createElement("iframe");
                    document.body.appendChild(el);
                    el.id = 'iframe';
                    el.style.width = "1px";
                    el.style.height = "1px";
                    el.style.position = 'absolute';
                    el.style.left = '-9999px';
                    el.src = fn_url('checkout.complete?order_id={$smarty.request.order_id}&pixel_id={$item.identifier_facebook_pixel}&company_id={$item.company_id}');
                {/if}
            {/foreach}
        }
    </script>
{/if}

{assign var="total" value=$childs_order_total|default:$order_info.total}

{if $addons.sd_facebook_pixel.purchase_facebook_pixel == 'Y'}
    <script type="text/javascript">
        {if $smarty.const.AJAX_REQUEST}
            /*ajax_rnd_{0|mt_rand:10000000}*/
        {/if}
        (function(_, $) {
            fbq('track', 'Purchase', {
                {if $addons.sd_facebook_pixel.account_facebook_pixel == 'business'}
                content_type: 'product',
                content_ids: [{foreach from=$order_info.products item=product} {if !$smarty.request.pixel_id || $product.company_id == $smarty.request.company_id} '{$product.product_id}',{/if}{/foreach}],
                {/if}
                value: {math equation="{$total} / {$currencies[$order_info.secondary_currency].coefficient}" format="%.{$currencies[$order_info.secondary_currency].decimals}f"},
                currency: '{$order_info.secondary_currency}'
            });
        }(Tygh, Tygh.$));
    </script>
{/if}

{if $addons.sd_facebook_pixel.add_payment_info_facebook_pixel == 'Y'}
    <script type="text/javascript">
        {if $smarty.const.AJAX_REQUEST}
            /*ajax_rnd_{0|mt_rand:10000000}*/
        {/if}
        (function(_, $) {
            fbq('track', 'AddPaymentInfo');
        }(Tygh, Tygh.$));
    </script>
{/if}