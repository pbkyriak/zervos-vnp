<!-- Send the Order data -->
{assign var="skTaxes" value=$order_info.total-$order_info.total/1.24}
{assign var="psurcharge" value=$order_info.payment_surcharge|default:0}
{assign var="gtotal" value=$order_info.total-$psurcharge}

<script>
  skroutz_analytics('ecommerce', 'addOrder', JSON.stringify({ldelim}
    order_id: '{$order_info.order_id}',  // Order ID. Required.
    revenue:  '{$gtotal}', // Grand Total. Includes Tax and Shipping.
    shipping: '{$order_info.shipping_cost}',    // Total Shipping Cost.
    tax:      '{$skTaxes|round:2}'  // Total Tax.
  {rdelim}));


{foreach from=$order_info.products item="product"}
  skroutz_analytics('ecommerce', 'addItem', JSON.stringify({ldelim}
    order_id:   '{$order_info.order_id}',
    product_id: '{$product.product_id}',
    name:       '{$product.product}',
    price:      '{($product.display_subtotal/$product.amount)|round:2}',
    quantity:   '{$product.amount}'
  {rdelim}));
{/foreach}

</script>
