{*literal}
<script>
  (function(a,b,c,d,e,f,g){a[e]= a[e] || function(){
    (a[e].q = a[e].q || []).push(arguments);};f=b.createElement(c);f.async=true;
    f.src=d;g=b.getElementsByTagName(c)[0];g.parentNode.insertBefore(f,g);
  })(window,document,'script','https://analytics.skroutz.gr/analytics.min.js','sa');

  sa('session', 'connect', '{/literal}{$addons.slx_skroutz_analytics.shop_account_id}{literal}');  // Connect your Account.
</script>
{/literal*} 

{literal}
<script>
  (function(a,b,c,d,e,f,g){a['SkroutzAnalyticsObject']=e;a[e]= a[e] || function(){
    (a[e].q = a[e].q || []).push(arguments);};f=b.createElement(c);f.async=true;
    f.src=d;g=b.getElementsByTagName(c)[0];g.parentNode.insertBefore(f,g);
  })(window,document,'script','https://analytics.skroutz.gr/analytics.min.js','skroutz_analytics');

  skroutz_analytics('session', 'connect', '{/literal}{$addons.slx_skroutz_analytics.shop_account_id}{literal}');  // Connect your Account.
</script>
{/literal}