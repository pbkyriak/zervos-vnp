﻿{if !$smarty.cookies.slx_cookie_law}
<div id="slx_cookie_law">
<div class="slx_cookie_law_container">
<div class="slx_coockie_law-icon ty-center">
<div class="slx-icon-cookie"><i class="fas fa-info-circle"></i></div>
</div>
<p class="slx_cookie_law_message">{__("cookies_law_text")}
</p>
<div class="slx_cookie_law_button_wrapper ty-center">
<a class="wish-link nowrap cm-submit text-button slx_cookie_law_button" target="_blank" href="#null" onclick="return fn_slx_cookie_law();">{__("cookies_law_button")}</a>
</div>
</div>
</div>
{/if}
{literal}
<script type="text/javascript">
function fn_slx_cookie_law(){
	var exdate=new Date();
	var _slx_cookie_law = 15;  
	exdate.setDate(exdate.getDate() + _slx_cookie_law);
	document.cookie = "slx_cookie_law=1; expires="+exdate.toUTCString()+"; path=/";
	$('#slx_cookie_law').hide();
	return false;
}
</script>
{/literal}