{* [panos] *}
{assign var="ctotal" value=$cart.total-$cart.interest}
{assign var="insts" value=$ctotal|fn_check_installmnets:$cart.payment_id}
{assign var="period" value=$cart.payment_period|default:1}

{if count($insts)>0 }
<div class="form-field" style="padding-left: 10px">
	<table border="0" width="50%" class="checkout-installments-table" cellspacing="0" cellpadding="0">
	<tr>
		<th>&nbsp;</th>
		<th nowrap>{__("installments")}</th>
		<th nowrap>{__("payment_per_month")}</th>
		<th nowrap>{__("total")}</th>
	</tr>
	{foreach from=$insts item=istallmentCount name="period"}
		<tr {cycle values=",class='table-row'"}>
			<td>
				<input type="radio" 
                  id="payment_method_{$istallmentCount}" name="payment_period" 
                  value="{$istallmentCount}" 
                  class ="cm-select-payment-period"
                  data-payment-id="{$cart.payment_id}"
                  {if $period == $istallmentCount || (!$period && !$selected_value)}checked="checked"{assign var="selected_value" value=true}{/if}  />
			</td>
			{assign var="interest" value=$ctotal|fn_check_installment:$istallmentCount:$cart.payment_id}
			{if $istallmentCount == 1}
				<td colspan="2"><b>{__("first_installment")}.</b></td>
			{else}
				<td><b>{$istallmentCount}</b></td>
				<td>
					{math equation="(y + z) / x" y=$interest z=$ctotal x=$istallmentCount assign="per_month"}
					{include file="common/price.tpl" value=$per_month}
				</td>
			{/if}
			<td>
				{include file="common/price.tpl" value=$interest+$ctotal}
            </td>
		</tr>
    {/foreach}
	</table>
</div>
{else}
  <p>This payment method is not setuped properly.</p>
{/if}
{* [/panos] *}
