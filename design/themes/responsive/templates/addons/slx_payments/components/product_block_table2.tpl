    <p class="installements">
      <span class="cm-reload-{$obj_prefix}{$obj_id}" id="installements_update_{$obj_prefix}{$obj_id}">
        <input type="hidden" name="appearance[show_installements]" value="{$show_sku}" />
            {foreach from=$insts item=istallmentCount name="period"}
              {if $istallmentCount > 1}
                  {assign var="interest" value=$productPrice|fn_check_installment:$istallmentCount:$payment_id}               
                  {math equation="(y + z) / x" y=$interest z=$productPrice x=$istallmentCount assign="per_month"}
                  <p style="font-weight: bold;">{include file="common/price.tpl" value=$per_month} / {__("month_in")} {$istallmentCount} {__("installments")}</p>
              {/if}
            {/foreach}
        </span>
      <!--installements_update_{$obj_prefix}{$obj_id}--></span>
    </p>