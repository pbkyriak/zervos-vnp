<table class="product_installments_table" width="100%" cellspacing="0" cellpadding="0" border="0">
    <thead>
    <tr>
        <th nowrap>{__("installments")}</th>
        <th nowrap>{__("payment_per_month")}</th>
        <th nowrap>{__("total")}</th>
    </tr>
    </thead>
    <tbody>
    {foreach from=$insts item=istallmentCount name="period"}
        {if $istallmentCount > 0}
            {assign var="interest" value=$productPrice|fn_check_installment:$istallmentCount:$payment_id}
            {math equation="(y + z) / x" y=$interest z=$productPrice x=$istallmentCount assign="per_month"}
            <tr>
                <td width="50%">{if $istallmentCount == 1}{__("first_installment")}{else}{$istallmentCount}{/if}</td>
                <td width="25%">{include file="common/price.tpl" value=$per_month}</td>
                <td width="25%">{include file="common/price.tpl" value=$per_month*$istallmentCount}</td>
            </tr>
        {/if}
    {/foreach}
    </tbody>
</table>