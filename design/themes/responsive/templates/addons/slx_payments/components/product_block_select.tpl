    <p class="installements">
      <span class="cm-reload-{$obj_prefix}{$obj_id}" id="installements_update_{$obj_prefix}{$obj_id}">
        <input type="hidden" name="appearance[show_installements]" value="{$show_sku}" />
        <span id="installements_{$obj_prefix}{$obj_id}">
          <select name="payment_info[period]">
            <option value="0">{__("select_installments")}</option>
            {foreach from=$insts item=istallmentCount name="period"}
              {if $istallmentCount > 0}
              <option value="{$istallmentCount}" >
                {assign var="interest" value=$productPrice|fn_check_installment:$istallmentCount:$payment_id}               
                  {math equation="(y + z) / x" y=$interest z=$productPrice x=$istallmentCount assign="per_month"}
                  {if $istallmentCount == 1}{__("first_installment")}{else}{$istallmentCount} {__("installments")}{/if} X {include file="common/price.tpl" value=$per_month}
              </option>
              {/if}
            {/foreach}
          </select>
        </span>
      <!--installements_update_{$obj_prefix}{$obj_id}--></span>
    </p>