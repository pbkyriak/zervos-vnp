{** block-description:product_installments **}
{assign var="payment_id" value=$addons.slx_payments.product_block_payment_id}
{if $payment_id!=0 && $show_add_to_cart}
    {assign var="productPrice" value=$product.price}
    {assign var="insts" value=$productPrice|fn_check_installmnets:$payment_id}
    {if $insts }
        {if $addons.slx_payments.product_installments_block==1 }
            <p>{__("slx_payments.product_installments_text")}</p>
            {include file="addons/slx_payments/components/product_block_select.tpl" }
        {elseif $addons.slx_payments.product_installments_block==2 }
            <p>{__("slx_payments.product_installments_text")}</p>
            {include file="addons/slx_payments/components/product_block_table.tpl" }
        {elseif $addons.slx_payments.product_installments_block==3 }
            <p>{__("slx_payments.product_installments_text")}</p>
            {include file="addons/slx_payments/components/product_block_table2.tpl" }
        {/if}
    {/if}
{/if}
