{if $cart.interest}
  <li id="interest_line">
    <span>{if $cart.payment_info.period == 1 }{__("epivarinsi_pistotikis")}{else}{__("interest")}{/if}:</span>
    <strong>{include file="common_templates/price.tpl" value=$cart.interest|default:0.00 span_id="interest_value" class="price"}</strong>
  </li>
  {math equation="x+y" x=$_total|default:$cart.total y=$cart.interest|default:0 assign="_total"}
{/if}