
<script type="text/javascript">
(function(_, $) {
// [panos]
    $(_.doc).on('click', '.cm-select-payment-period', function() {
        var self = $(this);
        var paymentId = self.data("payment-id");
        
        $.ceAjax('request', fn_url('{$url}&payment_id='+paymentId+'&period='+ self.val()), {
            result_ids: '{$result_ids}',
            full_render: true
        });
    });
// [/panos]
}(Tygh, Tygh.$));
</script>