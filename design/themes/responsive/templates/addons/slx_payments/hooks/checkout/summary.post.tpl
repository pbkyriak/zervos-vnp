{if $cart.interest|floatval}
  <tr>
    <td class="ty-checkout-summary__item">{__("epivarinsi_pistotikis")}</td>
    <td class="ty-checkout-summary__item ty-right" data-ct-checkout-summary="payment-interest">
      <span>{include file="common/price.tpl" value=$cart.interest}</span>
    </td>
  </tr>
  {math equation="x+y" x=$_total|default:$cart.total y=$cart.interest|default:0 assign="_total"}
{/if}
