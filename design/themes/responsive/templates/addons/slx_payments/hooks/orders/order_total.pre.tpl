{if $order_info.interest}
    <tr>
        <td>{__("interest")}:&nbsp;</td>
        <td style="width: 57%" data-ct-orders-summary="summary-payment">{include file="common/price.tpl" value=$order_info.interest}</td>
    </tr>
    <tr>
        <td>{__("installments")}:&nbsp;</td>
        <td style="width: 57%" data-ct-orders-summary="summary-payment">{$order_info.payment_period}</td>
    </tr>
{/if}
