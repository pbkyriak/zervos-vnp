{if "AJAX_REQUEST"|defined}
<div class="scibyl_cart" style="display:none">
	{foreach from=$_cart_products key="key" item="p" name="cart_products"}
	<div class="scibyl_cart_item">
		<span class="product_id">{$p.product_id}</span>
		<span class="quantity">{$p.amount}</span>
		<span class="unit_price">{$p.price|string_format:"%.2f"}</span>
		<span class="currency_code">EUR</span>
	</div>        
	{/foreach}
</div>

{literal}
<script class="cm-ajax-force">
var b = new Basket();
b.basket_update();
</script>
{/literal}
{/if}
