<div class="scibyl_cart_purchased" style="display:none">
	{foreach from=$order_info.products item="oi"}
	<div class="scibyl_cart_item">
		<span class="product_id">{$oi.product_id}</span>
		<span class="quantity">{$oi.amount}</span>
		<span class="unit_price">{$oi.price}</span>
		<span class="currency_code">EUR</span>
	</div>
	{/foreach}
</div>
