{literal}
<script>
(function(d, s, id) { 
var js, sjs = d.getElementsByTagName(s)[0];
if (d.getElementById(id)) return;
js = d.createElement(s);
js.id = id;
js.type = 'text/javascript';
js.async = true;
js.src = "https://app.scibyl.com/static/js/scibyl.min.js";
sjs.parentNode.insertBefore(js, sjs);
}
(document, 'script', 'scibyl-js')); 
</script>
{/literal}