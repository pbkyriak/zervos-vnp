
{if $carrier == "acs"}
    {assign var="url" scope="root" value="http://www.acscourier.gr/podSearch.aspx?podString=`$tracking_number`"}
    {assign var="carrier_name"  scope="root" value=__("carrier_acs")}
{elseif $carrier == "geniki"}
    {assign var="url"  scope="root" value="http://www.taxydromiki.com/track/`$tracking_number`"}
    {assign var="carrier_name"  scope="root" value=__("carrier_geniki")}
{elseif $carrier == "speedex"}
    {assign var="url"  scope="root" value="http://www.speedex.gr/pelates/isapohi.asp?voucher_code=`$tracking_number`"}
    {assign var="carrier_name"  scope="root" value=__("carrier_speedex")}
{/if}
