{if $smarty.session.edit_step=='step_two' && $edit && $settings.Appearance.invoice_on=='Y'}
<script type="text/javascript">
	var invoice_type='{$smarty.session.cart.invoice_or_receipt}';
	var i_ship_to_another={if $smarty.session.cart.ship_to_another}true{else}false{/if};
	var i_same_fields_in_b=new Array({''|fn_get_billing_and_shipping_common_fields nofilter});
	var i_receipt_from_store='{$cart.receipt_from_store}';
{literal}
	
	jQuery(document).ready(function() {
		fn_invoice_receipt_place_copy_address_selector();
		fn_invoice_receipt_define_disabled_status(!i_ship_to_another);
		if(i_receipt_from_store=='Y'){
			jQuery("#step_two_body .clearfix .checkout__block").hide();
			if (invoice_type=='I'){
				jQuery('#ba').find('.checkout__block').show();
				fn_invoice_receipt_define_disabled_status(false);
			}
		}
		else {
		}
	});
	
	jQuery(document).ajaxComplete(function () {
		fn_invoice_receipt_place_copy_address_selector();
		if ($('#sw_b_suffix_yes').is(":checked")){
			i_ship_to_another=true;
		}else{
			i_ship_to_another=false;
		}
		fn_invoice_receipt_define_disabled_status(i_ship_to_another);
		if($('#receipt-from-store').is(":checked")){
			jQuery("#step_two_body .clearfix .checkout__block").hide();
			if ($('#invoice-receipt-i').is(':checked')){
				jQuery('#ba').find('.checkout__block').show();
				fn_invoice_receipt_define_disabled_status(false);
			}
		}	
	});
	
	function fn_invoice_receipt_place_copy_address_selector(){
		
		var recipient=jQuery("#ba");
		if (recipient.is(':visible')){
			var cntr=jQuery("#billing_selector_copy_address");
			recipient.prepend(cntr.removeClass('hidden'));
		}
	}
	
	
	function fn_invoice_receipt_change_invoice_type_select(sl) {
		var sl = $(sl);
		if(sl.val()=="R" || sl.val()=="I") {
			fn_invoice_receipt_change_invoice_type(sl.val());
		}
	}
	
	function fn_invoice_receipt_change_invoice_type(invoice_type){
		fn_invoice_receipt_submit_form("checkout.select_invoice_type."+invoice_type);
	}	
	
	function fn_invoice_receipt_copy_address_act(copy_address){
		fn_invoice_receipt_submit_form("checkout.copy_s_address_to_b."+copy_address);
	}	
	
	function fn_invoice_receipt_receipt_from_store(){
		var dispatch="checkout.receipt_from_store";
		if($('#receipt-from-store').is(":checked")){
			dispatch=dispatch+'.Y';
		}else{
			dispatch=dispatch+'.N';
		}
		
		fn_invoice_receipt_submit_form(dispatch);

	}
	
	function fn_invoice_receipt_submit_form(dispatch){
		var submit_button=jQuery("#step_two_body .ty-checkout-buttons button[name='dispatch[checkout.update_steps]']");
		if(submit_button.length>0){
			fn_invoice_receipt_remove_required_from_fields();
			submit_button.attr("name","dispatch["+dispatch+"]");
			submit_button.click();
		}
	}
	
	function fn_invoice_receipt_remove_required_from_fields(){
		jQuery('#step_two_body label.cm-required').removeClass("cm-required").addClass("i-required"); 
	}
	
	function fn_invoice_receipt_define_disabled_status(disable){
		if(i_same_fields_in_b.length>0){
			for(i=0;i<i_same_fields_in_b.length;i++){
				if(disable){
					jQuery("#"+i_same_fields_in_b[i]).attr("disabled", "disabled").addClass("i-disable-field");
				}else{
					 jQuery("#"+i_same_fields_in_b[i]).removeAttr("disabled").removeClass("i-disable-field");
				}
			}
		}
	}
	
{/literal}



</script>

{**$config|@var_dump**}
<h2>
	{*
	//Aferesi sxoliwn gia na mporei o pelatis na epilksei an tha paralavei to proion apo tin edra gia na min xriazetai na
	//simplirosei dieythinsis apsotolis kai pliromis. Episis prepei na figei to hiden field amesos meta to sxolio
	//kai o tropos apostolis paralavi apo tin edra mas na energopoieithei
	*}
	<div id="i-receipt-store-cntr">
		<input type="hidden" value="N" name="receipt_from_store">
		<label for="receipt-from-store" id="receipt-from-store-label">{__("receipt_store_title")}</label>	
		<input type="checkbox" class="checkbox" value="Y" name="receipt_from_store" id="receipt-from-store"{if $cart.receipt_from_store=='Y'} checked="checked"{/if} onclick="javascript:fn_invoice_receipt_receipt_from_store();">				
		<span id="i-receipt-store-tooltip">{__("receipt_store_desc")}</span>
	</div>
		
	{if $cart.receipt_from_store!='Y'}{* Me tin paralavi apo to katastima na min iparxei epilogi eidous parastatikou *}
		<p id ="invoice_receipt_question" class="invoice_receipt">
			{__("invoice_receipt_question")} 
		</p>	
		<p class="invoice_receipt">	{__("invoice_receipt_question_law")} 
		</p>
		<a id="i-receipt-selector" class="invoice_receipt" rev="step_two" onclick="javascript:fn_invoice_receipt_change_invoice_type('R');"><input type="radio" name="invoice_receipt" id="invoice-receipt-r" value="R" class="radio"{if $smarty.session.cart.invoice_or_receipt=="R"} checked="checked"{/if}><label for="invoice-receipt-r">{__("invoice_receipt_receipt")}</label></a>	
		
		<a id="i-invoice-selector" class="invoice_receipt " rev="step_two"  onclick="javascript:fn_invoice_receipt_change_invoice_type('I');"><input type="radio" name="invoice_receipt" id="invoice-receipt-i" value="I" class="radio"{if $smarty.session.cart.invoice_or_receipt=="I"} checked="checked"{/if}><label for="invoice-receipt-i">{__("invoice_receipt_invoice")}</label></a>
	{else}
		<input type="hidden" value="R" name="invoice_receipt">
	{/if}

	<div id="billing_selector_copy_address" class="hidden">
		<div id="i-copy-address-cntr" class="clearfix address-switch{if $cart.receipt_from_store=='Y'} hidden{/if}">
			<div class="ty-float-left"><span>{__("invoice_receipt_same_us_above")}</span></div>
			<div class="ty-float-right">
				<input class="radio" type="radio" name="i_some_as_above" value="Y" id="sw_b_suffix_yes" {if !$smarty.session.cart.ship_to_another}checked="checked"{/if} onclick="javascript:fn_invoice_receipt_copy_address_act('Y');"/><label for="sw_b_suffix_yes">{__("yes")}</label>
				<input class="radio" type="radio" name="i_some_as_above" value="N" id="sw_b_suffix_no" {if $smarty.session.cart.ship_to_another}checked="checked"{/if} onclick="javascript:fn_invoice_receipt_copy_address_act('N');"/><label for="sw_b_suffix_no">{__("no")}</label>
			</div>
		</div>
	
	</div>
</h2>
{/if}