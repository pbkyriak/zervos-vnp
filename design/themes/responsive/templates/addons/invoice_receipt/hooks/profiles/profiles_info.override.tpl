{* $Id$ *}

{*

	Author Ioannis Matziaris - imatzgr@gmail.com - April 2013
	
	Einai custom hook sto arxeio views/profile/components/profiles_info.tpl
	Sto Hook mpainei olo to arxeio
	Emfanizei ta stoixeia parastatikou prliromis stin paragkelia pou anoigei mesa apo to account tou customer

*}
{if $order_info.invoice_or_receipt!='N' || $order_info.twigmo=="Y"}

	{include file="common/subheader.tpl" title=__("customer_information")}

	{assign var="profile_fields" value=$location|fn_get_profile_fields}
	{split data=$profile_fields.C size=2 assign="contact_fields" simple=true size_is_horizontal=true}

	<table class="orders-info valign-top">
	<tr class="valign-top">
		{if $profile_fields.S}
			<td id="tygh_order_shipping_adress" style="width: 31%">
				<h5>{__("shipping_address")}</h5>
				<div class="orders-field">{include file="views/profiles/components/profile_fields_info.tpl" fields=$profile_fields.S title=__("shipping_address")}</div>
			</td>
		{/if}
		{if $profile_fields.B}
			<td id="tygh_order_billing_adress" style="width: 31%">
				{if $order_info.invoice_or_receipt=='R'}
					<h5>{__("invoice_receipt_question")}</h5>
					<div class="orders-field">{__("invoice_receipt_receipt")}</div>
				{elseif $order_info.invoice_or_receipt=='I'}
					<h5>{__("billing_address")}</h5>
					<div class="orders-field">{include file="views/profiles/components/profile_fields_info.tpl" fields=$profile_fields.B title=__("billing_address")}</div>
				{elseif $order_info.twigmo=="Y"}
					<h5>{__("billing_address_real")}</h5>
					<div class="orders-field">{* include file="views/profiles/components/profile_fields_info.tpl" fields=$profile_fields.B title=__("billing_address") *}
					{* O katw kwdikas einai apo to pio panw arxeio me allages *}
						{foreach from=$profile_fields.B item=field}

						{assign var="value" value=$user_data|fn_get_profile_field_value:$field}
						{if $value && $value!="-|-"}
						<div class="info-field {$field.field_name|replace:"_":"-"}">{$value}</div>
						{/if}
						{/foreach}
					{* END *}
					</div>
				{/if}
			</td>
		{/if}
		<td style="width: 35%">
			{if $contact_fields.0}
				{capture name="contact_information"}
					{include file="views/profiles/components/profile_fields_info.tpl" fields=$contact_fields.0 title=__("contact_information")}
				{/capture}
				{if $smarty.capture.contact_information|trim != ""}
					<h5>{__("contact_information")}</h5>
					<div class="orders-field">{$smarty.capture.contact_information nofilter}</div>
				{/if}
			{/if}
		</td>
	</tr>
	</table>
{/if}