{* $Id$ *}
{*

	Author Ioannis Matziaris - imatzgr@gmail.com - April 2013

	Metakinisi email, password kai birtday fields se nees thesis. Sto checkout, vima 1, stin eggrafi tou pelati
*}

<script type="text/javascript">

	var i_lang_register='{__("register")}';
	var i_lang_please_sign_in='{__("please_sign_in")}';
	
{literal}
	var i_checkout_register_fields_moved=false;
	
	jQuery(document).ready(function(){
		if(jQuery("#register_checkout a").length>0){
			if(!i_checkout_register_fields_moved){
				fn_invoice_receipt_move_checkout_regiter_fields();
			}
			jQuery("#register_checkout a").bind("click",function(){ jQuery("#step_one a.title").html(i_lang_register); });
		}
		if(jQuery("#step_one_register a,az_button_link").length>0){
			jQuery("#step_one_register a,az_button_link").bind("click",function(){ jQuery("#step_one a.title").html(i_lang_please_sign_in); });
		}
	});
	
	function fn_invoice_receipt_move_checkout_regiter_fields(){
		var r_fields_cntr=jQuery("#step_one_register .form-field");
		var r_fields_to_move=new Array();
		var i_counter=-1;
		var r_field_age='';
		var r_field_age_exist=false;
		r_fields_cntr.each(function(){
			if(jQuery(this).find("input#email").length>0 || jQuery(this).find("input#password1").length>0 || jQuery(this).find("input#password2").length>0){
				i_counter++;
				r_fields_to_move[i_counter]=jQuery(this);
				jQuery(this).remove();				
			}
			if(jQuery(this).find("input#birthday").length>0 && jQuery("#step_one_register .form-field.last-name").length>0){
				r_field_age_exist=true;
				r_field_age=jQuery(this);
				jQuery(this).remove();
				jQuery("#step_one_register .form-field.last-name").after(jQuery(this));
			}
		});
		
		if(i_counter>=0){
			for(i=0;i<=i_counter;i++){
				jQuery('.checkout-inside-block').append(r_fields_to_move[i]);
			}
		}
	}
	
{/literal}



</script>