

{if $carrier == "acs"}
    {assign var="url" scope="root" value="https://www.acscourier.net/el/track-and-trace?p_p_id=ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet&p_p_lifecycle=1&p_p_state=normal&p_p_mode=view&p_p_col_id=column-4&p_p_col_pos=1&p_p_col_count=2&_ACSCustomersAreaTrackTrace_WAR_ACSCustomersAreaportlet_javax.portlet.action=trackTrace&generalCode=`$tracking_number`"}
    {assign var="carrier_name"  scope="root" value=__("carrier_acs")}
{elseif $carrier == "geniki"}
    {assign var="url"  scope="root" value="http://www.taxydromiki.com/track/`$tracking_number`"}
    {assign var="carrier_name"  scope="root" value=__("carrier_geniki")}
{elseif $carrier == "speedex"}
    {assign var="url"  scope="root" value="http://www.speedex.gr/pelates/isapohi.asp?voucher_code=`$tracking_number`"}
    {assign var="carrier_name"  scope="root" value=__("carrier_speedex")}
{/if}
