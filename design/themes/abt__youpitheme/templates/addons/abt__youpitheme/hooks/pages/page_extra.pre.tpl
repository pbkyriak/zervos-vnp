{if $page.page_type == $smarty.const.PAGE_TYPE_BLOG and !empty($b)}
    <br/>
    <hr/>
    <div class="ypi-blog-extended-content">
        {* Отобразить товары*}
        {if $b.abt__yt_product_list_use == 'Y' and $b.abt__yt_product_list}
            <p>&nbsp;</p>
            <div class="y-mainbox-title">{$b.abt__yt_product_list_title}</div>
            {include file="blocks/product_list_templates/short_list.tpl" no_pagination=true no_sorting=true products=$b.products}
        {/if}
    </div>
{/if}