<script type="text/javascript">
    (function(_, $) {
        // сохранеям в js настройки темы
        $.extend(_, {
            abt__yt: {$settings.abt__yt|json_encode nofilter},
        });

    }(Tygh, Tygh.$));
</script>
{script src="js/addons/abt__youpitheme/jquery.countdown.min.js"}
{script src="js/addons/abt__youpitheme/jquery.popupoverlay.min.js"}
{script src="js/addons/abt__youpitheme/abt__yt_func.js"}
{script src="js/addons/abt__youpitheme/abt__yt_grid_tabs.js"}

<!-- Toggle buttos -->
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {

            $(document).on('click', '.ypi-menu__fixed-button', function () {
                $('.ypi-menu__fixed-button').toggleClass('active');
                $('body').toggleClass('menu_fixed-top');
            });
            $(document).on('click', '.closer_fixed-menu', function () {
                $('body').removeClass('menu_fixed-top');
                $('.ypi-menu__fixed-button').removeClass('active');
            });

        });
    })(jQuery);
</script>

<!-- Toggle vertical filters buttos -->
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {

            $(document).on('click', '.ypi-white-vfbt', function () {
                $('.ty-horizontal-product-filters').toggleClass('vertical-position');
                $('.ty-horizontal-product-filters-dropdown__content').toggleClass('hidden').css('display','inline-block');
                $('.ty-horizontal-product-filters .v-filers-header').toggleClass('hidden');
                $('.ypi-white-vfbt').toggleClass('open');
                $('body').toggleClass('body-not-scroll');
            });

            $(document).on('click', '.ty-horizontal-product-filters.vertical-position .closer', function () {
                $('.ty-horizontal-product-filters').removeClass('vertical-position');
                $('.v-filers-header').addClass('hidden');
                $('.ty-horizontal-product-filters-dropdown__content').toggleClass('hidden').css('display','none');
                $('.ty-horizontal-product-filters-dropdown__wrapper').removeClass('open');
                $('.ypi-white-vfbt').removeClass('open');
                $('body').removeClass('body-not-scroll');
            });
            
            $(document).on('click', '.enable-v-filers .ypi-white-vfbt', function () {
                $('.enable-v-filers').addClass('mobile-view');         
            });
            
            $(document).on('click', '.enable-v-filers .v-filter .closer', function () {
                $('.enable-v-filers').removeClass('mobile-view');
                $('.ypi-white-vfbt').removeClass('open');
                $('body').removeClass('body-not-scroll');
            });
            
        });
    })(jQuery);
</script>

<!-- Toggle list -->
<script type="text/javascript">
    (function ($) {
        $(document).ready(function () {

            $(document).on('click', '.ToggleItem', function () {
                $(this).toggleClass('is-open');
            });
            
        });
    })(jQuery);
</script>

{literal}
<!--  Функция для реализации запаздывания меню -->
<script type="text/javascript">
    function fn_abt_timer_menu (){
        var timer, timer2, opened_menu = null, opened_menu2 = null, second_lvl;
        $('.first-lvl').hover(function () {
            var elem = $(this).children('.ty-menu__submenu').children('ul');
            clearTimeout(timer);
            timer = setTimeout(function () {
                if (opened_menu !== null) {
                    opened_menu.hide();
                    opened_menu = null;
                }
                opened_menu = elem.show();
            }, 200);
        });
        $('.hover-zone').mouseleave(function () {
            clearTimeout(timer);
            if (opened_menu !== null) {
                opened_menu.hide();
                opened_menu = null;
            }
        });

        $('.second-lvl').hover(function () {
            second_lvl = $(this);
            var elem = $(this).children('.ty-menu__submenu');
            clearTimeout(timer2);
            timer2 = setTimeout(function () {
                if (opened_menu2 !== null) {
                    second_lvl.removeClass('hover2');
                    opened_menu2 = null;
                }
                $('.second-lvl').removeClass('hover2');
                second_lvl.addClass('hover2');
            }, 200);
        });
        $('.hover-zone2').mouseleave(function () {
            clearTimeout(timer2);
            $('.second-lvl').removeClass('hover2');
            if (opened_menu2 !== null) {
                opened_menu2.hide();
                $('.second-lvl').removeClass('hover2');
                opened_menu2 = null;
            }
        });
    }

    if (document.documentElement.clientWidth >= 768) {
        if ($('.cat-menu-vertical .ty-dropdown-box__title').hasClass('open')) {
            $('.cat-menu-vertical .ty-menu__items').removeClass('hover-zone');
        } else {
            $('.cat-menu-vertical .ty-menu__items').addClass('hover-zone');
            $('.hover-zone').hover(
                function () {
                    $('body').addClass('shadow')
                },
                function () {
                    $('body').removeClass('shadow')
                }
            );
        }

        (function (_, $) {
            $(document).ready(function () {
                // если нет меню с ajax-подгрузкой, запускаем запаздывание
                if ($('div.abtam').length == 0){
                    fn_abt_timer_menu();
                }
            });
        })(Tygh, Tygh.$)
    }

    // ---------------------------------------------------------------------
    // Ищем меню требующие ajax дозагрузку
    // ---------------------------------------------------------------------
    (function(_, $) {
        $(document).ready(function () {
            var abtam = $('div.abtam');
            if (abtam.length){
                var ids = [];
                abtam.each(function(){
                    ids.push($(this).attr('id'));
                });

                $.ceAjax('request', fn_url('abt__yt.load_menu'), {
                    result_ids: ids.join(','),
                    method: 'post',
                    hidden: true,
                    callback: function(data) {
                        if (document.documentElement.clientWidth >= 768) {
                            fn_abt_timer_menu();
                        }
                        $(document).on('click', 'li.second-lvl > a.cm-responsive-menu-toggle', function () {
                            $(this).toggleClass('ty-menu__item-toggle-active');
                            $('.icon-down-open', this).toggleClass('icon-up-open');
                            $(this).parent().find('.cm-responsive-menu-submenu').first().toggleClass('ty-menu__items-show');
                        });
                    }
                });
            }
        });
    }(Tygh, Tygh.$));
</script>
{/literal}



{*//--------------------------------------------------------------------------*}
{*// HIDE CONTENT - Скрываем контент блоков*}
{*//--------------------------------------------------------------------------*}
<script type="text/javascript">
    (function (_, $) {
        $.ceEvent('on', 'ce.commoninit', function(context) {
            $("div.abt-yt-hc", context).each(function(){
                var tab_div = $(this);
                var tab_content = $(this).parent();
                var style = tab_div.attr('style');

                var tab_hidden = tab_content.hasClass('hidden');

                if (tab_hidden) tab_content.removeClass('hidden');
                tab_div.removeAttr('style');

                var h = tab_div.outerHeight();

                if (tab_hidden) tab_content.addClass('hidden');
                tab_div.attr('style', style);

                if (parseInt(_.abt__yt.products.tab_content_view_height) > 0 && parseInt(h) > parseInt(_.abt__yt.products.tab_content_view_height)) {
                    tab_div.addClass('active').after("<a data-tab='" + tab_div.parent().attr('id') + "' class='show_more'><span>{__("more")}</span><i class='material-icons'>&#xE313;</i></a>");
                }
            });
        });
        $(document).on('click', 'a.show_more', function () {
            var t = $("#" + $(this).data('tab') + " > div");
            if (t.length){
                $(this).remove();
                t.removeClass('abt-yt-hc').removeClass('active').css('max-height', '');
            }
        });
    })(Tygh, Tygh.$)

</script>