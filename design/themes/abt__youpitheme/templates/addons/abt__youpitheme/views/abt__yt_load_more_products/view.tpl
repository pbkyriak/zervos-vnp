{if $product_list_page and $settings.abt__yt.general.load_more_products == "Y"}
    {$abt__yt_pagination=$search|fn_generate_pagination}

    {if $abt__yt_pagination.current_page < $abt__yt_pagination.total_pages}
        {* можем еще загрузить*}
        {assign var="c_url" value=$config.current_url|fn_query_remove:"page"}
        <div class="more-products-data" id="product_list_page{$abt__yt_pagination.next_page}"></div>
        <div class="col-tile">
            <div class="more-products-link product_list_page{$abt__yt_pagination.next_page}" style="height: {$settings.abt__yt.product_list.height_list_prblock}px">
                <a href="{"`$c_url`&page=`$abt__yt_pagination.next_page``$extra_url`"|fn_url}"
                   data-ca-page="{$abt__yt_pagination.next_page}"
                   class="cm-history ty-pagination__item cm-ajax"
                   data-ca-append="true"
                   data-ca-event="ce.abt__yt_more_products_callback"
                   data-ca-target-id="pagination_block,pagination_blockY,product_list_page{$abt__yt_pagination.next_page}">
                    {$show_more=$abt__yt_pagination.items_per_page}
                    {* вычислим остаток товаров *}
                    {$left_products=$abt__yt_pagination.total_items-($abt__yt_pagination.items_per_page*$abt__yt_pagination.current_page)}
                    {if $left_products < $abt__yt_pagination.items_per_page}{$show_more=$left_products}{/if}
                    <div class="loader"></div><br>
                    {__("abt__yt.load_more_products.show_more", [$show_more])}
                </a>
            </div>
        </div>
    {elseif $abt__yt_pagination.total_items > $abt__yt_pagination.items_per_page}
        {* загружены все товары *}
        <div class="col-tile">
            <div class="more-products-link product_list_page{$abt__yt_pagination.next_page}" style="height: {$settings.abt__yt.product_list.height_list_prblock}px">
                <p><span class="already-showing-all-products">{__("abt__yt.load_more_products.already_showing_all_products", [$abt__yt_pagination.total_items])}</span></p>
            </div>
        </div>
    {/if}
{/if}
