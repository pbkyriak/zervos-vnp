<a class="closer abyt-closer-{$b.link_id}"><i class="material-icons">&#xE5CD;</i></a>
{if $b.abt__yt_data_type == 'blog'}
    <div class="ypi-popup-content">
	    
        {* Отобразить Видео*}
        <h1>{$b.page_data.page}</h1>
        {if $b.abt__yt_youtube_use == 'Y' and $b.abt__yt_youtube_id}
        <div class="ypi-popup-media">
            <iframe id="youtube-player-{$b.link_id}" width="600" height="400" src="{$b|fn_abt__yt_build_youtube_link}&enablejsapi=1&version=3" frameborder="0" allowfullscreen="true" allowscriptaccess="always"></iframe>
        </div>
        {/if}

        {* Отобразить описание*}
        <div class="ypi-popup-descr not-full-height">{$b.page_data.description nofilter}</div>
        <a class="more-btn" alt="" onclick="$(this).prev().removeClass('not-full-height');$(this).addClass('hidden');"><span>{__("more")}</span><i class="icon-down-dir material-icons"></i></a>

        {* Отобразить товары*}
        {if $b.abt__yt_product_list_use == 'Y' and $b.abt__yt_product_list}
            <div class="y-mainbox-title">{$b.abt__yt_product_list_title}</div>
            {include file="blocks/product_list_templates/short_list.tpl" no_pagination=true no_sorting=true}
        {/if}

    </div>
{elseif $b.abt__yt_data_type == 'promotion'}

    {*промоакция*}
    <h1 class="abt-yt-bp-title">{$b.promotion_data.name}</h1>

    <div class="abt-yt-bp-body">
        
        <div class="ypi-popup-media">
            {if !empty($b.promotion_data.main_pair)}
            <div class="pd-grid-media-image">
                <img src="{$b.promotion_data.main_pair.icon.image_path}" alt="{$b.promotion_data.main_pair.icon.alt}" title="{$b.promotion_data.main_pair.icon.alt}">
            </div>
            {/if}
            
            {if $b.abt__yt_countdown_use == "Y" and $b.promotion_data.to_date > $smarty.const.TIME}
                <div class="pd-grid-cd-counter">
                    <div class="cd-counter-content">{__("abt__yt.banners.countdown.left")}:<br><span data-countdown="{$b.promotion_data.to_date|date_format:"%Y/%m/%d %H:%M:%S"}" data-str="{if $b.promotion_data.left_days != 0}<span class='days'>%-D {__("abt__yt.banners.countdown.left_days", [$b.promotion_data.left_days])}</span> {/if}%H:%M:%S"></span></div>
                </div>
            {/if}
        </div>

        <div class="pd-grid-cd-days">
            {if $b.promotion_data.from_date}
                {__('ab__dotd.from')} {$b.promotion_data.from_date|date_format:"`$settings.Appearance.date_format`"}
            {/if}
            {if $b.promotion_data.to_date}
                {__('ab__dotd.to')} {$b.promotion_data.to_date|date_format:"`$settings.Appearance.date_format`"}
            {/if}
        </div>
        
        <div class="ypi-popup-descr">{$b.promotion_data.detailed_description nofilter}</div>

        <p><a href="{"promotions.view&promotion_id=`$b.promotion_data.promotion_id`"|fn_url}" class="ty-btn ypi-more-btn active">{__("more")}</a>
        <br/>   
        <br/>
    </div>
{/if}