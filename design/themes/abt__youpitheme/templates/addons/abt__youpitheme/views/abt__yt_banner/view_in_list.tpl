{capture name="banner"}
<div class="{if $b.abt__yt_how_to_open == 'in_popup'}abyt-in-popup-{$b.link_id}{/if} abyt-a-bg-banner {if $b.abt__yt_background_color =='#ffffff' and $b.abt__yt_background_color_use =="Y"}white-bg{/if} {$b.abt__yt_color_scheme} valign-{$b.abt__yt_content_valign} align-{$b.abt__yt_content_align}" style="background: {$b.abt__yt_background_color} {if !empty($b.abt__yt_background_image) and is_array($b.abt__yt_background_image)}url('{$b.abt__yt_background_image.icon.image_path}') no-repeat center{/if};height: {$settings.abt__yt.product_list.height_list_prblock}px;">

    <div class="bg-opacity-{$b.abt__yt_content_bg}">

        <div class="abyt-a-content" style="height: -webkit-calc({$settings.abt__yt.product_list.height_list_prblock}px - 60px);height: calc({$settings.abt__yt.product_list.height_list_prblock}px - 60px);">

	        {if !empty($b.abt__yt_main_image) and is_array($b.abt__yt_main_image)}
              <div class="abyt-a-img {if $b.abt__yt_data_type == "promotion"}promotion{/if} {if $b.abt__yt_data_type == "blog" && $b.abt__yt_youtube_use == 'Y' and $b.abt__yt_youtube_id}video{/if}"><img src="{$b.abt__yt_main_image.icon.image_path}" alt="{$b.abt__yt_main_image.icon.alt}" title="{$b.abt__yt_main_image.icon.alt}"></div>
            {/if}
            
            {if $b.abt__yt_data_type == "promotion" and $b.abt__yt_countdown_use == "Y" and $b.promotion_data.to_date > $smarty.const.TIME}
                <div class="pd-grid-cd-counter"><div class="cd-counter-content">{__("abt__yt.banners.countdown.left")}:<br><span data-countdown="{$b.promotion_data.to_date|date_format:"%Y/%m/%d %H:%M:%S"}" data-str="{if $b.promotion_data.left_days != 0}<span class='days'>%-D {__("abt__yt.banners.countdown.left_days", [$b.promotion_data.left_days])}</span> {/if}%H:%M:%S"></span></div></div>
            {/if}
            
            <div class="abyt-a-content-width{if $b.abt__yt_content_full_width =="Y"}-full{/if}" style="{if !empty($b.abt__yt_padding)}padding:{$b.abt__yt_padding};{/if}">
                <{$b.abt__yt_title_tag} class="abyt-a-title {if $b.abt__yt_title_shadow =="Y"}shadow{/if} weight-{$b.abt__yt_title_font_weight}" style="font-size: {$b.abt__yt_title_font_size};{if $b.abt__yt_title_color_use == 'Y'}color: {$b.abt__yt_title_color};{/if}">{$b.abt__yt_title}</{$b.abt__yt_title_tag}>
                <div class="abyt-a-descr" style="{if $b.abt__yt_description_color_use == 'Y'}color: {$b.abt__yt_description_color};{/if}{if $b.abt__yt_description_bg_color_use == 'Y'}background-color:{$b.abt__yt_description_bg_color};position: relative;left: 5px;box-shadow: -5px 0 0 1px {$b.abt__yt_description_bg_color}, 5px 0 0 1px {$b.abt__yt_description_bg_color};{/if} font-size: {$b.abt__yt_description_font_size};">{$b.abt__yt_description nofilter}</div>
            </div>
        </div>

        {if $b.abt__yt_button_use == 'Y'}
        <div class="abyt-a-button bottom-35-{$settings.abt__yt.product_list.show_qty_discounts}">
            {$button_style=""}
            {if $b.abt__yt_button_text_color_use == 'Y'}{$button_style="`$button_style`color:`$b.abt__yt_button_text_color`;"}{/if}
            {if $b.abt__yt_button_color_use == 'Y'}{$button_style="`$button_style`background:`$b.abt__yt_button_color`;"}{/if}
            {if $button_style}{$button_style="style={$button_style}"}{/if}
            <span class="ty-btn ty-btn__primary" {$button_style}>{$b.abt__yt_button_text|default:"button"}</span>
        </div>
        {/if}
    </div>

    {** место под попап **}
    {if $b.abt__yt_how_to_open == 'in_popup'}
        <div class="abt-yt-bp-container" data-link="{$b.link_id}"></div>
    {/if}
</div>
{/capture}

{if $b.abt__yt_how_to_open|in_array:['in_new_window','in_this_window']}
    {$url=$b.abt__yt_url}
    {if $b.abt__yt_data_type == "blog" && $b.abt__yt_page_id > 0}
        {$url="pages.view&page_id={$b.abt__yt_page_id}"}
    {/if}
    <a href="{$url|fn_url}" {if $b.abt__yt_how_to_open == 'in_new_window'}target="_blank"{/if}>{$smarty.capture.banner nofilter}</a>
{else}
    {$smarty.capture.banner nofilter}
{/if}