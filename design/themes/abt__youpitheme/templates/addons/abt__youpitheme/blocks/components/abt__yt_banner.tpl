<div class="abyt_banner {$b.abt__yt_class}">

{if $b.abt__yt_button_use == 'N' and $b.abt__yt_url|trim}
    <a {if $b.abt__yt_object == 'video' and $b.abt__yt_youtube_id}data-content="video"{/if} href="{$b.abt__yt_url|fn_url}"{if $b.abt__yt_how_to_open == 'in_new_window'} target="_blank"{/if}>
{/if}
<div class="abyt-a-bg-banner {if $b.abt__yt_background_color =='#ffffff' and $b.abt__yt_background_color_use =="Y"}white-bg{/if} {$b.abt__yt_color_scheme} valign-{$b.abt__yt_content_valign} align-{$b.abt__yt_content_align}" style="background: {$b.abt__yt_background_color} {if !empty($b.abt__yt_background_image) and is_array($b.abt__yt_background_image)}url('{$b.abt__yt_background_image.icon.image_path}') no-repeat center{/if};margin: {$block.properties.margin};height: {$block.properties.height};">

    <div class="bg-opacity-{$b.abt__yt_content_bg}">

        <div class="abyt-a-content" style="height: {$block.properties.height};">

            {if $b.abt__yt_object == 'image' and !empty($b.abt__yt_main_image) and is_array($b.abt__yt_main_image)}
                <div class="abyt-a-img"><div class="box" style="height:{$block.properties.height};"><img src="{$b.abt__yt_main_image.icon.image_path}" alt="{$b.abt__yt_main_image.icon.alt}" title="{$b.abt__yt_main_image.icon.alt}"></div></div>
            {elseif $b.abt__yt_object == 'video' and $b.abt__yt_youtube_id}
                <div class="abyt-a-img abyt-a-video">
                    <div class="box" style="height:{$block.properties.height};" data-banner-youtube-id="{$b.abt__yt_youtube_id}" data-banner-youtube-params="{$b|fn_abt__yt_build_youtube_link:true}&enablejsapi=1&version=3">
                        <img data-type="youtube-img" src="https://img.youtube.com/vi/{$b.abt__yt_youtube_id}/maxresdefault.jpg" alt="{$b.abt__yt_title|strip_tags}">
                    </div>
                </div>
            {/if}

            <div class="abyt-a-content-width{if $b.abt__yt_content_full_width =="Y"}-full{else}-half{/if}">
                <div class="box" style="{if !empty($b.abt__yt_padding)}padding:{$b.abt__yt_padding};{/if}height:{$block.properties.height};">
                <{$b.abt__yt_title_tag} class="abyt-a-title {if $b.abt__yt_title_shadow =="Y"}shadow{/if} weight-{$b.abt__yt_title_font_weight}" style="font-size: {$b.abt__yt_title_font_size};{if $b.abt__yt_title_color_use == 'Y'}color: {$b.abt__yt_title_color};{/if}">{$b.abt__yt_title nofilter}</{$b.abt__yt_title_tag}>
                <div class="abyt-a-descr" style="{if $b.abt__yt_description_color_use == 'Y'}color: {$b.abt__yt_description_color};{/if}{if $b.abt__yt_description_bg_color_use == 'Y'}background-color:{$b.abt__yt_description_bg_color};position: relative;left: 5px;box-shadow: -5px 0 0 0 {$b.abt__yt_description_bg_color}, 5px 0 0 0 {$b.abt__yt_description_bg_color};{/if} font-size: {$b.abt__yt_description_font_size};">{$b.abt__yt_description nofilter}</div>

                {if $b.abt__yt_button_use == 'Y' and $b.abt__yt_url|trim}
                <div class="abyt-bb-a-button">
                    {$button_style=""}
                    {if $b.abt__yt_button_text_color_use == 'Y'}{$button_style="`$button_style`color:`$b.abt__yt_button_text_color`;"}{/if}
                    {if $b.abt__yt_button_color_use == 'Y'}{$button_style="`$button_style`background:`$b.abt__yt_button_color`;"}{/if}
                    {if $button_style}{$button_style="style={$button_style}"}{/if}
                    <a class="ty-btn ty-btn__primary" {$button_style} href="{$b.abt__yt_url|fn_url}"{if $b.abt__yt_how_to_open == 'in_new_window'} target="_blank"{/if}>{$b.abt__yt_button_text|default:"button"}</a>
                </div>
                {/if}
                </div>
            </div>
        </div>
    </div>
</div>
{if $b.abt__yt_button_use == 'N' and $b.abt__yt_url|trim}
    </a>
{/if}
</div>
