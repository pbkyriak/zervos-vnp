{if $page.page_type == $smarty.const.PAGE_TYPE_BLOG}

    {if $subpages}
        {capture name="mainbox_title"}{/capture}
        <div class="ypi-blog">
            {include file="common/pagination.tpl"}
            {foreach from=$subpages item="subpage"}
                <div class="ypi-blog__item">
                    <a href="{"pages.view?page_id=`$subpage.page_id`"|fn_url}">

                        <span class="ypi-blog__img-block">
                            {$image_data=$subpage.main_pair|fn_image_to_display:400:300}
                            <span class="ypi-blog__date">{$subpage.timestamp|date_format:"`$settings.Appearance.date_format`"}</span>
                            <span class="image-cover {if !$subpage.main_pair}no-image{/if}" {if $subpage.main_pair}style="background: url('{$image_data.image_path}') no-repeat center"{/if}></span>
                        </span>

                        <span class="ypi-blog__description">
                        <span class="ypi-blog__post-title">
                            {if $subpage.abt__yt_youtube_id|trim}<span class="m-label rps-video">{__("abt__yt.blog.there_is_video")}</span>{/if}
                            {if $subpage.abt__yt_product_list|trim}<span class="m-label rps-products">{__("abt__yt.blog.there_are_offers")}</span>{/if}
                            {$subpage.page}
                        </span>
                    <div class="ypi-blog__text">{$subpage.spoiler|strip_tags|truncate:130:"..." nofilter}</div>
                    </span>
                    </a>
                </div>
            {/foreach}
            {include file="common/pagination.tpl"}
        </div>
    {/if}

    {capture name="mainbox_title"}<span class="ty-blog__post-title" {live_edit name="page:page:{$page.page_id}"}>{$page.page}</span>{/capture}

{/if}