{** block-description:blog.recent_posts_scroller **}

{if $items}

{assign var="obj_prefix" value="`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="material-icons">&#xE408;</i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="material-icons">&#xE409;</i></div>
            </div>
        </div>
    </div>
{/if}

<div class="ty-mb-l">
    <div class="ty-blog-recent-posts-scroller">
        <div id="scroll_list_{$block.block_id}" class="owl-carousel ty-scroller-list">
        {foreach from=$items item="page"}
            <a href="{"pages.view?page_id=`$page.page_id`"|fn_url}">
            <span class="ty-blog-recent-posts-scroller__item">
                <span class="ty-blog-recent-posts-scroller__img-block">
				{$image_data=$page.main_pair|fn_image_to_display:369}
                <span class="image-cover {if !$page.main_pair}no-image{/if}" {if $page.main_pair}style="background-image: url('{$image_data.image_path}');"{/if}><span class="ty-blog__date">{$page.timestamp|date_format:"`$settings.Appearance.date_format`"}</span></span>
                </span>
                <span class="ypi-posts-scroller__item">
                {if $page.abt__yt_youtube_id|trim}<span class="m-label rps-video">{__("abt__yt.blog.there_is_video")}</span>{/if}
                {if $page.abt__yt_product_list|trim}<span class="m-label rps-products">{__("abt__yt.blog.there_are_offers")}</span>{/if}
                {$page.page}
                </span>
            </span>
            </a>
        {/foreach}

        </div>
    </div>
</div>

{include file="common/scroller_init_with_quantity.tpl" prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}

{/if}
