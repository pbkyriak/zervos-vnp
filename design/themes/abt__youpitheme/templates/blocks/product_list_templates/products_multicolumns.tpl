{** template-description:tmpl_grid **}

{if $settings.abt__yt.product_list.show_sku == 'Y'}
    {assign var="show_sku" value=true}
{/if}

{if $settings.abt__yt.product_list.show_amount == 'Y'}
    {assign var="show_product_amount" value=true}
{/if}

{if $settings.abt__yt.product_list.show_qty == 'Y'}
    {assign var="show_qty" value=true}
{else}
    {assign var="show_qty" value=false}
{/if}

{include file="blocks/list_templates/grid_list_with_banners.tpl"
show_trunc_name=true
show_rating=true
show_old_price=true
show_price=true
hide_qty_label=true
show_sku_label=false
show_amount_label=false
show_features=true
show_descr=true
show_list_discount=true
show_clean_price=true
show_list_buttons=true
but_role="action"
show_discount_label=true
show_add_to_cart=$show_add_to_cart|default:true
abt__yt_banners="category"|fn_abt__yt_get_object_banners:$category_data.category_id:$search
is_category=true
}