{if $products}
    {** Запрашиваем хар-ки **}
    {if $settings.abt__yt.product_list.grid_list_descr == 'features'}
        {** запрашиваем все доступные хар-ки по товарам **}
        {$products=$products|fn_abt__yt_add_products_features_list:0:true}

    {elseif $settings.abt__yt.product_list.show_brand != 'none' and $settings.abt__yt.general.brand_feature_id > 0}
        {** запрашиваем хар-ку БРЕНД по товарам **}
        {$products=$products|fn_abt__yt_add_products_features_list:$settings.abt__yt.general.brand_feature_id:true}
    {/if}

    {script src="js/tygh/exceptions.js"}

    {if !$no_pagination}
        {include file="common/pagination.tpl"}
    {/if}

    {if !$no_sorting}
        {include file="views/products/components/sorting.tpl"}
    {/if}

    {if !$show_empty}
        {split data=$products size=$columns|default:"2" assign="splitted_products"}
    {else}
        {split data=$products size=$columns|default:"2" assign="splitted_products" skip_complete=true}
    {/if}

    {math equation="100 / x" x=$columns|default:"2" assign="cell_width"}
    {if $item_number == "Y"}
        {assign var="cur_number" value=1}
    {/if}

    {* FIXME: Don't move this file *}
    {script src="js/tygh/product_image_gallery.js"}

    {if $settings.Appearance.enable_quick_view == 'Y'}
        {$quick_nav_ids = $products|fn_fields_from_multi_level:"product_id":"product_id"}
    {/if}
    {$product_list_page=$search.page|default:$smarty.request.page}
    <div class="grid-list" {if $is_category}id="product_list_page{$product_list_page}"{/if}>
        {strip}
            {foreach from=$splitted_products item="sproducts" name="sprod"}
                {foreach from=$sproducts item="product" name="sproducts"}
                    <div class="col-tile">
                        {if $product}
                            {assign var="obj_id" value=$product.product_id}
                            {assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
                            {include file="common/product_data.tpl" product=$product}
                            <div class="ty-grid-list__item {if $settings.abt__yt.product_list.grid_list_descr == 'none'}no-features{/if} ty-quick-view-button__wrapper" style="height: {if $show_add_to_cart}{$settings.abt__yt.product_list.height_list_prblock}px{else}{$settings.abt__yt.product_list.height_list_prblock - 48}px{/if}">
                                {assign var="form_open" value="form_open_`$obj_id`"}
                                {$smarty.capture.$form_open nofilter}
                                {hook name="products:product_multicolumns_list"}

                                {if $settings.abt__yt.product_list.show_sku == 'Y' or  $settings.abt__yt.product_list.show_amount == 'Y'}
                                    <div class="stock-block">
                                        {assign var="sku" value="sku_$obj_id"}
                                        {$smarty.capture.$sku nofilter}

                                        {if $settings.abt__yt.product_list.show_brand == 'none'}
                                            {assign var="product_amount" value="product_amount_`$obj_id`"}
                                            {$smarty.capture.$product_amount nofilter}
                                        {/if}
                                    </div>
                                {/if}
                                    <div class="ty-grid-list__image">
                                        {include file="views/products/components/product_icon.tpl" product=$product show_gallery=true}

                                        {assign var="discount_label" value="discount_label_`$obj_prefix``$obj_id`"}
                                        {$smarty.capture.$discount_label nofilter}
                                    </div>
                                    <div class="block-top">
	                                    {hook name="products:ypi_product_list_block_top"}{/hook}
	                                    
                                        {assign var="rating" value="rating_$obj_id"}
                                        {if $smarty.capture.$rating|strlen > 40}
                                            <div class="grid-list__rating">
                                                {if ($ab_dotd_product_ids && $product.product_id|in_array:$ab_dotd_product_ids) or ($product.promotions)}
                                                    <div class="ab_dotd_product_label">{__('ab_dotd_product_label')}</div>
                                                {/if}
                                                {$smarty.capture.$rating nofilter}{if $product.discussion_amount_posts > 0}
                                                    <span class="cn-comments">({$product.discussion_amount_posts})</span>
                                                {/if}
                                            </div>
                                        {else}
                                            <div class="grid-list__rating no-rating">
                                                {if ($ab_dotd_product_ids && $product.product_id|in_array:$ab_dotd_product_ids) or ($product.promotions)}
                                                    <div class="ab_dotd_product_label">{__('ab_dotd_product_label')}</div>
                                                {/if}
                                                {if $addons.discussion.status == "A"}
                                                    <span class="ty-nowrap ty-stars"><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i></span>
                                                {elseif $product.discussion_amount_posts > 0}
                                                    <span class="cn-comments">({$product.discussion_amount_posts})</span>
                                                {/if}
                                            </div>
                                        {/if}
                                    </div>
                                {if $settings.abt__yt.product_list.show_brand == 'as_text' and $settings.abt__yt.general.brand_feature_id > 0}
                                    {$b_feature=$product.abt__yt_features[$settings.abt__yt.general.brand_feature_id]}
                                    <div class="ypi-brand">{$b_feature.variant}</div>
                                {elseif $settings.abt__yt.product_list.show_brand == 'as_logo' and $settings.abt__yt.general.brand_feature_id > 0}
                                    {$b_feature=$product.abt__yt_features[$settings.abt__yt.general.brand_feature_id]}
                                    <div class="ypi-brand-img">
                                        <a href="{"categories.view?category_id=`$product.main_category`&features_hash=`$b_feature.features_hash`"|fn_url}">
                                            {include file="common/image.tpl" image_height=20 images=$b_feature.variants[$b_feature.variant_id].image_pairs no_ids=true}
                                        </a>
                                    </div>
                                {/if}
                                    <div class="ty-grid-list__item-name">
                                        {if $item_number == "Y"}
                                            <span class="item-number">{$cur_number}.&nbsp;</span>
                                            {math equation="num + 1" num=$cur_number assign="cur_number"}
                                        {/if}

                                        {assign var="name" value="name_$obj_id"}
                                        <bdi>{$smarty.capture.$name nofilter}</bdi>
                                    </div>
                                    <div class="block-middle">
                                        {if $settings.abt__yt.product_list.grid_list_descr == 'description'}
                                            <div class="product-description">
                                                {assign var="prod_descr" value="prod_descr_`$obj_id`"}
                                                {$smarty.capture.$prod_descr nofilter}
                                            </div>
                                        {else}
                                            <div class="product-feature">
                                                {assign var="product_features" value="product_features_`$obj_id`"}
                                                {$smarty.capture.$product_features nofilter}
                                            </div>
                                        {/if}
                                    </div>
                                    <div class="block-bottom {if $product.taxed_price}taxed-price{/if}">
                                        <div class="v-align-bottom">

                                            {if !$smarty.capture.capt_options_vs_qty}
                                                <div class="ypi-product__option">
                                                    {assign var="product_options" value="product_options_`$obj_id`"}
                                                    {$smarty.capture.$product_options nofilter}
                                                </div>
                                                <div class="ypi-product__qty">
                                                    {assign var="qty" value="qty_`$obj_id`"}
                                                    {$smarty.capture.$qty nofilter}
                                                </div>
                                            {/if}

                                            <div class="ty-grid-list__price {if $show_qty}qty-wrap{/if} {if $product.price == 0}ty-grid-list__no-price{/if}">
                                                {assign var="old_price" value="old_price_`$obj_id`"}
                                                {if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}

                                                {assign var="price" value="price_`$obj_id`"}
                                                {$smarty.capture.$price nofilter}
                                                
												{if $product.ab__is_qty_discount}
                                                    <div class="qty_discounts_grid">
                                                        <a class="ab__show_qty_discounts ajax-link cm-tooltip" data-ca-product-id="{$product.product_id}" data-ca-target-id="qty_discounts_{$obj_id}" title="{__("qty_discounts")}"><i class="material-icons">&#xE88F;</i></a>
                                                        <div id="qty_discounts_{$obj_id}" class="qty_discounts_popup hidden">
                                                        <!--qty_discounts_{$obj_id}--></div>
                                                    </div>
                                                {/if}

                                                {assign var="clean_price" value="clean_price_`$obj_id`"}
                                                {$smarty.capture.$clean_price nofilter}

                                                {assign var="list_discount" value="list_discount_`$obj_id`"}
                                                {$smarty.capture.$list_discount nofilter}
                                                
                                            </div>

                                            {if $settings.abt__yt.product_list.show_buttons == 'Y'}
                                                <div class="ty-grid-list__control {if $show_list_buttons}show-list-buttons{/if}">
                                                    {if $show_add_to_cart}
                                                        <div class="button-container {if $addons.call_requests.buy_now_with_one_click == "Y"}call-requests__show{/if}">
                                                            {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
                                                            {$smarty.capture.$add_to_cart nofilter}
                                                        </div>
                                                    {/if}
                                                </div>
                                            {/if}

                                        </div>
                                    </div>
                                {/hook}
                                {assign var="form_close" value="form_close_`$obj_id`"}
                                {$smarty.capture.$form_close nofilter}
                            </div>
                        {/if}
                    </div>
                {/foreach}
                {if $show_empty && $smarty.foreach.sprod.last}
                    {assign var="iteration" value=$smarty.foreach.sproducts.iteration}
                    {capture name="iteration"}{$iteration}{/capture}
                    {hook name="products:products_multicolumns_extra"}
                    {/hook}
                    {assign var="iteration" value=$smarty.capture.iteration}
                    {if $iteration % $columns != 0}
                        {math assign="empty_count" equation="c - it%c" it=$iteration c=$columns}
                        {section loop=$empty_count name="empty_rows"}
                            <div class="ty-column{$columns}">
                                <div class="ty-product-empty">
                                    <span class="ty-product-empty__text">{__("empty")}</span>
                                </div>
                            </div>
                        {/section}
                    {/if}
                {/if}
            {/foreach}
        {/strip}
        <!--product_list_page{$product_list_page}--></div>
    {if !$no_pagination}
        {include file="common/pagination.tpl"}
    {/if}

{/if}

{capture name="mainbox_title"}{$title}{/capture}