{hook name="blocks:topmenu_dropdown"}

{if $items}
    <div class="abt_up-menu {if $settings.abt__yt.general.use_scroller_for_menu == 'Y'}extended{/if}">
        <div class="ty-menu__wrapper">
            <ul class="ty-menu__items cm-responsive-menu">
                {hook name="blocks:topmenu_dropdown_top_menu"}
                    <li class="ty-menu__item ty-menu__menu-btn visible-phone">
                        <a class="ty-menu__item-link">
                            <i class="material-icons">&#xE5D2;</i>
                            <span>{__("menu")} <i class="material-icons">&#xE5CF;</i></span>
                        </a>
                    </li>

                {foreach from=$items item="item1" name="item1"}
                    {assign var="cat_image" value=""}
                    {if $item1.category_id > 0}{assign var="cat_image" value=$item1.category_id|fn_get_image_pairs:'category':'M':true:true}{/if}
                    {assign var="item1_url" value=$item1|fn_form_dropdown_object_link:$block.type}
                    {assign var="unique_elm_id" value=$item1_url|md5}
                    {assign var="unique_elm_id" value="topmenu_`$block.block_id`_`$unique_elm_id`"}

                    <li class="ty-menu__item{if !$item1.$childs} ty-menu__item-nodrop{else} cm-menu-item-responsive{/if}{if $item1.active || $item1|fn_check_is_active_menu_item:$block.type} ty-menu__item-active{/if}{if $item1.class} {$item1.class}{/if}">
                        {if $item1.$childs}
                            <a class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
                                <i class="ty-menu__icon-open material-icons">&#xE145;</i>
                                <i class="ty-menu__icon-hide material-icons">&#xE15B;</i>
                            </a>
                        {/if}
                        <a {if $item1_url} href="{$item1_url}"{/if} class="ty-menu__item-link a-first-lvl">{if $item1.abt__yt_mwi__status == 'Y' and $item1.abt__yt_mwi__icon}<img class="ab-ut-mwi-icon" src="{$item1.abt__yt_mwi__icon}" alt="">{/if}<bdi>{$item1.$name}</bdi>{if $item1.abt__yt_mwi__status == 'Y' and $item1.abt__yt_mwi__label}<span class="m-label" style="color: {$item1.abt__yt_mwi__label_color}; background-color: {$item1.abt__yt_mwi__label_background}">{$item1.abt__yt_mwi__label}</span>{/if}{if $item1.$childs}<i class="icon-down-dir material-icons">&#xE5CF;</i>{/if}</a>

                        {if $item1.$childs}

                            {if !$item1.$childs|fn_check_second_level_child_array:$childs}
                                {* Only two levels. Vertical output *}
                                <div class="ty-menu__submenu">
                                    <ul class="ty-menu__submenu-items ty-menu__submenu-items-simple {if $item1.abt__yt_mwi__text}with-pic{/if} cm-responsive-menu-submenu">
                                        {hook name="blocks:topmenu_dropdown_2levels_elements"}

                                        {foreach from=$item1.$childs item="item2" name="item2"}
                                            {assign var="item_url2" value=$item2|fn_form_dropdown_object_link:$block.type}
                                            <li class="ty-menu__submenu-item{if $item2.active || $item2|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}{if $item2.class} {$item2.class}{/if}">
                                                <a class="ty-menu__submenu-link" {if $item_url2} href="{$item_url2}"{/if}><bdi>{$item2.$name}</bdi></a>
                                            </li>
                                        {/foreach}
                                        {if $item1.show_more && $item1_url}
                                            <li class="ty-menu__submenu-item ty-menu__submenu-alt-link">
                                                <a href="{$item1_url}"
                                                   class="ty-menu__submenu-alt-link">{__("text_topmenu_view_more")}</a>
                                            </li>
                                        {/if}
                                        {if $item1.abt__yt_mwi__status == 'Y' and $item1.abt__yt_mwi__text}
                                            <li class="ypi-mwi-html {if $item1.abt__yt_mwi__dropdown == "Y"}bottom{else}{$item1.abt__yt_mwi__text_position}{/if}">{$item1.abt__yt_mwi__text nofilter}</li>
                                        {/if}

                                        {/hook}
                                    </ul>
                                </div>
                            {else}
                                <div class="ty-menu__submenu" id="{$unique_elm_id}">
                                    {hook name="blocks:topmenu_dropdown_3levels_cols"}
                                        <ul class="ty-menu__submenu-items {if $item1.abt__yt_mwi__text}with-pic{/if} cm-responsive-menu-submenu">
                                            {assign var="rows" value=(($item1.$childs|count)/5)|ceil}
                                            {split data=$item1.$childs size=$rows assign="splitted_categories" skip_complete=true}
                                            {foreach from=$splitted_categories item="row"}
                                                <ul class="ty-menu__submenu-col">
                                                    {foreach from=$row item="item2" name="item2"}
                                                        <li class="ty-top-mine__submenu-col">
                                                            {assign var="item2_url" value=$item2|fn_form_dropdown_object_link:$block.type}
                                                            <div class="ty-menu__submenu-item-header{if $item2.active || $item2|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-header-active{/if}{if $item2.class} {$item2.class}{/if}">
                                                                <a{if $item2_url} href="{$item2_url}"{/if} class="ty-menu__submenu-link"><bdi>{$item2.$name}</bdi>{if $item2.abt__yt_mwi__status == 'Y' and $item2.abt__yt_mwi__label}<span class="m-label" style="color: {$item2.abt__yt_mwi__label_color}; background-color: {$item2.abt__yt_mwi__label_background}">{$item2.abt__yt_mwi__label}</span>{/if}</a>
                                                            </div>
                                                            {if $item2.$childs}
                                                                <a class="ty-menu__item-toggle visible-phone cm-responsive-menu-toggle">
                                                                    <i class="ty-menu__icon-open material-icons">&#xE145;</i>
                                                                    <i class="ty-menu__icon-hide material-icons">&#xE15B;</i>
                                                                </a>
                                                            {/if}
                                                            <div class="ty-menu__submenu">
                                                                <ul class="ty-menu__submenu-list cm-responsive-menu-submenu">
                                                                    {if $item2.$childs}
                                                                        {hook name="blocks:topmenu_dropdown_3levels_col_elements"}
                                                                        {foreach from=$item2.$childs item="item3" name="item3"}
                                                                            {assign var="item3_url" value=$item3|fn_form_dropdown_object_link:$block.type}
                                                                            <li class="ty-menu__submenu-item{if $item3.active || $item3|fn_check_is_active_menu_item:$block.type} ty-menu__submenu-item-active{/if}{if $item3.class} {$item3.class}{/if}">
                                                                                <a{if $item3_url} href="{$item3_url}"{/if} class="ty-menu__submenu-link"><bdi>{$item3.$name}</bdi>{if $item3.abt__yt_mwi__status == 'Y' and $item3.abt__yt_mwi__label}<span class="m-label" style="color: {$item3.abt__yt_mwi__label_color};background-color: {$item3.abt__yt_mwi__label_background}">{$item3.abt__yt_mwi__label}</span>{/if}</a>
                                                                            </li>
                                                                        {/foreach}
                                                                        {if $item2.show_more && $item2_url}
                                                                            <li class="ty-menu__submenu-item ty-menu__submenu-alt-link">
                                                                                <a href="{$item2_url}" class="ty-menu__submenu-link">{__("text_topmenu_view_more")}</a>
                                                                            </li>
                                                                        {/if}
                                                                        {/hook}
                                                                    {/if}
                                                                </ul>
                                                            </div>
                                                        </li>
                                                    {/foreach}
                                                </ul>
                                            {/foreach}

                                            {if $item1.abt__yt_mwi__status == 'Y' and $item1.abt__yt_mwi__text}
                                                <li class="ypi-mwi-html {if $item1.abt__yt_mwi__dropdown == "Y"}bottom{else}{$item1.abt__yt_mwi__text_position}{/if}">{$item1.abt__yt_mwi__text nofilter}</li>
                                            {else}
                                                {if $item1.show_more && $item1_url}
                                                    <li class="ty-menu__submenu-item ty-menu__submenu-alt-link">
                                                        <a href="{$item1_url}">{__("text_topmenu_more", ["[item]" => $item1.$name])}</a>
                                                    </li>
                                                {/if}
                                            {/if}

                                        </ul>
                                    {/hook}
                                </div>
                            {/if}

                        {/if}
                    </li>
                {/foreach}
                {/hook}

            </ul>
        </div>
        {if $settings.abt__yt.general.use_scroller_for_menu == 'Y'}
            <div class="abt_yp_menu-show_more hidden-phone">9/9</div>
        {/if}
    </div>
{/if}
{/hook}
