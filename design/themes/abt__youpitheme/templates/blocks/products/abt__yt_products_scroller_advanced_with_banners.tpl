{** block-description:abt__yt_products_scroller_advanced_with_banners **}

{if $block.properties.enable_quick_view == "Y"}
    {$quick_nav_ids = $items|fn_fields_from_multi_level:"product_id":"product_id"}
{/if}

{if $block.properties.hide_add_to_cart_button == "Y"}
    {assign var="show_add_to_cart" value=false}
{else}
    {assign var="show_add_to_cart" value=true}
{/if}

{if $settings.abt__yt.product_list.show_qty == 'Y'}
    {assign var="show_qty" value=true}
{else}
    {assign var="show_qty" value=false}
{/if}

{if $settings.abt__yt.product_list.show_sku == 'Y'}
    {assign var="show_sku" value=true}
{/if}

{if $settings.abt__yt.product_list.show_amount == 'Y'}
    {assign var="show_product_amount" value=true}
{/if}

{$abt__yt_banners="block"|fn_abt__yt_get_object_banners:$block.block_id}

{assign var="show_descr" value=true}
{assign var="show_features" value=true}
{assign var="show_trunc_name" value=false}
{assign var="show_name" value=true}
{assign var="show_old_price" value=true}
{assign var="show_price" value=true}
{assign var="show_rating" value=true}
{assign var="show_clean_price" value=true}
{assign var="hide_qty_label" value=true}
{assign var="show_sku_label" value=false}
{assign var="show_amount_label" value=false}
{assign var="show_list_discount" value=true}
{assign var="show_list_buttons" value=true}
{assign var="but_role" value="action"}
{assign var="show_discount_label" value=true}

{* FIXME: Don't move this file *}
{script src="js/tygh/product_image_gallery.js"}

{assign var="obj_prefix" value="`$block.block_id`000"}
{$block.properties.outside_navigation = "N"}

<div id="scroll_list_{$block.block_id}" class="jcarousel-skin owl-carousel ty-scroller-list grid-list {if !$show_add_to_cart}no-buttons{/if} ty-scroller-advanced">

    {strip}
    {foreach from=$items item="product" name="for_products"}
        {hook name="products:product_scroller_advanced_list"}
        {** Вставка баннера **}
        {if !empty($product.product_id)}
            {$product.product_id|fn_abt__yt_insert_banners_into_list:$abt__yt_banners:$items:$search:"ypi-scroller-list__item" nofilter}
        {/if}
        <div class="ypi-scroller-list__item">
            {if $product}
                {assign var="obj_id" value=$product.product_id}
                {assign var="obj_id_prefix" value="`$obj_prefix``$product.product_id`"}
                {include file="common/product_data.tpl" product=$product}
                <div class="ty-grid-list__item {if $block.properties.hide_add_to_cart_button == "Y"}no-buttons{/if} {if $settings.abt__yt.product_list.grid_list_descr == 'none'}no-features{/if} ty-quick-view-button__wrapper" 
	                style="height: 
		                {if $show_add_to_cart}
							{if $settings.abt__yt.product_list.show_qty == 'Y'}
								{$settings.abt__yt.product_list.height_list_prblock + 20}px
									{else}
								{$settings.abt__yt.product_list.height_list_prblock}px
							{/if}
				        {else}
							{$settings.abt__yt.product_list.height_list_prblock - 48}px   
						{/if}">
                    {assign var="form_open" value="form_open_`$obj_id`"}
                    {$smarty.capture.$form_open nofilter}
                    {hook name="products:product_multicolumns_list"}

                    {if $settings.abt__yt.product_list.show_sku == 'Y' or  $settings.abt__yt.product_list.show_amount == 'Y'}
                        <div class="stock-block">
                            {assign var="sku" value="sku_$obj_id"}
                            {$smarty.capture.$sku nofilter}

                            {if $settings.abt__yt.product_list.show_brand == 'none'}
                                {assign var="product_amount" value="product_amount_`$obj_id`"}
                                {$smarty.capture.$product_amount nofilter}
                            {/if}
                        </div>
                    {/if}
                        <div class="ty-grid-list__image">
                            {include file="views/products/components/product_icon.tpl" product=$product show_gallery=false}

                            {assign var="discount_label" value="discount_label_`$obj_prefix``$obj_id`"}
                            {$smarty.capture.$discount_label nofilter}
                        </div>
                        <div class="block-top">
	                        {hook name="products:ypi_product_list_block_top"}{/hook}
                            {assign var="rating" value="rating_$obj_id"}
                            {if $smarty.capture.$rating|strlen > 40}
                                <div class="grid-list__rating">
                                    {if ($ab_dotd_product_ids && $product.product_id|in_array:$ab_dotd_product_ids) or ($product.promotions)}
                                        <div class="ab_dotd_product_label">{__('ab_dotd_product_label')}</div>
                                    {/if}
                                    {$smarty.capture.$rating nofilter}{if $product.discussion_amount_posts > 0}
                                        <span class="cn-comments">({$product.discussion_amount_posts})</span>
                                    {/if}
                                </div>
                            {else}
                                <div class="grid-list__rating no-rating">
                                    {if ($ab_dotd_product_ids && $product.product_id|in_array:$ab_dotd_product_ids) or ($product.promotions)}
                                        <div class="ab_dotd_product_label">{__('ab_dotd_product_label')}</div>
                                    {/if}
                                    {if $addons.discussion.status == "A"}
                                        <span class="ty-nowrap ty-stars"><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i><i class="ty-icon-star-empty"></i></span>
                                    {elseif $product.discussion_amount_posts > 0}
                                        <span class="cn-comments">({$product.discussion_amount_posts})</span>
                                    {/if}
                                </div>
                            {/if}
                        </div>
                    {if $settings.abt__yt.product_list.grid_list_descr == 'description' and $settings.abt__yt.product_list.show_brand == 'as_text' and $settings.abt__yt.general.brand_feature_id > 0}
                        {$b_feature=$product.abt__yt_features[$settings.abt__yt.general.brand_feature_id]}
                        <div class="ypi-brand">{$b_feature.variant}</div>
                    {elseif $settings.abt__yt.product_list.show_brand == 'as_logo'}
                        {$b_feature=$product.abt__yt_features[$settings.abt__yt.general.brand_feature_id]}
                        <div class="ypi-brand-img">
                            <a href="{"categories.view?category_id=`$product.main_category`&features_hash=`$b_feature.features_hash`"|fn_url}">
                                {include file="common/image.tpl" image_height=20 images=$b_feature.variants[$b_feature.variant_id].image_pairs no_ids=true}
                            </a>
                        </div>
                    {/if}
                        <div class="ty-grid-list__item-name">
                            {if $item_number == "Y"}
                                <span class="item-number">{$cur_number}.&nbsp;</span>
                                {math equation="num + 1" num=$cur_number assign="cur_number"}
                            {/if}

                            {assign var="name" value="name_$obj_id"}
                            {$smarty.capture.$name nofilter}
                        </div>
                        <div class="block-middle">
                            {if $settings.abt__yt.product_list.grid_list_descr == 'description'}
                                <div class="product-description">
                                    {assign var="prod_descr" value="prod_descr_`$obj_id`"}
                                    {$smarty.capture.$prod_descr nofilter}
                                </div>
                            {else}
                                <div class="product-feature">
                                    {assign var="product_features" value="product_features_`$obj_id`"}
                                    {$smarty.capture.$product_features nofilter}
                                </div>
                            {/if}
                        </div>
                        <div class="block-bottom">
                            <div class="v-align-bottom">

                                {if !$smarty.capture.capt_options_vs_qty}
                                    <div class="ypi-product__option">
                                        {assign var="product_options" value="product_options_`$obj_id`"}
                                        {$smarty.capture.$product_options nofilter}
                                    </div>
                                    <div class="ypi-product__qty">
                                        {assign var="qty" value="qty_`$obj_id`"}
                                        {$smarty.capture.$qty nofilter}
                                    </div>
                                {/if}

                                <div class="ty-grid-list__price {if $show_qty}qty-wrap{/if} {if $product.price == 0}ty-grid-list__no-price{/if}">
                                    {assign var="old_price" value="old_price_`$obj_id`"}
                                    {if $smarty.capture.$old_price|trim}{$smarty.capture.$old_price nofilter}{/if}

                                    {assign var="price" value="price_`$obj_id`"}
                                    {$smarty.capture.$price nofilter}
                                    
									{if $product.ab__is_qty_discount}
                                        <div class="qty_discounts_grid">
                                            <a class="ab__show_qty_discounts ajax-link cm-tooltip" data-ca-product-id="{$product.product_id}" data-ca-target-id="qty_discounts_{$obj_id}" title="{__("qty_discounts")}"><i class="material-icons">&#xE88F;</i></a>
                                            <div id="qty_discounts_{$obj_id}" class="qty_discounts_popup hidden">
                                            <!--qty_discounts_{$obj_id}--></div>
                                        </div>
                                    {/if}

                                    {assign var="clean_price" value="clean_price_`$obj_id`"}
                                    {$smarty.capture.$clean_price nofilter}

                                    {assign var="list_discount" value="list_discount_`$obj_id`"}
                                    {$smarty.capture.$list_discount nofilter}
                                </div>

                                {if $settings.abt__yt.product_list.show_buttons == 'Y'}
                                    <div class="ty-grid-list__control {if $show_list_buttons}show-list-buttons{/if}">
                                        {if $show_add_to_cart}
                                            <div class="button-container {if $addons.call_requests.buy_now_with_one_click == "Y"}call-requests__show{/if}">
                                                {assign var="add_to_cart" value="add_to_cart_`$obj_id`"}
                                                {$smarty.capture.$add_to_cart nofilter}
                                            </div>
                                        {/if}
                                    </div>
                                {/if}
                            </div>
                        </div>
                    {/hook}
                    {assign var="form_close" value="form_close_`$obj_id`"}
                    {$smarty.capture.$form_close nofilter}
                </div>
            {/if}
        </div>
        {/hook}
    {/foreach}
    {/strip}
</div>

{include file="common/scroller_init.tpl"}