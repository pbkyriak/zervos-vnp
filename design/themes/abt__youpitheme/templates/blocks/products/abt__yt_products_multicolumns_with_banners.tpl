{** block-description:abt__yt_products_multicolumns_with_banners **}

{if $block.properties.hide_add_to_cart_button == "Y"}
    {assign var="_show_add_to_cart" value=false}
{else}
    {assign var="_show_add_to_cart" value=true}
{/if}

{if $settings.abt__yt.product_list.show_sku == 'Y'}
    {assign var="show_sku" value=true}
{/if}

{if $settings.abt__yt.product_list.show_amount == 'Y'}
    {assign var="show_product_amount" value=true}
{/if}

{if $settings.abt__yt.product_list.show_qty == 'Y'}
    {assign var="show_qty" value=true}
    {else}
    {assign var="show_qty" value=false}
{/if}

{include file="blocks/list_templates/grid_list_with_banners.tpl"
products=$items
columns=$block.properties.number_of_columns
form_prefix="block_manager"
no_sorting="Y"
no_pagination="Y"
no_ids="Y"
obj_prefix="`$block.block_id`000"
item_number=$block.properties.item_number
show_name=true
hide_qty_label=true
show_sku_label=false
show_amount_label=false
show_old_price=true
show_price=true
show_rating=true
show_features=true
show_descr=true
show_clean_price=true
show_list_discount=true
show_add_to_cart=$_show_add_to_cart
show_list_buttons=true
but_role="action"
show_discount_label=true
abt__yt_banners="block"|fn_abt__yt_get_object_banners:$block.block_id
}