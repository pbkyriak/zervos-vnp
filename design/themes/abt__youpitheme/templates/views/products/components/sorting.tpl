<div class="ypi-sort-container">
{if !$config.tweaks.disable_dhtml}
    {assign var="ajax_class" value="cm-ajax"}
{/if}

{assign var="curl" value=$config.current_url|fn_query_remove:"sort_by":"sort_order":"result_ids":"layout"}
{assign var="sorting" value=""|fn_get_products_sorting}
{assign var="sorting_orders" value=""|fn_get_products_sorting_orders}
{assign var="layouts" value=""|fn_get_products_views:false:false}
{assign var="pagination_id" value=$id|default:"pagination_contents"}
{assign var="avail_sorting" value=$settings.Appearance.available_product_list_sortings}

{if $search.sort_order_rev == "asc"}
    {capture name="sorting_text"}
        <a>{$sorting[$search.sort_by].description}<i class="ty-icon-up-dir"></i></a>
    {/capture}
{else}
    {capture name="sorting_text"}
        <a>{$sorting[$search.sort_by].description}<i class="ty-icon-down-dir"></i></a>
    {/capture}
{/if}

<div class="ypi-sort-buttons">
{if $avail_sorting}
    {include file="common/sorting.tpl"}
{/if}

{assign var="pagination" value=$search|fn_generate_pagination}

{if $pagination.total_items}
{assign var="range_url" value=$config.current_url|fn_query_remove:"items_per_page":"page"}
{assign var="product_steps" value=$settings.Appearance.columns_in_products_list|fn_get_product_pagination_steps:$settings.Appearance.products_per_page}
<div class="ty-sort-dropdown">
<a id="sw_elm_pagination_steps" class="ty-sort-dropdown__wrapper cm-combination"><i class="material-icons">&#xE5C3;</i></a>
    <ul id="elm_pagination_steps" class="ty-sort-dropdown__content cm-popup-box hidden">
	    	<li class="ypi-sort-active-item"><span>{$pagination.items_per_page} {__("per_page")}</span></li>
        {foreach from=$product_steps item="step"}
        {if $step != $pagination.items_per_page}
            <li class="ty-sort-dropdown__content-item">
                {hook name="ab__so_seohide:items_per_page_link"}
                    <a class="{$ajax_class} ty-sort-dropdown__content-item-a" href="{"`$range_url`&items_per_page=`$step`"|fn_url}" data-ca-target-id="{$pagination_id}" rel="nofollow">{$step} {__("per_page")}</a>
                {/hook}
            </li>
        {/if}
        {/foreach}
    </ul>
</div>
{/if}
</div>

</div>

