{include file="common/breadcrumbs.tpl"}

{* Horizontal filters *}
{if $settings.abt__yt.general.block_filters_id !=="" && $products}
    <div class="ypi-filters-container">
        <a class="ypi-white-vfbt"><i class="material-icons">&#xE16D;</i></a>
        <span class="f-title hidden">{__("filters")}</span>
    </div>
{/if}
{* End *}

<div id="product_features_{$block.block_id}">

{if $products}
{assign var="layouts" value=""|fn_get_products_views:false:0}
{if $layouts.$selected_layout.template}
    {include file="`$layouts.$selected_layout.template`" columns=$settings.Appearance.columns_in_products_list}
{/if}
{else}
    <p class="ty-no-items">{__("text_no_products")}</p>
{/if}

<div class="ty-feature">
    <div class="ty-feature__description ty-wysiwyg-content">
	    {if $variant_data.image_pair}
	    <div class="ty-feature__image">
	        {include file="common/image.tpl" images=$variant_data.image_pair}
	    </div>
	    {/if}
        {if $variant_data.url}
        <p><a href="{$variant_data.url}">{$variant_data.url}</a></p>
        {/if}
        {$variant_data.description nofilter}
    </div>
</div>

<!--product_features_{$block.block_id}--></div>
{capture name="mainbox_title"}{$variant_data.variant nofilter}{/capture}