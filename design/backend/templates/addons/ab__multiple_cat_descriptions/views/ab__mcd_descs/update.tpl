{if $ab__mcd_desc}
{assign var="id" value=$ab__mcd_desc.desc_id}
{assign var="return_url" value=$smarty.request.return_url}
{else}
{assign var="id" value=0}
{/if}
{assign var="allow_save" value=true}
{if $separate}
{script src="js/tygh/tabs.js"}
{/if}
{if $separate}{capture name="mainbox"}{/if}
<div id="content_group{$id}">
<form action="{""|fn_url}" method="post" id="update_ab__mcd_descs_form_{$id}" name="update_ab__mcd_descs_form_{$id}" class="form-horizontal form-edit cm-disable-empty-files" enctype="multipart/form-data">
<input type="hidden" name="redirect_url" value="{$config.current_url}" />
<input type="hidden" name="desc_id" value="{$id}" />
{if $separate}
<input type="hidden" name="separate" value="Y" >
{/if}
<input type="hidden" name="desc_data[category_id]" value="{$ab__mcd_desc.category_id|default:$category_id}" />
<div class="cm-tabs-content" id="tabs_content_{$id}">
<div id="content_tab_join_{$id}">
<fieldset>
<div class="control-group">
<label class="control-label" for="elm_desc_position_{$id}">{__("ab__mcd_position")}</label>
<div class="controls">
<input type="text" name="desc_data[position]" value="{$ab__mcd_desc.position|default:0}" class="input-micro cm-decimal cm-integer" id="elm_desc_position_{$id}" />
</div>
</div>
<div class="control-group">
<label class="control-label cm-required" for="elm_desc_title_{$id}">{__("ab__mcd_title")}</label>
<div class="controls">
<input class="span8 cm-trim" type="text" name="desc_data[title]" value="{$ab__mcd_desc.title}" id="elm_desc_title_{$id}" />
</div>
</div>
<div class="control-group">
<label class="control-label cm-required" for="elm_desc_description_{$id}">{__("ab__mcd_description")}</label>
<div class="controls">
<textarea class="span8 cm-wysiwyg" type="text" name="desc_data[description]" id="elm_desc_description_{$id}">{$ab__mcd_desc.description}</textarea>
</div>
</div>
{include file="common/select_status.tpl" input_name="desc_data[status]" id="elm_desc_status_{$id}" obj_id=$id obj=$ab__mcd_desc}
</fieldset>
</div>
</div>
<div class="buttons-container">
{if $id}
{if !$allow_save && $shared_product != "Y"}
{assign var="hide_first_button" value=true}
{/if}
{/if}
{if $separate}
{capture name="buttons"}
{include file="buttons/save_cancel.tpl" but_name="dispatch[ab__mcd_descs.update]" but_target_form="update_ab__mcd_descs_form_`$id`" hide_second_button=true save=true}
{/capture}
{else}
{include file="buttons/save_cancel.tpl" but_name="dispatch[ab__mcd_descs.update]" cancel_action="close" extra="" hide_first_button=$hide_first_button save=$id}
{/if}
</div>
</form>
</div>
{if $separate}
{capture name='title'}
{__("ab__mcd_popup_edit", ["[name]" => $ab__mcd_desc.title])}
{/capture}
{/capture}
{include file="common/mainbox.tpl" title=$smarty.capture.title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=true}
{/if}
