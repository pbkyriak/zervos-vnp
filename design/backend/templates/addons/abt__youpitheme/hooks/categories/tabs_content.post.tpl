<div id="content_abt__yt_banners">
{** таблица связей Категории с баннерами **}
{include file="addons/abt__youpitheme/views/abt__yt/product_list_banners.tpl"
d_name="category_data"
d_id="elm_category"
banners_use=$category_data.abt__yt_banners_use
banner_max_position=$category_data.abt__yt_banner_max_position|default:100
abt__yt_product_list_banners="category"|fn_abt__yt_get_product_list_banners:$category_data.category_id
abt__yt_banners=fn_abt__yt_get_abyt_banners()
}
</div>
