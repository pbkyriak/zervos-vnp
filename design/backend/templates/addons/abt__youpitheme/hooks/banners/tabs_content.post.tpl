{if $banner.type|in_array:['abyt','abyt_advanced']}
<div id="content_{$banner.type}">
<h4 class="ty-subheader">{__("abt__yt.banner.params_of_block")}</h4>
{$field="color_scheme"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div for="elm_banner_{$elm}" class="controls">
<select class="" name="banner_data[{$elm}]" id="elm_banner_{$elm}">
{foreach ['light', 'dark'] as $e}
<option value="{$e}" {if $banner.$elm == $e}selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.{$e}")}</option>
{/foreach}
</select>
</div>
</div>
{$field="content_valign"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
<select class="" name="banner_data[{$elm}]" id="elm_banner_{$elm}">
<option value="top" {if $banner.$elm == 'top'} selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.top")}</option>
<option value="center" {if $banner.$elm == 'center' or !$banner.$elm} selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.center")}</option>
<option value="bottom" {if $banner.$elm == 'bottom'} selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.bottom")}</option>
</select>
</div>
</div>
{$field="content_align"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
<select class="" name="banner_data[{$elm}]" id="elm_banner_{$elm}">
<option value="left" {if $banner.$elm == 'left'} selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.left")}</option>
<option value="center" {if $banner.$elm == 'center' or !$banner.$elm} selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.center")}</option>
<option value="right" {if $banner.$elm == 'right'} selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.right")}</option>
</select>
</div>
</div>
{$field="padding"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div for="elm_banner_{$elm}" class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm|default:"20px 20px 20px 20px"}" size="25" class="" />
</div>
</div>
{$field="content_full_width"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} />
</div>
</div>
<hr>
<h4 class="ty-subheader">{__("abt__yt.banner.params_of_title")}</h4>
{$field="title"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label{if $banner.type == 'abyt'} cm-required{/if}">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
</div>
</div>
{$field="title_font_size"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm|default:"18px"}" size="25" class="input-mini" />
</div>
</div>
{$field="title_color"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div for="elm_banner_{$elm}" class="controls">
{include file="addons/abt__youpitheme/hooks/banners/components/colorpicker.tpl"}
</div>
</div>
{$field="title_font_weight"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
<select class="" name="banner_data[{$elm}]" id="elm_banner_{$elm}">
{foreach ['300', '400', '700', '900'] as $e}
<option value="{$e}" {if $banner.$elm == $e}selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.{$e}")}</option>
{/foreach}
</select>
</div>
</div>
{$field="title_tag"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
<select class="" name="banner_data[{$elm}]" id="elm_banner_{$elm}">
{foreach ['div', 'h1', 'h2', 'h3'] as $e}
<option value="{$e}" {if $banner.$elm == $e}selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.{$e}")}</option>
{/foreach}
</select>
</div>
</div>
{$field="title_shadow"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} />
</div>
</div>
<hr>
<h4 class="ty-subheader">{__("abt__yt.banner.params_of_description")}</h4>
{$field="description"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
<textarea id="elm_banner_{$elm}" name="banner_data[{$elm}]" cols="35" rows="6" class="input-large">{$banner.$elm}</textarea>
</div>
</div>
{$field="description_font_size"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm|default:"13px"}" size="25" class="input-small" />
</div>
</div>
{$field="description_color"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div for="elm_banner_{$elm}" class="controls">
{include file="addons/abt__youpitheme/hooks/banners/components/colorpicker.tpl"}
</div>
</div>
{$field="description_bg_color"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div for="elm_banner_{$elm}" class="controls">
{include file="addons/abt__youpitheme/hooks/banners/components/colorpicker.tpl"}
</div>
</div>
<hr>
<h4 class="ty-subheader">{__("image")}</h4>
{if $banner.type == 'abyt_advanced'}
{** Управление типом внутренней картинки **}
<script type="text/javascript">
function fn_change_object_type(v) {
var img_obj = $('.control-group.object-image');
var vd_obj = $('.control-group.object-video');
switch (v){
case 'image':
img_obj.removeClass('hidden');
vd_obj.addClass('hidden');
break;
case 'video':
img_obj.addClass('hidden');
vd_obj.removeClass('hidden');
break;
}
}
</script>
{$field="object"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
<select class="" name="banner_data[{$elm}]" id="elm_banner_{$elm}" onchange="fn_change_object_type(this.value);">
{foreach ['image', 'video'] as $e}
<option value="{$e}" {if $banner.$elm == $e}selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.{$e}")}</option>
{/foreach}
</select>
</div>
</div>
{$field="main_image"}{$elm="abt__yt_$field"}
<div class="control-group{if $banner.abt__yt_object != 'image'} hidden{/if} object-image">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
{include file="common/attach_images.tpl" image_name=$elm image_object_type="abt__yt_banners" image_type="M" image_pair=$banner.$elm image_object_id=$id no_detailed=true hide_titles=true}
</div>
</div>
{$field="youtube_id"}{$elm="abt__yt_$field"}
<div class="control-group{if $banner.abt__yt_object != 'video'} hidden{/if} object-video">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
</div>
</div>
{$field="youtube_playlist"}{$elm="abt__yt_$field"}
<div class="control-group{if $banner.abt__yt_object != 'video'} hidden{/if} object-video">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
</div>
</div>
{$field="youtube_autoplay"}{$elm="abt__yt_$field"}
{*Value 0 (default): The video will not play automatically when the player loads.*}
{*Value 1: The video will play automatically when the player loads.*}
<div class="control-group hidden">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="Y" />
</div>
</div>
{$field="youtube_loop"}{$elm="abt__yt_$field"}
{*Value 0 (default): The video will play only once. *}
{*Value 1: The video will loop (forever). *}
<div class="control-group{if $banner.abt__yt_object != 'video'} hidden{/if} object-video">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} />
</div>
</div>
{$field="youtube_hide_controls"}{$elm="abt__yt_$field"}
{*Value 0: Player controls does not display.*}
{*Value 1 (default): Player controls display.*}
<div class="control-group{if $banner.abt__yt_object != 'video'} hidden{/if} object-video">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} />
</div>
</div>
{else}
{$field="main_image"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
{include file="common/attach_images.tpl" image_name=$elm image_object_type="abt__yt_banners" image_type="M" image_pair=$banner.$elm image_object_id=$id no_detailed=true hide_titles=true}
</div>
</div>
{/if}
{$field="background_image"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
{include file="common/attach_images.tpl" image_name=$elm image_object_type="abt__yt_banners" image_type="A" image_pair=$banner.$elm image_object_id=$id no_detailed=true hide_titles=true}
</div>
</div>
{$field="background_color"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
{include file="addons/abt__youpitheme/hooks/banners/components/colorpicker.tpl"}
</div>
</div>
<hr>
<h4 class="ty-subheader">{__("abt__yt.banner.params_of_button")}</h4>
{** Управление кнопкой **}
<script language="javascript">
function fn_button_use(el) {
if (!el.checked){
Tygh.$('#abt__yt_button_text,#abt__yt_button_text_color,#abt__yt_button_color').addClass('hidden');
}else{
Tygh.$('#abt__yt_button_text,#abt__yt_button_text_color,#abt__yt_button_color').removeClass('hidden');
}
}
</script>
{$field="button_use"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} onclick="fn_button_use(this);" />
</div>
</div>
{$field="button_text"}{$elm="abt__yt_$field"}
<div class="control-group {if $banner.abt__yt_button_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="" />
</div>
</div>
{$field="button_text_color"}{$elm="abt__yt_$field"}
<div class="control-group {if $banner.abt__yt_button_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
{include file="addons/abt__youpitheme/hooks/banners/components/colorpicker.tpl"}
</div>
</div>
{$field="button_color"}{$elm="abt__yt_$field"}
<div class="control-group {if $banner.abt__yt_button_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
{include file="addons/abt__youpitheme/hooks/banners/components/colorpicker.tpl"}
</div>
</div>
<hr>
<h4 class="ty-subheader">{__("abt__yt.banner.params_additional")}</h4>
{$field="content_bg"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} />
</div>
</div>
{$field="how_to_open"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
{$how_to_opens=['in_this_window', 'in_new_window']}
{if $banner.type == 'abyt'}{$how_to_opens[]='in_popup'}{/if}
<select class="" name="banner_data[{$elm}]" id="elm_banner_{$elm}">
{foreach $how_to_opens as $e}
<option value="{$e}" {if $banner.$elm == $e}selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.{$e}")}</option>
{/foreach}
</select>
</div>
</div>
{** Задать период действия баннера **}
<script language="javascript">
function fn_activate_calendar(el) {
Tygh.$('#elm_banner_abt__yt_avail_from').prop('disabled', !el.checked);
Tygh.$('#elm_banner_abt__yt_avail_till').prop('disabled', !el.checked);
if (!el.checked){
Tygh.$('#period_abt__yt_avail_from,#period_abt__yt_avail_till').addClass('hidden');
}else{
Tygh.$('#period_abt__yt_avail_from,#period_abt__yt_avail_till').removeClass('hidden');
}
}
</script>
{$field="use_avail_period"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N">
<span class="checkbox">
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" {if $banner.$elm == "Y"}checked="checked"{/if} value="Y" onclick="fn_activate_calendar(this);">
</span>
</div>
</div>
{capture name="calendar_disable"}{if $banner.$elm != "Y"}disabled="disabled"{/if}{/capture}
{$field="avail_from"}{$elm="abt__yt_$field"}
<div class="control-group {if $banner.abt__yt_use_avail_period == 'N'}hidden{/if}" id="period_{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
{include file="common/calendar.tpl" date_id="elm_banner_{$elm}" date_name="banner_data[{$elm}]" date_val=$banner.$elm|default:$smarty.const.TIME start_year=$settings.Company.company_start_year extra=$smarty.capture.calendar_disable}
</div>
</div>
{$field="avail_till"}{$elm="abt__yt_$field"}
<div class="control-group {if $banner.abt__yt_use_avail_period == 'N'}hidden{/if}" id="period_{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
{include file="common/calendar.tpl" date_id="elm_banner_{$elm}" date_name="banner_data[{$elm}]" date_val=$banner.$elm|default:$smarty.const.TIME start_year=$settings.Company.company_start_year extra=$smarty.capture.calendar_disable}
</div>
</div>
<hr>
{$field="class"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
</div>
</div>
<hr>
<h4 class="ty-subheader">{__("abt__yt.banner.params_of_content")}</h4>
{** Отображаем эти настройки для типа abyt_advanced **}
{if $banner.type == 'abyt_advanced'}
{$field="data_type"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<select class="" name="banner_data[{$elm}]" id="elm_banner_{$elm}" onchange="Tygh.$('div[id*=data_type_]').addClass('hidden');Tygh.$('div[id*=data_type_' + this.value + ']').removeClass('hidden');">
{foreach ['url'] as $e}
<option value="{$e}" {if $banner.$elm == $e}selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.{$e}")}</option>
{/foreach}
</select>
</div>
</div>
{* тип данных - URL ***************************************************}
<div id="data_type_url" class="{if $banner.abt__yt_data_type|default:'url' != 'url'}hidden{/if}">
{$field="url"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
</div>
</div>
</div>
{** Отображаем эти настройки для типа abyt **}
{elseif $banner.type == 'abyt'}
{$field="data_type"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<select class="" name="banner_data[{$elm}]" id="elm_banner_{$elm}" onchange="Tygh.$('div[id*=data_type_]').addClass('hidden');Tygh.$('div[id*=data_type_' + this.value + ']').removeClass('hidden');">
{foreach ['url', 'blog', 'promotion'] as $e}
<option value="{$e}" {if $banner.$elm == $e}selected="selected"{/if}>{__("abt__yt.banner.params.{$field}.variants.{$e}")}</option>
{/foreach}
</select>
</div>
</div>
{* тип данных - URL ***************************************************}
<div id="data_type_url" class="{if $banner.abt__yt_data_type|default:'url' != 'url'}hidden{/if}">
{$field="url"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
</div>
</div>
</div>
{* тип данных - BLOG **************************************************}
<div id="data_type_blog" class="{if $banner.abt__yt_data_type|default:'url' != 'blog'}hidden{/if}">
{$field="page_id"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
{include file="pickers/pages/picker.tpl" data_id="elm_banner_{$elm}" input_name="banner_data[{$elm}]" item_ids=$banner.$elm default_name=__("all_pages") status="A" extra="" extra_url="&page_type=`$smarty.const.PAGE_TYPE_BLOG`" no_container=true prepend=true}
{if $banner.$elm > 0}
{btn type="list" class="hand icon-cog" href="pages.update&page_id=`$banner.$elm`" target="_blank"}
{/if}
</div>
</div>
<script language="javascript">
function fn_youtube_use(el) {
var i = Tygh.$('#abt__yt_youtube_id,#abt__yt_youtube_playlist,#abt__yt_youtube_autoplay,#abt__yt_youtube_loop,#abt__yt_youtube_hide_controls');
if (!el.checked) { i.addClass('hidden'); } else { i.removeClass('hidden'); }
}
</script>
{$field="youtube_use"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} onclick="fn_youtube_use(this);" />
</div>
</div>
{$field="youtube_id"}{$elm="abt__yt_$field"}
<div class="control-group {if $banner.abt__yt_youtube_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
</div>
</div>
{$field="youtube_playlist"}{$elm="abt__yt_$field"}
<div class="control-group {if $banner.abt__yt_youtube_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
</div>
</div>
{$field="youtube_autoplay"}{$elm="abt__yt_$field"}
{*Value 0 (default): The video will not play automatically when the player loads.*}
{*Value 1: The video will play automatically when the player loads.*}
<div class="control-group {if $banner.abt__yt_youtube_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} />
</div>
</div>
{$field="youtube_loop"}{$elm="abt__yt_$field"}
{*Value 0 (default): The video will play only once. *}
{*Value 1: The video will loop (forever). *}
<div class="control-group {if $banner.abt__yt_youtube_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} />
</div>
</div>
{$field="youtube_hide_controls"}{$elm="abt__yt_$field"}
{*Value 0: Player controls does not display.*}
{*Value 1 (default): Player controls display.*}
<div class="control-group {if $banner.abt__yt_youtube_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} />
</div>
</div>
<script language="javascript">
function fn_product_list_use(el) {
var i = Tygh.$('#abt__yt_product_list_title,#abt__yt_product_list');
if (!el.checked) { i.addClass('hidden'); } else { i.removeClass('hidden'); }
}
</script>
{$field="product_list_use"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} onclick="fn_product_list_use(this);" />
</div>
</div>
{$field="product_list_title"}{$elm="abt__yt_$field"}
<div class="control-group {if $banner.abt__yt_product_list_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
</div>
</div>
{$field="product_list"}{$elm="abt__yt_$field"}
<div class="control-group {if $banner.abt__yt_product_list_use == 'N'}hidden{/if}" id="{$elm}">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div class="controls">
{include file="pickers/products/picker.tpl" data_id=$elm item_ids=$banner.$elm input_name="banner_data[{$elm}]" type="links" no_item_text=__("text_no_products_defined") holder_name="gift_certificates" but_role="text" but_meta="ty-btn__tertiary" but_text=__("add_products") no_container = true icon_plus = true}
</div>
</div>
</div>
{* тип данных - PROMOTION *********************************************}
<div id="data_type_promotion" class="{if $banner.abt__yt_data_type|default:'url' != 'promotion'}hidden{/if}">
{$field="promotion_id"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}</label>
<div for="elm_banner_{$elm}" class="controls">
{if $addons.ab__deal_of_the_day.status == "A"}
{include file="addons/ab__deal_of_the_day/pickers/promotions/picker.tpl" data_id="elm_banner_{$elm}" input_name="banner_data[{$elm}]" item_ids=$banner.$elm default_name=__("no_data") extra="" no_container=true prepend=true}
{else}
<input type="text" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="{$banner.$elm}" size="25" class="input-large" />
{/if}
{if $banner.$elm > 0}
{btn type="list" class="hand icon-cog" href="promotions.update&promotion_id=`$banner.$elm`" target="_blank"}
{/if}
</div>
</div>
{$field="countdown_use"}{$elm="abt__yt_$field"}
<div class="control-group">
<label for="elm_banner_{$elm}" class="control-label">{__("abt__yt.banner.params.{$field}")}{include file="common/tooltip.tpl" tooltip=__("abt__yt.banner.params.{$field}.tooltip")}</label>
<div class="controls">
<input type="hidden" name="banner_data[{$elm}]" value="N" />
<input type="checkbox" name="banner_data[{$elm}]" id="elm_banner_{$elm}" value="Y" {if $banner.$elm == "Y"}checked="checked"{/if} />
</div>
</div>
</div>
{/if}
</div>
<br/>
<br/>
<br/>
{/if}
