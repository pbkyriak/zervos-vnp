{capture name="mainbox"}
<form action="{""|fn_url}" method="post" name="abt__yt_demo_data_form" id="abt__yt_demo_data_form">
<p>{__("abt__yt.demodata_description")}</p>
<table class="table" width="100%">
<thead>
<tr>
<th width="60%">{__("abt__yt.demodata.table.description")}</th>
<th width="20%">{__("abt__yt.demodata.table.action")}</th>
</tr>
</thead>
<tbody>
<tr>
<td>{__("abt__yt.demodata.actions.add_banners")}</td>
<td>{btn type="list" class="cm-ajax cm-post btn btn-primary" text=__("add") dispatch="dispatch[abt__yt.update_demodata.add_banners]"}</td>
</tr>
<tr>
<td>{__("abt__yt.demodata.actions.add_menu")}</td>
<td>{btn type="list" class="cm-ajax cm-post btn btn-primary" text=__("add") dispatch="dispatch[abt__yt.update_demodata.add_menu]"}</td>
</tr>
</tbody>
</table>
</form>
{/capture}
{include file="common/mainbox.tpl"
title=__("abt__yt.demodata")
content=$smarty.capture.mainbox
buttons=$smarty.capture.buttons
content_id="abt__yt_demo_data_form"}