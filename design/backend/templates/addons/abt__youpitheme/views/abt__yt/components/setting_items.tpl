{capture name="mainbox"}
<form action="{""|fn_url}" method="post" name="abt__youpitheme_settings_form" id="abt__youpitheme_settings_form">
{$smarty.capture.abt__yt_styles nofilter}
<input type="hidden" name="selected_section" value="{$smarty.request.selected_section}" />
<input type="hidden" name="abt__yt_type" value="{$abt__yt_type}" />
{capture name="tabsbox"}
{foreach $abt__yt_settings as $section => $section_settings}
<div id="content_{$section}">
<div>{__("`$ls`.`$section`_description")}</div>
<table class="table table-middle">
<thead>
<tr>
<th width="25%">{__('name')}</th>
<th width="75%">{__('value')}</th>
</tr>
</thead>
<tbody>
{foreach from=$section_settings item="s"}
<tr>
<td nowrap>{__("`$ls`.`$section`.`$s.name`")}{include file="common/tooltip.tpl" tooltip={__("`$ls`.`$section`.`$s.name`.tooltip")}}</td>
<td>
{$f_v="abt__youpitheme_data[`$section`][`$s.name`]"}
{** Checkbox **}
{if $s.type == "checkbox"}
<input type="hidden" value="N" name="{$f_v}">
<input type="checkbox" value="Y" name="{$f_v}" {if $s.value == 'Y'}checked="checked"{/if}>
{** selectbox **}
{elseif $s.type == 'selectbox'}
<select name="{$f_v}" class="{$s.class|default:'span10'}">
{foreach from=$s.variants item="v"}
<option value="{$v}" {if $v == $s.value}selected="selected"{/if}>
{if $s.variants_as_language_variable|default:'Y' == 'Y'}
{__({"`$ls`.`$section`.`$s.name`.variants.`$v`"})}
{else}
{$v}
{/if}
</option>
{/foreach}
</select>
{if $s.suffix}&nbsp;{$s.suffix}{/if}
{** input **}
{elseif $s.type == 'input'}
<input type="text" name="{$f_v}" value="{$s.value}" class="cm-trim {$s.class|default:'span10'}">
{if $s.suffix}&nbsp;{$s.suffix}{/if}
{** textarea **}
{elseif $s.type == 'textarea'}
<textarea name="{$f_v}" class="cm-trim {$s.class|default:'span10'}">{$s.value}</textarea>
{** colorpicker **}
{elseif $s.type == 'colorpicker'}
<input class="cm-abt-colorpicker" style="font-family: monospace;" type="text" name={$f_v} id="{"storage_elm_te_`$section`_`$s.name`"}" value="{$s.value|replace:"transparent":""|default:"#ffffff"}"/>
{/if}
</td>
</tr>
{/foreach}
</tbody>
</table>
<!--content_{$section}--></div>
{/foreach}
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox group_name=$runtime.controller active_tab=$smarty.request.selected_section track=true}
</form>
{/capture}
{capture name="buttons"}
{include file="buttons/button.tpl" but_text=__("save") but_role="submit-link" but_name="dispatch[abt__yt.update_settings]" but_meta="btn-primary" but_target_form="abt__youpitheme_settings_form"}
{/capture}
{include file="common/mainbox.tpl" title=$abt__yt_title select_languages=$abt__yt_select_languages content=$smarty.capture.mainbox buttons=$smarty.capture.buttons}
