{capture name="mainbox_title"}{__("abt__yt.generate")}{/capture}
{capture name="mainbox"}
<form id='form' action="{""|fn_url}" method="post" name="generate_form" class="form-horizontal form-edit cm-disable-empty-files">
{capture name="buttons"}
{include file="buttons/button.tpl" but_text=__("abt__yt.form.generate") but_role="submit-link" but_name="dispatch[abt__yt_buy_together.generate]" but_meta="btn-primary" but_target_form="generate_form"}
{/capture}
</form>
{/capture}
{include file="common/mainbox.tpl" title=$smarty.capture.mainbox_title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons sidebar=$smarty.capture.sidebar}
