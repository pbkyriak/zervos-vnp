{$abt__yt_type='less'}
{$abt__yt_title=__("abt__yt.less_settings")}
{$abt__yt_select_languages=false}
{$ls='abt__yt.less_settings'}
{capture name='abt__yt_styles'}
<div class="language-wrap">
<h6 class="muted">Style {include file="common/tooltip.tpl" tooltip=__('abt__yt.less_settings.style.tooltip')}:</h6>
{include file="common/select_object.tpl" style="graphic" suffix="currency" link_tpl=$config.current_url|fn_link_attach:"style=" items=$abt__yt_styles selected_id=$smarty.request.style|default:$current_style display_icons=false key_name='name'}
<input type="hidden" name="abt__yt_style" value="{$smarty.request.style|default:$current_style}">
</div>
{/capture}
{include file="addons/abt__youpitheme/views/abt__yt/components/setting_items.tpl"}
