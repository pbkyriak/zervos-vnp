{**
* @author Panos Kyriakakis <panos@salix.gr>
* @since Sep 9, 2017
*}
{if $payment}
    {assign var="id" value=$payment.payment_id}
{else}
    {assign var="id" value="0"}
{/if}
<div class="control-group" >
    <label class="control-label" for="elm_apply_time_cond_{$id}">{__("apply_time_cond")}:</label>
    <div class="controls">
        <input type="hidden" name="payment_data[apply_time_cond]" value="N" />
        <input type="checkbox" name="payment_data[apply_time_cond]" id="elm_apply-time_cond" value="Y" {if $payment.apply_time_cond == "Y"}checked="checked"{/if} />
    </div>
</div>

<div class="control-group" data-ca-form-group="time_cond">
    <label class="control-label" for="elm_time_cond_{$id}">{__("time_cond")}:</label>
    <div class="controls">
        <table class="table" style="width: 70%;" id="time_cond_{$id}">
            <thead>
            <th>Day</th>
            <th>Start</th>
            <th>End</th>
            </thead>
            {for $wd=1 to 7}
        {if $wd==7}{assign var="wkd" value=0}{else}{assign var="wkd" value=$wd}{/if}
        <tr>
            <td>{__("weekday_`$wkd`")}</td>
            <td>
                <select class="span1 time-cond-select" name="payment_data[time_cond][{$wd}][0]" id="payment_data[time_cond][{$wd}][0]_{$id}" data-id="{$id}" data-day="{$wd}" data-limit="start">
                    <option value="-1">--</option>
                    {for $i=0 to 23}
                        <option value="{$i}" {if $payment.time_cond.$wd.0==$i}selected="selected"{/if}>{"%02d"|sprintf:$i}</option>
                    {/for}
                </select>
            </td>
            <td>
                <select class="span1 time-cond-select" name="payment_data[time_cond][{$wd}][1]" id="payment_data[time_cond][{$wd}][1]_{$id}" data-id="{$id}" data-day="{$wd}" data-limit="end">
                    <option value="-1">--</option>
                    {for $i=0 to 23}
                        <option value="{$i}" {if $payment.time_cond.$wd.1==$i}selected="selected"{/if}>{"%02d"|sprintf:$i}</option>
                    {/for}
                </select>
            </td>
        </tr>
    {/for}
</table>
<div class="time_cond_rules">
    <p>{__("last_saved_rules")}:</p>
    <ul>
        {foreach from=$payment.time_cond_human item="tt"}
            <li>{$tt}</li>
            {/foreach}
    </ul>
</div>

</div>

</div>

<script type="text/javascript">

(function(_, $) {
    $.ceEvent('on', 'ce.commoninit', function(context) {
        var frm = $("form", context);
        var ppurl = "{"paymenttimecond.humanize"|fn_url}";
        $(".time-cond-select",frm).change(function(ev) {
            var disp = $(".time_cond_rules",frm);
            var sel = $(ev.target);
            var pers = [];
            $(".time-cond-select",frm).each(function(idx,el){
                var d = $(el).data('day');
                var l = $(el).data('limit');
                var v = $(el).val();
                if(l=='start') {
                    pers[d]= {
                            0: v, 1:-1
                        };
                }
                else {
                    pers[d][1] = v;
                }
            });
            console.log(pers);
            var data = {
                data: pers
            }
            $.get(ppurl, data, "json").done(function(data){
                disp.html(data['a']);
            });
        });

    });
}(Tygh, Tygh.$));
</script>