<div class="control-group">
        <label class="control-label" for="elm_payment_active_from_{$id}">{__("active_range")}:</label>
        <div class="controls">
        	<input id="elm_payment_active_from_{$id}" type="text" name="payment_data[active_from]" value="{$payment.active_from}" class="input-text-price" size="6" /> {$currencies.$primary_currency.symbol  nofilter} - <input id="elm_payment_active_to_{$id}" type="text" name="payment_data[active_to]" value="{$payment.active_to}" class="input-text-price" size="6" /> {$currencies.$primary_currency.symbol nofilter}
        </div>
</div>
