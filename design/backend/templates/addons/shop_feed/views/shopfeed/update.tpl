{if $feed_data}
    {assign var="id" value=$feed_data.id}
{else}
    {assign var="id" value=0}
{/if}

{assign var="allow_save" value=$feed_data|fn_allow_save_object:"shopfeed"}
{$show_save_btn = $allow_save scope = root}

{capture name="mainbox"}

{capture name="tabsbox"}

<form action="{""|fn_url}" method="post" name="news_update_form" class="form-horizontal form-edit {if !$allow_save} cm-hide-inputs{/if}">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="feed_id" value="{$id}" />
<input type="hidden" class="cm-no-hide-input" name="selected_section" value="{$smarty.request.selected_section|default:"detailed"}" />

<div id="content_detailed">
<fieldset>
    <div class="control-group">
        <label for="elm_feed" class="control-label cm-required">{__("feed")}</label>
        <div class="controls">
            <input type="text" name="feed_data[feed]" id="elm_feed" value="{$feed_data.feed}" size="40" class="input-large" />
            <p>Μοναδικό όνομα με λατινικούς χαρακτήρες. Πχ skroutz1</p>
        </div>
    </div>
    <div class="control-group">
        <label for="elm_title" class="control-label cm-required">{__("title")}</label>
        <div class="controls">
            <input type="text" name="feed_data[title]" id="elm_feed" value="{$feed_data.title}" size="40" class="input-large" />
            <p>Λεκτικό που θα εμφανίζεται στις επιλογές πχ 'Αρχείο για Skroutz'</p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label cm-required" for="elm_exporter">{__("exporter")}</label>
        <div class="controls">
            <select name="feed_data[exporter]" id="elm_exporter" class="input-large">
              {foreach from=$exporters item="expo"}
              {assign var="expoId" value=$expo.id}
                <option {if $feed_data.exporter==$expoId}selected{/if} value="{$expoId}">{__($expo.title)}</option>
              {/foreach}
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label cm-required" for="elm_filename">{__("filename")}</label>
        <div class="controls">
            <input type="text" name="feed_data[filename]" id="elm_filename" value="{$feed_data.filename}" size="40" class="input-large" />
            <p>Μοναδικό όνομα χωρίς την επέκταση, κάτι που να είναι αποδεκτό όνομα αρχείου πχ skroutz_1</p>
        </div>
    </div>

    <div class="control-group">
      <label for="feed_data_export_features" class="control-label">{__('export_features')}:</label>
      <div class="controls">
        {foreach from=$features item="feature"}
        {assign var="featureId" value=$feature.feature_id}
        <label class="checkbox" for="elm_feed_export_features_{$featureId}">
          <input type="checkbox" 
                 class="cm-combo-checkbox" 
                 name="feed_data[export_features][{$featureId}]" 
                 id="elm_feed_export_features_{$featureId}" value="{$featureId}" 
                 {if ($feed_data.export_features.$featureId) }checked="checked"{/if} 
          />{$feature.description}
        </label>
        {/foreach}
      </div>
    </div>
	{if $addons.suppliers }
    <div class="control-group">
      <label for="feed_data_suppliers_to_export" class="control-label">{__('suppliers_to_export')}:</label>
      <div class="controls">
        {foreach from=$suppliers item="supplier"}
        {assign var="supplierId" value=$supplier.supplier_id}
        <label class="checkbox" for="elm_feed_supplier_to_export_{$supplierId}">
          <input type="checkbox" 
                 class="cm-combo-checkbox" 
                 name="feed_data[suppliers_to_export][{$supplierId}]" 
                 id="elm_feed_supplier_to_export{$supplierId}" value="{$supplierId}" 
                 {if ($feed_data.suppliers_to_export.$supplierId) }checked="checked"{/if} 
          />{$supplier.name}
        </label>
        {/foreach}
      </div>
    </div>
	{else}
	{/if}

</fieldset>
</div>

{capture name="buttons"}
{if !$id}
    {include file="buttons/save_cancel.tpl" but_name="dispatch[shopfeed.update]" but_role="submit-link" but_target_form="news_update_form"}
{else}
    {if !$show_save_btn}
        {assign var="hide_first_button" value=true}
        {assign var="hide_second_button" value=true}
    {/if}
    {include file="buttons/save_cancel.tpl" but_name="dispatch[shopfeed.update]" hide_first_button=$hide_first_button hide_second_button=$hide_second_button but_role="submit-link" but_target_form="news_update_form" save=$id}
{/if}
{/capture}

</form>

{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox track=true}

{/capture}

{if $id}
    {assign var="title" value="{__("editing_feed")}: `$feed_data.feed`"}
{else}
    {assign var="title" value=__("new_feed")}
{/if}

{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox select_languages=true buttons=$smarty.capture.buttons}
