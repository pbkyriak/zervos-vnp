{assign var="allow_save" value=$feed_data|fn_allow_save_object:"shopfeed"}
{$show_save_btn = $allow_save scope = root}

{capture name="mainbox"}

{capture name="tabsbox"}

<form action="{""|fn_url}" method="post" name="options_update_form" class="form-horizontal form-edit {if !$allow_save} cm-hide-inputs{/if}">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />

<div id="content_detailed">
<fieldset>
    <div class="control-group">
        <label for="elm_option_name" class="control-label cm-required">{__("options")}</label>
        <div class="controls">
            <input type="text" name="option_data[option_name]" id="elm_option_name" value="{$option_data.option_name}" size="40" class="input-large" />
            <p>Το όνομα των option που θέλουμε να ορίσουμε τη χρήση τους</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="elm_is_shopfeed">{__("is_shopfeed_option")}</label>
        <div class="controls">
            <select id="elm_is_shopfeed" name="option_data[is_shopfeed_option]">
                <option value="N" {if $option_data.is_shopfeed_option == "N"}selected="selected"{/if}>{__("none")}</option>
                <option value="C" {if $option_data.is_shopfeed_option == "C"}selected="selected"{/if}>{__("color")}</option>
                <option value="S" {if $option_data.is_shopfeed_option == "S"}selected="selected"{/if}>{__("size")}</option>
            </select>
        </div>
    </div>

</fieldset>
</div>

{capture name="buttons"}
    {include file="buttons/save.tpl" but_name="dispatch[shopfeed.updateopts]" but_role="submit-link" but_target_form="options_update_form"}
{/capture}

</form>

{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox track=true}

{/capture}

{assign var="title" value=__("update_options")}

{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox select_languages=true buttons=$smarty.capture.buttons}
