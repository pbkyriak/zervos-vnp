

{capture name="mainbox"}

<div id="content_manage_shopfeed">
  {assign var="baselink" value="http://`$config.http_host`/xml_feed/[name].xml"}
  <p>
    Links:<br />
    <ul>
    {if $addons.shop_feed.export_bestprice=='Y' }<li><a href="{$baselink|replace:"[name]":"bestprice"}" target="_blank">{$baselink|replace:"[name]":"bestprice"}</a></li>{/if}
    {if $addons.shop_feed.export_skroutz=='Y' }<li><a href="{$baselink|replace:"[name]":"skroutz"}" target="_blank">{$baselink|replace:"[name]":"skroutz"}</a></li>{/if}
    {if $addons.shop_feed.export_ricardo=='Y' }<li><a href="{$baselink|replace:"[name]":"ricardo"}" target="_blank">{$baselink|replace:"[name]":"ricardo"}</a></li>{/if}
    {if $addons.shop_feed.export_azshop=='Y' }<li><a href="{$baselink|replace:"[name]":"azshop"}" target="_blank">{$baselink|replace:"[name]":"azshop"}</a></li>{/if}
    </ul>
  </p>
  
<form action="{""|fn_url}" method="post" name="shopfeed_form" class="cm-hide-inputs">
<input type="hidden" name="fake" value="1" />

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table hidden-inputs">
<tr>    
	<th>
		<input type="checkbox" name="check_all" value="Y" class="checkbox cm-check-items cm-no-hide-input" /></th>
	<th>{__("date")}</th>
	<th>{__("products_exported")}</th>
    <th>{__("message")}</th>
</tr>
{foreach from=$rows item=n}
<tr {cycle values="class=\"table-row\", "} valign="top" >    
	{if "COMPANY_ID"|defined && $n.company_id != $smarty.const.COMPANY_ID}
		{assign var="no_hide_input" value=""}
	{else}
		{assign var="no_hide_input" value="cm-no-hide-input"}
	{/if}
	<td class="center {$no_hide_input}">
		<input type="checkbox" name="table_ids[]" value="{$n.id}" class="checkbox cm-item" /></td>
    <td width="20%">
      {$n.timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
    </td>
    <td >
      {$n.pcount}
    </td>
    <td width="70%" >
      {$n.feeds}
    </td>
</tr>
{foreachelse}
<tr class="no-items">
	<td colspan="4"><p>{__("no_items")}</p></td>
</tr>
{/foreach}
</table>

{capture name="buttons"}
    {capture name="tools_list"}
		{if $rows}
            <li>{btn type="delete_selected" dispatch="dispatch[shopfeed.deletelog]" form="shopfeed_form"}</li>
        {/if}
        <li>{btn type="list" text=__('manage_feeds') href="shopfeed.manage"}</li>
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}

<div class="clearfix">
    {include file="common/pagination.tpl" div_id=$smarty.request.content_id}
</div>

</form>
<!--content_manage_shopfeed--></div>


{/capture}
{include file="common/mainbox.tpl" title=__("shopfeed_log") buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox}
