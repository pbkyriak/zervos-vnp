

{capture name="mainbox"}

<div id="content_manage_shopfeed">
  
<form action="{""|fn_url}" method="post" name="shopfeed_form" class="cm-hide-inputs">
<input type="hidden" name="fake" value="1" />

{include file="common/pagination.tpl" save_current_page=true save_current_url=true div_id=$smarty.request.content_id}

<table cellpadding="0" cellspacing="0" border="0" width="100%" class="table hidden-inputs">
<tr>    
	 <th width="1%">
		<input type="checkbox" name="check_all" value="Y" class="checkbox cm-check-items cm-no-hide-input" /></th>
    <th>{__("feed")}</th>
	<th>{__("title")}</th>
	<th>{__("exporter")}</th>
    <th>{__("filename")}</th>
    <th>&nbsp;</th>
</tr>
{foreach from=$rows item=n}
<tr {cycle values="class=\"table-row\", "} valign="top" >    
	{if "COMPANY_ID"|defined && $n.company_id != $smarty.const.COMPANY_ID}
		{assign var="no_hide_input" value=""}
	{else}
		{assign var="no_hide_input" value="cm-no-hide-input"}
	{/if}
	<td class="left {$no_hide_input}">
		<input type="checkbox" name="table_ids[]" value="{$n.id}" class="checkbox cm-item" /></td>
    <td width="20%">
      <a href="{"shopfeed.update&feed_id=`$n.id`"|fn_url}">{$n.feed}</a>
    </td>
    <td>
      <a href="{"shopfeed.update&feed_id=`$n.id`"|fn_url}">{$n.title}</a>
    </td>
    <td>
      {$n.exporter}
    </td>
    <td>
      {$n.filename}
    </td>
    <td class="center nowrap">
        {capture name="tools_list"}
            <li>{btn type="list" text=__("edit") href="shopfeed.update?feed_id=`$n.id`"}</li>
            <li>{btn type="list" class="cm-confirm" text=__("delete") href="shopfeed.delete?feed_id=`$n.id`"}</li>
        {/capture}
        <div class="hidden-tools right">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>
{foreachelse}
<tr class="no-items">
	<td colspan="4"><p>{__("no_items")}</p></td>
</tr>
{/foreach}
</table>

{capture name="buttons"}
    {capture name="tools_list"}
	{if $rows}
            <li>{btn type="delete_selected" dispatch="dispatch[shopfeed.mdelete]" form="shopfeed_form"}</li>
        {/if}
        <li>{btn type="list" text=__('update_options') href="shopfeed.updateopts"}</li>
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}


{capture name="adv_buttons"}
  {include file="common/tools.tpl" tool_href="shopfeed.add" prefix="top" title=__("add_feed") hide_tools=true icon="icon-plus"}
{/capture}

<div class="clearfix">
    {include file="common/pagination.tpl" div_id=$smarty.request.content_id}
</div>

</form>
<!--content_manage_shopfeed--></div>


{/capture}
{include file="common/mainbox.tpl" title=__("shopfeed_feeds") buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons sidebar=$smarty.capture.sidebar content=$smarty.capture.mainbox}
