
        <div class="control-group">
            <label for="elm_availability" class="control-label">{__("availability")}</label>
            <div class="controls">
            <select name="availability" id="elm_availability">
                <option value="">--</option>
				{for $opt=1 to 8}
                <option value="{$opt}" {if $search.availability == $opt}selected="selected"{/if}>{__("shopFeed_avail_opt`$opt`")}</option>
				{/for}
            </select>
            </div>
        </div>