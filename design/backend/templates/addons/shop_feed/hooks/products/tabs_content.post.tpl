<div id="content_shopfeed">
  <fieldset>
    {include file="common/subheader.tpl" title=__('shop_feed')}
    <div class="control-group">
      <input type="hidden" name="product_data[fake_shopfeed]" value="{$product_data.product_id}" />
      <label for="product_ean" class="control-label">{__('ean')}:</label>
      <div class="controls">
        <input type="text" name="product_data[ean]" id="product_ean" size="20" maxlength="32"  value="{$product_data.ean}" class="input-text-medium" />
      </div>
    </div>
    <div class="control-group">
      <label for="product_mpn_code" class="control-label">{__('mpn_code')}:</label>
      <div class="controls">
        <input type="text" name="product_data[mpn_code]" id="product_mpn_code" size="20" maxlength="32"  value="{$product_data.mpn_code}" class="input-text-medium" />
      </div>
    </div>
    <div class="control-group">
      <label for="product_feed_min_qty" class="control-label">{__('feed_min_qty')}:</label>
      <div class="controls">
        <input type="text" name="product_data[feed_min_qty]" id="product_feed_min_qty" size="20" maxlength="20"  value="{$product_data.feed_min_qty}" class="input-text-medium" />
      </div>
    </div>
    <div class="control-group">
      <label for="product_data_exclude_from_shopfeed" class="control-label">{__('exclude_from_shopfeed')}:</label>
      <div class="controls">
        {foreach from=$exporters item="feed"}
        {assign var="feedId" value=$feed.id}
        <label class="checkbox" for="elm_product_exclude_from_shopfeed_{$feedId}">
          <input type="checkbox" 
                 class="cm-combo-checkbox" 
                 name="product_data[exclude_from_shopfeed][{$feedId}]" 
                 id="elm_product_exclude_from_shopfeed_{$feedId}" value="{$feedId}" 
                 {if ($product_data.exclude_from_shopfeed.$feedId) }checked="checked"{/if} 
          />{$feed.title}
        </label>
        {/foreach}
      </div>
    </div>

    {include file="addons/slx_updater_admin/common/shop_skroutz_availability_selector.tpl" data_selector="product_data" shop_avail=$product_data.shop_feed_avail_opt fldname="shop_feed_avail_opt"}
    {include file="addons/slx_updater_admin/common/shop_skroutz_availability_selector.tpl" data_selector="product_data" shop_avail=$product_data.skroutz_availability fldname="skroutz_availability"}
    <div class="control-group">
      <label for="product_manufacturer" class="control-label">{__('manufacturer')}:</label>
      <div class="controls">
        <input type="text" name="product_data[manufacturer]" id="product_manufacturer" size="20" maxlength="32"  value="{$product_data.manufacturer}" class="input-text-medium" />
      </div>
    </div>

  </fieldset>

  <!--content_shopfeed--></div>
