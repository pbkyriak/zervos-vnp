<div class="control-group">
    <label class="control-label" for="elm_is_shopfeed_option_{$id}">{__("is_shopfeed_option")}</label>
    <div class="controls">
        <select id="elm_is_shopfeed_{$id}" name="option_data[is_shopfeed_option]">
            <option value="N" {if $option_data.is_shopfeed_option == "N"}selected="selected"{/if}>{__("none")}</option>
            <option value="C" {if $option_data.is_shopfeed_option == "C"}selected="selected"{/if}>{__("color")}</option>
            <option value="S" {if $option_data.is_shopfeed_option == "S"}selected="selected"{/if}>{__("size")}</option>
        </select>
    </div>
</div>

