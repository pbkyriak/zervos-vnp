<div id="content_shopfeed">
  <fieldset>
    {include file="common/subheader.tpl" title=__('shop_feed')}
    <div class="control-group">
      <input type="hidden" name="category_data[fake_shopfeed]" value="{$category_data.category_id}" />
      <label for="category_data_exclude_from_shopfeed" class="control-label">{__('exclude_from_shopfeed')}:</label>
      <div class="controls">
        {foreach from=$exporters item="feed"}
        {assign var="feedId" value=$feed.id}
        <label class="checkbox" for="elm_category_exclude_from_shopfeed_{$feedId}">
          <input type="checkbox" 
                 class="cm-combo-checkbox" 
                 name="category_data[exclude_from_shopfeed][{$feedId}]" 
                 id="elm_category_exclude_from_shopfeed_{$feedId}" value="{$feedId}" 
                 {if ($category_data.exclude_from_shopfeed.$feedId) }checked="checked"{/if} 
          />{$feed.title}
        </label>
        {/foreach}
        <p class="note">Αν εξαιρέσετε αυτή την κατηγορία, στο xml δεν θα εμφανιστούν και οι υποκατηγορίες της</p>
      </div>
    </div>
    <div class="control-group">
      <label for="category_shop_feed_avail_opt" class="control-label">{__('feed_avail_option_category')}:</label>
      <div class="controls">
        <select name="category_data[shop_feed_avail_opt]" id="category_shop_feed_avail_opt">
          <option value="0" {if $category_data.shop_feed_avail_opt=='0'}selected{/if}>{__('shopFeed_avail_opt0')}</option>
          <option value="1" {if $category_data.shop_feed_avail_opt=='1'}selected{/if}>{__('shopFeed_avail_opt1')}</option>
          <option value="2" {if $category_data.shop_feed_avail_opt=='2'}selected{/if}>{__('shopFeed_avail_opt2')}</option>
          <option value="3" {if $category_data.shop_feed_avail_opt=='3'}selected{/if}>{__('shopFeed_avail_opt3')}</option>
          <option value="4" {if $category_data.shop_feed_avail_opt=='4'}selected{/if}>{__('shopFeed_avail_opt4')}</option>
          <option value="5" {if $category_data.shop_feed_avail_opt=='5'}selected{/if}>{__('shopFeed_avail_opt5')}</option>
          <option value="6" {if $category_data.shop_feed_avail_opt=='6'}selected{/if}>{__('shopFeed_avail_opt6')}</option>
          <option value="7" {if $category_data.shop_feed_avail_opt=='7'}selected{/if}>{__('shopFeed_avail_opt7')}</option>
          <option value="8" {if $category_data.shop_feed_avail_opt=='8'}selected{/if}>{__('shopFeed_avail_opt8')}</option>
        </select>
      </div>
    </div>
  </fieldset>

  <!--content_shopfeed--></div>
