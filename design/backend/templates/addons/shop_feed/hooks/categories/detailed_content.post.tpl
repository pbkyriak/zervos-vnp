    {include file="common/subheader.tpl" title=__('google_category_id_header')}
		<div class="control-group">
			<label for="elm_google_category_id" class="control-label">{__("google_category_id_label")}:</label>
			<div class="controls">
				<input type="text" name="category_data[google_category_id]" id="elm_google_category_id" size="10" value="{$category_data.google_category_id}" class="input-text-short" />
			</div>
		</div>