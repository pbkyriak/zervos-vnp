<div id="content_updaters">
    {include file="common/subheader.tpl" title=__("general") target="#acc_my_general"}
    <div id="acc_my_general" class="collapse in">
        <div class="control-group">
            <label class="control-label cm-required">{__("default_product_status")}</label>
            <div class="controls">
                <label class="radio inline" for="elm_supplier_default_product_status_a"><input type="radio" name="supplier_data[default_product_status]" id="elm_supplier_default_product_status_a" {if $supplier.default_product_status == "A" || !$supplier.default_product_status}checked="checked"{/if} value="A" />{__("active")}</label>
                <label class="radio inline" for="elm_supplier_default_product_status_h"><input type="radio" name="supplier_data[default_product_status]" id="elm_supplier_default_product_status_h" {if $supplier.default_product_status == "H"}checked="checked"{/if} value="H" />{__("hidden")}</label>
                <label class="radio inline" for="elm_supplier_default_product_status_d"><input type="radio" name="supplier_data[default_product_status]" id="elm_supplier_default_product_status_d" {if $supplier.default_product_status == "D"}checked="checked"{/if} value="D" />{__("disabled")}</label>
            </div>
        </div>
        <div class="control-group">
            <label for="elm_supplier_stock_limit" class="control-label cm-required">{__("supplier_stock_limit_for_update")} {include file="common/tooltip.tpl" tooltip=__("supplier_stock_limit_for_update")}:</label>
            <div class="controls">
                <input type="text" name="supplier_data[stock_limit]" id="elm_supplier_stock_limit" size="10" value="{$supplier.stock_limit}" class="" />
            </div>
        </div>
        <div class="control-group">
            <label for="elm_supplier_default_amount" class="control-label cm-required">{__("supplier_default_amount_for_update")} {include file="common/tooltip.tpl" tooltip=__("supplier_default_amount_for_update")}:</label>
            <div class="controls">
                <input type="text" name="supplier_data[default_amount]" id="elm_supplier_default_amount" size="10" value="{$supplier.default_amount}" class="" />
            </div>
        </div>
        <div class="control-group">
            <label for="elm_supplier_price_limit" class="control-label cm-required">{__("supplier_price_limit_for_update")} {include file="common/tooltip.tpl" tooltip=__("supplier_price_limit_for_update")}:</label>
            <div class="controls">
                <input type="text" name="supplier_data[price_limit]" id="elm_supplier_price_limit" size="10" value="{$supplier.price_limit}" class="" />
            </div>
        </div>
        <div class="control-group">
            <label for="elm_supplier_max_price_limit" class="control-label cm-required">{__("supplier_max_price_limit_for_update")} {include file="common/tooltip.tpl" tooltip=__("supplier_max_price_limit_for_update")}:</label>
            <div class="controls">
                <input type="text" name="supplier_data[max_price_limit]" id="elm_supplier_max_price_limit" size="10" value="{$supplier.max_price_limit}" class="" />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="elm_category_parent_id">{__("new_products_category")}:</label>
            <div class="controls">
                {include file="pickers/categories/picker.tpl" data_id="location_category" 
                        input_name="supplier_data[new_products_category_id]" 
                        item_ids=$supplier.new_products_category_id|default:"0" 
                        hide_link=true hide_delete_button=true default_name=__("none") 
                        display_input_id="elm_new_products_category_id" }
            </div>
        </div>
        <div class="control-group">
            <label for="elm_supplier_pm_category_flt_type" class="control-label cm-required">{__("pm_category_flt_type")}:</label>
            <div class="controls">
                <select name="supplier_data[pm_category_flt_type]" id="elm_supplier_pm_category_flt_type">
                    <option value="X" {if $supplier.pm_category_flt_type=="X"}selected{/if}>{__("exclude_categories")}</option>
                    <option value="I" {if $supplier.pm_category_flt_type=="I"}selected{/if}>{__("include_only_categories")}</option>
                </select>
            </div>
        </div>
        <div class="control-group">
            <label for="elm_pm_category_flt_cat_codes" class="control-label">{__("pm_category_flt_cat_codes")}:</label>
            <div class="controls">
                <textarea name="supplier_data[pm_category_flt_cat_codes]" id="elm_pm_category_flt_cat_codes" cols="55" rows="10" class="input-large">{$supplier.pm_category_flt_cat_codes}</textarea>
                <p>One per line</p>
            </div>
        </div>
    </div>

    {include file="common/subheader.tpl" title=__("availability") target="#acc_my_availability"}
    <div id="acc_my_availability" class="collapse in">
        {include file="addons/slx_updater_admin/common/shop_skroutz_availability_selector.tpl" data_selector="supplier_data" shop_avail=$supplier.shop_availability}
        {include file="addons/slx_updater_admin/common/shop_skroutz_availability_selector.tpl" data_selector="supplier_data" shop_avail=$supplier.skroutz_availability fldname="skroutz_availability"}
    </div>
    {* panos 22/7/15 *}
    {include file="common/subheader.tpl" title=__("markup") target="#acc_my_markup"}
    <div id="acc_my_markup" class="collapse in">
        <div class="control-group">
            <label for="elm_supplier_validity" class="control-label">{__("markup")}:</label>
            <div class="controls">
                (BasePrice + 
                <select class="input-medium" name="supplier_data[markup_profit_type]">
                    <option value='P' {if $supplier.markup_profit_type=='P'}selected{/if}>Percent</option>
                    <option value='A' {if $supplier.markup_profit_type=='A'}selected{/if}>Amount</option>
                </select>
                <input type="text" size="10" class="input-mini"  name="supplier_data[markup_profit_amount]" value="{$supplier.markup_profit_amount}" />
                +
                <select class="input-medium"  name="supplier_data[markup_add_hat]">
                    <option value='Y' {if $supplier.markup_add_hat=='Y'}selected{/if}>Καπέλο</option>
                    <option value='N' {if $supplier.markup_add_hat=='N'}selected{/if}>Χωρίς καπέλο</option>
                </select>
                ) +
                Φόρος
                <input type="text" size="10" class="input-mini"  name="supplier_data[markup_tax_amount]" value="{$supplier.markup_tax_amount}" />
                %
            </div>
            <div class="control-group">
                <label for="elm_supplier_validity" class="control-label">Προσαύξη βάρους:</label>
                <div class="controls">
                    <label class="checkbox">
                        <input type="hidden" name="supplier_data[wmarkup_enable]" value="N" />
                        <input type="checkbox" name="supplier_data[wmarkup_enable]" id="elm_supplier_wmarkup_enable" value="Y" {if $supplier.wmarkup_enable == "Y"}checked="checked"{/if}/>
                    </label>   
                </div>
                <div class="control-group">
                    <label for="elm_supplier_validity" class="control-label">Βάρος(όριο βάρους) > :</label>
                    <div class="controls">
                        <input type="text" size="10" class="input-mini"  name="supplier_data[wmarkup_weight]" value="{$supplier.wmarkup_weight}" />
                    </div>
                </div>
                <div class="control-group">
                    <label for="elm_supplier_validity" class="control-label">Βάρος(offset βάρους) > :</label>
                    <div class="controls">
                        <input type="text" size="10" class="input-mini"  name="supplier_data[wmarkup_weight_offset]" value="{$supplier.wmarkup_weight_offset}" />
                    </div>
                </div>
                <div class="control-group">
                    <label for="elm_supplier_validity" class="control-label">Βάρος για υπολογισμό :</label>
                    <div class="controls">
                        <select class="input-medium"  name="supplier_data[wmarkup_cweight_type]">
                            <option value='A' {if $supplier.wmarkup_cweight_type=='A'}selected{/if}>Βάρος</option>
                            <option value='B' {if $supplier.wmarkup_cweight_type=='B'}selected{/if}>Βάρος - Όριο βάρους</option>
                            <option value='D' {if $supplier.wmarkup_cweight_type=='D'}selected{/if}>Βάρος - Offset βάρους</option>
                            <option value='C' {if $supplier.wmarkup_cweight_type=='C'}selected{/if}>Όριο βάρους</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label for="elm_supplier_wmarkup_calc_type" class="control-label">Υπολογισμός :</label>
                    <div class="controls">
                        <select class="input-medium"  name="supplier_data[wmarkup_calc_type]">
                            <option value='A' {if $supplier.wmarkup_calc_type=='A'}selected{/if}>Βάρος*Ποσό</option>
                            <option value='B' {if $supplier.wmarkup_calc_type=='B'}selected{/if}>+Ποσό</option>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label for="elm_supplier_validity" class="control-label">Ποσό:</label>
                    <div class="controls">
                        <input type="text" size="10" class="input-mini"  name="supplier_data[wmarkup_amount]" value="{$supplier.wmarkup_amount}" />
                    </div>
                </div>

            </div>

        </div>

        <script type="text/javascript">
            //<![CDATA[
            {literal}
                $(document).ready(function () {
                    $("#content_updaters").appendTo("#supplier_update_form");
                });
            {/literal}  
                //]]>
        </script>