
{include file="common/subheader.tpl" title=__("updaters") target="#acc_addon_auto_updates"}
<div id="acc_addon_auto_updates" class="collapsed in">
    <div class="control-group">
        <label class="control-label" for="supplier_category_name">{__("supplier_category_name")}:</label>
        <div class="controls">
            <input type="text" name="category_data[supplier_category_name]" id="supplier_category_name" size="55" value="{$category_data.supplier_category_name}" class="input-large" />
            <p>Μπορεί να είναι λίστα με κόμματα, αλλά χωρίς κενά μετά ή πριν από το κόμμα. Νοκια, Nokia -> Λάθος! Νοκια,Nokia -> σωστό!</p>
        </div>
        
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_category_bridge_comments">{__("comments")}:</label>
        <div class="controls">
            <textarea name="category_data[bridge_comments]" id="elm_category_bridge_comments" cols="55" rows="4" class="input-large">{$category_data.bridge_comments}</textarea>
        </div>
    </div>
	
        <div class="control-group">
            <label for="elm_shipping_freight_surcharge" class="control-label">{__("shipping_freight_surcharge")}:</label>
            <div class="controls">
                <input type="text" name="category_data[shipping_freight_surcharge]" id="elm_shipping_freight_surcharge" size="10" value="{$category_data.shipping_freight_surcharge}" class="" />
            </div>
        </div>

        <div class="control-group">
            <label for="elm_additional_weigth" class="control-label">{__("additional_weigth")}:</label>
            <div class="controls">
                <input type="text" name="category_data[additional_weigth]" id="elm_additional_weigth" size="10" value="{$category_data.additional_weigth}" class="" />
            </div>
        </div>

</div>
