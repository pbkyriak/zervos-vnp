<div id="content_pricemarkup">
  {*assign var="suppliers" value=1|fn_get_slxSuppliers_for_select*}
  <fieldset>
    {include file="common/subheader.tpl" title=__('price_markup')}

    <table class="table table-middle">
        <thead>
        <td class="cm-non-cb">&nbsp;{__('supplier')}&nbsp;</td>
        <td class="cm-non-cb">&nbsp;{__('from')}&nbsp;</td>
        <td class="cm-non-cb">&nbsp;{__('markup')}&nbsp;</td>
        <td class="cm-non-cb">&nbsp;{__('markup_type')}&nbsp;</td>
        <td class="cm-non-cb">&nbsp;{__('round_to')}&nbsp;</td>
        <td class="cm-non-cb">&nbsp;</td>
      </tr>
      </thead>
      {if count($pricing_rules)>0 }
        {foreach from=$pricing_rules item="rule" name="fe_r"}
        {assign var="num" value=$smarty.foreach.fe_r.iteration}
         <tbody class="hover cm-row-item" id="rule_row_{$rule.id}">
          <tr>
            <td class="cm-non-cb">
              <select name="rules_data[{$rule.id}][supplier_id]" style="width:150px;">
                {foreach from=$suppliers item="supplier"}
                  <option value="{$supplier.supplier_id}" {if $supplier.supplier_id == $rule.supplier_id}selected{/if}>{$supplier.name}</option>
                {/foreach}
              </select>
            </td>
            <td class="cm-non-cb">
              <input type="text" name="rules_data[{$rule.id}][from_price]" value="{$rule.from_price}" size="3" class="input-mini" />
            </td>
            <td class="cm-non-cb">
              <input type="text" name="rules_data[{$rule.id}][markup]" value="{$rule.markup}" size="2" class="input-mini" />
            </td>
            <td class="cm-non-cb">
              <select name="rules_data[{$rule.id}][markup_type]" style="width:100px;">
                <option value="A" {if $rule.markup_type=='A'}selected{/if}>{__('value')}</option>
                <option value="P" {if $rule.markup_type=='P'}selected{/if}>{__('percent')}</option>
              </select>
            </td>
            <td class="cm-non-cb">
              <select name="rules_data[{$rule.id}][round_to]" style="width:100px;">
                <option value="0" {if $rule.round_to=='0'}selected{/if}>0.00</option>
                <option value="20" {if $rule.round_to=='20'}selected{/if}>0.20</option>
                <option value="50" {if $rule.round_to=='50'}selected{/if}>0.50</option>
              </select>
            </td>
            <td class="cm-non-cb">
            {include file="buttons/multiple_buttons.tpl" item_id="rule_row_`$rule.id`" only_delete="Y" tag_level="1"}
           </td>
          </tr>
          <div class="hidden"><input id="delete_rule_row_{$rule.id}" type="checkbox" class="checkbox cm-item" value="{$rule.id}"  name="delete_rules[]" /></div>
          </tbody>
        {/foreach}
      {/if}
      
       {*math equation="x + 1" assign="num" x=$num|default:0}{assign var="rule" value=""*}
       <tbody class="hover cm-row-item" id="box_add_rule_{$rule.id}">
      <tr>
        <td class="cm-non-cb">
          <select name="add_rules[0][supplier_id]" style="width:150px;">
            {foreach from=$suppliers item="supplier"}
              <option value="{$supplier.supplier_id}">{$supplier.name}</option>
            {/foreach}
          </select>
        </td>
        <td class="cm-non-cb">
          <input type="text" name="add_rules[0][from_price]" value="" class="input-mini" /></td>
        <td>
          <input type="text" name="add_rules[0][markup]" value="" class="input-mini" /></td>
        <td class="cm-non-cb">
          <select name="add_rules[0][markup_type]" style="width:100px;">
            <option value="A" >{__('value')}</option>
            <option value="P" >{__('percent')}</option>
          </select>
        </td>
        <td class="cm-non-cb">
          <select name="add_rules[0][round_to]" style="width:100px;">
            <option value="0">0.0</option>
            <option value="20">0.20</option>
            <option value="50">0.50</option>
          </select>
        </td>
        <td class="cm-non-cb">
          {include file="buttons/multiple_buttons.tpl" item_id="add_rule_{$rule.id}" tag_level="1"}
        </td>
      </tr>
      </tbody>
    </table>
  </fieldset>

  <!--content_autoupdater--></div>
<script type="text/javascript">
    //<![CDATA[
    var num = "{$num|escape:'javascript'}";
     {literal}
        $(document).ready(function() {
            $(".icon-remove").click(function(){
                var id = $(this).attr('id');
                //console.log("c_vrnt_id: "+id);
                $('#delete_'+id).prop('checked', true);
            });
            $(".icon-plus").click(function(){
               // num = Number(num) +1;
                    console.log("next_val: "+num);
            });
        });   
    {/literal}
    //]]>
</script>    