<div class="control-group">
    <label class="control-label" for="xproduct_related">{__("xproduct_related")}</label>
    <div class="controls">
        <select name="xproduct_related" id="xproduct_related">
            <option value="">--</option>
            <option value="Y" {if $search.xproduct_related == "Y"}selected="selected"{/if}>{__("yes")}</option>
            <option value="N" {if $search.xproduct_related == "N"}selected="selected"{/if}>{__("no")}</option>
        </select>
    </div>
</div>
