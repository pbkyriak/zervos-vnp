


{include file="common/subheader.tpl" title=__("updaters") target="#acc_auto_updates"}
<div id="acc_auto_updates" class="collapse in">
    <div class="control-group">
        <label class="control-label" for="elm_pricelist_updater_skip">{__("pricelist_updater_action")}:</label>
        <div class="controls">
            <select name="product_data[skip_updater]" id="elm_pricelist_updater_skip" >
                <option value="N" {if $product_data.skip_updater == "N"}selected{/if}>Κανονικά</option>
                <option value="Y" {if $product_data.skip_updater == "Y"}selected{/if}>Να μην ενημερωθεί</option>
                <option value="P" {if $product_data.skip_updater == "P"}selected{/if}>Μόνο τιμή </option>
                <option value="S" {if $product_data.skip_updater == "S"}selected{/if}>Μόνο απόθεμα</option>
            </select>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_product_codeB">{__("product_codeB")}:</label>
        <div class="controls">
            <p>{$product_data.product_codeB|default:"N/A"}</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="elm_product_codeB">{__("product_codeC")}:</label>
        <div class="controls">
            <p>{$product_data.product_codeC|default:"N/A"}</p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_ean">{__("ean")}:</label>
        <div class="controls">
            <p>{$product_data.ean|default:"N/A"}</p>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="elm_price_buy">{__("price_buy")} (+{$config.product_merger.vat_rate}%):</label>
        <div class="controls">
            <p>{$product_data.price_buy|default:"N/A"}</p>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="elm_price_buy">{__("xproduct")}:</label>
        <div class="controls">
            <p>{if $product_data.xproduct_id}<a href="{"xproducts.details&id=`$product_data.xproduct_id`"|fn_url}">View ({$product_data.xproduct_id})</a>{/if}</p>
			{if $product_data.xproduct_id}<p><a href="{"xproducts.unlink&cid=`$product_data.product_id`&xid=`$product_data.xproduct_id`"|fn_url}" class="cm-post cm-confirm">Unlink</a> (Αποσυνδέει το ενοποιημένο από το προιον. Το ενοποιημένο σε επόμενη ενημέρωση θα δημιουργήσει νέο cs-cart προιον)</p>{/if}
			{if $product_data.xproduct_id}<p><a href="{"xproducts.unlink_and_block&cid=`$product_data.product_id`&xid=`$product_data.xproduct_id`"|fn_url}" class="cm-post cm-confirm">Unlink and block</a> (Αποσυνδέει το ενοποιημένο από το προιον και μπλοκάρει το ενοποιημένο. Το ενοποιημένο δεν θα εμφανιστεί ξανά στις ενημερώσεις)</p>{/if}
        </div>
    </div>

</div>

