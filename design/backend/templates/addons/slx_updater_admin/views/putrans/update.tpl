{if $hub_data}
    {assign var="id" value=$hub_data.trans_id}
{/if}

{assign var="allow_save" value=true}
{$show_save_btn = $allow_save scope = root}

{capture name="mainbox"}

        <form action="{""|fn_url}" method="post" name="hub_update_form" class="form-horizontal form-edit">
            <input type="hidden" name="trans_id" value="{$hub_data.trans_id}" />
            <div class="product-manage" id="content_general">
                <fieldset>
                    <div class="control-group">
                        <label class="cm-required control-label" for="elm_title">{__("original")}:</label>
                        <div class="controls">
                            <input type="text" id="elm_title" name="hub_data[original]" size="8" value="{$hub_data.original}" />
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="elm_translation">{__("translation")}:</label>
                        <div class="controls">
                            <input type="text" id="elm_translation" name="hub_data[translation]" size="8" value="{$hub_data.translation}" />
                        </div>
                    </div>

                </fieldset>
            </div>

        </form>


{/capture}

{capture name="buttons"}
    {if !$id}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[putrans.update]" but_role="submit-link" but_target_form="hub_update_form"}
    {else}
        {if !$show_save_btn}
            {assign var="hide_first_button" value=true}
            {assign var="hide_second_button" value=false}
        {/if}
        {include file="buttons/save_cancel.tpl" but_name="dispatch[putrans.update]" hide_first_button=$hide_first_button hide_second_button=$hide_second_button but_role="submit-link" but_target_form="hub_update_form" save=$id}
    {/if}
{/capture}

{include file="common/mainbox.tpl" title=__("editing") content=$smarty.capture.mainbox select_languages=true buttons=$smarty.capture.buttons}
