{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="hubs_form">

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true}

        {if $hubs}
            <table width="100%" class="table table-middle">
                <thead>
                <tr>
                    <th>{__("ID")}</th>
                    <th>{__("original")}</th>
                    <th>{__("translation")}</th>
                    <th width="5%">&nbsp;</th>
                </tr>
                </thead>
                {foreach from=$hubs item="hub"}
                    <tr >
                        <td>
                            <a href="{"putrans.update?trans_id=`$hub.trans_id`"|fn_url}">{$hub.trans_id}</a>
                        </td>
                        <td>
                            {$hub.original}
                        </td>
                        <td>
                            {$hub.translation}
                        </td>
                        <td class="nowrap">
                            {capture name="tools_list"}
                                <li>{btn type="list" text=__("edit") href="putrans.update?trans_id=`$hub.trans_id`"}</li>
                                <li>{btn type="list" class="cm-confirm" text=__("delete") href="putrans.delete?trans_id=`$hub.trans_id`" method="POST"}</li>
                            {/capture}
                            <div class="hidden-tools">
                                {dropdown content=$smarty.capture.tools_list}
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl"}

    </form>


{/capture}

    {capture name="sidebar"}
        <div class="sidebar-row">
            <h6>{__("search")}</h6>
            <form action="{""|fn_url}" name="states_filter_form" method="get">
                <div class="sidebar-field">
                    <label>{__("original")}:</label>
                    <input class="search-input-text" type="text" name="original" value="{$search.original}" />
                    <p>Use * as wildcard, eg 'sch*' to find strings starting with 'sch'</p>
                </div>
                <div class="sidebar-field">
                    <label>{__("translation")}:</label>
                    <input class="search-input-text" type="text" name="translation" value="{$search.translation}" />
                    <p>Use * as wildcard</p>
                </div>

                {include file="buttons/search.tpl" but_name="dispatch[putrans.manage]"}
            </form>
        </div>
    {/capture}

    {capture name="adv_buttons"}
        {include file="common/tools.tpl" tool_href="putrans.add" prefix="top" title=__("add_putrans") hide_tools=true icon="icon-plus"}
    {/capture}

{include file="common/mainbox.tpl" title=__("putranslations") content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons sidebar=$smarty.capture.sidebar select_languages=true}