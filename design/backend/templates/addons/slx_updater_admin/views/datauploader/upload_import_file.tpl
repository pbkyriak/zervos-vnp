{capture name="mainbox"}    
	{capture name="tabsbox"}        
			<form action="{""|fn_url}" method="POST" enctype="multipart/form-data" name="braintrust_update_form" class="form-horizontal form-edit">
			<input type="hidden" name="dispatch" value="datauploader.upload_import_file" />
            <div id="content_detailed">
			<fieldset>
			{include file="common/subheader.tpl" title=__("difox_uploader")}
			<div class="control-group">
			<label for="elm_catalog" class="control-label cm-required">{__("filename")}</label>
			<div class="controls">
			<input type="file" id="elm_catalog" name="catalog" />
			<p>Ανεβάζουμε το αρχείο <b>csv</b> της Difox</p>
			</div>
			</div>
			<input type="submit" />
			</fieldset>
            </div>
			</form>
			
			<hr />
			
			<form action="{""|fn_url}" method="POST" enctype="multipart/form-data" name="eglobal_update_form" class="form-horizontal form-edit">
			<input type="hidden" name="dispatch" value="datauploader.upload_import_file_eglobal" />
            <div id="content_detailed">
			<fieldset>
			{include file="common/subheader.tpl" title=__("eglobal_uploader")}
			<div class="control-group">
			<label for="elm_catalog_eglobal" class="control-label cm-required">{__("filename")}</label>
			<div class="controls">
			<input type="file" id="elm_catalog_eglobal" name="catalog" />
			<p>Ανεβάζουμε το αρχείο <b>csv</b> της eglobal</p>
			</div>
			</div>
			<input type="submit" />
			</fieldset>
            </div>
			</form>

	{/capture}
	{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox track=true}

{/capture}
			{assign var="title" value=__("difox_uploader")}
			{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox select_languages=true}