{assign var="fldname" value=$fldname|default:"shop_availability"}
	<div class="control-group">
       	<label class="control-label" for="elm_{$fldname}">{__($fldname)}:</label>
        <div class="controls">
            <select class="span3" name="{$data_selector}[{$fldname}]" id="elm_{$fldname}">
            	<option value="0" {if $shop_avail==0}selected{/if}>{__('shopFeed_avail_opt0')}</option>
            	<option value="1" {if $shop_avail==1}selected{/if}>{__('shopFeed_avail_opt1')}</option>
            	<option value="2" {if $shop_avail==2}selected{/if}>{__('shopFeed_avail_opt2')}</option>
                <option value="3" {if $shop_avail==3}selected{/if}>{__('shopFeed_avail_opt3')}</option>
            	<option value="4" {if $shop_avail==4}selected{/if}>{__('shopFeed_avail_opt4')}</option>
            	<option value="5" {if $shop_avail==5}selected{/if}>{__('shopFeed_avail_opt5')}</option>
                <option value="6" {if $shop_avail==6}selected{/if}>{__('shopFeed_avail_opt6')}</option>
                <option value="7" {if $shop_avail==7}selected{/if}>{__('shopFeed_avail_opt7')}</option>
                <option value="8" {if $shop_avail==8}selected{/if}>{__('shopFeed_avail_opt8')}</option>
            </select>
        </div>
    </div>
