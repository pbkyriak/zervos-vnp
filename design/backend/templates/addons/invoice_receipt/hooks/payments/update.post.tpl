{*

	Author Ioannis Matziaris [imatz] - imatzgr@gmail.com - March 2014

	Prosthiki stin selida tropou pliromis tin dinatotia na kathorizoume an isxiei gia paralavi apo to katastima
	
*}

<div class="control-group">
	<label class="control-label" for="receipt-from-store">{__("receipt_store_title_admin")}{include file="common/tooltip.tpl" tooltip=__("receipt_store_payment_desc_admin")}:</label>	
	<div class="controls">
		<label class="checkbox">
			<input type="hidden" name="payment_data[receipt_from_store]" value="N" />
			<input type="checkbox" name="payment_data[receipt_from_store]" id="receipt-from-store" value="Y" {if $payment.receipt_from_store == "Y"}checked="checked"{/if}/>
		</label>
	</div>
</div>