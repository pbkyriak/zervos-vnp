{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="iproducts_form">

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true}

        {if $iproducts}
            <table width="100%" class="table table-middle">
                <thead>
                <tr>
                    <th>{__("ean")}</th>
                    <th>{__("source")}</th>
                    <th>{__("pcode")}</th>
                    <th>{__("lastchecked")}</th>
                </tr>
                </thead>
                {foreach from=$iproducts item="iproduct"}
                    <tr >
                        <td>
                            {$iproduct.ean}
                        </td>
                        <td>
                            {$iproduct.source}
                        </td>
                        <td>
                            {$iproduct.pcode}
                        </td>
                        <td>
                            {if $iproduct.lastchecked!=0}
                            {$iproduct.lastchecked|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}
                            {else}
                            --
                            {/if}
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl"}

    </form>

    {capture name="sidebar"}
        {include file="addons/slx_product_merger/views/components/sidemenu.tpl"}
        <div class="sidebar-row">
            <h6>{__("search")}</h6>
            <form action="{""|fn_url}" name="states_filter_form" method="get">
                <div class="sidebar-field">
                    <label>{__("ean")}:</label>
                    <input class="search-input-text" type="text" name="ean" value="{$search.ean}" />
                </div>
                <div class="sidebar-field">
                    <label>{__("source")}:</label>
                    <input class="search-input-text" type="text" name="source" value="{$search.source}" />
                </div>
                <div class="sidebar-field">
                    <label>{__("checked")}:</label>
                    <select name="checked" >
                        <option value="">(Any)</option>
                        <option value="no" {if $search.checked=="no"}selected{/if}>No</option>
                        <option value="yes" {if $search.checked=="yes"}selected{/if}>Yes</option>
                    </select>
                </div>
                <div class="sidebar-field">
                    <label>{__("pcode")}:</label>
                    <input class="search-input-text" type="text" name="pcode" value="{$search.pcode}" />
                </div>

                {include file="buttons/search.tpl" but_name="dispatch[iproductspending.manage]"}
            </form>
        </div>
    {/capture}


{/capture}
{include file="common/mainbox.tpl" title=__("iproducts_pending") content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar select_languages=true}