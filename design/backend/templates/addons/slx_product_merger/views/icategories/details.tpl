{capture name="mainbox"}
    {capture name="tabsbox"}

        <form action="{""|fn_url}" method="post" name="iproducts_form" class="form-horizontal form-edit">

            <div class="control-group">
                <label class="control-label" for="elm_hsn_code">{__("id")}:</label>
                <div class="controls">
                    <span class="shift-input">{$category_data.category_id}</span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_hsn_code">{__("title")}:</label>
                <div class="controls">
                    <span class="shift-input">{$category_data.titles}</span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_hsn_code">{__("product_count")}:</label>
                <div class="controls">
                    <span class="shift-input">{$category_data.cnt}</span>
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" for="elm_hsn_code">{__("linked_categories")}:</label>
                <div class="controls">
                    <div class="shift-input">
                        <table class="table">
                            {foreach from=$category_data.linked_categories item="linkedcat"}
                                <tr>
                                    <td><a href="{"categories.update?category_id=`$linkedcat.category_id`"|fn_url}">{$linkedcat.category}</a></td>
                                    <td><a href="{"icategories.unlink?icategory_id=`$category_data.category_id`&category_id=`$linkedcat.category_id`"|fn_url}" class="cm-confirm cm-post"><i class="icon-trash"></i> Unlink</a></td>
                                </tr>
                            {/foreach}
                        </table>
                    </div>
                </div>
            </div>
        </form>
    {include file="common/subheader.tpl" title=__("new_relation") target="#acc_new_rel"}

    <div id="acc_new_rel" class="collapse in">

        <form action="{""|fn_url}" method="post" name="icategory_link_form" class="form-horizontal form-edit">
            <input type="hidden" name="icategory_id" value="{$category_data.category_id}" />

            <div class="control-group">
                <label class="control-label cm-required" for="elm_category_parent_id">{__("location")}:</label>
                <div class="controls">
                    {include file="pickers/categories/picker.tpl"
                    data_id="location_category"
                    input_name="category_id"
                    item_ids=$category_data.parent_id|default:"0"
                    hide_link=true hide_delete_button=true default_name=__("root_level") display_input_id="elm_category_parent_id"}
                </div>
            </div>
            <div>
                {include file="buttons/save_cancel.tpl" but_name="dispatch[icategories.link]"
                hide_first_button=$hide_first_button hide_second_button=true but_role="submit-link" but_target_form="icategory_link_form" save=$category_data.category_id}
           </div>
        </form>
    </div>
    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox track=true}

{/capture}
{include file="common/mainbox.tpl" title=__("icategory") content=$smarty.capture.mainbox select_languages=true}