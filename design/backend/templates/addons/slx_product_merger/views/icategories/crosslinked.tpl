{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="iproducts_form">

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true}

        {if $icategories}
            <table width="100%" class="table table-middle">
                <thead>
                <tr>
                    <th>{__("icategory_id")}</th>
                    <th>{__("categories")}</th>
                </tr>
                </thead>
                {foreach from=$icategories item="icategory"}
                    <tr >
                        <td>
                            <a href="{"icategories.details&icategory_id=`$icategory.icategory_id`"|fn_url}">{$icategory.icategory_id}</a>
                        </td>
                        <td>
                            {$icategory.cnt}
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl"}

    </form>
    {capture name="sidebar"}
        {include file="addons/slx_product_merger/views/components/sidemenu.tpl"}
    {/capture}

{/capture}
{include file="common/mainbox.tpl" title=__("icategories_crosslinked") content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar  select_languages=true}