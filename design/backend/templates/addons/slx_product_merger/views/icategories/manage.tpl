{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="iproducts_form">

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true}

        {if $icategories}
            <table width="100%" class="table table-middle">
                <thead>
                <tr>
                    <th>{__("ID")}</th>
                    <th>{__("title")}</th>
                    <th>{__("ctitle")}</th>
                    <th>{__("products")}</th>
                </tr>
                </thead>
                {foreach from=$icategories item="icategory"}
                    <tr >
                        <td>
                            <a href="{"icategories.details?icategory_id=`$icategory.category_id`"|fn_url}">{$icategory.category_id}</a>
                        </td>
                        <td>
                            {$icategory.titles}
                        </td>
                        <td>
                            {if $icategory.ccategory_id}
                                {foreach from=$icategory.ccategory item="cc" key="ccidx" name="cc"}
                                    <a href="{"categories.update?category_id=`$icategory.ccategory_id[$ccidx]`"|fn_url}">{$cc}</a>{if !$smarty.foreach.cc.last}<br />{/if}
                                {/foreach}
                            {/if}
                        </td>
                        <td>
                            <a href="{"iproducts.manage?cid=`$icategory.category_id`"|fn_url}">{$icategory.cnt}</a>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl"}

    </form>

    {capture name="sidebar"}
        {include file="addons/slx_product_merger/views/components/sidemenu.tpl"}
        <div class="sidebar-row">
            <h6>{__("search")}</h6>
            <form action="{""|fn_url}" name="states_filter_form" method="get">
                <div class="sidebar-field">
                    <label>{__("title")}:</label>
                    <input class="search-input-text" type="text" name="title" value="{$search.title}" />
                    <span class="help-block">Wildcard character * can be used to control your search.</span>
                </div>
                <div class="sidebar-field">
                    <label>{__("icategory_id")}:</label>
                    <input class="search-input-text" type="text" name="icid" value="{$search.icid}" />
                </div>
                {include file="buttons/search.tpl" but_name="dispatch[icategories.manage]"}
            </form>
        </div>
    {/capture}


{/capture}
{include file="common/mainbox.tpl" title=__("icategories") content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar select_languages=true}