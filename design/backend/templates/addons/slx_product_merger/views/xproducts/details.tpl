{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="xproducts_form" class="form-horizontal form-edit ">
        {capture name="tabsbox"}

        <div id="content_general">
            <div class="control-group">
                <label class="control-label" for="elm_notification_lang">{__("product")}</label>
                <div class="controls">
                    <div class="shift-input">{$xproduct.title}</div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_notification_lang">{__("mean")}</label>
                <div class="controls">
                    <div class="shift-input">{$xproduct.mean}</div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_notification_lang">{__("istatus")}</label>
                <div class="controls">
                    <div class="shift-input">{$xproduct.istatus}</div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_notification_lang">{__("pu_result_id")}</label>
                <div class="controls">
                    <div class="shift-input">{$xproduct.pu_result_id}</div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_notification_lang">{__("is_blocked")}</label>
                <div class="controls">
                    <p>{if $xproduct.blocked==0}No{else}Yes{/if}</p>
					{if $xproduct.blocked==1}
					<a href="{"xproducts.unblock&xid=`$xproduct.id`"|fn_url}" class="cm-post cm-confirm">Unblock</a> (Το ενοποιημένο σε επόμενη ενημέρωση θα δημιουργήσει νέο cs-cart προιον)</p>
					{/if}
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_notification_lang">{__("mismatch_checked")}</label>
                <div class="controls">
                    <p>{if $xproduct.mismatch_checked==0}No{else}Yes{/if}</p>
					{if $xproduct.mismatch_checked==1}
					<a href="{"xproducts.mismatchecked?check=0&xid=`$xproduct.id`"|fn_url}" class="cm-post cm-confirm">Un-check</a> (Άρση Επιβεβαίωσης από άνθρωπο η σύνδεση Χ-CS προιοντων)</p>
					{else}
					<a href="{"xproducts.mismatchecked?check=1&xid=`$xproduct.id`"|fn_url}" class="cm-post cm-confirm">Check</a> (Επιβεβαίωση από άνθρωπο η σύνδεση Χ-CS προιοντων)</p>
					{/if}
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_notification_lang">{__("dominant_product")}</label>
                <div class="controls">
                    <div class="shift-input">{if $xproduct.eproduct_id}<a href="{"sproducts.details?id=`$xproduct.eproduct_id`"|fn_url}">View ({$xproduct.eproduct_id})</a>{/if}</div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="elm_notification_lang">{__("cart_product")}</label>
                <div class="controls">
                    <div class="shift-input">{if $xproduct.cproduct_id}<a href="{"products.update&product_id=`$xproduct.cproduct_id`"|fn_url}">View ({$xproduct.cproduct_id})</a>{/if}</div>
                </div>
            </div>
        </div>
        <div id="content_sproduct"  class="hidden">
            <h5>{__("supplier_products")}</h5>
            <table class="table table-middle" width="100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Supplier</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>P Code</th>
                        <th>Avail</th>
                        <th>UT</th>
                        <th>Category</th>
                        <th>EAN</th>
                    </tr>
                </thead>
                <tbody>
                {foreach from=$xproduct.sproducts item=sproduct}
                    <tr>
                        <td>{$sproduct.id}</td>
                        <td>{$sproduct.supplier}</td>
                        <td><a href="{"sproducts.details?id=`$sproduct.id`"|fn_url}">{$sproduct.title}</a></td>
                        <td>{$sproduct.price}</td>
                        <td>{$sproduct.pcode}</td>
                        <td>{$sproduct.availability}</td>
                        <td>{$sproduct.upd_timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
                        <td>{$sproduct.category}</td>
                        <td>{$sproduct.eans}</td>
                    </tr>
                {/foreach}
                </tbody>

            </table>
        </div>
        <div id="content_iproduct" class="hidden">
            <h5>Icecat <a href="{"iproducts.details?iproduct_id=`$xproduct.iproduct_id`"|fn_url}">go</a></h5>
            {include file="addons/slx_product_merger/views/iproducts/components/iproduct_info.tpl"}
        </div>
    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox track=true}


    </form>


    {capture name="adv_buttons"}
        {include file="common/tools.tpl" tool_href="xproducts.add" prefix="top" title=__("add_hsncode") hide_tools=true icon="icon-plus"}
    {/capture}

    {capture name="buttons"}
        {capture name="tools_list"}
            {if $xproducts}
                <li>{btn type="delete_selected" dispatch="dispatch[xproducts.m_delete]" form="xproducts_form"}</li>
                <li>{btn type="list" text=__("apply_taxes")  dispatch="dispatch[xproducts.store_selection]" form="xproducts_form"}</li>
            {/if}
        {/capture}
        {dropdown content=$smarty.capture.tools_list}

    {/capture}


{/capture}
{include file="common/mainbox.tpl" title=__("xproducts") content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar select_languages=true}

