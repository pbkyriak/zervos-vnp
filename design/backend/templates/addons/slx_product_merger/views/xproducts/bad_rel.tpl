{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="xproducts_form">

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true}

        {if $results}
            <table width="100%" class="table table-middle">
                <thead>
                <tr>
                    <th>cs-id</th>
                    <th>{__("title")}</th>
                    <th>x-Title</th>
                    <th>s-Titles</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                {foreach from=$results item="result"}
                    <tr >
                        <td class="left nowrap row-status">
                            <a href="{"products.update?product_id=`$result.product_id`&selected_section=addons"|fn_url}" target="_blank">{$result.product_id}</a>
                        <td>
                            {$result.product}
                        </td>
                        <td>
                            {$result.xtitle}
                        </td>
                        <td>
						{assign var="sts" value="$$"|explode:$result.stitles}
						{foreach from=$sts item="st"}
						{$st}<br />
						{/foreach}
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl"}

    </form>


{/capture}
{include file="common/mainbox.tpl" title="Πιθανώς λάθος CS-X συσχέτιση" content=$smarty.capture.mainbox select_languages=true}