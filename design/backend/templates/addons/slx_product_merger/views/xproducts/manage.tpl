{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="xproducts_form">

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true}

        {if $xproducts}
            <table width="100%" class="table table-middle">
                <thead>
                <tr>
                    <th width="1%">{include file="common/check_items.tpl"}</th>
                    <th width="10%">{__("title")}</th>
                    <th width="30%">{__("mean")}</th>
                    <th width="10%">{__("istatus")}</th>
                    <th width="5%">&nbsp;</th>
                </tr>
                </thead>
                {foreach from=$xproducts item="xproduct"}
                    <tr >
                        <td>
                            <input type="checkbox" name="xproduct_ids[]" value="{$xproduct.id}" class="checkbox cm-item" /></td>
                        <td class="left nowrap row-status">
                            <a href="{"xproducts.details?id=`$xproduct.id`"|fn_url}">{$xproduct.title}</a>
                        <td>
                            {$xproduct.mean}
                        </td>
                        <td>
                            {if $xproduct.istatus==1}{__("istatus_1")}{elseif $xproduct.istatus==4}{__("istatus_4")}{elseif $xproduct.istatus==9}{__("istatus_9")}{else}-{/if}
                        </td>
                        <td class="nowrap">
                            {capture name="tools_list"}
                                <li>{btn type="list" text=__("edit") href="xproducts.update?hsn_code_id=`$hsncode.hsn_code_id`"}</li>
                                <li>{btn type="list" class="cm-confirm" text=__("delete") href="xproducts.delete?hsn_code_id=`$hsncode.hsn_code_id`" method="POST"}</li>
                            {/capture}
                            <div class="hidden-tools">
                                {dropdown content=$smarty.capture.tools_list}
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl"}

    </form>

    {capture name="adv_buttons"}
        {include file="common/tools.tpl" tool_href="xproducts.add" prefix="top" title=__("add_hsncode") hide_tools=true icon="icon-plus"}
    {/capture}

    {capture name="buttons"}
        {capture name="tools_list"}
            {if $xproducts}
                <li>{btn type="delete_selected" dispatch="dispatch[xproducts.m_delete]" form="xproducts_form"}</li>
                <li>{btn type="list" text=__("apply_taxes")  dispatch="dispatch[xproducts.store_selection]" form="xproducts_form"}</li>
            {/if}
        {/capture}
        {dropdown content=$smarty.capture.tools_list}

    {/capture}

    {capture name="sidebar"}
        {include file="addons/slx_product_merger/views/components/sidemenu.tpl"}
        <div class="sidebar-row">
            <h6>{__("search")}</h6>
            <form action="{""|fn_url}" name="states_filter_form" method="get">
                <div class="sidebar-field">
                    <label>{__("title")}:</label>
                    <input class="search-input-text" type="text" name="title" value="{$search.title}" />
                    <span class="help-block">Wildcard character * can be used to control your search.</span>
                </div>
                <div class="sidebar-field">
                    <label>{__("mean")}:</label>
                    <input class="search-input-text" type="text" name="mean" value="{$search.mean}" />
                </div>
                <div class="sidebar-field">
                    <label>{__("pcode")}:</label>
                    <input class="search-input-text" type="text" name="pcode" value="{$search.pcode}" />
                </div>
                <div class="sidebar-field">
                    <label>{__("istatus")}:</label>
                    <select name="istatus">
                        <option value="">--</option>
                        <option value="1" {if $search.istatus==1}selected{/if}>{__("istatus_1")}</option>
                        <option value="4" {if $search.istatus==4}selected{/if}>{__("istatus_4")}</option>
                        <option value="5" {if $search.istatus==4}selected{/if}>{__("istatus_5")}</option>
                        <option value="9" {if $search.istatus==9}selected{/if}>{__("istatus_9")}</option>
                    </select>
                </div>
                <div class="sidebar-field">
                    <label>{__("csrelated")}:</label>
                    <select name="csrelated">
                        <option value="">--</option>
                        <option value="Y" {if $search.csrelated=="Y"}selected{/if}>{__("yes")}</option>
                        <option value="N" {if $search.csrelated=="N"}selected{/if}>{__("no")}</option>
                    </select>
                </div>
                <div class="sidebar-field">
                    <label>{__("is_blocked")}:</label>
                    <select name="blocked">
                        <option value="">--</option>
                        <option value="Y" {if $search.blocked=="Y"}selected{/if}>{__("yes")}</option>
                        <option value="N" {if $search.blocked=="N"}selected{/if}>{__("no")}</option>
                    </select>
                </div>
                <div class="sidebar-field">
                    <label>{__("pu_result_id")}:</label>
                    <select name="pu_result_id">
                        <option value="">--</option>
                        <option value="-2" {if $search.pu_result_id==-2}selected{/if}>{__("pu_result_id_m2")}</option>
                        <option value="-1" {if $search.pu_result_id==-1}selected{/if}>{__("pu_result_id_m1")}</option>
                        <option value="1" {if $search.pu_result_id==1}selected{/if}>{__("pu_result_id_1")}</option>
                        <option value="2" {if $search.pu_result_id==2}selected{/if}>{__("pu_result_id_2")}</option>
                        <option value="3" {if $search.pu_result_id==3}selected{/if}>{__("pu_result_id_3")}</option>
                        <option value="101" {if $search.pu_result_id==101}selected{/if}>{__("pu_result_id_101")}</option>
                        <option value="102" {if $search.pu_result_id==102}selected{/if}>{__("pu_result_id_102")}</option>
                        <option value="103" {if $search.pu_result_id==103}selected{/if}>{__("pu_result_id_103")}</option>
                        <option value="104" {if $search.pu_result_id==104}selected{/if}>{__("pu_result_id_104")}</option>
                    </select>
                </div>
                {include file="buttons/search.tpl" but_name="dispatch[xproducts.manage]"}
            </form>
        </div>
    {/capture}


{/capture}
{include file="common/mainbox.tpl" title=__("xproducts") content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar select_languages=true}