{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="iproducts_form">
        <table width="100%">
            <tr>
                <td width="60%" valign="top">
                    <h3>Suppliers</h3>
                    {foreach from=$suppliers item="supplier"}
                        <div class="pm-supplier-info">
                            <h4 class="{if $supplier.status=="A"}green{else}black{/if}">{$supplier.supplier}</h4>
                            <ul>
                                <li>Status: <b>{$supplier.status}</b></li>
                                <li>InStream: {$supplier.instreamer}</li>
                                <li>Fetcher: {$supplier.fetcher}</li>
                                <li>bypasser: {$supplier.byPasser}</li>
                                <li>S-Products: <b>{$supplier.products}</b></li>
                                <li>Pending I-Products (Total/Not checked yet): <b>{$supplier.ipproducts} ({$supplier.ipproductsnc})</b></li>
                            </ul>
                        </div>
                    {/foreach}
                </td>
                <td style="vertical-align: top;">
                    <h3>IceCat Products</h3>
                    <div class="pm-supplier-info">
                        <table width="100%" class="table">
                            <tr><td>Complete:</td><td class="right"><b>{$iproducts.total}</b></td></tr>
                            <tr><td>Pending:</td><td class="right"><b>{$iproducts.pending}</b></td></tr>
                            <tr><td>Pending not checked yet:</td><td class="right"><b>{$iproducts.pending_not}</b></td></tr>
                        </table>
                    </div>
                    <h3>Supplier Products</h3>
                    <div class="pm-supplier-info">
                        <table width="100%" class="table">
                            <tr><td >Total:</td><td class="right"><b>{$sproducts.total}</b></td></tr>
                            <tr><td>Σε αναμονή:</td><td class="right"><b>{$sproducts.status1}</b></td></tr>
                            <tr><td>Μόνα:</td><td class="right"><b>{$sproducts.status4}</b></td></tr>
                            <tr><td>Me episkeyi:</td><td class="right"><b>{$sproducts.status5}</b></td></tr>
                            <tr><td>Ενωποιημένα:</td><td class="right"><b>{$sproducts.status9}</b></td></tr>
                            <tr><td colspan="2"><b>Ανά προμηθευτή</b></td></tr>
                            {foreach from=$sproducts.bysupplier item="bys"}
                            <tr><td>{$bys.supplier}:</td><td class="right"><b>{$bys.cnt}</b></td></tr>
                            {/foreach}
                        </table>
                        <table width="100%" class="table">
                            <tr><td colspan="3"><b>Ανά προμηθευτή Με διαθεσιμότητα</b></td></tr>
                            {foreach from=$sproducts.bysupplier_upd item="bys"}
                                <tr>
                                    <td>{$bys.supplier}</td>
                                    <td>{$bys.tim|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
                                    <td class="right"><b>{$bys.cnt}</b></td>
                                </tr>
                            {/foreach}
                        </table>
                    </div>
                    <h3>X Products</h3>
                    <div class="pm-supplier-info">
                        <table width="100%" class="table">
                            <tr><td>Total:</td><td class="right"><b>{$xproducts.total}</b></td></tr>
                            <tr><td>Σε αναμονή:</td><td class="right"><b>{$xproducts.status1}</b></td></tr>
                            <tr><td>Μόνα:</td><td class="right"><b>{$xproducts.status4}</b></td></tr>
                            <tr><td>Ενωποιημένα:</td><td class="right"><b>{$xproducts.status9}</b></td></tr>
                            <tr><td>Matched with CsCart:</td><td class="right"><b>{$xproducts.matched}</b></td></tr>
                            <tr><td>Με τιμή/απόθεμα από προμηθευτή:</td><td class="right"><b>{$xproducts.dominant}</b></td></tr>
                            <tr><td colspan="2"><b>Αποτελέσματα ενημέρωσης CS products</b></td></tr>
                            {foreach from=$xproducts.pu_result item="re"}
                            <tr><td>{$re.pu_result_id}:</td><td class="right">{$re.cnt}</td></tr>
                            {/foreach}
                        </table>
                    </div>
                    <h3>Cs Products</h3>
                    <div class="pm-supplier-info">
                        <table width="100%" class="table">
                        {foreach from=$cproducts item="v" key="k"}
                            {if $k=='suppliers_by_status'}
                                <tr><td colspan="3"><b>Προμηθευτές ανά status:</b></td></tr>
                                {assign var="tot" value=0}
                                {assign var="cvr" value=""}
                                {foreach from=$v item="vr"}
                                    {if $cvr!=$vr.name}
                                        {if $cvr!=""}
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td class="right"><b>{$tot}</b></td>
                                        </tr>
                                        {/if}
                                        {assign var="cvr" value=$vr.name}
                                        {assign var="tot" value=0}
                                    {/if}
                                    <tr>
                                        <td>{$vr.name}</td>
                                        <td>{$vr.status}</td>
                                        <td class="right">{$vr.cnt}</td>
                                    </tr>
                                    {assign var="tot" value=$tot+$vr.cnt}
                                {/foreach}
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td class="right"><b>{$tot}</b></td>
                                </tr>
                            {elseif is_array($v)}
                                <tr><td>{$k}:</td> <td colspan="2" class="right"><b>{$v|print_r}</b></td></tr>
                            {else}
                                <tr><td>{$k}:</td><td colspan="2" class="right"><b>{$v}</b></td></tr>
                            {/if}
                        {/foreach}
                        </table>
                    </div>
                </td>
            </tr>
        </table>

    </form>
    {capture name="sidebar"}
        {include file="addons/slx_product_merger/views/components/sidemenu.tpl"}
    {/capture}

{/capture}
{include file="common/mainbox.tpl" title=__("product_merger_overview") content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar  select_languages=true}

<style>
    .pm-supplier-info {
        font-size: 13px;
    }

    .pm-supplier-info h4 {

    }
    .pm-supplier-info li {
        list-style: none;
        padding: 2px;
    }
    .pm-supplier-info h4.green {
        color: #1dc116;
    }

    .pm-supplier-info h4.black {
        color: #1f6377;
    }

</style>