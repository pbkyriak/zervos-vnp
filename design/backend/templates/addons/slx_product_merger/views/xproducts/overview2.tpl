{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="iproducts_form">

        <h3>Cs Products</h3>
        <div class="pm-supplier-info">
            <ul>
                {foreach from=$cproducts item="v" key="k"}
                    {if $k=='suppliers_by_status'}
                        <li>{$k}:<br/>
                            <table width="100%" class="table">
                                {foreach from=$v item="vr"}
                                    <tr>
                                        <td>{$vr.name}</td>
                                        <td>{$vr.status}</td>
                                        <td>{$vr.cnt}</td>
                                    </tr>
                                {/foreach}
                            </table>
                        </li>

                    {elseif is_array($v)}
                        <li>{$k}: <b>{$v|print_r}</b></li>
                    {else}
                        <li>{$k}: <b>{$v}</b></li>
                    {/if}
                {/foreach}
            </ul>
        </div>
    </form>
    {capture name="sidebar"}
        {include file="addons/slx_product_merger/views/components/sidemenu.tpl"}
    {/capture}

{/capture}
{include file="common/mainbox.tpl" title=__("product_merger_overview") content=$smarty.capture.mainbox sidebar=$smarty.capture.sidebar  select_languages=true}

<style>
    .pm-supplier-info {
        font-size: 13px;
    }

    .pm-supplier-info h4 {

    }
    .pm-supplier-info li {
        list-style: none;
    }
    .pm-supplier-info h4.green {
        color: #1dc116;
    }

    .pm-supplier-info h4.black {
        color: #1f6377;
    }

</style>