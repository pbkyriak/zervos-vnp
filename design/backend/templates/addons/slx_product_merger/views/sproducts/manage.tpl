{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="sproducts_form">

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true}

        {if $sproducts}
            <table width="100%" class="table table-middle">
                <thead>
                <tr>
                    <th width="1%">{include file="common/check_items.tpl"}</th>
                    <th>{__("supplier")}</th>
                    <th>{__("pcode")}</th>
                    <th>{__("title")}</th>
                    <th>{__("mean")}</th>
                    <th>{__("price")}</th>
                    <th>{__("availability")}</th>
                    <th>{__("istatus")}</th>
                    <th width="5%">&nbsp;</th>
                </tr>
                </thead>
                {foreach from=$sproducts item="sproduct"}
                    <tr >
                        <td>
                            <input type="checkbox" name="sproduct_ids[]" value="{$sproduct.id}" class="checkbox cm-item" /></td>
                        <td>
                            {$sproduct.supplier}
                        </td>
                        <td>
                            {$sproduct.pcode}
                        </td>
                        <td class="left nowrap row-status">
                            <a href="{"sproducts.details?id=`$sproduct.id`"|fn_url}">{$sproduct.title}</a>
                        <td>
                            {$sproduct.mean}
                        </td>
                        <td>
                            {$sproduct.price}
                        </td>
                        <td>
                            {$sproduct.availability}
                        </td>
                        <td>
                            {if $sproduct.istatus==1}{__("istatus_1")}{elseif $sproduct.istatus==4}{__("istatus_4")}{elseif $sproduct.istatus==9}{__("istatus_9")}{else}-{/if}
                        </td>
                        <td class="nowrap">
                            {capture name="tools_list"}
                                <li>{btn type="list" text=__("edit") href="sproducts.update?hsn_code_id=`$hsncode.hsn_code_id`"}</li>
                                <li>{btn type="list" class="cm-confirm" text=__("delete") href="sproducts.delete?hsn_code_id=`$hsncode.hsn_code_id`" method="POST"}</li>
                            {/capture}
                            <div class="hidden-tools">
                                {dropdown content=$smarty.capture.tools_list}
                            </div>
                        </td>
                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl"}

    </form>

    {capture name="adv_buttons"}
        {include file="common/tools.tpl" tool_href="sproducts.add" prefix="top" title=__("add_hsncode") hide_tools=true icon="icon-plus"}
    {/capture}

    {capture name="buttons"}
        {capture name="tools_list"}
            {if $sproducts}
                <li>{btn type="delete_selected" dispatch="dispatch[sproducts.m_delete]" form="sproducts_form"}</li>
                <li>{btn type="list" text=__("apply_taxes")  dispatch="dispatch[sproducts.store_selection]" form="sproducts_form"}</li>
            {/if}
        {/capture}
        {dropdown content=$smarty.capture.tools_list}

    {/capture}

    {capture name="sidebar"}
        {include file="addons/slx_product_merger/views/components/sidemenu.tpl"}
        <div class="sidebar-row">
            <h6>{__("search")}</h6>
            <form action="{""|fn_url}" name="states_filter_form" method="get">
                <div class="sidebar-field">
                    <label>{__("title")}:</label>
                    <input class="search-input-text" type="text" name="title" value="{$search.title}" />
                    <span class="help-block">Wildcard character * can be used to control your search.</span>
                </div>
                <div class="sidebar-field">
                    <label>{__("mean")}:</label>
                    <input class="search-input-text" type="text" name="mean" value="{$search.mean}" />
                </div>
                <div class="sidebar-field">
                    <label>{__("supplier")}:</label>
                    <select name="supplier" >
                        <option value="">(Any)</option>
                        {foreach from=$suppliers item="supplier"}
                            <option value="{$supplier}" {if $search.supplier==$supplier}selected{/if}>{$supplier}</option>
                        {/foreach}
                    </select>
                </div>
                <div class="sidebar-field">
                    <label>{__("pcode")}:</label>
                    <input class="search-input-text" type="text" name="pcode" value="{$search.pcode}" />
                </div>
                <div class="sidebar-field">
                    <label>{__("istatus")}:</label>
                    <select name="istatus">
                        <option value="">--</option>
                        <option value="1" {if $search.istatus==1}selected{/if}>{__("istatus_1")}</option>
                        <option value="4" {if $search.istatus==4}selected{/if}>{__("istatus_4")}</option>
                        <option value="9" {if $search.istatus==9}selected{/if}>{__("istatus_9")}</option>
                    </select>
                </div>

                {include file="buttons/search.tpl" but_name="dispatch[sproducts.manage]"}
            </form>
        </div>
    {/capture}


{/capture}
{include file="common/mainbox.tpl" title=__("sproducts") content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar select_languages=true}