{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="xproducts_form" class="form-horizontal form-edit ">
        {capture name="tabsbox"}

        <div id="content_general">
            <h4>Supplier product info</h4>
            <table class="table table-middle" width="100%">
                <tr>
                    <th>ID</th>
                    <td>{$sproduct.id}</td>
                </tr>
                <tr>
                    <th>Supplier</th>
                    <td>{$sproduct.supplier}</td>
                </tr>
                <tr>
                    <th>Title</th>
                    <td>{$sproduct.title}</td>
                </tr>
                <tr>
                    <th>Price</th>
                    <td>{$sproduct.price}</td>
                </tr>
                <tr>
                    <th>P Code</th>
                    <td>{$sproduct.pcode}</td>
                </tr>
                <tr>
                    <th>Avail</th>
                    <td>{$sproduct.availability}</td>
                </tr>
                <tr>
                    <th>UT</th>
                    <td>{$sproduct.upd_timestamp|date_format:"`$settings.Appearance.date_format`, `$settings.Appearance.time_format`"}</td>
                </tr>
                <tr>
                    <th>Category</th>
                    <td>{$sproduct.category}</td>
                </tr>
                <tr>
                    <th>Main EAN</th>
                    <td>{$sproduct.mean}</td>
                </tr>
                <tr>
                    <th>EAN</th>
                    <td>{', '|implode:$sproduct.eans}</td>
                </tr>
            </table>
            <h4>Matched X product</h4>
                {if $xproduct}
                    <a href="{"xproducts.details?id=`$xproduct.id`"|fn_url}">{$xproduct.title}</a>
                {/if}
        </div>
        <div id="content_sproduct"  class="hidden">
            TODO
        </div>
        <div id="content_iproduct" class="hidden">
            TODO
        </div>
    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox track=true}


    </form>


    {capture name="adv_buttons"}
        {include file="common/tools.tpl" tool_href="xproducts.add" prefix="top" title=__("add_hsncode") hide_tools=true icon="icon-plus"}
    {/capture}

    {capture name="buttons"}
        {capture name="tools_list"}
            {if $xproducts}
                <li>{btn type="delete_selected" dispatch="dispatch[xsiproducts.m_delete]" form="xproducts_form"}</li>
                <li>{btn type="list" text=__("apply_taxes")  dispatch="dispatch[xsiproducts.store_selection]" form="xproducts_form"}</li>
            {/if}
        {/capture}
        {dropdown content=$smarty.capture.tools_list}

    {/capture}


{/capture}
{include file="common/mainbox.tpl" title=__("sproduct") content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar select_languages=true}

