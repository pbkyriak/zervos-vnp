{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" name="iproducts_form">

        {include file="common/pagination.tpl" save_current_page=true save_current_url=true}

        {if $iproducts}
            <table width="100%" class="table table-middle">
                <thead>
                <tr>
                    <th width="1%">{include file="common/check_items.tpl"}</th>
                    <th>{__("title")}</th>
                    <th>{__("brand")}</th>
                    <th>{__("mpc")}</th>
                    <th>{__("ean")}</th>
                </tr>
                </thead>
                {foreach from=$iproducts item="iproduct"}
                    <tr >
                        <td>
                            <input type="checkbox" name="iproduct_ids[]" value="{$iproduct.id}" class="checkbox cm-item" /></td>
                        <td>
                            <a href="{"iproducts.details?iproduct_id=`$iproduct.id`"|fn_url}">{$iproduct.title}</a>
                        </td>
                        <td>
                            {$iproduct.brand}
                        </td>
                        <td>
                            {$iproduct.mpc}
                        </td>
                        <td>
                            {$iproduct.ean}
                        </td>
                        <td>
                            {$iproduct.category}
                        </td>

                    </tr>
                {/foreach}
            </table>
        {else}
            <p class="no-items">{__("no_data")}</p>
        {/if}

        {include file="common/pagination.tpl"}

    </form>

    {capture name="adv_buttons"}
        {include file="common/tools.tpl" tool_href="iproducts.add" prefix="top" title=__("add_hsncode") hide_tools=true icon="icon-plus"}
    {/capture}

    {capture name="buttons"}
        {capture name="tools_list"}
            {if $iproducts}
                <li>{btn type="delete_selected" dispatch="dispatch[iproducts.m_delete]" form="iproducts_form"}</li>
                <li>{btn type="list" text=__("apply_taxes")  dispatch="dispatch[iproducts.store_selection]" form="iproducts_form"}</li>
            {/if}
        {/capture}
        {dropdown content=$smarty.capture.tools_list}

    {/capture}

    {capture name="sidebar"}
        {include file="addons/slx_product_merger/views/components/sidemenu.tpl"}
        <div class="sidebar-row">
            <h6>{__("search")}</h6>
            <form action="{""|fn_url}" name="states_filter_form" method="get">
                <div class="sidebar-field">
                    <label>{__("title")}:</label>
                    <input class="search-input-text" type="text" name="title" value="{$search.title}" />
                    <span class="help-block">Wildcard character * can be used to control your search.</span>
                </div>
                <div class="sidebar-field">
                    <label>{__("mean")}:</label>
                    <input class="search-input-text" type="text" name="mean" value="{$search.mean}" />
                </div>
                <div class="sidebar-field">
                    <label>{__("mpc")}:</label>
                    <input class="search-input-text" type="text" name="mpc" value="{$search.mpc}" />
                </div>
                <div class="sidebar-field">
                    <label>{__("brand")}:</label>
                    <select name="brand" >
                        <option value="">(Any)</option>
                        {foreach from=$brands item="brand"}
                            <option value="{$brand}" {if $search.brand==$brand}selected{/if}>{$brand}</option>
                        {/foreach}
                    </select>
                </div>
                {include file="buttons/search.tpl" but_name="dispatch[iproducts.manage]"}
            </form>
        </div>
    {/capture}


{/capture}
{include file="common/mainbox.tpl" title=__("iproducts") content=$smarty.capture.mainbox adv_buttons=$smarty.capture.adv_buttons buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar select_languages=true}