<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("title")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getTitle()}</span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("description")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getDescription()}</span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("dimensions")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getDimH()}x{$iproduct->getDimW()}x{$iproduct->getDimD()}</span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("category")}:</label>
    <div class="controls">
        <span class="shift-input"><a href="{"icategories.details&icategory_id=`$iproduct->getCategoryId()`"|fn_url}">{$iproduct->getCategory()} ({$iproduct->getCategoryId()})</a></span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("brand")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getBrand()}</span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("ean")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getEan()}</span>
        {foreach from=$iproduct->getEans() item="ean"}
            <span class="shift-input">{$ean} </span>

        {/foreach}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("iceid")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getIceId()}</span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("pcode")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getPcode()}</span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("status")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getStatus()}</span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("mpc")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getMpc()}</span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("weight")}:</label>
    <div class="controls">
        <span class="shift-input">{$iproduct->getWeight()}</span>
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("Images")}:</label>
    <div class="controls">
        {foreach from=$iproduct->getImages() item="img"}
            <span class="shift-input">{$img} </span><br />

        {/foreach}
    </div>
</div>
<div class="control-group">
    <label class="control-label" for="elm_hsn_code">{__("features")}:</label>
    <div class="controls">
        {foreach from=$iproduct->getFeatureGroups() item="grp"}
            <span class="shift-input"><h5>{$grp['name']}</h5> </span><br />
            <table class="table table-middle" width="100%">
                {foreach from=$iproduct->getFeatures() item="fts"}
                    {if $fts[3]==$grp['id']}
                        <tr>
                            <td>{$fts[0]}</td>
                            <td>{$fts[1]}</td>
                        </tr>
                    {/if}
                {/foreach}
            </table>
        {/foreach}
    </div>
</div>