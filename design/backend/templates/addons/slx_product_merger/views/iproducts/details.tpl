
{capture name="mainbox"}

    {capture name="tabsbox"}

        <form action="{""|fn_url}" method="post" name="hsn_update_form" class="form-horizontal form-edit >
            <input type="hidden" name="hsn_code_id" value="{$hsn_data.hsn_code_id}" />

            <div class="cm-j-tabs">
                <ul class="nav nav-tabs">
                    <li id="tab_new_states" class="cm-js active"><a>{__("general")}</a></li>
                </ul>
            </div>

            <div class="cm-tabs-content">
                <fieldset>
                    {include file="addons/slx_product_merger/views/iproducts/components/iproduct_info.tpl"}
                </fieldset>
            </div>

            {capture name="buttons"}
            {/capture}

        </form>

    {/capture}
    {include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox track=true}

{/capture}

    {assign var="title" value=__("iproduct")}

{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox select_languages=true buttons=$smarty.capture.buttons}
