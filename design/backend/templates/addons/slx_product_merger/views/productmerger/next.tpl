
{capture name="mainbox"}

    <form action="{""|fn_url}" method="post" enctype="multipart/form-data" name="newsletters_form" class="form-horizontal form-edit ">
        <input type="hidden" name="fake" value="1" />
        <input type="hidden" name="notification_id" value="{$notification.id}" />
        <input type="hidden" name="dispatch" value="" />
        <div class="control-group">
            <label class="control-label" for="elm_notification_lang">{__("product")}</label>
            <div class="controls">
                <div class="shift-input">{$p.product.title}</div>
                <div class="shift-input">Price: {$p.product.price}</div>
                <div class="shift-input">supplier: {$p.product.supplier}</div>
                <div class="shift-input">supplier: {$p.product.pcode}</div>
                <div class="shift-input">supplier: {$p.product.id}</div>
            </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="elm_notification_lang">{__("possible matches")}</label>
            <div class="controls">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                {foreach from=$p.results item="r"}
                    <tr>
                        <td>{$r.product.title}, {$r.product.supplier}, {$r.product.pcode}, {$r.product.id}</td>
                        <td>{$r.price}</td>
                        <td>
                            <select >
                                <option value="">Select</option>
                                <option value="fp">False positive</option>
                                <option value="m">Match</option>
                            </select>
                        </td>
                    </tr>

                {/foreach}

                </table>
            </div>
        </div>
    </form>

    {capture name="buttons"}
        {* include file="buttons/button.tpl" but_text=__("clone") but_name="dispatch[onesignal.clone]" but_role="submit-link" but_target_form="newsletters_form" allow_href=true *}
    {/capture}
{/capture}

{include file="common/mainbox.tpl" title="{__("man merge")}" content=$smarty.capture.mainbox select_languages=true buttons=$smarty.capture.buttons sidebar=$smarty.capture.sidebar}
