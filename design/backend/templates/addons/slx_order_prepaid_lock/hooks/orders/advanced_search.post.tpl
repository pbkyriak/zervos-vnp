﻿<div class="group">
        <div class="control-group">
            <label class="checkbox" for="elm_is_prepaid">{__("is_prepaid")}
                <input type="checkbox" name="is_prepaid" id="elm_is_prepaid" value="Y"{if $search.is_prepaid} checked="checked"{/if} />
            </label>
        </div>

    <div class="control-group" id="field_supplier_id">
		<label class="control-label" for="supplier_id">{__("supplier")}</label>
		<div class="controls">
        <div class="correct-picker-but">
            <input type="hidden" name="supplier_id" id="supplier_id" value="{$search.supplier_id}"  />
            {include file="common/ajax_select_object.tpl" data_url="suppliers.get_suppliers_list" text=$search.supplier_id|fn_get_supplier_name result_elm="supplier_id" id="prod_123"}
        </div>
		</div>
		
    </div>
</div>
