﻿	<div class="control-group">
		<label class="control-label cm-required" for="elm_payment_mode">{__("payment_mode")}</label>
		<div class="controls">
			<select id="elm_prepaid" class="input-small " name="update_order[payment_mode]">
				<option value="U">--</option>
				<option value="N" {if $order_info.payment_mode=="N"}selected{/if}>{__("no")}</option>
				<option value="Y" {if $order_info.payment_mode=="Y"}selected{/if}>{__("yes")}</option>
			</select>
		</div>
	</div>
	<div class="control-group">
		<label class="control-label" id="elm_prepaid_amount_label" for="elm_prepaid_amount">{__("prepaid_amount")}</label>
		<div class="controls">
			<input id="elm_prepaid_amount" class="input-small" type="text" name="update_order[prepayed_amount]" size="45" value="{$order_info.prepayed_amount}" />
		</div>
	</div>

	
	<script type="text/javascript">
	
	$(document).ready(function() {
		function slxxx_checkprepaid(sel) {
			if( sel.val()=='Y' ) {
				$("#elm_prepaid_amount_label").addClass("cm-required");
				var am = parseFloat($("#elm_prepaid_amount").val());
				if( am==0 ) {
					$("#elm_prepaid_amount").val('');
				}
			}
			else {
				$("#elm_prepaid_amount_label").removeClass("cm-required");
			}
		}
		slxxx_checkprepaid($("#elm_prepaid"));
		$("#elm_prepaid").change(function(ev) {
			slxxx_checkprepaid($(ev.target));
		});
		$("#elm_prepaid_amount").blur(function (evt) {
			var am = parseFloat($("#elm_prepaid_amount").val());
			if( am==0 ) {
				$("#elm_prepaid_amount").val('');
			}
		});
	});
	
	</script>