{if $order_info.interest|floatval}
  <tr>
      <td>{if $order_info.payment_info.period==1 }{__("epivarinsi_pistotikis")}{else}{__("interest")}{/if}:</td>
      <td data-ct-totals="payment_interest">{include file="common/price.tpl" value=$order_info.interest}</td>
  </tr>
{/if}
