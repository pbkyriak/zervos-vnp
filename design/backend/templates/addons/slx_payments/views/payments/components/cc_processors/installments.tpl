
<br />
<h2>{__("installments")}</h2>
{if $payment_id!=0}

{assign var="insts_data" value=$processor_params.installments|default:array()}
	<table class="table table-middle" width="100%">
		<thead class="cm-first-sibling">
		<tr>
			<th width="5%">{__("installments")}</th>
			<th width="25%">{__("interest")}</th>
			<th width="20%">{__("from")}</th>
			<th width="15%">&nbsp;</th>
		</tr>
		</thead>
		<tbody>
        {foreach from=$insts_data item="inst" key="_key" name="prod_prices"}
			<tr class="cm-row-item">
				<td width="10%" class="{$no_hide_input_if_shared_product}">
					<input type="number" min="1" step="1"  name="insts_data[{$_key}][installments]" value="{$inst.installments}" class="input-mini" />
				</td>
				<td width="20%" class="{$no_hide_input_if_shared_product}">
					<input type="text" name="insts_data[{$_key}][p_interest]" value="{$inst.p_interest|escape:html}" class="input-mini" size="4" /> % +
					<input type="text" name="insts_data[{$_key}][a_interest]" value="{$inst.a_interest|escape:html}" class="input-mini" size="4"/> {$currencies.$primary_currency.symbol}
				</td>
				<td width="25%" class="{$no_hide_input_if_shared_product}">
					<input type="text" name="insts_data[{$_key}][amount_from]" value="{$inst.amount_from}" size="6" class="input-text" />
				</td>
				<td width="15%" class="nowrap {$no_hide_input_if_shared_product} right">
                    {include file="buttons/clone_delete.tpl" microformats="cm-delete-row" no_confirm=true}
				</td>
			</tr>
        {/foreach}
        {math equation="x+1" x=$_key|default:0 assign="new_key"}
		<tr class="{cycle values="table-row , " reset=1}{$no_hide_input_if_shared_product}" id="box_add_qty_discount">
			<td width="10%" class="{$no_hide_input_if_shared_product}">
				<input type="number" min="1" step="1" name="insts_data[{$new_key}][installments]" value="" class="input-mini" />
			</td>
			<td width="20%" class="{$no_hide_input_if_shared_product}">
				<input type="text" name="insts_data[{$new_key}][p_interest]" value="" class="input-mini" size="4" /> % +
				<input type="text" name="insts_data[{$new_key}][a_interest]" value="" class="input-mini" size="4"/> {$currencies.$primary_currency.symbol}
			</td>
			<td width="25%" class="{$no_hide_input_if_shared_product}">
				<input type="text" name="insts_data[{$new_key}][amount_from]" value="" size="6" class="input-text" />
			</td>
			<td width="15%" class="right">
                {include file="buttons/multiple_buttons.tpl" item_id="add_qty_discount"}
			</td>
		</tr>
		</tbody>
	</table>


{else}
  <p>Save the payment and then set installment settings.</p>
{/if}