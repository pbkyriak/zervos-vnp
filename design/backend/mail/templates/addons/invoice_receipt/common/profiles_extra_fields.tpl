{*

	Editor: Ioannis Maztiaris [imatz]
	Email: imatzgr@gmail.com
	Date: May 2015
	Details: An paraggelia apo twigmo tote min emfanisei stoixeiwn timogoliou an einai kena (-|-)
		Einai copy tou arxeiou mail/templates/profiles/profiles_extra_fields.tpl

*}

{foreach from=$fields item="field"}
{if !$field.field_name}
{assign var="value" value=$order_info.fields[$field.field_id]}
{if $value && $value!="-|-"}{* [imatz] *}
<p>
    {$field.description}:
    {if "AOL"|strpos:$field.field_type !== false} {* Titles/States/Countries *}
        {assign var="title" value="`$field.field_id`_descr"}
        {$user_data.$title}
    {elseif $field.field_type == "C"}  {* Checkbox *}
        {if $value == "Y"}{__("yes")}{else}{__("no")}{/if}
    {elseif $field.field_type == "D"}  {* Date *}
        {$value|date_format:$settings.Appearance.date_format}
    {elseif "RS"|strpos:$field.field_type !== false}  {* Selectbox/Radio *}
        {$field.values.$value}
    {else}  {* input/textarea *}
        {$value|default:"-"}
    {/if}
</p>
{/if}{* [imatz] *}
{/if}
{/foreach}