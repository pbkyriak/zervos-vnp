{* [panos] *}
{if $order_info.interest|floatval}
  <tr>
    <td style="text-align: right; white-space: nowrap; font-size: 12px; font-family: Arial;">
    {if $order_info.payment_period==1 }<b>{__("epivarinsi_pistotikis")}</b>{else}<b>{__("interest")}{__("installments")}{$order_info.payment_period}{/if}:</b></td>
    <td style="text-align: right; white-space: nowrap; font-size: 12px; font-family: Arial;"><b>{include file="common/price.tpl" value=$order_info.interest}</b></td>
  </tr>
{/if}
{* [/panos] *}